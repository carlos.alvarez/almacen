<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Client_category
 * @property Client_category_model $client_category_model
 */
class Client_category extends MY_Controller {

	/**
	 * [datatable_json description]
	 */
	public function datatable_json()
	{
		echo $this->{$this->_model}->datatable_json();
	}

    /**
     * @param $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ){ exit("Incorrect method."); }

        try {
            $result = $this->client_category_model->delete($id);
            if ( $result )
            {
                $this->_return_json_success('Categoría eliminada sastifactoriamente.');
            }
            else
            {
                $this->_return_json_error('No se pudo eliminar la categoría.');
            }
        } catch (Exception $e) {
            $this->_return_json_error($e->getMessage());
        }
    }

}
