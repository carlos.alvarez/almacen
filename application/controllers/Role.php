<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Role
 * @property Role_model $role_model
 */
class Role extends MY_Controller {

	/**
	 * [$_controller description]
	 * @var string
	 */
	protected $_controller = 'role';

	/**
	 * [$_model description]
	 * @var string
	 */
	protected $_model = 'role_model';

	/**
	 * [datatable_json description]
	 */
	public function datatable_json()
	{
		echo $this->{$this->_model}->datatable_json();
	}

    /**
     * @param $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ) { $this->_response_error('Incorrect method.'); }

        try {
            $result = $this->role_model->delete($id);

            if ( $result )
            {
                $this->_return_json_success('Rol eliminado sastifactoriamente.');
            }
            else
            {
                $this->_return_json_error('No se pudo eliminar el rol.');
            }
        } catch (Exception $e) {
            $this->_return_json_error($e->getMessage());
        }
    }
}
