<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User: cristian.rodriguez
 * Date: 9/13/2017
 * Time: 11:37 PM
 */

/**
 * Class Project_brand
 * @property Brand_model $brand_model
 */
class Project_brand extends MY_Controller
{
    /**
     * @var string
     */
    protected $_controller = 'project_brand';

    /**
     * @var string
     */
    protected $_model = 'project_brand_model';

    public function brand_list_date()
    {
        $this->_template("{$this->_controller}/brand_list_date", $this->_get_assets('list', $this->data));
    }

    /**
     * @param null $date
     * @param string $status
     * @param string $company
     * @param string $category
     * @param string $brand
     */
    public function today_datatable_json($date = NULL, $status='all', $company='all', $category='all', $brand='all',$project_id='all')
    {
        echo $this->{$this->_model}->today_datatable_json($date,$status,$company,$category,$brand,$project_id);
    }

    /**
     * @param $project_id
     */
    public function create_budget($project_id)
    {
        if ( ! grant_access('project_brand', 'create') )
        {
            redirect();
        }

        $this->load->model('brand_model');
        $this->load->model('project_brand_model');

        $this->data = array(
            'styles'    => array(
                'public/plugins/jquery-select2/css/select2.min.css',
                'public/plugins/jquery-datepicker/datepicker.css'
            ),
            'scripts'   => array(
                'public/plugins/jquery-select2/js/select2.min.js',
                'public/plugins/jquery-datepicker/datepicker.js',
                'public/plugins/jquery-datepicker/i18n/datepicker-es.js'
            ),
            'brands'            => $this->brand_model->all(array('order_by'=>'name ASC')),
            'project_id'        => $project_id,
        );

        if ( $this->input->method() === 'post' )
        {
            if ( $this->{$this->_model}->insert($this->input->post()) )
            {
                if ( !$this->input->is_ajax_request() )
                {
                    redirect('project/view/'.$project_id);
                }
                else
                {
                    $this->_response_success('Listo');
                    //exit('Listo');
                }
            }
            else
            {
                $this->_response_error(validation_errors());
            }

        }

        $this->_template("{$this->_controller}/create", $this->_get_assets('create', $this->data));
    }

    public function get_budget($id)
    {
        $this->load->model('brand_model');
        $brand_select = '';
        $brands = $this->brand_model->all();
        $budget = $this->project_brand_model->get($id);

        foreach($brands as $brand)
        {
            if ($brand->id === $budget->brand_id)
            {
                $brand_select .= '<option value="'.$brand->id.'" selected>'.$brand->name.'</option>';
            }
            else
            {
                $brand_select .= '<option value="'.$brand->id.'">'.$brand->name.'</option>';
            }
        }

        $data = array(
            'budget'    => $budget,
            'brands'    => $brand_select
        );
        $this->_response_success($data);
    }

    public function edit($id)
    {
        $this->load->model('brand_model');
        $budget_id = $this->{$this->_model}->get($id);

        $this->data = array(
            'styles'    => array(
                'public/plugins/jquery-select2/css/select2.min.css',
                'public/plugins/jquery-datepicker/datepicker.css'
            ),
            'scripts'   => array(
                'public/plugins/jquery-select2/js/select2.min.js',
                'public/plugins/jquery-datepicker/datepicker.js',
                'public/plugins/jquery-datepicker/i18n/datepicker-es.js'
            ),
            'brands'            => $this->brand_model->all(array('order_by'=>'name ASC')),
            'project_id'        => $budget_id->project_id,
            'selected_brand'    => $budget_id->brand_id,
        );

       parent::edit($id);

    }

    public function delete($id)
    {
        if ( !$this->input->method() == 'get' ) { $this->_response_error('Incorrect method.'); }

        if ( $this->{$this->_model}->delete($id) ) {
            $this->_return_json_success('Presupuesto eliminiado.');
        } else {
            $this->_return_json_error('No se pudo eliminar el presupuesto.');
        }
    }

    public function view($id)
    {
        redirect('project/view/'.$this->{$this->_model}->get($id)->project_id);
    }
}