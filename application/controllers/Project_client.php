<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User: cristian.rodriguez
 * Date: 9/27/2017
 * Time: 6:03 AM
 */

/**
 * Class Project_client
 * @property Project_tag_model $project_tag_model
 * @property Project_comment_model $project_comment_model
 * @property Project_file_model $project_file_model
 */
class Project_client extends MY_Controller
{
    /**
     * @var string
     */
    protected $_controller = 'project_client';

    /**
     * @var string
     */
    protected $_model = 'project_model';

    /**
     * @param int|string $id
     */
    public function view($id)
    {
        $this->load->model('project_tag_model');
        $tags = $this->project_tag_model->find(array('select'=>'tag','where'=>array('project_id'=>$id)), TRUE);
        $tags = array_column($tags, 'tag');

        $this->data = array(
            'styles'	=> array(
                'public/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css',
                //'public/plugins/Responsive-master/css/responsive.bootstrap.css',
                'public/plugins/jsgantt-improved/jsgantt.css'
            ),
            'scripts' 	=> array(
                'public/plugins/jquery-datatable/jquery.dataTables.js',
                'public/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js',
                //'public/plugins/Responsive-master/js/dataTables.responsive.js',
                //'public/plugins/Responsive-master/js/responsive.bootstrap.js',
                'public/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.flash.min.js',
                'public/plugins/jquery-datatable/extensions/export/jszip.min.js',
                'public/plugins/jquery-datatable/extensions/export/pdfmake.min.js',
                'public/plugins/jquery-datatable/extensions/export/vfs_fonts.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.html5.min.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.print.min.js',
                'public/plugins/jquery-datatable/extensions/export/ellipsis.js',
                'public/plugins/jquery-datepicker/datepicker.js',
                'public/plugins/jquery-datepicker/i18n/datepicker-es.js',
                'public/plugins/jsgantt-improved/jsgantt.js'
            ),
            'tags' => $tags
        );
        parent::view($id);
    }

    /**
     * @param int $id
     */
    public function get_comments($id)
    {
        $this->load->model('project_comment_model');
        $this->load->helper('comments_helper');

        $comments = $this->project_comment_model->find(array(
            'where'     => array(
                'project_id'        => $id,
                'client_visibility' => 'true'
            ),
            'order_by'  => 'projects_comments.id DESC'
        ));

        echo comments_panel($comments);
    }


    /**
     * @param int $id
     */
    public function files_datatable_json($id)
    {
        $this->load->model('project_file_model');
        echo $this->project_file_model->project_client_datatable_json($id);
    }
}