<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Company
 *
 * @property Company_model $company_model
 * @property Calendar_model $calendar_model
 * @property Avatar $avatar
 */
class Company extends MY_Controller {

	public function create()
    {
        $this->load->model('calendar_model');

        $this->data = array(
            'calendars'     => $this->calendar_model->dropdown('id', 'name', array('where' => array('active' => 'true'), 'order_by' => 'name ASC'))
        );

        parent::create();
    }

    /**
     * @param int|string $id
     */
    public function edit($id)
    {
        $this->load->model('calendar_model');

        $this->data = array(
            'calendars'     => $this->calendar_model->dropdown('id', 'name', array('where' => array('active' => 'true'), 'order_by' => 'name ASC'))
        );

        parent::edit($id);
    }

	public function datatable_json()
	{
		echo $this->{$this->_model}->datatable_json();
	}

    /**
     * @param int $id
     */
    protected function _on_insert_success($id)
    {
        if ( $this->input->method() === 'post' && isset($_FILES['picture_file']) && $_FILES['picture_file']['size'] > 0 )
        {
            $result = $this->_create_avatar($id);
            if ( $result !== TRUE )
            {
                $this->_alert($result, 'warning');
            }
        }
    }

    /**
     * @param int $id
     */
    protected function _on_edit_success($id)
    {
        if ( $this->input->method() === 'post' && isset($_FILES['picture_file']) && $_FILES['picture_file']['size'] > 0 )
        {
            $result = $this->_create_avatar($id);
            if ( $result !== TRUE )
            {
                $this->_alert($result, 'warning');
            }
        }
    }

    /**
     * @param int $id
     * @return bool|string
     */
    private function _create_avatar($id)
    {
        $this->load->library('avatar');

        $options = array(
            'file_input'        => 'picture_file',
            'avatar_path'       => "./public/img/companies/{$id}.jpg",
            'allowed_types'     => 'jpg|jpeg'
        );

        try
        {
            return $this->avatar->create($options);
        }
        catch ( Exception $e)
        {
            return $e->getMessage();
        }

    }

    /**
     * @param int $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ){ exit("Incorrect method."); }

        try {
            if ( $this->company_model->delete($id) ){
                $this->_return_json_success('Empresa eliminado sastifactoriamente.');
            } else {
                $this->_return_json_error('No se pudo eliminar la empresa.');
            }
        } catch (Exception $e) {
            $this->_return_json_error($e->getMessage());
        }
    }

}
