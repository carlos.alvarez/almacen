<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Area
 * @property Company_model $company_model
 * @property Department_model $department_model
 * @property Employee_model $employee_model
 * @property Area_model $area_model
 */
class Area extends MY_Controller {

	/**
	 * [$_controller description]
	 * @var string
	 */
	protected $_controller = 'area';

	/**
	 * [$_model description]
	 * @var string
	 */
	protected $_model = 'area_model';

    /**
     *
     */
    public function create()
    {
        $this->load->model('company_model');
        $this->load->model('department_model');
        $this->load->model('employee_model');

        $companies = $this->company_model->all(array('where' => array('active' => 'true')));
        $departments = $this->department_model->all(array('where' => array('active' => 'true')));
        $employees = $this->employee_model->all(array('where' => array('employees.active' => 'true'), 'order_by' => 'full_name ASC'));

        $this->data = array(
            'companies'     => $companies,
            'departments'   => $departments,
            'employees'     => $employees
        );

        parent::create();
    }

    /**
     * @param int|string $id
     */
    public function edit($id)
    {
        $this->load->model('company_model');
        $this->load->model('department_model');
        $this->load->model('employee_model');

        $area  = $this->_exist($id);
        $manager_id = ($area->manager_id) ? $area->manager_id : 0;

        $companies = $this->company_model->all(array('where' => array('active' => 'true')));
        $departments = $this->department_model->all(array('where' => array('active' => 'true')));
        $employees = $this->employee_model->all(array('where' => 'employees.active = true OR employees.user_id = '.$manager_id, 'order_by' => 'full_name ASC'));

        $this->data = array(
            'companies'     => $companies,
            'departments'   => $departments,
            'employees'     => $employees
        );

        parent::edit($id);
    }

    /**
     * @param int|string $id
     */
    public function view($id)
    {
        $this->load->model('employee_model');

        $area = $this->_exist($id);
        $manager = $this->employee_model->get($area->manager_id);

        $this->data = array(
            'manager'       => $manager
        );

        parent::view($id);
    }

	public function datatable_json()
	{
		echo $this->{$this->_model}->datatable_json();
	}

    /**
     * @param $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ){ exit("Incorrect method."); }

        try {

            $result = $this->area_model->delete($id);
            if ( $result )
            {
                $this->_return_json_success('Área eliminado sastifactoriamente.');
            }
            else
            {
                $this->_return_json_error('No se pudo eliminar el área.');
            }
        } catch (Exception $e) {
            $this->_return_json_error($e->getMessage());
        }
    }
}
