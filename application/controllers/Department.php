<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Department
 * @property Department_model $department_model
 * @property Company_model $company_model
 * @property Employee_model $employee_model
 */
class Department extends MY_Controller {

	/**
	 * [$_controller department]
	 * @var string
	 */
	protected $_controller = 'department';

	/**
	 * [$_model departament]
	 * @var string
	 */
	protected $_model = 'department_model';

    /**
     *
     */
    public function create()
    {
        $this->load->model('company_model');
        $this->load->model('employee_model');

        $this->data = array(
            'companies'     => $this->company_model->dropdown('id', 'name', array('where' => array('active' => 'true'), 'order_by' => 'name ASC')),
            'employees'     => $this->employee_model->dropdown('user_id', 'full_name', array('where' => array('employees.active' => 'true'), 'order_by' => 'full_name ASC'))
        );

        parent::create();
    }

    /**
     * @param int|string $id
     */
    public function edit($id)
    {
        $this->load->model('company_model');
        $this->load->model('employee_model');

        $department  = $this->_exist($id);
        $manager_id = ($department->manager_id) ? $department->manager_id : 0;

        $companies = $this->company_model->all(array('where' => array('active' => 'true')));
        $companies = array_object_column($companies, 'name', 'id');
        $companies[''] = lang('choose_an_option').' *';

        $employees = $this->employee_model->all(array('where' => 'employees.active = true OR employees.user_id = '.$manager_id, 'order_by' => 'full_name ASC'));
        $employees = array_object_column($employees, 'full_name', 'user_id');
        $employees[''] = lang('choose_an_option').' *';

        $this->data = array(
            'companies'     => $companies,
            'employees'     => $employees
        );

        parent::edit($id);
    }

    /**
     * @param int|string $id
     */
    public function view($id)
    {
        $this->load->model('company_model');
        $this->load->model('employee_model');

        $department = $this->department_model->get($id);

        $company = $this->company_model->get($department->company_id);
        $manager = $this->employee_model->get($department->manager_id);

        $this->data = array(
            'company'       => $company,
            'manager'       => $manager
        );

        parent::view($id);
    }

	public function datatable_json()
	{
		echo $this->{$this->_model}->datatable_json($_GET);
	}

    /**
     * @param $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ){ exit("Incorrect method."); }

        try {
            $result = $this->department_model->delete($id);

            if ( $result )
            {
                $this->_return_json_success('Departamento eliminado sastifactoriamente.');
            }
            else
            {
                $this->_response_error('No se pudo eliminar el departamento.');
            }
        } catch (Exception $e) {
            $this->_response_error($e->getMessage());
        }
    }
}
