<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Action
 * @property Action_model $action_model
 */
class Action extends MY_Controller {

	/**
	 * [$_controller description]
	 * @var string
	 */
	protected $_controller = 'action';

	/**
	 * [$_model description]
	 * @var string
	 */
	protected $_model = 'action_model';

    /**
     * [datatable_json description]
     */
	public function datatable_json()
	{
		echo $this->action_model->datatable_json();
	}

}
