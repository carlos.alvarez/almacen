<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Notification
 * @property Notification_model $notification_model
 */
class Notification extends MY_Controller {
    /**
     * @var string
     */
	protected $_controller = 'notification';
    /**
     * @var string
     */
	protected $_model = 'notification_model';

    public function index() { show_404(); }
	public function create() { show_404(); }
    public function view($id) { show_404(); }
    public function edit($id) { show_404(); }
    public function delete($id) { show_404(); }

    /**
     *
     */
    public function json()
    {
        if ( !$this->input->is_ajax_request() ) { show_404(); }
        if ( $this->input->method() !== 'get' ) { $this->_return_json_error('Invalid method'); }
        if ( ! $user_id = $this->session->userdata('user_id') ) { $this->_return_json_error('Unauthorized Access'); }

        $notifications = $this->notification_model->get_for_web($user_id);

        $this->_return_json_success('OK', $notifications);
    }

    /**
     * @param $id
     */
    public function mark_as_read($id)
    {
        if ( !$this->input->is_ajax_request() ) { show_404(); }
        if ( $this->input->method() !== 'get' ) { $this->_return_json_error('Invalid method'); }
        if ( ! $user_id = $this->session->userdata('user_id') ) { $this->_return_json_error('Unauthorized Access'); }

        if ( $this->notification_model->update($id, array('read'=>'true')) )
        {
            $this->_response_success();
        }
        else
        {
            $this->_return_json_error('Notification not found or error updating: '.validation_errors());
        }
    }
}
