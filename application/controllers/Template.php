<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Project_category_model $project_category_model
 * @property Company_model $company_model
 * @property Position_model $position_model
 * @property Template_model $template_model
 * @property Tpl_stage_model $tpl_stage_model
 * @property Tpl_task_model $tpl_task_model
 * @property Setting_model $setting_model
 * @property Tpl_task_position_model $tpl_task_position_model
 * @property Tpl_task_prerequisite_model $tpl_task_prerequisite_model
 * @property Tpl_task_tag_model $tpl_task_tag_model
 * @property Tpl_task_requirement_model $tpl_task_requirement_model
 * @property Stage_status_model $stage_status_model
 * @property Reason_model $reason_model
 */
class Template extends MY_Controller {

    /**
     * [$_controller description]
     * @var string
     */
    protected $_controller = 'template';
    protected $_model = 'template_model';

    /**
     * @param $id
     */
    public function view($id)
    {
        $this->load->model('position_model');
        $this->load->model('company_model');
        $this->load->model('project_category_model');

        $positions_q = $this->position_model->find(array('order_by'=>'company_id, name ASC'));
        $companies_q = $this->company_model->all();

        $positions = array();
        foreach ($positions_q as $pos)
        {
            $positions[$pos->company_id][$pos->id] = $pos->name;
        }

        $companies = array();
        foreach ($companies_q as $comp)
        {
            $companies[$comp->id] = $comp->name;
        }

        $data = array(
            'styles'	=> array(
                'public/plugins/nestable/jquery-nestable.css',
                'public/plugins/jstree/dist/themes/default/style.min.css',
                'public/plugins/sweetalert/sweetalert.css',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',
                'public/plugins/multi-select/css/multi-select.css'
            ),
            'scripts' 	=> array(
                'public/plugins/jstree/dist/jstree.min.js',
                'public/plugins/sweetalert/sweetalert.min.js',
                'public/plugins/jquery-validation/jquery.validate.js',
                'public/plugins/jquery-validation/localization/messages_es.js',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
                'public/plugins/multi-select/js/jquery.multi-select.js',
                'public/plugins/sticky/jquery.sticky.js',
                'public/js/src/template.js'
            ),
            'template_id'       => $id,
            'positions'         => $positions,
            'companies'         => $companies
        );

        $this->_template("template/view", $data);
    }

    public function create()
    {
        $data = array(
            'scripts' 	=> array(
                'public/plugins/jstree/dist/jstree.min.js',
                'public/plugins/sticky/jquery.sticky.js',
                'public/js/src/template.js'
            )
        );

        $this->_template("template/create", $data);
    }

    public function duplicate_template()
    {
        if ( ! $this->input->method() == 'post') {
            exit('Incorrect method.');
        }else{
            $result = $this->{$this->_model}->duplicate($this->input->post('id'), $this->input->post('title'));

            if ( $result ){
                $this->_return_json_success('Plantilla duplicada correctamente.', array('id'=>$result));
            }else{
                $this->_return_json_error(validation_errors());
            }
        }
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        $this->load->model('position_model');
        $this->load->model('company_model');
        $this->load->model('project_category_model');
        $this->load->model('stage_status_model');
        $this->load->model('reason_model');

        $positions_q = $this->position_model->find(array('order_by'=>'company_id, name ASC'));
        $companies_q = $this->company_model->all();

        $positions = array();
        foreach ($positions_q as $pos)
        {
            $positions[$pos->company_id][$pos->id] = $pos->name;
        }

        $companies = array();
        foreach ($companies_q as $comp)
        {
            $companies[$comp->id] = $comp->name;
        }

        $status_q = $this->stage_status_model->find(array('order_by'=>'name ASC'));
        $reasons_q = $this->reason_model->find(array('order_by'=>'name ASC'));

        $data = array(
            'styles'	=> array(
                'public/plugins/nestable/jquery-nestable.css',
                'public/plugins/jstree/dist/themes/default/style.min.css',
                'public/plugins/sweetalert/sweetalert.css',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',
                'public/plugins/multi-select/css/multi-select.css',
                'public/plugins/jquery-duration-picker/duration-picker.min.css'
            ),
            'scripts' 	=> array(
                'public/plugins/jstree/dist/jstree.min.js',
                'public/plugins/sweetalert/sweetalert.min.js',
                'public/plugins/jquery-validation/jquery.validate.js',
                'public/plugins/jquery-validation/localization/messages_es.js',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
                'public/plugins/multi-select/js/jquery.multi-select.js',
                'public/plugins/jquery-duration-picker/duration-picker.min.js',
                'public/plugins/autosize/autosize.js',
                'public/plugins/nouislider/nouislider.js',
                'public/plugins/sticky/jquery.sticky.js',
                'public/js/src/template.js'
            ),
            'template_id'       => $id,
            'positions'         => $positions,
            'companies'         => $companies,
            'categories'        => $this->project_category_model->dropdown('id', 'name', array('order_by'=>'name ASC')),
            'status'            => $status_q,
            'reasons'           => $reasons_q
        );

        $this->_template("template/edit", $data);
    }

    /**
     *
     */
    public function datatable_json()
    {
        echo $this->{$this->_model}->datatable_json();
    }

    /**
     *
     */
    public function new_template()
    {
        $this->load->model('project_category_model');
        $category = $this->project_category_model->find(array('limit' => 1));

        $title = $this->input->post('title');

        $id = $this->template_model->insert(array(
            'title' => $title,
            'project_category_id' => $category->id
        ));

        if ( $id )
        {
            $this->_return_json_success('Plantilla creada correctamente.', array('id'=>$id));
        }
        else
        {
            $this->_return_json_error(validation_errors());
        }
    }

    /**
     *
     */
    public function get_template_node_json()
    {
        $this->load->model('template_model');
        $this->load->model('tpl_stage_model');

        $id = $this->input->get('id');

        $template = $this->template_model->get($id);
        $children = $this->tpl_stage_model->find(array('where'=>array('template_id'=>$id),'limit'=>1)) ? TRUE : FALSE;

        $node = $this->_create_node($id, $template->title, 'template', 1, $children, $template);
        $this->output->set_content_type('application/json')->set_output(json_encode($node));
    }

    /**
     *
     */
    public function get_template_children_json()
    {
        $this->load->model('tpl_stage_model');
        $this->load->model('tpl_task_model');

        $id = $this->input->get('id');

        $stages = $this->tpl_stage_model->find(array('where'=>array('template_id'=>$id),'order_by'=>'order ASC'));

        $nodes = array();
        foreach ($stages as $stage)
        {
            $children = $this->tpl_task_model->find(array('where'=>array('tpl_stage_id'=>$stage->id),'limit'=>1)) ? TRUE : FALSE;
            $nodes[] = $this->_create_node($stage->id, $stage->title, 'stage', $stage->order, $children, $stage);
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($nodes));
    }

    /**
     *
     */
    public function get_stage_children_json()
    {
        $this->load->model('tpl_task_model');
        $this->load->model('tpl_task_position_model');
        $this->load->model('tpl_task_prerequisite_model');
        $this->load->model('tpl_task_tag_model');
        $this->load->model('tpl_task_requirement_model');

        $id = $this->input->get('id');

        $tasks = $this->tpl_task_model->find(array('where'=>array('tpl_stage_id'=>$id),'order_by'=>'order ASC'));

        $nodes = array();
        $laborable_hours = $this->setting_model->get('laborable_hours');
        $laborable_hours = ($laborable_hours) ? $laborable_hours->value : 8;
        foreach ($tasks as $task)
        {
            $positions = $this->tpl_task_position_model->find(array('where'=>array('tpl_task_id'=>$task->id)));
            $prerequisites = $this->tpl_task_prerequisite_model->find(array('where'=>array('tpl_task_id'=>$task->id)));
            $tags = $this->tpl_task_tag_model->find(array('where'=>array('tpl_task_id'=>$task->id)));
            $requirements = $this->tpl_task_requirement_model->find(array('where'=>array('tpl_task_id'=>$task->id)));

            $nodes[] = $this->_create_node($task->id, $task->title, 'task', $task->order, FALSE, array(
                'duration_days'         => $task->duration_days,
                'duration_hours'        => $task->duration_hours,
                'laborable_hours'       => $laborable_hours,
                'day_type'              => $task->day_type,
                'weight'                => $task->weight,
                'client_visibility'     => $task->client_visibility,
                'client_notification'   => $task->client_notification,
                'description'           => $task->description,
                'status_changer'        => $task->status_changer,
                'stage_status_id'       => $task->stage_status_id,
                'reason_id'             => $task->reason_id,
                'stage_status'          => $task->stage_status,
                'reason'                => $task->reason,
                'positions'             => json_encode($positions),
                'prerequisites'         => json_encode($prerequisites),
                'tags'                  => json_encode($tags),
                'requirements'          => json_encode($requirements)
            ));
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($nodes));
    }

    /**
     * @param $id
     * @param $template_id
     * @param int $position
     */
    public function move_stage($id, $template_id, $position = 0)
    {
        $this->load->model('tpl_stage_model');

        $stages = $this->tpl_stage_model->find(array('where'=>array('template_id'=>$template_id),'order_by'=>'order ASC'));
        $i = 0;
        foreach ($stages as $stage)
        {
            if ( $stage->id == $id ) { continue; }
            if ( $i == $position ) { $i++; }

            if ( $stage->order != $i )
            {
                $this->tpl_stage_model->update($stage->id, array('order' => $i));
            }
            $i++;
        }
        $result = $this->tpl_stage_model->update($id, array('order' => $position));

        if ( $result )
        {
            $this->_return_json_success('Tarea movida correctamente.');
        }
        else
        {
            $this->_return_json_error(validation_errors());
        }
    }

    /**
     * @param $id
     * @param $stage_id
     * @param int $position
     */
    public function move_task($id, $stage_id, $position = 0)
    {
        $this->load->model('tpl_task_model');

        $tasks = $this->tpl_task_model->find(array('where'=>array('tpl_stage_id'=>$stage_id),'order_by'=>'order ASC'));
        $i = 0;
        foreach ($tasks as $task)
        {
            if ( $task->id == $id ) { continue; }
            if ( $i == $position ) { $i++; }

            if ( $task->order != $i )
            {
                $this->tpl_task_model->update($task->id, array('order' => $i));
            }
            $i++;
        }
        $result = $this->tpl_task_model->update($id, array(
            'tpl_stage_id'  => $stage_id,
            'order'         => $position
        ));

        if ( $result )
        {
            $this->_return_json_success('Tarea movida correctamente.');
        }
        else
        {
            $this->_return_json_error(validation_errors());
        }
    }

    /**
     * @param $template_id
     */
    public function create_stage($template_id)
    {
        $this->load->model('tpl_stage_model');
        $title = $this->input->post('title');

        $order = count($this->tpl_stage_model->find(array('template_id' => $template_id)));

        $id = $this->tpl_stage_model->insert(array(
            'template_id'       => $template_id,
            'title'             => $title,
            'weight'            => 5,
            'order'             => $order
        ));

        if ( $id )
        {
            $stage = $this->tpl_stage_model->get($id);

            $node = $this->_create_node($stage->id, $stage->title, 'stage',$stage->order, FALSE, $stage);

            $this->_return_json_success('Fase creada correctamente.', array('node' => $node));
        }
        else
        {
            $this->_return_json_error(validation_errors());
        }
    }

    /**
     * @param $stage_id
     */
    public function create_task($stage_id)
    {
        $this->load->model('tpl_task_model');
        $title = $this->input->post('title');

        $order = count($this->tpl_task_model->find(array('tpl_stage_id' => $stage_id)));

        $id = $this->tpl_task_model->insert(array(
            'tpl_stage_id'      => $stage_id,
            'title'             => $title,
            'duration_days'     => 0,
            'duration_hours'    => 1,
            'weight'            => 5,
            'order'             => $order
        ));

        if ( $id )
        {
            $task = $this->tpl_task_model->get($id);

            $node = $this->_create_node($task->id, $task->title, 'task', $task->order, FALSE, array(
                'duration_days'         => $task->duration_days,
                'duration_hours'        => $task->duration_hours,
                'day_type'              => $task->day_type,
                'weight'                => $task->weight,
                'client_visibility'     => $task->client_visibility,
                'client_notification'   => $task->client_notification,
                'description'           => $task->description,
                'positions'             => '[]',
                'prerequisites'         => '[]',
                'tags'                  => '[]',
                'requirements'          => '[]'
            ));

            $this->_return_json_success('Tarea creada correctamente.', array('node' => $node));
        }
        else
        {
            $this->_return_json_error(validation_errors());
        }
    }

    /**
     * @param $id
     */
    public function update_template($id)
    {
        $result = $this->template_model->update($id, $_POST);

        if ( $result )
        {
            $this->_return_json_success('Plantilla actualizada correctamente.');
        }
        else
        {
            $this->_return_json_error(validation_errors());
        }
    }

    /**
     * @param $id
     */
    public function update_stage($id)
    {
        $this->load->model('tpl_stage_model');

        $result = $this->tpl_stage_model->update($id, $_POST);

        if ( $result )
        {
            $this->_return_json_success('Fase actualizada correctamente.');
        }
        else
        {
            $this->_return_json_error(validation_errors());
        }
    }

    /**
     * @param $id
     */
    public function update_task($id)
    {
        $this->load->model('tpl_task_model');
        $this->load->model('tpl_task_tag_model');
        $this->load->model('tpl_task_position_model');
        $this->load->model('tpl_task_prerequisite_model');

        if ( ! preg_match('/^[0-9]+d,[0-9]+h$/', $_POST['duration']) )
        {
            $this->_return_json_error('Duración errónea.');
            exit();
        }

        $matches = array();
        preg_match_all('/([0-9]+|[0-9]+)/', $_POST['duration'], $matches);

        $_POST['duration_days'] = $matches[0][0];
        $_POST['duration_hours'] = $matches[0][1];


        $result = $this->tpl_task_model->update($id, $_POST);

        if ( $result )
        {
            try
            {
                $positions = isset($_POST['positions']) ? $_POST['positions'] : array();
                $this->_update_task_positions($id, $positions);

                $prerequisites = isset($_POST['prerequisites']) ? $_POST['prerequisites'] : array();
                $this->_update_task_prerequisites($id, $prerequisites);

                $tags = isset($_POST['tags']) ? strtoupper($_POST['tags']) : array();
                $this->_update_task_tags($id, $tags);

                $requirements = isset($_POST['requirements']) ? $_POST['requirements'] : array();
                $this->update_task_requirements($id, $requirements);
            } catch (Exception $e)
            {
                $this->_return_json_error($e->getMessage());
                exit();
            }

            $this->_return_json_success('Tarea actualizada correctamente.');
        }
        else
        {
            $this->_return_json_error(validation_errors());
        }
    }

    public function delete($id)
    {
        $result = $this->template_model->delete($id);

        if ( $result )
        {
            $this->_return_json_success('Plantilla eliminada.');
        }
        else
        {
            $this->_return_json_error('No fue posible eliminar la plantilla.');
        }
    }

    /**
     * @param $id
     */
    public function delete_stage($id)
    {
        $this->load->model('tpl_stage_model');

        $result = $this->tpl_stage_model->delete($id);

        if ( $result )
        {
            $this->_return_json_success('Fase Eliminada.');
        }
        else
        {
            $this->_return_json_error('No fue posible eliminar la fase.');
        }
    }

    /**
     * @param $id
     */
    public function delete_task($id)
    {
        $this->load->model('tpl_task_model');

        $result = $this->tpl_task_model->delete($id);

        if ( $result )
        {
            $this->_return_json_success('Tarea Eliminada.');
        }
        else
        {
            $this->_return_json_error('No fue posible eliminar la tarea.');
        }
    }

    /**
     * @param $id
     * @param $text
     * @param $type
     * @param $position
     * @param bool $children
     * @param array $data
     * @return array
     */
    private function _create_node($id, $text, $type, $position, $children = FALSE, $data = array())
    {

        $node = array(
            'text'      => $text,
            'type'      => $type,
            'position'  => $position,
            'li_attr'   => array(
                'data-id'   => $id
            ),
            'children'  => $children
        );

        foreach ($data as $key => $attr)
        {
            $node['li_attr']['data-'.$key] = $attr;
        }

        return $node;
    }

    /**
     * @param $template_id
     * @param $task_id
     */
    public function get_prerequisites_json($template_id, $task_id)
    {
        $prereqs = $this->template_model->get_task_prerequisites_candidates($template_id, $task_id);

        if ( $prereqs )
        {
            $this->_return_json_success('OK', $prereqs);
        }
        else
        {
            $this->_return_json_error('No se pudieron obtener los prerequisitos.');
        }
    }

    /**
     * @param $task_id
     * @param $positions
     */
    private function _update_task_positions($task_id, $positions)
    {
        $this->load->model('tpl_task_position_model');

        $temp = $this->tpl_task_position_model->find(array('where'=>array('tpl_task_id'=>$task_id)));
        $actions = array();

        foreach ( $temp as $pos ) { $actions[$pos->position_id] = 'delete'; }
        foreach ( $positions as $pos ) { $actions[$pos] = (isset($actions[$pos])) ? 'nothing' : 'insert'; }

        foreach ($actions as $position_id => $action)
        {
            switch ($action)
            {
                case 'delete':
                    $this->tpl_task_position_model->delete(array(
                        'tpl_task_id'       => $task_id,
                        'position_id'       => $position_id
                    ));
                    break;
                case 'insert':
                    $this->tpl_task_position_model->insert(array(
                        'tpl_task_id'       => $task_id,
                        'position_id'       => $position_id
                    ));
                    break;
            }
        }
    }

    /**
     * @param $task_id
     * @param $prerequisites
     * @throws Exception
     */
    private function _update_task_prerequisites($task_id, $prerequisites)
    {
        $this->load->model('tpl_task_prerequisite_model');

        $temp = $this->tpl_task_prerequisite_model->find(array('where'=>array('tpl_task_id'=>$task_id)));
        $actions = array();

        foreach ( $temp as $pre ) { $actions[$pre->tpl_task_prerequisite] = 'delete'; }
        foreach ( $prerequisites as $pre ) { $actions[$pre] = (isset($actions[$pre])) ? 'nothing' : 'insert'; }

        foreach ($actions as $prerequisite_id => $action)
        {
            switch ($action)
            {
                case 'delete':
                    $this->tpl_task_prerequisite_model->delete(array(
                        'tpl_task_id'               => $task_id,
                        'tpl_task_prerequisite'     => $prerequisite_id
                    ));
                    break;
                case 'insert':

                    if ( $this->tpl_task_prerequisite_model->check_loop($task_id, $prerequisite_id) )
                    {
                        throw new Exception('Uno o más prerequisitos causan bucles.');
                    }

                    $this->tpl_task_prerequisite_model->insert(array(
                        'tpl_task_id'               => $task_id,
                        'tpl_task_prerequisite'     => $prerequisite_id
                    ));
                    break;
            }
        }
    }

    /**
     * @param $task_id
     * @param $tags
     */
    private function _update_task_tags($task_id, $tags)
    {
        $this->load->model('tpl_task_tag_model');

        $tags = explode(',', $tags);

        foreach ($tags as $k => $t) { $tags[$k] = trim($t); }

        $temp = $this->tpl_task_tag_model->find(array('where'=>array('tpl_task_id'=>$task_id)));
        $actions = array();

        foreach ( $temp as $tag ) { $actions[$tag->tag] = 'delete'; }
        foreach ( $tags as $tag ) { $actions[$tag] = (isset($actions[$tag])) ? 'nothing' : 'insert'; }

        foreach ($actions as $tag => $action)
        {
            switch ($action)
            {
                case 'delete':
                    $this->tpl_task_tag_model->delete_where(array(
                        'tpl_task_id'               => $task_id,
                        'tag'                       => $tag
                    ));
                    break;
                case 'insert':
                    if ( $tag === '' ) { continue; }
                    $this->tpl_task_tag_model->insert(array(
                        'tpl_task_id'               => $task_id,
                        'tag'                       => $tag,
                        'color'                     => '#888888'
                    ));
                    break;
            }
        }
    }

    /**
     * @param $task_id
     * @param $requirements
     * @return bool
     */
    private function update_task_requirements($task_id, $requirements)
    {
        $this->load->model('tpl_task_requirement_model');

        $reqs_db = $this->tpl_task_requirement_model->find(array('where'=>array('tpl_task_id'=>$task_id)));

        $actions = array();
        foreach ( $reqs_db as $r ) { $actions['req_'.$r->id] = '##delete##'; }
        foreach ( $requirements as $k => $r ) { $actions[$k] = $r; }

        foreach ( $actions as $k => $action )
        {
            if ( preg_match('/req_[0-9]+/', $k) === 1 )
            {
                $id = trim(substr($k, 4));
                if ( $action === '##delete##' )
                {
                    $this->tpl_task_requirement_model->delete($id);
                }
                else
                {
                    $this->tpl_task_requirement_model->update($id, array(
                        'requirement'   => $action
                    ));
                }
            }
            else
            {
                $this->tpl_task_requirement_model->insert(array(
                    'tpl_task_id'   => $task_id,
                    'requirement'   => $action,
                    'weight'        => 5
                ));
            }
        }

        return TRUE;
    }

    /**
     * @param $former
     * @param $latter
     */
    public function get_structure($former, $latter)
    {
        echo json_encode(array(
            'former'    => $this->template_model->get_structure($former),
            'latter'    => $this->template_model->get_structure($latter)
        ));
    }

    /**
     * @param $id
     */
    public function get_positions($id)
    {
        echo json_encode($this->template_model->get_positions($id));
    }

    /**
     * @param $category_id
     */
    public function get_templates_by_category($category_id)
    {
        echo json_encode($this->template_model->find(array('where' => array('project_category_id' => $category_id))));
    }
}
