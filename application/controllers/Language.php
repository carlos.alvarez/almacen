<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends MY_Controller{

    protected $_model = FALSE;

    /**
     * [datatable description]
     * @param  [type] $language [description]
     */
	public function datatable($language = NULL)
	{
		$language = ($language) ? $language : $this->config->item('language');

		$file = "./public/plugins/jquery-datatable/languages/{$language}.json";

		if ( file_exists($file) )
		{
			echo file_get_contents($file);
		}
		else
		{
			echo file_get_contents('./public/plugins/jquery-datatable/languages/english.json');
		}
	}

}
