<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Push
 * @property Push_subscription_model $push_subscription_model
 * @property GCM $gcm
 * @property Notification_model $notification_model
 */
class Push extends MY_Controller {
    /**
     * @var string
     */
	protected $_controller = 'push';
    /**
     * @var string
     */
	protected $_model = 'push_subscription_model';

    public function index() { show_404(); }
	public function create() { show_404(); }
    public function view($id) { show_404(); }
    public function edit($id) { show_404(); }
    public function delete($id) { show_404(); }

    /**
     *
     */
    public function subscribe()
    {
        if ( !$this->input->is_ajax_request() ) { show_404(); }
        if ( $this->input->method() !== 'post' ) { $this->_return_json_error('Invalid method'); }
        if ( !isset($_POST['subscription']) ) { $this->_return_json_error('Invalid Request'); }
        if ( ! $user_id = $this->session->userdata('user_id') ) { $this->_return_json_error('Unauthorized Access'); }

        $subscription = json_decode($_POST['subscription']);

        $endpoint = explode('/', $subscription->endpoint);

        $subsc_id = $this->push_subscription_model->insert(array(
            'user_id'           => $user_id,
            'endpoint'          => end($endpoint),
            'expirationTime'    => is_null($subscription->expirationTime) ? date('Y-m-d H:i:s', strtotime('+3 months')) : $subscription->expirationTime
        ));

        if ( !$subsc_id ) { $this->_return_json_error('Error with subscription. '.validation_errors()); }

        $this->_return_json_success();
    }

    /**
     *
     */
    public function unsubscribe()
    {
        if ( !$this->input->is_ajax_request() ) { show_404(); }
        if ( $this->input->method() !== 'post' ) { $this->_return_json_error('Invalid method'); }
        if ( !isset($_POST['subscription']) ) { $this->_return_json_error('Invalid Request'); }
        if ( ! $user_id = $this->session->userdata('user_id') ) { $this->_return_json_error('Unauthorized Access'); }

        $subscription = json_decode($_POST['subscription']);
        $endpoint = explode('/', $subscription->endpoint);

        $subsc_to_delete = $this->push_subscription_model->find(array(
            'limit'     => 1,
            'where'     => array(
                'user_id'           => $user_id,
                'endpoint'          => end($endpoint)
            )
        ));
        if ( !$subsc_to_delete ) { $this->_return_json_error('Subscription not found.'); }

        $res = $this->push_subscription_model->delete($subsc_to_delete->id);
        if ( !$res ) { $this->_return_json_error('Error trying to delete subscription.'); }

        $this->_return_json_success();
    }

    public function test()
    {
        if ( ! $user_id = $this->session->userdata('user_id') ) { $this->_return_json_error('Unauthorized Access'); }
        $endpoints = $this->push_subscription_model->get_by_user($user_id);
        if ( count($endpoints) === 0 ) { return; }

        $this->load->library('gcm');

        $this->gcm->setRecipients($endpoints);
        //$this->gcm->setMessage('Esta es una prueba de gcm. Hola :)');

        $this->gcm->setData(array(
            'message'   => 'Hola, prueba.',
            'user_id'   => $user_id
        ));

        $this->gcm->send();
    }

    /**
     *
     */
    public function notifications()
    {
        if ( $this->input->method() !== 'post' ) { $this->_return_json_error('Invalid method'); }
        if ( !isset($_POST['endpoint']) ) { $this->_return_json_error('Invalid Request'); }

        $subscription = $this->push_subscription_model->find(array(
            'where'     => array('endpoint' => $_POST['endpoint']),
            'limit'     => 1
        ));
        if ( ! $subscription ) { $this->_return_json_error('Unauthorized Access'); }
        if ( strtotime($subscription->expirationTime) < time() )
        {
            $this->push_subscription_model->delete($subscription->id);
            $this->_return_json_error('Unauthorized Access');
        }

        $this->load->model('notification_model');

        $this->push_subscription_model->update($subscription->id, array(
            'expirationTime'    => date('Y-m-d H:i:s', strtotime('+3 months'))
        ));

        $notifications = $this->notification_model->get_for_push($subscription->user_id);

        $this->_return_json_success('OK', $notifications);
    }
}
