<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Grant
 *
 * @property Grant_model $grant_model
 * @property Role_model $role_model
 * @property Module_model $module_model
 *
 */
class Grant extends MY_Controller {

	/**
	 * [$_controller description]
	 * @var string
	 */
	protected $_controller = 'grant';

	/**
	 * [$_model description]
	 * @var string
	 */
	protected $_model = 'grant_model';

    /**
     *
     */
	public function index()
	{
		$this->load->model('role_model');
		$this->load->model('module_model');
		$this->load->model('action_model');

		$roles = $this->role_model->all(array('order_by'=>'name ASC'));
		$modules = $this->module_model->all(array('where'=>array('active'=>'true'),'order_by'=>'name ASC'));
		$grants = $this->grant_model->get_grants_array();
        $post_grants = $grants;

		if ($this->input->method() === 'post')
        {

            $result = $this->grant_model->apply_changes($this->input->post('grants'));

            if ( $result )
            {
                $this->_alert(lang('success_message'), 'success');
                $grants = $this->grant_model->get_grants_array();
                $post_grants = $grants;
            }
            else
            {
                $post_grants = array_replace_recursive($grants, $this->input->post('grants'));
            }
        }

        $actions = array('view', 'create', 'edit', 'delete');

		$data = array(
			'roles'		    => $roles,
			'modules'	    => $modules,
			'actions'	    => $actions,
			'grants'	    => $grants,
            'post_grants'   => $post_grants,
			'scripts'	    => array(
                'public/plugins/sticky/jquery.sticky.js',
                'public/js/src/grant.js'
			)
		);

		$this->_template('grant/list', $data);
	}

    /**
     * @param $id
     */
	public function view($id)
    {
        redirect($this->_controller);
    }

    /**
     *
     */
    public function create()
    {
        redirect($this->_controller);
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        redirect($this->_controller);
    }

}
