<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class User
 * @property User_model $user_model
 * @property Role_model $role_model
 * @property Avatar $avatar
 * @property Client_contact_model $client_contact_model
 * @property Acceso_model $acceso_model
 * @property User_role_model $user_role_model
 */
class User extends MY_Controller {

	/**
	 * [$_controller description]
	 * @var string
	 */
	protected $_controller = 'user';

	/**
	 * [$_model description]
	 * @var string
	 */
	protected $_model = 'user_model';

    /**
     * @param $id
     */
	public function view($id)
    {
        $this->data = array(
            'user_roles'    => $this->user_model->get_roles($id)
        );

        parent::view($id);
    }

	public function create()
	{
		$this->load->model('role_model');

        $roles = $this->role_model->all(array('order_by'=>'name ASC'));

        $post_roles = array();
        if ( $this->input->method() === 'post' && $this->input->post('user_roles') )
        {
            $post_roles = $this->input->post('user_roles');
        }

		$this->data = array(
			'roles'         => $roles,
            'post_roles'    => $post_roles
		);

		parent::create();
	}

    /**
     * @param $id
     */
	public function edit($id)
	{
	    $this->_exist($id);

		if ( $this->input->method() === 'post' )
		{
            // Remove password if is empty
			if ( isset($_POST['password']) && trim($_POST['password']) === '' )
			{
				unset($_POST['password']);
			}
		}

		$this->load->model('role_model');
		$roles = $this->role_model->all(array('order_by'=>'name ASC'));

        if ( $this->input->method() === 'post' && $this->input->post('user_roles') )
        {
            $user_roles = $this->input->post('user_roles');
        }
        else
        {
            $user_roles = $this->user_model->get_roles($id);
            $user_roles = array_map(function($item) { return $item->role_id; }, $user_roles);
        }

		$this->data = array(
			'roles'         => $roles,
            'user_roles'    => $user_roles
		);

		parent::edit($id);
	}

    /**
     * [login description]
     */
	public function login()
	{
        $this->lang->load('login');

		// If already logged redirect to welcome page
		if ( $this->session->userdata('logged_in') === TRUE )
		{
			redirect('welcome');
			exit();
		}

		$this->load->library('form_validation');
        $this->form_validation->reset_validation();
		$this->form_validation->set_rules(array(
			array( // Username
	            'field'     => 'username',
	            'label'     => 'lang:username',
	            'rules'     => 'trim|required|min_length[4]|max_length[25]'
	        ),
	        array( // Password
	            'field'     => 'password',
	            'label'     => 'lang:password',
	            'rules'     => 'trim|required|min_length[4]'
	        )
		));

		if ( $this->form_validation->run() === TRUE )
		{
			$user = $this->user_model->auth($this->input->post());

			if ($user)
			{
				$acceso = $this->acceso_model->obtener();
			    $data = array(
                    'logged_in'         => TRUE,
                    'user_id'           => $user->id,
                    'username'          => $user->username,
                    'first_name'        => $user->first_name,
                    'last_name'         => $user->last_name,
                    'full_name'         => $user->first_name . ' ' . $user->last_name,
                    'email'             => $user->email,
                    'grants'            => $this->user_model->get_grants($user->id),
                    'acceso'            => ($acceso == NULL) ? 'normal' : $acceso->estado,
                    'acceso_mensaje'    => ($acceso == NULL) ? '' : $acceso->mensaje
				);

				$this->session->set_userdata($data);
				redirect('welcome');
			}
			else
			{
			    $this->_alert(lang('Wrong_user_or_password'), 'danger');
			}
		}

		$dir_content = scandir('./public/img/gallery');
		$images = array();

		foreach ( $dir_content as $item )
        {
            if ( is_file("./public/img/gallery/{$item}") && preg_match('/^[\S\s]+\.jpg$/', $item) )
            {
                $images[] = $item;
            }
        }

        $data = array(
            'images'    => $images
        );

		$this->load->view('user/login', $data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('user/login');
	}

	public function datatable_json()
	{
		echo $this->user_model->datatable_json();
	}

	public function get_captcha()
    {
        $this->load->helper('number_captcha_helper');

        $captcha = create_captcha(array(
            'img_path'      => './public/img/captchas/',
            'img_url'       => base_url('public/img/captchas/'),
            'font_path'     => './public/fonts/roboto_regular/OpenSans-Regular.ttf',
            'font_size'     =>  24
        ));

        if ( $captcha )
        {
            $this->session->set_userdata('captcha_solution', $captcha['result']);

            return $this->_return_json_success(lang('success_message'), $captcha['filename']);
        }
        else
        {
            return $this->_return_json_error('Captcha Failed!');
        }
    }

    /**
     * @param $solution
     * @return bool
     */
    public function validate_captcha($solution)
    {
        return $solution == $this->session->userdata('captcha_solution');
    }

    /**
     * @param $id
     */
    protected function _on_insert_success($id)
    {
        if ( $this->input->method() === 'post' && isset($_FILES['picture_file']) && $_FILES['picture_file']['size'] > 0 )
        {
            $result = $this->_create_avatar($id);
            if ( $result !== TRUE )
            {
                $this->_alert($result, 'warning');
            }
        }
    }

    /**
     * @param $id
     */
    protected function _on_edit_success($id)
    {
        if ( $this->input->method() === 'post' && isset($_FILES['picture_file']) && $_FILES['picture_file']['size'] > 0 )
        {
            $result = $this->_create_avatar($id);
            if ( $result !== TRUE )
            {
                $this->_alert($result, 'warning');
            }
        }
    }

    /**
     * @param $id
     * @return bool|string
     */
    private function _create_avatar($id)
    {
        $this->load->library('avatar');

        $options = array(
            'file_input'        => 'picture_file',
            'avatar_path'       => "./public/img/users/{$id}.jpg",
            'allowed_types'     => 'jpg|jpeg'
        );

        try
        {
            return $this->avatar->create($options);
        }
        catch ( Exception $e)
        {
            return $e->getMessage();
        }

    }

	public function acceso()
    {

        if ( $this->session->userdata('acceso') == 'bloqueado' )
        {
            $this->load->view('acceso/mensaje', array('mensaje' => $this->session->userdata('acceso_mensaje')));
            //exit();
        }
        else
        {
            redirect('user/login');
        }

    }

    /**
     * @param $id
     */
    public function get_user_json($id)
    {
        $user = $this->user_model->get($id);

        if ( $user )
        {
            $this->_return_json_success('OK', array('user'=>$user));
        }
        else
        {
            $this->_return_json_error('Usuario no encontrado');
        }
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ) { $this->_response_error('Incorrect method.'); }

        try {
            $result = $this->user_model->delete($id);
            if ( $result )
            {
                $this->_return_json_success('Usuario eliminado sastifactoriamente.');
            }
            else
            {
                $this->_return_json_error('No se pudo eliminar el usuario.');
            }
        } catch (Exception $e) {
            $this->_return_json_error($e->getMessage());
        }
    }

    public function my_profile()
    {
        if ( $this->input->method() === 'post' )
        {
            // Remove password if is empty
            if ( isset($_POST['password']) && trim($_POST['password']) === '' )
            {
                unset($_POST['password']);
            }
        }

        $row = $this->_exist($this->session->userdata('user_id'));

        if ( $this->input->method() === 'post' )
        {
            if ( $this->{$this->_model}->update($this->session->userdata('user_id'), $this->input->post()) )
            {
                $this->_on_edit_success($this->session->userdata('user_id'));
                $this->_response_success();

                if ( !$this->input->is_ajax_request() )
                {
                    redirect("{$this->_controller}/my_profile");
                }
            }
            else
            {
                $this->_response_error(validation_errors());
            }
        }

        $this->data[$this->_controller] = $row;

        $this->_template("{$this->_controller}/my_profile", $this->_get_assets('edit', $this->data));
    }
}
