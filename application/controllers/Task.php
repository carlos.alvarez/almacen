<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Action
 * @property Task_model $task_model
 * @property Task_prerequisite_model $task_prerequisite_model
 * @property Task_requirement_model $task_requirement_model
 * @property Task_employee_model $task_employee_model
 * @property Employee_model $employee_model
 * @property Position_model $position_model
 * @property Company_model $company_model
 * @property Stage_model $stage_model
 * @property Task_tag_model $task_tag_model
 * @property Project_model $project_model
 * @property Calendar_model $calendar_model
 * @property Notification_model $notification_model
 * @property Push_subscription_model $push_subscription_model
 * @property GCM $gcm
 * @property Client_model $client_model
 * @property Stage_status_model $stage_status_model
 * @property Reason_model $reason_model
 */
class Task extends MY_Controller {

	/**
     * @var string
     */
	protected $_model = 'task_model';

    /**
     * @param $id
     */
    public function project_datatable_json($id)
    {
        echo $this->task_model->project_datatable_json($id);
    }

    /**
     *
     */
    public function datatable_json()
    {
        echo $this->task_model->datatable_json();
    }

    /**
     * @param $id
     */
    public function f_view($id)
    {
        $this->load->model('project_model');

        if ( !grant_access('task', 'view') && !in_array($this->session->userdata('user_id'), $this->project_model->get_employees($id)) )
        {
            redirect("task");
        }

        $this->view($id);
    }

    /**
     * @param int|string $id
     */
    public function view($id)
    {
        $this->load->model('task_prerequisite_model');
        $this->load->model('task_requirement_model');
        $this->load->model('task_employee_model');
        $this->load->model('task_tag_model');
        $this->load->model('employee_model');
        $this->load->model('position_model');
        $this->load->model('company_model');
        $this->load->model('stage_model');
        $this->load->model('project_model');

        $data_employee = array();
        $data_prerequisite = array();
        // STAGE AND PREREQUISITE
        $pre = $this->task_prerequisite_model->find(array('where' => array('task_id' => $id)));
        foreach($pre as $prerequisite){
            $task = $this->task_model->get($prerequisite->task_prerequisite);
            $data_prerequisite['stage'][$this->stage_model->get($task->stage_id)->title][] = $this->task_model->get($prerequisite->task_prerequisite)->title;
        }
        // COMPANY AND POSITION
        $task_employee = $this->task_employee_model->find(array('where' => array('task_id' => $id)));
        foreach($task_employee as $employee){
            $get_position_id = $this->employee_model->get($employee->user_id)->position_id;
            $get_position = $this->position_model->get($get_position_id);
            $data_employee['company'][$this->company_model->get($get_position->company_id)->name] = array($get_position->name);
        }

        $task = $this->task_model->get($id);
        $stage = $this->stage_model->get($task->stage_id);
        $project = $this->project_model->get($stage->project_id);
        $requirements = $this->task_requirement_model->find(array('where' => array('task_id' => $id)));

        if ( count($requirements) > 0 )
        {
            $close_btn = TRUE;
            foreach ($requirements as $requirement) { if ( $requirement->done === 'false' ) { $close_btn = FALSE; break; } }
        }
        else
        {
            $close_btn = FALSE;
        }

        $this->data = array(
            'requirements'      => $requirements,
            'task_prerequisite' => $data_prerequisite,
            'task_employee'     => $data_employee,
            'tags'              => $this->task_tag_model->find(array('where' => array('task_id' => $id))),
            'stage'             => $stage,
            'project'           => $project,
            'employees'         => $task_employee,
            'close_btn'         => $close_btn
        );
        parent::view($id);
    }

    /**
     *
     */
    public function f_create()
    {
        $this->load->model('task_employee_model');
        $this->load->model('project_model');

        if ( !isset($_GET['project_id']) )
        {
            redirect();
        }
        $project_id = $_GET['project_id'];

        $project = $this->project_model->get($project_id);

        if ( !grant_access('task', 'create') && $project->manager_id !== $this->session->userdata('user_id') )
        {
            redirect();
        }

        $this->create();
    }

    /**
     *
     */
    public function create()
    {
        $this->load->model('stage_model');
        $this->load->model('project_model');
        $this->load->model('task_prerequisite_model');
        $this->load->model('task_employee_model');
        $this->load->model('employee_model');
        $this->load->model('task_tag_model');
        $this->load->model('task_requirement_model');
        $this->load->model('stage_status_model');
        $this->load->model('reason_model');

        if ( !isset($_GET['project_id']) )
        {
            redirect();
        }
        $project_id = $_GET['project_id'];

        $project = $this->project_model->get($project_id);
        $tasks = $this->project_model->get_tasks($project->id);
        $stages = $this->stage_model->dropdown('id', 'title', array('where'=>array('stages.project_id'=>$project->id)));
        $employees = $this->employee_model->find(array('where' => array('employees.active' => 'true')));

        $requirements = array();
        if ( isset($_POST['requirements']) )
        {
            $requirements = $_POST['requirements'];
        }

        $tags = '';
        if ( isset($_POST['tags']) )
        {
            $tags = $_POST['tags'];
        }

        $resources = array();
        if ( isset($_POST['resources']) )
        {
            $resources = $_POST['resources'];
        }

        $prerequisites = array();
        if ( isset($_POST['prerequisites']) )
        {
            $prerequisites = $_POST['prerequisites'];
        }

        $status_q = $this->stage_status_model->find(array('order_by'=>'name ASC'));
        $reasons_q = $this->reason_model->find(array('order_by'=>'name ASC'));

        $this->data = array(
            'styles'    => array(
                'public/plugins/jquery-select2/css/select2.min.css',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css'
            ),
            'scripts'   => array(
                'public/plugins/jquery-select2/js/select2.min.js',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
                'public/plugins/jquery-sheepIt/jquery.sheepItPlugin.js'
            ),
            'project'       => $project,
            'stages'        => $stages,
            'tasks'         => $tasks,
            'prerequisites' => $prerequisites,
            'employees'     => $employees,
            'resources'     => $resources,
            'tags'          => $tags,
            'requirements'  => $requirements,
            'status'        => $status_q,
            'reasons'       => $reasons_q
        );

        if ( $this->input->method() === 'post' )
        {
            $this->load->model('calendar_model');
            $days = $this->calendar_model->days_between(1, $_POST['start_date'], $_POST['estimated_end_date']);
            $_POST['duration_days'] = $days;
            $_POST['duration_hours'] = ($days >= 1) ? 0 : 2;

            if ( $id = $this->{$this->_model}->insert($this->input->post()) )
            {
                $this->task_model->reschedule($id, $_POST['start_date'], $_POST['estimated_end_date']);
                $this->_on_insert_success($id);
                $this->_response_success();
                if ( !$this->input->is_ajax_request() )
                {
                    redirect('project/f_edit/'.$project_id);
                }
            }
            else
            {
                $this->_response_error(validation_errors());
            }
        }

        $this->_template("{$this->_controller}/create", $this->_get_assets('create', $this->data));
    }

    /**
     * @param $id
     */
    public function f_edit($id)
    {
        $this->load->model('task_employee_model');

        if ( !grant_access('task', 'edit') )
        {
            $employees = $this->task_employee_model->find(array('where' => array('task_id' => $id)));
            $redirect = TRUE;
            foreach ($employees as $employee)
            {
                if ( $employee->user_id === $this->session->userdata('user_id') )
                { $redirect = FALSE; break; }
            }
            if ( $redirect ) { redirect("task/f_view/{$id}"); }
        }

        $this->edit($id);
    }

    /**
     * @param $id
     * @param $done
     */
    public function requirement_done($id, $done)
    {
        $this->load->model('task_requirement_model');

        $res = $this->task_requirement_model->update($id, array(
            'done'  => $done
        ));

        if ( !$res )
        {
            $this->_return_json_error(validation_errors());
        }

        $this->_return_json_success();
    }

    /**
     * @param int|string $id
     */
    public function edit($id)
    {
        $this->load->model('stage_model');
        $this->load->model('project_model');
        $this->load->model('task_prerequisite_model');
        $this->load->model('task_employee_model');
        $this->load->model('employee_model');
        $this->load->model('task_tag_model');
        $this->load->model('task_requirement_model');
        $this->load->model('stage_status_model');
        $this->load->model('reason_model');

        $task = $this->task_model->get($id);
        $stage = $this->stage_model->get($task->stage_id);
        $project = $this->project_model->get($stage->project_id);
        $tasks = $this->project_model->get_tasks($project->id);
        $stages = $this->stage_model->dropdown('id', 'title', array('where'=>array('stages.project_id'=>$project->id)));
        $employees = $this->employee_model->find(array('where' => array('employees.active' => 'true'),'order_by'=>'companies.name,full_name ASC'));

        $status_q = $this->stage_status_model->find(array('order_by'=>'name ASC'));
        $reasons_q = $this->reason_model->find(array('order_by'=>'name ASC'));

        if ( isset($_POST['requirements']) )
        {
            $requirements = $_POST['requirements'];
        }
        else
        {
            $temp = $this->task_requirement_model->find(array('where'=>array('task_id'=>$id)));
            $requirements = array();
            foreach ($temp as $item)
            {
                $requirements[] = array(
                    'id'            => $item->id,
                    'requirement'   => $item->requirement
                );
            }
        }

        if ( isset($_POST['tags']) )
        {
            $tags = $_POST['tags'];
        }
        else
        {
            $tags = $this->task_tag_model->get_by_task($id, TRUE);
        }

        if ( isset($_POST['resources']) )
        {
            $resources = $_POST['resources'];
        }
        else
        {
            $temp = $this->task_employee_model->find(array('where' => array('task_id' => $id)));
            $resources = array();
            foreach ($temp as $item)
            {
                $resources[] = $item->user_id;
            }
        }

        if ( isset($_POST['prerequisites']) )
        {
            $prerequisites = $_POST['prerequisites'];
        }
        else
        {
            $temp = $this->task_prerequisite_model->find(array(
                'where' => array(
                    'task_id'   => $id
                )
            ));
            $prerequisites = array();
            if ( $temp )
            {
                foreach ($temp as $item) {
                    $prerequisites[] = $item->task_prerequisite;
                }
            }
        }

        $this->data = array(
            'styles'    => array(
                'public/plugins/jquery-select2/css/select2.min.css',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css'
            ),
            'scripts'   => array(
                'public/plugins/jquery-select2/js/select2.min.js',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
                'public/plugins/jquery-sheepIt/jquery.sheepItPlugin.js'
            ),
            'stage'         => $stage,
            'project'       => $project,
            'stages'        => $stages,
            'tasks'         => $tasks,
            'prerequisites' => $prerequisites,
            'employees'     => $employees,
            'resources'     => $resources,
            'tags'          => $tags,
            'requirements'  => $requirements,
            'status'            => $status_q,
            'reasons'           => $reasons_q
        );

        parent::edit($id);
    }

    /**
     * @param $id
     */
    public function analize_reschedule($id)
    {
        $data = $_POST;

        $task = $this->task_model->get($id);
        if ( !$task )
        {
            $this->_return_json_error('La tarea no existe');
        }

        if ( $task->start_date !== $data['start_date'] || $task->estimated_end_date !== $data['estimated_end_date'] )
        {
            $res = $this->task_model->analize_reschedule($id, $data['start_date'], $data['estimated_end_date']);
            if ( $res !== TRUE )
            {
                $this->_return_json_success('OK', $res);
            }
        }

        $this->_return_json_success('OK');
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ){ exit("Incorrect method."); }

        try {
            $result = $this->{$this->_model}->delete($id);

            if ( $result ){
                $this->_return_json_success('Tarea eliminada.');
            }else{
                $this->_return_json_error('No fue posible eliminar la tarea.');
            }
        } catch (Exception $error) {
            $this->_return_json_error($error->getMessage());
        }
    }

    /**
     * @param $id
     */
    public function close($id)
    {
        try {
            $this->task_model->close($id);
            $this->_close_task_notifications($id);
        } catch ( Exception $error ) {
            $this->_return_json_error($error->getMessage());
        }

        $this->_return_json_success();
    }

    /**
     * @param $id
     */
    public function cancel($id)
    {
        try {
            $this->task_model->cancel($id);
        } catch ( Exception $error ) {
            $this->_return_json_error($error->getMessage());
        }

        $this->_return_json_success();
    }

    /**
     * @param $start_date
     * @param $days
     * @param $hours
     */
    public function calc_end_date($start_date, $days, $hours)
    {
        $this->load->model('calendar_model');

        $res = $this->calendar_model->calc_end_date(1, $start_date, $days, $hours);

        if ( $res )
        {
            $this->_return_json_success('OK', array(
                'end_date'      => $res,
                'nice_date'     => date('d/m/Y', strtotime($res))
            ));
        }
    }


    protected function _close_task_notifications($task_id)
    {
        $this->load->model('project_model');
        $this->load->model('client_model');
        $this->load->model('notification_model');
        $this->load->model('push_subscription_model');

        $project_id = $this->task_model->get_project($task_id);
        $project = $this->project_model->get($project_id);

        $task = $this->task_model->get($task_id);
        $employees = $this->task_model->get_employees($project_id);

        if ( !in_array($project->manager_id, $employees) ) { $employees[] = $project->manager_id; }

        $not_message = "Tarea completada: {$task->title}.";
        if ( $task->status_changer === 'true' && $task->stage_status_id && $task->reason_id )
        {
            $not_message .= " Ha cambiado el estado interno del proyecto a: '{$task->stage_status}' por la causa: '{$task->reason}'.";
        }

        $notification = array(
            'message'   => $not_message,
            'actions'   => json_encode(array(array(
                'action'    => 'task/f_view/'.$task->id,
                'title'     => 'Ver Tarea'
            )))
        );

        foreach ($employees as $employee_id)
        {
            $notification['user_id'] = $employee_id;
            $this->notification_model->insert($notification);
        }

        // CLIENTS
        if ( $task->client_notification === 'true' )
        {
            $clients = $this->client_model->get_contacts($project->client_id);
            $notification = array(
                'message'   => 'Tarea completada: '.$task->title
            );

            foreach ($clients as $client)
            {
                if ( !$client->user_id ) { continue; }
                $notification['user_id'] = $client->user_id;
                $this->notification_model->insert($notification);
                $employees[] = $client->user_id;
            }
        }

        $endpoints = array();
        foreach ($employees as $employee)
        {
            $endps = $this->push_subscription_model->get_by_user($employee);
            foreach ($endps as $endp) { $endpoints[] = $endp; }
        }

        if ( count($endpoints) === 0 ) { return; }

        $this->load->library('gcm');
        $this->gcm->setRecipients($endpoints);
        $this->gcm->setMessage('Nuevas notificaciones');
        $this->gcm->send();
    }

    public function task_filter_list()
    {
        $this->_template("{$this->_controller}/task_filter_list", $this->_get_assets('list', $this->data));
    }

    /**
     * @param $day
     * @param $month
     * @param $project_id
     * @param string $stage
     * @param string $status
     * @param int $brand
     */
    public function tasks_filter_json($day,$month,$project_id, $stage='all', $status='all', $brand=0)
    {
        $stage = str_replace('_',' ',$stage);
        echo $this->task_model->tasks_filter_json($day,$month,$project_id,$stage,$status,$brand);
    }

    /**
     * @param $id
     */
    public function get_ideal_start_date($id)
    {
        $start_date = $this->task_model->get_ideal_start_date($id);

        if ( !$start_date ) { $this->_return_json_error('La tarea no existe'); }

        $this->_return_json_success('OK', array('start_date'=>$start_date));
    }
}
