<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration extends CI_Controller {

    /**
     * migration constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->library('migration');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $result = $this->migration->find_migrations();

        if (count($result) < 1) { exit('No se encontraron migraciones.'); };

        echo "<li><a href='".base_url('migration/version/0')."'>LIMPIAR_TODO</a></li>";
        echo '<ol>';
        foreach($result as $k => $migration)
        {
            $exp = explode('/', $migration);
            $txt = strtoupper(str_replace('.php', '', end($exp)));
            echo "<li><a href='".base_url('migration/version/'.$k)."'>{$txt}</a></li>";
        }
        echo '</ol>';

        echo "<li><a href='".base_url('migration/current')."'>VERSION DE PRODUCCION</a></li>";
    }

    /**
     * @param $vers
     */
    public function version($vers)
    {
        $result = $this->migration->version($vers);

        if ($result === FALSE) { show_error($this->migration->error_string()); }
        elseif ($result === TRUE) { echo 'No se encontraron migraciones'; }
        else { echo "Migración realizada correctamente: " . $result; }

        echo "<li><a href='".base_url('migration')."'>VOLVER</a></li>";
    }

    /**
     *
     */
    public function current()
    {
        $result = $this->migration->current();

        if ($result === FALSE) { show_error($this->migration->error_string()); }
        elseif ($result === TRUE) { echo 'No se encontraron migraciones'; }
        else { echo "Migración realizada correctamente: " . $result; }

        echo "<li><a href='".base_url('migration')."'>VOLVER</a></li>";
    }

}
