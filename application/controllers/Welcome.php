<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Brand_model $brand_model
 * @property Company_model $company_model
 * @property Project_model $project_model
 * @property Project_category_model $project_category_model
 * @property Stage_status_model $stage_status_model
 * @property Reason_model $reason_model
 * @property Producto_model $producto_model
 */
class Welcome extends MY_Controller
{

    protected $_model = FALSE;

    /**
     * Index Page for this controller.
     */
	public function index()
	{
	    $data = array(
            'styles'    => array(

            ),
            'scripts'   => array(
                'public/plugins/flot-charts/jquery.flot.js',
                'public/plugins/flot-charts/jquery.flot.resize.js',
                'public/plugins/flot-charts/jquery.flot.pie.js',
                'public/plugins/flot-charts/jquery.flot.categories.js',
                'public/plugins/flot-charts/jquery.flot.time.js',
                'public/plugins/flot-charts/barnumbers.js',
                'public/js/src/welcome.js'
            )
        );

        $this->_template('welcome_message', $data);
	}

    /**
     *
     */
    public function products_chart()
    {
        $this->load->model('producto_model');

        $productos = $this->producto_model->all();

        $data = array();
        foreach ($productos as $k => $producto)
        {
            $data[] = array(
                'color'     => $k,
                'label'     => "[{$producto->codigo}] {$producto->descripcion}",
                'data'      => array(array($k, (int)$producto->cantidad))
            );
        }

        $this->_return_json_success('OK', $data);
    }

}
