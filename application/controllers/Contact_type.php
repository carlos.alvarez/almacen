<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Contact_type
 *
 * @property Contact_type_model $contact_type_model
 */
class Contact_type extends MY_Controller {

	/**
	 * [$_controller description]
	 * @var string
	 */
	protected $_controller = 'contact_type';

	/**
	 * [$_model description]
	 * @var string
	 */
	protected $_model = 'contact_type_model';

	/**
	 * [datatable_json description]
	 */
	public function datatable_json()
	{
		echo $this->contact_type_model->datatable_json();
	}

    /**
     * @param int $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ){ exit("Incorrect method."); }

        try {
            $result = $this->contact_type_model->delete($id);
            if ( $result )
            {
                $this->_return_json_success('Tipo de contacto eliminado sastifactoriamente.');
            }
            else
            {
                $this->_return_json_error('No se pudo eliminar el tipo de contacto.');
            }
        } catch (Exception $e) {
            $this->_return_json_error($e->getMessage());
        }
    }
}
