<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Role
 * @property Role_model $role_model
 * * @property Entrada_model $entrada_model
 * @property Salida_model $salida_model
 */
class Api extends MY_Controller {

	/**
	 * [$_controller description]
	 * @var string
	 */
	protected $_controller = 'api';

	/**
	 * [$_model description]
	 * @var string
	 */
	protected $_model = FALSE;

    /**
     *
     */
	public function entrada()
    {
	    if ( !isset($_GET['empleado_id']) || !isset($_GET['codigo']) )
        {
            echo 'Faltan Parámetros';
        }

        $this->load->model('entrada_model');

	    $res = $this->entrada_model->insert($_GET);

	    if ( $res ) { echo 'Entrada registrada.'; }
	    else { echo validation_errors(); }
    }

    public function salida()
    {
        if ( !isset($_GET['empleado_id']) || !isset($_GET['codigo']) )
        {
            echo 'Faltan Parámetros';
        }

        $this->load->model('salida_model');

        $res = $this->salida_model->insert($_GET);

        if ( $res ) { echo 'Salida registrada.'; }
        else { echo validation_errors(); }
    }
}
