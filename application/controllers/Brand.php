<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User: cristian.rodriguez
 * Date: 9/7/2017
 * Time: 5:25 PM
 */

/**
 * Class Brand
 */
class Brand extends MY_Controller
{
    /**
     * @var string
     */
    protected $_controller = 'brand';

    /**
     * @var string
     */
    protected $_model = 'brand_model';

    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ){ exit("Incorrect method."); }
        try {
            $result = $this->{$this->_model}->delete($id);

            if ( $result )
            {
                $this->_return_json_success('Marca eliminada.');
            }
            else
            {
                $this->_return_json_error('No fue posible eliminar la marca.');
            }
        } catch (Exception $e) {
            $this->_return_json_error($e->getMessage());
        }

    }

    public function get_brands_datatable_json()
    {
        echo $this->{$this->_model}->datatable_json();
    }
}