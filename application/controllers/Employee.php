<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Employee
 * @property Employee_model $employee_model
 * @property User_model $user_model
 * @property Company_model $company_model
 * @property Department_model $department_model
 * @property Area_model $area_model
 * @property Position_model $position_model
 * @property Correo $correo
 */
class Employee extends MY_Controller {

	/**
	 * [$_controller employee]
	 * @var string
	 */
	protected $_controller = 'employee';

	/**
	 * [$_model employee]
	 * @var string
	 */
	protected $_model = 'employee_model';

    /**
     * @var array
     */
    protected $rules = array(
        array( // Username
            'field'     => 'username',
            'label'     => 'lang:username',
            'rules'     => 'trim|max_length[30]|is_unique[users.username]'
        ),
        array( // Password
            'field'     => 'password',
            'label'     => 'lang:password',
            'rules'     => 'trim|min_length[4]'
        ),
        array( // Repeat Password
            'field'     => 'repeat_password',
            'label'     => 'lang:repeat_password',
            'rules'     => 'trim|min_length[4]|matches[password]'
        ),
        array( // Email
            'field'     => 'email',
            'label'     => 'lang:email',
            'rules'     => 'trim|valid_email|max_length[250]'
        ),
        array( // First name
            'field'     => 'first_name',
            'label'     => 'lang:first_name',
            'rules'     => 'trim|min_length[2]|max_length[100]'
        ),
        array( // Last name
            'field'     => 'last_name',
            'label'     => 'lang:last_name',
            'rules'     => 'trim|min_length[2]|max_length[100]'
        ),
        array( // area_id
            'field'     => 'area_id',
            'label'     => 'lang:area',
            'rules'     => 'trim|required|is_natural_no_zero|exist[areas.id]'
        ),
        array( // position_id
            'field'     => 'position_id',
            'label'     => 'lang:position',
            'rules'     => 'trim|required|is_natural_no_zero|exist[positions.id]'
        ),
        array( // supervisor_id
            'field'     => 'supervisor_id',
            'label'     => 'lang:supervisor',
            'rules'     => 'trim|is_natural_no_zero|exist[employees.user_id]'
        ),
        array( //
            'field'     => 'phone',
            'label'     => 'lang:phone',
            'rules'     => 'trim|min_length[2]|max_length[20]'
        ),
        array( //
            'field'     => 'ext',
            'label'     => 'lang:ext',
            'rules'     => 'trim|min_length[1]|max_length[8]'
        ),
        array( //
            'field'     => 'mobile',
            'label'     => 'lang:mobile',
            'rules'     => 'trim|min_length[2]|max_length[20]'
        ),
    );

    /**
     *
     */
    public function create()
    {
        $this->load->model('company_model');
        $this->load->model('department_model');
        $this->load->model('area_model');
        $this->load->model('position_model');
        $this->load->model('user_model');

        if ( $this->input->method() === 'post' )
        {
            if ( $this->input->post('creation_mode') === 'new' )
            {
                $this->form_validation->set_rules($this->rules);
                if ( $this->form_validation->run() !== TRUE )
                {
                    $_SERVER['REQUEST_METHOD'] = 'get';
                }
                else
                {
                    $user_id = $this->user_model->insert($this->input->post());
                    if ( $user_id )
                    {
                        $this->_send_welcome_email($user_id, $this->input->post('password'));
                        $_POST['user_id'] = $user_id;
                    }
                    else
                    {
                        $_SERVER['REQUEST_METHOD'] = 'get';
                    }
                }
            }
        }

        $companies = $this->company_model->find(array(
            'where'         => array(
                'active'        => 'true'
            ),
            'order_by'      => 'companies.name ASC'
        ));

        $departments = $this->department_model->find(array('where'=>array('active'=>'true')));
        $areas = $this->area_model->find(array('where'=>array('areas.active'=>'true')));
        $positions = $this->position_model->find();

        $users = $this->user_model->get_no_employees();
        $users = array_object_column($users, 'full_name', 'id');
        $users[''] = lang('choose_an_option').' *';

        $this->data = array(
            'companies'     => $companies,
            'departments'   => $departments,
            'areas'         => $areas,
            'positions'     => $positions,
            'users'         => $users,
            'supervisors'    => $this->employee_model->find(array('where' => array('employees.active' => 'true')))
        );

        parent::create();
    }

    /**
     * @param int|string $id
     */
    public function edit($id)
    {
        $this->load->model('company_model');
        $this->load->model('department_model');
        $this->load->model('area_model');
        $this->load->model('position_model');
        $this->load->model('user_model');

        $companies = $this->company_model->find(array(
            'where'         => array(
                'active'        => 'true'
            ),
            'order_by'      => 'companies.name ASC'
        ));

        $departments = $this->department_model->find(array('where'=>array('active'=>'true')));
        $areas = $this->area_model->find(array('where'=>array('areas.active'=>'true')));
        $positions = $this->position_model->find();
        $user = $this->user_model->get($id);

        $this->data = array(
            'companies'     => $companies,
            'departments'   => $departments,
            'areas'         => $areas,
            'positions'     => $positions,
            'user'          => $user,
            'supervisors'   => $this->employee_model->find(array('where' => array('employees.active' => 'true')))
        );

        parent::edit($id);
    }

    /**
     * @param int|string $id
     */
    public function view($id)
    {
        $this->load->model('user_model');

        $user = $this->user_model->get($id);
        $employee = $this->employee_model->get($id);
        $this->data = array(
            'user'         => $user,
            'supervisor'   => $this->employee_model->find(array('where' => array('user_id' => $employee->supervisor_id), 'limit' => 1))
        );

        parent::view($id);
    }

    /**
     * @param $position
     */
    public function get_by_position($position)
    {
        echo json_encode($this->employee_model->find(array(
            'where'     => array(
                'employees.position_id' => $position,
                'employees.active'      => 'true'
            ),
            'order_by'  => 'companies.name, full_name ASC'
        )));
    }

    /**
     *
     */
	public function datatable_json()
	{
		echo $this->{$this->_model}->datatable_json();
	}

    /**
     * @param int $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ){ exit("Incorrect method."); }

        try{
            $result = $this->employee_model->delete($id);
            if ( $result ) {
                $this->_return_json_success('Empleado eliminado sastifactoriamente.');
            } else {
                $this->_return_json_error('No se pudo eliminar el empleado.');
            }
        } catch (Exception $e) {
            $this->_return_json_error($e->getMessage());
        }
    }

    /**
     * @param $user_id
     * @param $password
     * @return bool
     */
    protected function _send_welcome_email($user_id, $password)
    {
        $this->load->model('user_model');
        $this->load->library('correo');

        $user = $this->user_model->get($user_id);

        $data = array(
            'to'            => $user->email,
            'subject'       => 'Bienvenido a ergospace',
            'content'       => 'welcome_employee',
            'data'          => array(
                'username'      => $user->username,
                'password'      => $password
            )
        );
        $this->correo->send($data);

        return TRUE;
    }
}
