<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Client
 *
 * @property Client_model $client_model
 * @property Employee_model $employee_model
 * @property Contact_type_model $contact_type_model
 * @property Client_category_model $client_category_model
 * @property Avatar $avatar
 * @property Client_contact_model $client_contact_model
 */
class Client extends MY_Controller {

	/**
	 * [$_controller description]
	 * @var string
	 */
	protected $_controller = 'client';

	/**
	 * [$_model description]
	 * @var string
	 */
	protected $_model = 'client_model';

	public function view($id)
    {
        $contacts = $this->client_model->get_contacts($id);
        $this->data = array(
            'contacts'      => $contacts
        );

        parent::view($id);
    }

    public function create()
    {
        $this->load->model('contact_type_model');
        $this->load->model('employee_model');
        $this->load->model('client_category_model');

        $employees = $this->employee_model->find(array('where'=>array('manage_clients'=>'true'),'order_by'=>'full_name ASC'));

        $pre_contacts = array();
        if ( isset($_POST['contacts']) ) { $pre_contacts = $_POST['contacts']; }

        $contact_types = $this->contact_type_model->all();
        $this->data = array(
            'scripts'   => array(
                'public/plugins/jquery-sheepIt/jquery.sheepItPlugin.js'
            ),
            'contact_types' => $contact_types,
            'contacts'      => $pre_contacts,
            'employees'     => $employees,
            'categories'    => $this->client_category_model->dropdown('id', 'name', array('sort_by' => 'name ASC'))
        );

        parent::create();
    }


    /**
     * @param $contact_id
     */
    public function create_user_contact($contact_id)
    {
        if ( $this->input->method() === 'post' && $this->input->is_ajax_request() )
        {
            $this->load->model('client_contact_model');
            try
            {
                if ( $this->client_contact_model->create_user($contact_id, $this->input->post()) )
                {
                    $this->_return_json_success('OK');
                }
                else
                {
                    $this->_return_json_error('No se pudo crear el usuario.');
                }
            }
            catch (Exception $e)
            {
                $this->_return_json_error($e->getMessage());
            }
        }
        else
        {
            redirect($this->_controller);
        }
    }

    public function edit($id)
    {
        $this->load->model('contact_type_model');
        $this->load->model('employee_model');
        $this->load->model('client_category_model');

        $employees = $this->employee_model->find(array('where'=>array('manage_clients'=>'true'),'order_by'=>'full_name ASC'));

        if ( isset($_POST['contacts']) ) { $pre_contacts = $_POST['contacts']; }
        else { $pre_contacts = (array) $this->client_model->get_contacts($id); }

        $contact_types = $this->contact_type_model->all();
        $this->data = array(
            'scripts'   => array(
                'public/plugins/jquery-sheepIt/jquery.sheepItPlugin.js',
                'public/js/pages/ui/modals.js'
            ),
            'contact_types' => $contact_types,
            'contacts'      => $pre_contacts,
            'employees'     => $employees,
            'categories'    => $this->client_category_model->dropdown('id', 'name', array('sort_by' => 'name ASC'))
        );

        //dump($_POST, TRUE);

        parent::edit($id);
    }

	public function datatable_json()
	{
		echo $this->{$this->_model}->datatable_json();
	}

    /**
     * @param $id
     */
	public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ){ exit("Incorrect method."); }

        try {
            $result = $this->client_model->delete($id);
            if ( $result ) {
                $this->_return_json_success('Cliente eliminado sastifactoriamente.');
            } else {
                $this->_return_json_error('No se pudo eliminar el cliente.');
            }
        } catch (Exception $e) {
            $this->_return_json_error($e->getMessage());
        }
    }

    /**
     * @param $id
     */
    protected function _on_insert_success($id)
    {
        if ( $this->input->method() === 'post' && isset($_FILES['picture_file']) && $_FILES['picture_file']['size'] > 0 )
        {
            $result = $this->_create_avatar($id);
            if ( $result !== TRUE )
            {
                $this->_alert($result, 'warning');
            }
        }
    }

    /**
     * @param $id
     */
    protected function _on_edit_success($id)
    {
        if ( $this->input->method() === 'post' && isset($_FILES['picture_file']) && $_FILES['picture_file']['size'] > 0 )
        {
            $result = $this->_create_avatar($id);
            if ( $result !== TRUE )
            {
                $this->_alert($result, 'warning');
            }
        }
    }

    /**
     * @param $id
     * @return bool|string
     */
    private function _create_avatar($id)
    {
        $this->load->library('avatar');

        $options = array(
            'file_input'        => 'picture_file',
            'avatar_path'       => "./public/img/clients/{$id}.jpg",
            'allowed_types'     => 'jpg|jpeg'
        );

        try
        {
            return $this->avatar->create($options);
        }
        catch ( Exception $e)
        {
            return $e->getMessage();
        }
    }

    public function get_client($id)
    {
        if ($this->input->method() !== 'get' && !$this->input->is_ajax_request()) { exit("Incorrect method."); }
        $result = $this->client_model->get($id);
        if ( $result )
        {
            $this->_return_json_success($result);
        }
        else
        {
            $this->_return_json_error('Client not found');
        }
    }
}
