<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Position
 * @property Position_model $position_model
 * @property Company_model $company_model
 */
class Position extends MY_Controller {

    /**
     * [$_controller description]
     * @var string
     */
    protected $_controller = 'position';

    /**
     * [$_model description]
     * @var string
     */
    protected $_model = 'position_model';

    public function create()
    {
        $this->load->model('company_model');

        $companies = $this->company_model->all(array('where' => array('active' => 'true')));
        $companies = array_object_column($companies, 'name', 'id');
        $companies[''] = lang('choose_an_option').' *';

        $this->data = array(
            'companies'     => $companies
        );

        parent::create();
    }

    /**
     * @param int|string $id
     */
    public function edit($id)
    {
        $this->load->model('company_model');

        $companies = $this->company_model->all(array('where' => array('active' => 'true')));
        $companies = array_object_column($companies, 'name', 'id');
        $companies[''] = lang('choose_an_option').' *';

        $this->data = array(
            'companies'     => $companies
        );

        parent::edit($id);
    }

    /**
     * @param $id
     */
    public function view($id)
    {
        $this->load->model('company_model');

        $position = $this->position_model->get($id);

        $company = $this->company_model->get($position->company_id);

        $this->data = array(
            'company'       => $company
        );

        parent::view($id);
    }

    public function datatable_json()
    {
        echo $this->{$this->_model}->datatable_json();
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ) { $this->_response_error('Incorrect method.'); }

        try {
            $result = $this->position_model->delete($id);
            if ( $result )
            {
                $this->_return_json_success('Cargo eliminado sastifactoriamente.');
            }
            else
            {
                $this->_return_json_error('No se pudo eliminar el cargo.');
            }
        } catch (Exception $e) {
            $this->_return_json_error($e->getMessage());
        }
    }
}
