<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Calendar
 *
 * @property Calendar_holiday_model $calendar_holiday_model
 * @property Calendar_model $calendar_model
 */
class Calendar extends MY_Controller {

    /**
     * @param $id
     */
    public function view($id)
    {
        $this->data = $this->_assets_list;

        parent::view($id);
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        $this->data = $this->_assets_list;

        parent::edit($id);
    }

    /**
     *
     */
    public function datatable_json()
    {
        echo $this->{$this->_model}->datatable_json();
    }

    /**
     * @param $calendar_id
     * @param $year
     */
    public function holiday_datatable_json($calendar_id, $year)
    {
        $this->load->model('calendar_holiday_model');
        echo $this->calendar_holiday_model->datatable_json($calendar_id, $year);
    }

    /**
     *
     */
    public function create_holiday()
    {
        $this->load->model('calendar_holiday_model');

        if ( $this->input->method() !== 'post' ) { $this->_response_error('Should be POST'); }

        if ( $id = $this->calendar_holiday_model->insert($_POST) )
        {
            $this->_response_success(lang('success_message'), array('id' => $id));
        }
        else
        {
            $this->_response_error(validation_errors());
        }
    }

    /**
     * @param $holiday_id
     */
    public function edit_holiday($holiday_id)
    {
        $this->load->model('calendar_holiday_model');

        if ( $this->input->method() !== 'post' ) { $this->_response_error('Should be POST'); }

        if ( $this->calendar_holiday_model->update($holiday_id, $_POST) )
        {
            $this->_response_success();
        }
        else
        {
            $this->_response_error(validation_errors());
        }
    }

    /**
     * @param $holiday_id
     */
    public function delete_holiday($holiday_id)
    {
        $this->load->model('calendar_holiday_model');

        if ( $this->input->method() !== 'post' ) { $this->_response_error('Should be POST'); }

        if ( $this->calendar_holiday_model->delete($holiday_id) )
        {
            $this->_response_success();
        }
        else
        {
            $this->_response_error(validation_errors());
        }
    }


    /**
     * @param int $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ) { $this->_response_error('Incorrect method.'); }

        try {
            $result = $this->calendar_model->delete($id);
            if ($result)
            {
                $this->_return_json_success('Calendario eliminado sastifactoriamente.');
            }
            else
            {
                $this->_response_error('No se pudo eliminar el calendario.');
            }
        } catch (Exception $e){
            $this->_return_json_error($e->getMessage());
        }
    }
}
