<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User: cristian.rodriguez
 * Date: 9/21/2017
 * Time: 11:05 AM
 */

/**
 * Class My_space
 * @property Brand_model $brand_model
 * @property Company_model $company_model
 * @property Project_category_model $project_category_model
 * @property Project_model $project_model
 * @property Task_model $task_model
 * @property Project_brand_model project_brand_model
 */
class My_space extends MY_Controller
{
    protected $_model = FALSE;
    protected $_controller = 'my_space';

    public function my_projects()
    {
        if ($this->session->userdata('client')) { redirect($this->_controller.'/my_projects_client'); }

        $this->load->model('brand_model');
        $this->load->model('company_model');
        $this->load->model('project_category_model');

        $this->data = array(
            'styles'	=> array(
                'public/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css',
                //'public/plugins/Responsive-master/css/responsive.bootstrap.css'
            ),
            'scripts'   => array(
                'public/plugins/jquery-datatable/jquery.dataTables.js',
                'public/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js',
                //'public/plugins/Responsive-master/js/dataTables.responsive.js',
                //'public/plugins/Responsive-master/js/responsive.bootstrap.js',
                'public/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.flash.min.js',
                'public/plugins/jquery-datatable/extensions/export/jszip.min.js',
                'public/plugins/jquery-datatable/extensions/export/pdfmake.min.js',
                'public/plugins/jquery-datatable/extensions/export/vfs_fonts.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.html5.min.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.print.min.js',
                'public/plugins/jquery-datatable/extensions/export/ellipsis.js',
                'public/plugins/jquery-datepicker/datepicker.js',
                'public/plugins/jquery-datepicker/i18n/datepicker-es.js',
                'public/plugins/chartjs/Chart.bundle.js',
                'public/plugins/chartjs/Chart.js',
                'public/plugins/sticky/jquery.sticky.js',
                'public/js/src/my_projects.js'
            ),
            'brands'        => $this->brand_model->all(array('order_by' => 'name ASC')),
            'companies'     => $this->company_model->all(array('order_by' => 'name ASC')),
            'categories'     => $this->project_category_model->all(array('order_by' => 'name ASC'))
        );

        $this->_template($this->_controller.'/my_projects', $this->data);
    }

    public function get_my_projects()
    {
        $this->load->model('project_model');
        $result = NULL;
        if (  $this->session->userdata('client') )
        {
            $result = $this->project_model->my_datatable_json(0, $this->session->userdata('client_id'));
        }
        else
        {
            $result = $this->project_model->my_datatable_json($this->session->userdata('user_id'));
        }
        echo $result;
    }

    public function get_my_tasks()
    {
        $this->load->model('task_model');
        $result = NULL;
        if ( $this->session->userdata('client') )
        {
            $result = $this->task_model->my_datatable_json(0, $this->session->userdata('client_id'));
        }
        echo $this->task_model->my_datatable_json($this->session->userdata('user_id'));
    }

    public function my_projects_client()
    {
        if ( !$this->session->userdata('client') ) { redirect($this->_controller.'/my_projects'); }

        $this->data = array(
            'styles'	=> array(
                'public/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css',
                //'public/plugins/Responsive-master/css/responsive.bootstrap.css'
            ),
            'scripts'   => array(
                'public/plugins/jquery-datatable/jquery.dataTables.js',
                'public/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js',
                //'public/plugins/Responsive-master/js/dataTables.responsive.js',
                //'public/plugins/Responsive-master/js/responsive.bootstrap.js',
                'public/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.flash.min.js',
                'public/plugins/jquery-datatable/extensions/export/jszip.min.js',
                'public/plugins/jquery-datatable/extensions/export/pdfmake.min.js',
                'public/plugins/jquery-datatable/extensions/export/vfs_fonts.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.html5.min.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.print.min.js',
                'public/plugins/jquery-datatable/extensions/export/ellipsis.js',
                'public/plugins/jquery-datepicker/datepicker.js',
                'public/plugins/jquery-datepicker/i18n/datepicker-es.js',
                'public/plugins/chartjs/Chart.bundle.js',
                'public/plugins/chartjs/Chart.js',
                'public/js/src/my_projects_client.js'
            )
        );

        $this->_template($this->_controller.'/my_projects_client', $this->data);
    }
}