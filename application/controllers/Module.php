<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Module_model $module_model
 */
class Module extends MY_Controller {

	/**
	 * [$_controller description]
	 * @var string
	 */
	protected $_controller = 'module';

	/**
	 * [$_model description]
	 * @var string
	 */
	protected $_model = 'module_model';

	/**
	 * [datatable_json description]
	 */
	public function datatable_json()
	{
		echo $this->module_model->datatable_json();
	}

	public function delete($id)
    {
        if ( $this->module_model->delete($id) )
        {
            $this->_return_json_success('Módulo eliminado sastifactoriamente.');
        }
        else
        {
            $this->_return_json_error('No se pudo eliminar el módulo.');
        }
    }
}
