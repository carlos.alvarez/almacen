<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Client_model $client_model
 * @property Employee_model $employee_model
 * @property Company_model $company_model
 * @property Template_model $template_model
 * @property Project_category_model $project_category_model
 * @property Project_tag_model $project_tag_model
 * @property Project_comment_model $project_comment_model
 * @property Project_file_model $project_file_model
 * @property Project_model $project_model
 * @property Project_log_model $project_log_model
 * @property Project_brand_model $project_brand_model
 * @property Brand_model $brand_model
 * @property Notification_model $notification_model
 * @property Push_subscription_model $push_subscription_model
 * @property GCM $gcm
 * @property User_model $user_model
 * @property Correo $correo
 */
class Project extends MY_Controller {

    /**
     * @var string
     */
    protected $_controller = 'project';

    /**
     * @var string
     */
	protected $_model = 'project_model';

    /**
     *
     */
	public function create()
    {
        $this->load->model('client_model');
        $this->load->model('company_model');
        $this->load->model('employee_model');
        $this->load->model('template_model');
        $this->load->model('project_category_model');
        $this->load->model('brand_model');
        $this->data = array(
            'styles'    => array(
                'public/plugins/jquery-select2/css/select2.min.css',
                'public/plugins/nestable/jquery-nestable.css',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',
                'public/plugins/jquery-datepicker/datepicker.css'
            ),
            'scripts'   => array(
                'public/plugins/jquery-steps/jquery.steps.js',
                'public/plugins/jquery-select2/js/select2.min.js',
                'public/plugins/nestable/jquery.nestable.js',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
                'public/plugins/jquery-datepicker/datepicker.js'//,
                //'public/plugins/jquery-datepicker/i18n/datepicker-es.js'
            ),
            'clients'       => $this->client_model->all(array('select'=>'clients.id,clients.name','where'=>array('clients.active'=>'true'),'order_by'=>'clients.name ASC')),
            'companies'     => $this->company_model->all(array('select'=>'companies.id,companies.name','where'=>array('companies.active'=>'true'),'order_by'=>'companies.name ASC')),
            'employees'     => $this->employee_model->all(array('where'=>array('employees.active'=>'true'),'order_by'=>'full_name ASC')),
            'templates'     => $this->template_model->all(array('order_by'=>'title ASC')),
            'categories'    => $this->project_category_model->dropdown('id', 'name', array('order_by'=>'name ASC')),
            'tags'          => isset($_POST['tags']) ? $_POST['tags'] : '',
//            'brands'        => $this->brand_model->all()
        );

        if ( $this->input->method() === 'post' )
        {

            if ( $id = $this->project_model->create_from_template($this->input->post()) )
            {
                $this->_new_project_notifications($id);
                $this->_new_project_emails($id);
                if ( !$this->input->is_ajax_request() )
                {
                    redirect("{$this->_controller}/view/{$id}");
                }
                else
                {
                    $this->_response_success('Listo');
                    //exit('Listo');
                }
            }
            else
            {
                $this->_response_error(validation_errors());
            }
        }

        $this->_template("{$this->_controller}/create", $this->_get_assets('create', $this->data));
    }

    /**
     * @param $id
     */
    public function f_edit($id)
    {
        $project = $this->project_model->get($id);
        if( (grant_access('project', 'edit') || $project->manager_id === $this->session->userdata('user_id')) && $project->status === 'progreso' )
        {
            $this->edit($id);
        }
        else
        {
            redirect('project/view/'.$id);
        }
    }

    /**
     * @param int|string $id
     */
    public function edit($id)
    {
        $this->load->model('client_model');
        $this->load->model('company_model');
        $this->load->model('employee_model');
        $this->load->model('template_model');
        $this->load->model('project_category_model');
        $this->load->model('project_tag_model');

        $temp = $this->employee_model->all(array('where'=>array('employees.active'=>'true'),'group_by'=>'company_id'));
        $enabled_companies = array();
        foreach ($temp as $t) { $enabled_companies[$t->company_id] = TRUE; }

        $tags = $this->project_tag_model->find(array('select'=>'tag','where'=>array('project_id'=>$id)), TRUE);
        $tags = implode(',', array_column($tags, 'tag'));

        $this->data = array(
            'styles'    => array(
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',
                'public/plugins/jquery-datepicker/datepicker.css',
                'public/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css',
                //'public/plugins/Responsive-master/css/responsive.bootstrap.css'
            ),
            'scripts'   => array(
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
                'public/plugins/jquery-datepicker/datepicker.js',
                'public/plugins/jquery-datepicker/i18n/datepicker-es.js',
                'public/plugins/jquery-datatable/jquery.dataTables.js',
                'public/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js',
                //'public/plugins/Responsive-master/js/dataTables.responsive.js',
                //'public/plugins/Responsive-master/js/responsive.bootstrap.js',
                'public/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.flash.min.js',
                'public/plugins/jquery-datatable/extensions/export/jszip.min.js',
                'public/plugins/jquery-datatable/extensions/export/pdfmake.min.js',
                'public/plugins/jquery-datatable/extensions/export/vfs_fonts.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.html5.min.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.print.min.js',
                'public/plugins/jquery-datatable/extensions/export/ellipsis.js',
            ),
            'clients'       => $this->client_model->all(array('select'=>'clients.id,clients.name','where'=>array('clients.active'=>'true'),'order_by'=>'clients.name ASC')),
            'companies'     => $this->company_model->all(array('select'=>'companies.id,companies.name','where'=>array('companies.active'=>'true'),'order_by'=>'companies.name ASC')),
            'employees'     => $this->employee_model->all(array('where'=>array('employees.active'=>'true'),'order_by'=>'full_name ASC')),
            'enabled_companies' => $enabled_companies,
            'templates'     => $this->template_model->all(array('order_by'=>'title ASC')),
            'categories'    => $this->project_category_model->dropdown('id', 'name', array('order_by'=>'name ASC')),
            'tags'          => isset($_POST['tags']) ? $_POST['tags'] : $tags
        );

        parent::edit($id);
    }

    /**
     * @param $id
     */
    public function f_view($id)
    {
        if ( !grant_access('project', 'view') && !in_array($this->session->userdata('user_id'), $this->project_model->get_employees($id)) )
        {
            redirect('project');
        }

        $this->view($id);
    }

    /**
     * @param int|string $id
     */
    public function view($id)
    {
        $this->load->model('project_tag_model');
        $this->load->model('project_brand_model');
        $this->load->model('brand_model');

        $tasks = $this->project_model->get_tasks($id);
        $close_btn = TRUE;
        foreach ($tasks as $task)
        {
            if ( in_array($task->status, array('pendiente','proceso')) )
            {
                $close_btn = FALSE;
                break;
            }
        }

        $tags = $this->project_tag_model->find(array('select'=>'tag','where'=>array('project_id'=>$id)), TRUE);
        $tags = array_column($tags, 'tag');

        $this->data = array(
            'styles'	=> array(
                'public/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css',
                //'public/plugins/Responsive-master/css/responsive.bootstrap.css',
                'public/plugins/jsgantt-improved/jsgantt.css'
            ),
            'scripts' 	=> array(
                'public/plugins/jquery-datatable/jquery.dataTables.js',
                'public/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js',
                //'public/plugins/Responsive-master/js/dataTables.responsive.js',
                //'public/plugins/Responsive-master/js/responsive.bootstrap.js',
                'public/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.flash.min.js',
                'public/plugins/jquery-datatable/extensions/export/jszip.min.js',
                'public/plugins/jquery-datatable/extensions/export/pdfmake.min.js',
                'public/plugins/jquery-datatable/extensions/export/vfs_fonts.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.html5.min.js',
                'public/plugins/jquery-datatable/extensions/export/buttons.print.min.js',
                'public/plugins/jquery-datatable/extensions/export/ellipsis.js',
                'public/plugins/jquery-datepicker/datepicker.js',
                'public/plugins/jquery-datepicker/i18n/datepicker-es.js',
                'public/plugins/jsgantt-improved/jsgantt.js',
                'public/plugins/chartjs/Chart.bundle.js',
                'public/plugins/chartjs/Chart.js',
                'public/plugins/jquery-validation/jquery.validate.js',
                'public/plugins/jquery-validation/localization/messages_es.js'
            ),
            'tags'              => $tags,
            'budgets'           => $this->project_brand_model->find(array('where' => array('project_id' => $id))),
            'brands'            => $this->brand_model->all(array('order_by'=>'name ASC')),
            'progress'          => $this->project_model->progress($id),
            'close_btn'         => $close_btn
        );
        parent::view($id);
    }

    /**
     * @param $id
     */
    public function create_tbody($id)
    {
        $this->load->model('project_brand_model');

        $budgets = $this->project_brand_model->find(array('where' => array('project_id' => $id)));

        $tbody = '';
        foreach($budgets as $budget){
            $tbody .= '<tr>
                <td>'.$budget->id.'</td>
                <td>'.$budget->brands_name.'</td>
                <td class="text-center">$<span class="estimated-amount" data-amount="'.$budget->estimated_amount.'">'.number_format($budget->estimated_amount, 2,'.',',').'</span></td>
                <td>'.date('d/M/Y', strtotime($budget->estimated_date)).'</td>
                <td class="text-center">$<span class="actual-amount" data-amount="'.$budget->actual_amount.'">'.number_format($budget->actual_amount, 2,'.',',').'</span></td>
                <td>'.date('d/M/Y', strtotime($budget->actual_date)).'</td>
                <td>
                    <div class="btn-group">
                        <a data-id="'.$budget->id.'" class="btn btn-info btn-xs waves-effect '.grant_show('project_brand','edit').' budget-edit" data-toggle="tooltip" data-original-title="Editar" aria-describedby="tooltip62885"><i class="material-icons">edit</i></a>
                        <a href="#" class="btn btn-danger btn-xs waves-effect delete-budget '.grant_show('project_brand', 'delete').'" data-control="project_budget" data-id="'.$budget->id.'" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>
                    </div>
                </td>
            </tr>';
        }
        $this->_return_json_success('OK',$tbody);
    }

    /**
     * @param int $id
     */
    public function delete($id)
    {
        if ( !$this->input->is_ajax_request() ){ exit("Incorrect method."); }

        $result = $this->{$this->_model}->delete($id);

        if ( $result ){
            $this->_return_json_success('Proyecto eliminado.');
        }else{
            $this->_return_json_error('No fue posible eliminar el proyecto.');
        }
    }

    /**
     * @param $id
     */
    public function close($id)
    {
        try {
            $this->project_model->close($id);
        } catch ( Exception $error ) {
            $this->_return_json_error($error->getMessage());
        }
        $this->_close_project_notifications($id);
        $this->_close_project_emails($id);

        $this->_return_json_success();
    }

    /**
     * @param $id
     */
    public function cancel($id)
    {
        try {
            $this->project_model->cancel($id);
        } catch ( Exception $error ) {
            $this->_return_json_error($error->getMessage());
        }

        $this->_cancel_project_notifications($id);
        $this->_cancel_project_emails($id);

        $this->_return_json_success();
    }

    /**
     * @param int $id
     */
    public function get_comments($id)
    {
        $this->load->model('project_comment_model');
        $this->load->helper('comments_helper');

        $comments = $this->project_comment_model->find(array(
            'where'     => array(
                'project_id'    => $id
            ),
            'order_by'  => 'projects_comments.id DESC'
        ));
        echo comments_panel($comments);
    }

    /**
     *
     */
    public function new_comment()
    {
        $this->load->model('project_comment_model');

        $_POST['user_id'] = $this->session->userdata('user_id');
        $res = $this->project_comment_model->insert($_POST);

        if ( $res )
        {
            $this->_new_comment_notifications($res);
            $this->_response_success();
        }
        else { $this->_response_error(validation_errors()); }
    }

    /**
     * @param $id
     */
    public function update_comment_visibility($id)
    {
        if ( $this->input->method() !== 'post' ) { $this->_response_error('Incorrect method.'); }

        $this->load->model('project_comment_model');

        $res = $this->project_comment_model->update($id,$this->input->post());
        if ( $res ) { $this->_return_json_success(); }
        else { $this->_return_json_error('No se pudo cambiar la visibilidad.'); }
    }

    /**
     *
     */
	public function datatable_json()
	{
		echo $this->{$this->_model}->datatable_json();
	}

    /**
     * @param int $id
     */
    public function files_datatable_json($id)
    {
        $this->load->model('project_file_model');
        echo $this->project_file_model->project_datatable_json($id);
    }

    public function logs_datatable_json($id)
    {
        $this->load->model('project_log_model');
        echo $this->project_log_model->datatable_json($id);
    }

    /**
     * @param int $project_id
     */
    public function new_file($project_id)
    {
        $this->_exist($project_id);
        if ( $this->input->method() !== 'post' ) { $this->_response_error('Incorrect method.'); }

        $allowed_types = 'gif|jpg|jpeg|png|bmp|zip|rar|txt|csv|pdf|doc|docx|xls|xlsx|ppt|pptx';
        $config = array(
            'upload_path'		=> './public/project_files',
            'allowed_types'		=> $allowed_types,
            'file_ext_tolower'  => TRUE,
            'max_filename'		=> 100,
            'max_size'		    => 5120, // 5120 KB = 5 MB
            'file_name'			=> 'file_'.str_pad($project_id, 6, '0', STR_PAD_LEFT).'_'.str_pad($_SESSION['user_id'], 6, '0', STR_PAD_LEFT).'_'.date('YmdHis'),
            'overwrite'			=> TRUE
        );

        $this->load->library('upload', $config);
        if ( !$this->upload->do_upload('project_file') )
        {
            $this->_response_error($this->upload->display_errors('',''));
        }

        $this->load->model('project_file_model');
        $data = $this->upload->data();
        $res = $this->project_file_model->insert(array(
            'project_id'        => $project_id,
            'user_id'           => $_SESSION['user_id'],
            'name'              => $data['client_name'],
            'description'       => isset($_POST['description']) ? $_POST['description'] : NULL,
            'path'              => 'public/project_files/'.$data['file_name'],
            'client_visibility' => $this->input->post('client_visibility'),
        ));
        if ( !$res ) {
            unlink('./public/project_files/'.$data['file_name']);
            $this->_response_error(validation_errors());
        }

        $this->_response_success();
    }

    /**
     * @param $id
     */
    public function update_file_visibility($id)
    {
        if ( $this->input->method() !== 'post' ) { $this->_response_error('Incorrect method.'); }
        if ( ! ($client_visibility = $this->input->post('client_visibility')) ) { $this->_response_error('No client_visibility found.'); }

        $this->load->model('project_file_model');

        $res = $this->project_file_model->update($id, array('client_visibility'=>$client_visibility));
        if ( $res ) { $this->_return_json_success(); }
        else { $this->_return_json_error('No se pudo cambiar la visibilidad.'); }
    }

    /**
     * @param $id
     */
    public function update_file_description($id)
    {
        if ( $this->input->method() !== 'post' ) { $this->_response_error('Incorrect method.'); }
        if ( ! ($description = $this->input->post('description')) ) { $this->_response_error('No description found.'); }

        $this->load->model('project_file_model');

        $res = $this->project_file_model->update($id, array('description'=>$description));
        if ( $res ) { $this->_return_json_success(); }
        else { $this->_return_json_error('No se pudo cambiar la descripción.'); }
    }

    /**
     * @param int $file_id
     */
    public function delete_file($file_id)
    {
        if ( $this->input->method() !== 'post' ) { $this->_response_error('Incorrect method.'); }

        $this->load->model('project_file_model');
        $file = $this->project_file_model->get($file_id);

        if ( !$file ) { $this->_response_error(lang('not_found')); }

        if ( !$this->project_file_model->delete($file_id) )
        {
            $this->_response_error('No se pudo eliminar el archivo.');
        }
        unlink('./'.$file->path);

        $this->_response_success();
    }

    /**
     * @param int $file_id
     */
    public function edit_file($file_id)
    {
        if ( $this->input->method() !== 'post' ) { $this->_response_error('Incorrect method.'); }
        $this->load->model('project_file_model');


        if ( !$this->project_file_model->exist($file_id) )
        {
            $this->_response_error(lang('not_found'));
        }

        if ( !$this->project_file_model->update($file_id, $_POST) )
        {
            $this->_response_error(validation_errors());
        }

        $this->_response_success();
    }

    /**
     * @param $project_id
     */
    public function gantt($project_id)
    {
        echo json_encode($this->project_model->gantt_data($project_id));
    }

    public function gantt_client($project_id)
    {
        echo json_encode($this->project_model->gantt_client_data($project_id));
    }

    /**
     * @param $id
     */
    public function add_template($id)
    {
        $this->load->model('client_model');
        $this->load->model('company_model');
        $this->load->model('employee_model');
        $this->load->model('template_model');
        $this->load->model('project_category_model');

        $temp = $this->employee_model->all(array('where'=>array('employees.active'=>'true'),'group_by'=>'company_id'));
        $enabled_companies = array();
        foreach ($temp as $t) { $enabled_companies[$t->company_id] = TRUE; }

        $this->data = array(
            'styles'    => array(
                'public/plugins/bootstrap-select/css/bootstrap-select.min.css',
                'public/plugins/multi-select/css/multi-select.css',
                'public/plugins/jquery-select2/css/select2.min.css',
                'public/plugins/nestable/jquery-nestable.css',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',
                'public/plugins/jquery-datepicker/datepicker.css'
            ),
            'scripts'   => array(
                'public/plugins/multi-select/js/jquery.multi-select.js',
                'public/plugins/jquery-validation/jquery.validate.js',
                'public/plugins/jquery-validation/localization/messages_es.js',
                'public/plugins/jquery-steps/jquery.steps.js',
                'public/plugins/jquery-select2/js/select2.min.js',
                'public/plugins/nestable/jquery.nestable.js',
                'public/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
                'public/plugins/jquery-datepicker/datepicker.js',
                'public/js/src/add_template.js'
            ),
            //'clients'       => $this->client_model->all(array('select'=>'clients.id,clients.name','where'=>array('clients.active'=>'true'),'order_by'=>'clients.name ASC')),
            //'companies'     => $this->company_model->all(array('select'=>'companies.id,companies.name','where'=>array('companies.active'=>'true'),'order_by'=>'companies.name ASC')),
            //'employees'     => $this->employee_model->all(array('where'=>array('employees.active'=>'true'),'order_by'=>'full_name ASC')),
            //'enabled_companies' => $enabled_companies,
            'templates'     => $this->template_model->all(array('order_by'=>'title ASC')),
            'categories'    => $this->project_category_model->dropdown('id', 'name', array('order_by'=>'name ASC')),
            //'tags'          => isset($_POST['tags']) ? $_POST['tags'] : '',
            'project'       => $this->project_model->get($id)
        );

        if ( $this->input->method() === 'post' )
        {

            if ( $id = $this->{$this->_model}->create_from_template($this->input->post()) )
            {
                if ( !$this->input->is_ajax_request() )
                {
                    redirect($this->_controller);
                }
                else
                {
                    $this->_response_success('Listo');
                }
            }
            else
            {
                $this->_response_error(validation_errors());
            }
        }

        $this->_template("{$this->_controller}/add_template", $this->data);
    }

    /**
     * @param $id
     */
    public function add_template_save($id)
    {
        if ( $this->input->method() === 'post' )
        {

            if ( $id = $this->project_model->add_template($id, $this->input->post()) )
            {
                if ( !$this->input->is_ajax_request() )
                {
                    redirect($this->_controller);
                }
                else
                {
                    $this->_response_success('Listo');
                    //exit('Listo');
                }
            }
            else
            {
                $this->_response_error(validation_errors());
            }
        }
    }

    /**
     * @param $project_id
     * @param $template_id
     */
    public function get_structure($project_id, $template_id)
    {
        $this->load->model('template_model');

        echo json_encode(array(
            'former'    => $this->project_model->get_structure($project_id),
            'latter'    => $this->template_model->get_structure($template_id)
        ));
    }

    public function project_filter_list()
    {

        $this->_template("{$this->_controller}/project_filter_list", $this->_get_assets('list', $this->data));
    }

    public function project_filter_json($day,$month,$year='all',$status='all',$company='all',$category='all')
    {
        echo $this->project_model->projects_filter_json($day,get_number_month($month),$year,$status,$company,$category);
    }

    /**
     * @param $project_id
     */
    protected function _new_project_notifications($project_id)
    {
        $this->load->model('notification_model');
        $this->load->model('client_model');
        $this->load->model('push_subscription_model');

        $project = $this->project_model->get($project_id);
        $employees = $this->project_model->get_employees($project_id);

        //dump($project_id, TRUE);

        $notification = array(
            'message'   => 'Bienvenido al proyecto: '.$project->name,
            'actions'   => json_encode(array(array(
                'action'    => 'project/f_view/'.$project->id,
                'title'     => 'Ver Proyecto'
            )))
        );

        foreach ($employees as $employee_id)
        {
            $notification['user_id'] = $employee_id;
            $this->notification_model->insert($notification);
        }

        $clients = $this->client_model->get_contacts($project->client_id);

        $notification = array(
            'message'   => 'Bienvenido al proyecto: '.$project->name,
            'actions'   => json_encode(array(array(
                'action'    => 'project_client/view/'.$project->id,
                'title'     => 'Ver Proyecto'
            )))
        );

        foreach ($clients as $client)
        {
            if ( !$client->user_id ) { continue; }
            $notification['user_id'] = $client->user_id;
            $this->notification_model->insert($notification);
            $employees[] = $client->user_id;
        }

        $endpoints = array();

        foreach ($employees as $employee)
        {
            $endps = $this->push_subscription_model->get_by_user($employee);
            foreach ($endps as $endp) { $endpoints[] = $endp; }
        }

        if ( count($endpoints) === 0 ) { return; }

        $this->load->library('gcm');
        $this->gcm->setRecipients($endpoints);
        $this->gcm->setMessage('Nuevas notificaciones');
        $this->gcm->send();
    }

    /**
     * @param $project_id
     */
    protected function _close_project_notifications($project_id)
    {
        $this->load->model('notification_model');
        $this->load->model('client_model');
        $this->load->model('push_subscription_model');

        $project = $this->project_model->get($project_id);
        $employees = $this->project_model->get_employees($project_id);

        $notification = array(
            'message'   => 'Proyecto Finalizado: '.$project->name,
            'actions'   => json_encode(array(array(
                'action'    => 'project/f_view/'.$project->id,
                'title'     => 'Ver Proyecto'
            )))
        );

        foreach ($employees as $employee_id)
        {
            $notification['user_id'] = $employee_id;
            $this->notification_model->insert($notification);
        }

        $clients = $this->client_model->get_contacts($project->client_id);

        $notification = array(
            'message'   => 'Proyecto Finalizado: '.$project->name,
            'actions'   => json_encode(array(array(
                'action'    => 'project_client/view/'.$project->id,
                'title'     => 'Ver Proyecto'
            )))
        );

        foreach ($clients as $client)
        {
            if ( !$client->user_id ) { continue; }
            $notification['user_id'] = $client->user_id;
            $this->notification_model->insert($notification);
            $employees[] = $client->user_id;
        }

        $endpoints = array();

        foreach ($employees as $employee)
        {
            $endps = $this->push_subscription_model->get_by_user($employee);
            foreach ($endps as $endp) { $endpoints[] = $endp; }
        }

        if ( count($endpoints) === 0 ) { return; }

        $this->load->library('gcm');
        $this->gcm->setRecipients($endpoints);
        $this->gcm->setMessage('Nuevas notificaciones');
        $this->gcm->send();
    }

    /**
     * @param $project_id
     */
    protected function _cancel_project_notifications($project_id)
    {
        $this->load->model('notification_model');
        $this->load->model('client_model');
        $this->load->model('push_subscription_model');

        $project = $this->project_model->get($project_id);
        $employees = $this->project_model->get_employees($project_id);

        $notification = array(
            'message'   => 'Proyecto Anulado: '.$project->name
        );

        foreach ($employees as $employee_id)
        {
            $notification['user_id'] = $employee_id;
            $this->notification_model->insert($notification);
        }

        $clients = $this->client_model->get_contacts($project->client_id);

        foreach ($clients as $client)
        {
            if ( !$client->user_id ) { continue; }
            $notification['user_id'] = $client->user_id;
            $this->notification_model->insert($notification);
            $employees[] = $client->user_id;
        }

        $endpoints = array();
        foreach ($employees as $employee)
        {
            $endps = $this->push_subscription_model->get_by_user($employee);
            foreach ($endps as $endp) { $endpoints[] = $endp; }
        }

        if ( count($endpoints) === 0 ) { return; }

        $this->load->library('gcm');
        $this->gcm->setRecipients($endpoints);
        $this->gcm->setMessage('Nuevas notificaciones');
        $this->gcm->send();
    }

    /**
     * @param $comment_id
     * @return bool
     */
    protected function _new_comment_notifications($comment_id)
    {
        $this->load->model('notification_model');
        $this->load->model('client_model');
        $this->load->model('push_subscription_model');
        $this->load->model('project_comment_model');
        $this->load->model('user_model');

        $comment = $this->project_comment_model->get($comment_id);
        if ( !$comment ) { return FALSE; }

        $project = $this->project_model->get($comment->project_id);
        $employees = $this->project_model->get_employees($comment->project_id);
        $remmitent = $this->user_model->get($comment->user_id);

        //$this_user = $this->session->userdata('user_id');
        //if ( $this_user && !in_array($this_user, $employees) ) { $employees[] = $this_user; }

        $notification = array(
            'message'   => "{$remmitent->first_name} {$remmitent->last_name} ha escrito: \"{$comment->message}\" en el proyecto \"{$project->name}\""
        );

        foreach ($employees as $employee_id)
        {
            $notification['user_id'] = $employee_id;
            $this->notification_model->insert($notification);
        }

        if ( $comment->client_visibility === 'true' )
        {
            $clients = $this->client_model->get_contacts($project->client_id);

            foreach ($clients as $client)
            {
                if ( !$client->user_id ) { continue; }
                $notification['user_id'] = $client->user_id;
                $this->notification_model->insert($notification);
                $employees[] = $client->user_id;
            }
        }

        $endpoints = array();
        foreach ($employees as $employee)
        {
            $endps = $this->push_subscription_model->get_by_user($employee);
            foreach ($endps as $endp) { $endpoints[] = $endp; }
        }

        if ( count($endpoints) === 0 ) { return TRUE; }

        $this->load->library('gcm');
        $this->gcm->setRecipients($endpoints);
        $this->gcm->setMessage('Nuevas notificaciones');
        $this->gcm->send();

        return TRUE;
    }

    /**
     * @param $project_id
     */
    public function _new_project_emails($project_id)
    {
        $this->load->model('client_model');
        $this->load->model('user_model');
        $this->load->library('correo');

        $project = $this->project_model->get($project_id);
        $employees = $this->project_model->get_employees($project_id);
        $client = $this->client_model->get($project->client_id);
        $client_contacts = $this->client_model->get_contacts($project->client_id);

        $ids = implode(',', $employees);
        $employees = $this->user_model->find(array(
            'where'     => "users.id IN ({$ids})"
        ));
        $to = array();
        foreach ($employees as $employee) { $to[] = $employee->email; }
        $options = array(
            'to'        => implode(',', $to),
            //'cc'        => 'carlos.alvarez@dte-online.com,marco.antonio.diaz@dte-online.com',
            'subject'   => 'Nuevo Proyecto: '.$project->name,
            'content'   => 'project_new_employee',
            'data'      => array(
                'project'   => $project
            )
        );
        $this->correo->send($options);

        return;
        $to = array();
        foreach ($client_contacts as $client_contact) { $to[] = $client_contact->email; }
        $data = array(
            'to'        => implode(',', $to),
            'subject'   => 'Nuevo Proyecto Ergotec: '.$project->name,
            'content'   => 'project_new_client',
            'data'      => array(
                'project'   => $project,
                'client'    => $client
            )
        );
        $this->correo->send($data);

    }

    /**
     * @param $project_id
     */
    public function _close_project_emails($project_id)
    {
        $this->load->model('client_model');
        $this->load->model('user_model');
        $this->load->library('correo');

        $project = $this->project_model->get($project_id);
        $employees = $this->project_model->get_employees($project_id);
        $client = $this->client_model->get($project->client_id);
        $client_contacts = $this->client_model->get_contacts($project->client_id);

        $ids = implode(',', $employees);
        $employees = $this->user_model->find(array(
            'where'     => "users.id IN ({$ids})"
        ));
        $to = array();
        foreach ($employees as $employee) { $to[] = $employee->email; }
        $options = array(
            'to'        => implode(',', $to),
            //'cc'        => 'carlos.alvarez@dte-online.com,marco.antonio.diaz@dte-online.com',
            'subject'   => 'Finalización de Proyecto: '.$project->name,
            'content'   => 'project_close_employee',
            'data'      => array(
                'project'   => $project
            )
        );
        $this->correo->send($options);

        return;
        $to = array();
        foreach ($client_contacts as $client_contact) { $to[] = $client_contact->email; }
        $data = array(
            'to'        => implode(',', $to),
            'subject'   => 'Finalización de Proyecto Ergotec: '.$project->name,
            'content'   => 'project_close_client',
            'data'      => array(
                'project'   => $project,
                'client'    => $client
            )
        );
        $this->correo->send($data);

    }

    /**
     * @param $project_id
     */
    public function _cancel_project_emails($project_id)
    {
        $this->load->model('client_model');
        $this->load->model('user_model');
        $this->load->library('correo');

        $project = $this->project_model->get($project_id);
        $employees = $this->project_model->get_employees($project_id);
        $client = $this->client_model->get($project->client_id);
        $client_contacts = $this->client_model->get_contacts($project->client_id);

        $ids = implode(',', $employees);
        $employees = $this->user_model->find(array(
            'where'     => "users.id IN ({$ids})"
        ));
        $to = array();
        foreach ($employees as $employee) { $to[] = $employee->email; }
        $options = array(
            'to'        => implode(',', $to),
            //'cc'        => 'carlos.alvarez@dte-online.com,marco.antonio.diaz@dte-online.com',
            'subject'   => 'Proyecto Anulado: '.$project->name,
            'content'   => 'project_cancel_employee',
            'data'      => array(
                'project'   => $project
            )
        );
        $this->correo->send($options);

        return;
        $to = array();
        foreach ($client_contacts as $client_contact) { $to[] = $client_contact->email; }
        $data = array(
            'to'        => implode(',', $to),
            'subject'   => 'Proyecto Anulado Ergotec: '.$project->name,
            'content'   => 'project_cancel_client',
            'data'      => array(
                'project'   => $project,
                'client'    => $client
            )
        );
        $this->correo->send($data);

    }

    /**
     * @param $project_id
     */
    public function get_templates($project_id)
    {
        $templates = $this->project_model->get_templates($project_id);

        $this->_return_json_success('OK', $templates);
    }
}
