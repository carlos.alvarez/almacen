<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User: cristian.rodriguez
 * Date: 9/15/2017
 * Time: 4:40 PM
 */

/**
 * Class Chart
 * @property Stage_model $stage_model
 * @property Task_model $task_model
 * @property Project_brand_model $project_brand_model
 * @property Project_model $project_model
 */
class Chart extends MY_Controller
{
    /**
     * @var string
     */
    protected $_controller = 'chart';

    /**
     * @var bool
     */
    protected $_model = FALSE;

    public function create(){}
    public function edit($id){}
    public function view($id){}

    protected $_colors = array(
        '#000080',
        '#3cb44b',
        '#0082c8',
        '#e6194b',
        '#ffe119',
        '#f58231',
        '#911eb4',
        '#46f0f0',
        '#f032e6',
        '#d2f53c',
        '#fabebe',
        '#008080',
        '#e6beff',
        '#aa6e28',
        '#fffac8',
        '#800000',
        '#aaffc3',
        '#808000',
        '#ffd8b1'
    );

    /**
     * @param string $opacity
     *
     * @return string
     */
    protected function _generate_rgba($opacity = '0.8')
    {
        $r = rand(0, 250);
        $g = rand(2, 250) + 1;
        $b = rand(0, 250) + 4;

        if ($g > 250) { ($g - 2); }
        if ($b > 250) { ($b - 5); }

        return 'rgba(' . $r . ',' . $g . ',' . $b . ',' . $opacity . ')';
    }

    /**
     * @param int $id Project ID
     */
    public function stage_task($id)
    {
        $this->load->model('stage_model');
        $this->load->model('task_model');

        $data = array(
            'labels'          => array(),
            'data'            => array(),
            'backgroundColor' => array(),
        );

        $stages = $this->stage_model->find(array('where' => array('project_id' => $id)));

        // CREATING ARRAY DATA
        foreach ($stages as $i => $stage) {
            $number_of_task = count($this->task_model->find(array('where' => array('stage_id' => $stage->id))));
            $data['labels'][] = (strlen($stage->title) > 20 ) ? substr($stage->title,0,20).'...' : $stage->title;
            $data['labelskey'][$stage->id] = (strlen($stage->title) > 20 ) ? str_pad(substr($stage->title,0,20).'...',23,' ') : $stage->title;
            $data['backgroundColor'][] = $this->_colors[$i%count($this->_colors)];
            $data['data'][] = $number_of_task;//($number_of_task/$total)*100;
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param $id
     * @param $stage
     * @param string $month
     */
    public function stage_task_date($id, $stage, $month='')
    {
        $this->load->model('stage_model');
        $this->load->model('task_model');

        $id = (int)$id;
        $stage = str_replace('_', ' ', $stage);

        if ( $month !== '' ) {
            $data = array(
                'data'              => array_fill(0,31,0),
                'backgroundColor'   => array_fill(0,31,0),
            );
            $month = (int)$month;
            $stages = $this->stage_model->get_stage_task_date($id, $stage, $month);
            foreach ($stages as $stage_task) {
//                $tasks = $this->task_model->find(array('where' => array('stage_id' => $stage_task->id)));
//                foreach ($tasks as $task) {
                    $day = (int)date('d', strtotime($stage_task->start_date));
                    $data['data'][$day-1]++;
                    $data['backgroundColor'][$day-1] = 'rgba(0, 188, 212, 0.8)';
//                }
            }
        } else {
            $data = array(
                'data'              => array_fill(0,12,0),
                'backgroundColor'   => array_fill(0,12,0),
            );

            $stages = $this->stage_model->get_stage_task_date($id, $stage);
            foreach ($stages as $stage_task) {
//                $tasks = $this->task_model->find(array('where' => array('stage_id' => $stage_task->id)));
//                foreach ($tasks as $task) {
                    $month = (int)date('m', strtotime($stage_task->start_date));
                    $data['data'][$month-1]++;
                    $data['backgroundColor'][$month-1] = 'rgba(0, 188, 212, 0.8)';
//                }
            }
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param int $id Project ID
     */
    public function task_status($id)
    {
        $this->load->model('stage_model');
        $this->load->model('task_model');

        $status = array(
            'pendiente'  => 0,
            'proceso'  => 1,
            'cerrado' => 2,
            'anulado' => 3,
        );
        $data = array(
            'data'            => array(),
            'backgroundColor' => array(),
        );

        $stages = $this->stage_model->find(array('where' => array('project_id' => $id)));
        $total = 0;
        $statu = array(
            'pendiente'  => 0,
            'proceso'  => 0,
            'cerrado' => 0,
            'anulado' => 0
        );
        $color = array(
            'pendiente'  => 'rgba(31, 117, 254, 0.8)',
            'proceso'  => 'rgba(252, 232, 131, 0.8)',
            'cerrado' => 'rgba(28, 172, 120, 0.8)',
            'anulado' => 'rgba(238,32 ,77, 0.8)',
        );

        // GETTING DATA FOR
        foreach ($stages as $stage) {
            $total += count($this->task_model->find(array('where' => array('stage_id' => $stage->id))));
            foreach ($status as $name => $task) {
                $statu[$name] += count($this->task_model->find(array('where' => array('stage_id' => $stage->id, 'status' => $name))));
            }
        }
        // CALC ALL DATA FOR GETTING PORCENT FOR EACH STATUS
        foreach ($statu as $name => $val) {
//            $result = ($val / $total) * 100;
            $data['data'][] = $val;
            $data['labels'][] = str_pad(ucfirst($name),25);
            $data['labelskey'][$name] = str_pad(ucfirst($name),25);
            $data['backgroundColor'][] = $color[$name];
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param int $id
     * @param string $task_status
     */
    public function status_line_chart_month($id, $task_status)
    {
        $this->load->model('stage_model');
        $this->load->model('task_model');
        $task_status = strtolower($task_status);

        $data = array(
            'data'            => array_fill(0,12,0),
            'backgroundColor' => array_fill(0,12,0),
        );

        $stages = $this->stage_model->find(array('where' => array('project_id' => $id)));
        $color = array(
            'pendiente'  => 'rgba(31, 117, 254, 0.8)',
            'proceso'  => 'rgba(252, 232, 131, 0.8)',
            'cerrado' => 'rgba(28, 172, 120, 0.8)',
            'anulado' => 'rgba(238,32 ,77, 0.8)',
        );

        // GETTING DATA FOR
        foreach ($stages as $stage) {
            $tasks = $this->task_model->find(array('where' => array('stage_id' => $stage->id, 'status' => $task_status)));

            foreach ($tasks as $task){
                $month = (int)date('m', strtotime($task->start_date));
                $data['data'][$month-1]++;
                $data['backgroundColor'][$month-1] = $color[$task_status];
            }
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param $id
     * @param $task_status
     * @param $month
     */
    public function status_line_chart_day($id, $task_status, $month)
    {
        $this->load->model('stage_model');
        $this->load->model('task_model');
        $task_status = strtolower($task_status);

        $data = array(
            'data'            => array_fill(0,31,0),
            'backgroundColor' => array_fill(0,31,0),
        );

        $stages = $this->stage_model->find(array('where' => array('project_id' => $id)));
        $color = array(
            'pendiente'  => 'rgba(31, 117, 254, 0.8)',
            'proceso'  => 'rgba(252, 232, 131, 0.8)',
            'cerrado' => 'rgba(28, 172, 120, 0.8)',
            'anulado' => 'rgba(238,32 ,77, 0.8)',
        );

        // GETTING DATA FOR
        foreach ($stages as $stage) {
            $tasks = $this->task_model->find(array('where' => array('stage_id' => $stage->id, 'status' => $task_status, 'MONTH(start_date)' => $month)));

            foreach ($tasks as $task){
                $day = (int)date('d', strtotime($task->start_date));
                $data['data'][$day-1]++;
                $data['backgroundColor'][$day-1] = $color[$task_status];
            }
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param int $id Project ID
     */
    public function estimated_budget($id)
    {
        $this->load->model('project_brand_model');
        $data = array(
            'labels'          => array(),
            'data'            => array(),
            'backgroundColor' => array(),
        );

        $budgets = $this->project_brand_model->get_amounts($id);
        if ($budgets) {
            foreach ($budgets as $i => $budget) {
                $data['labels'][] = (strlen($budget->brands_name) > 20 ) ? substr($budget->brands_name,0,20).'...' : str_pad($budget->brands_name,25);
                $data['labelskey'][$budget->brand_id] = (strlen($budget->brands_name) > 20 ) ? substr($budget->brands_name,0,20).'...' : str_pad($budget->brands_name,25);
                $data['data'][] = $budget->estimated_amount;
                $data['backgroundColor'][] = $this->_colors[$i%count($this->_colors)];
            }
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param $id
     * @param $brand
     * @param string $month
     */
    public function estimated_budget_date($id, $brand, $month='')
    {
        $this->load->model('project_brand_model');
        $brand = (string)strtolower($brand);
        $id = (int)$id;

        if ( $month !== '' ){
            $data = array(
                'data'              => array_fill(0,31,0),
                'backgroundColor'   => array_fill(0,31,0),
            );
            $month = (int)$month;
            $budgets = $this->project_brand_model->find(array('where' => array('projects_brands.project_id' => $id, 'brands.id' => $brand, 'MONTH(estimated_date)' => $month)));
            if ( $budgets ) {
                foreach ($budgets as $budget) {
                    $day = (int)date('d', strtotime($budget->estimated_date));
                    $data['data'][$day-1] += $budget->estimated_amount;
                    $data['backgroundColor'][$day-1] = 'rgba(221, 161, 152, 0.8)';
                }
            }
        } else {
            $data = array(
                'data'              => array_fill(0,12,0),
                'backgroundColor'   => array_fill(0,12,0),
            );
            $budgets = $this->project_brand_model->find(array('where' => array('projects_brands.project_id' => $id, 'brands.id' => $brand)));
            if ($budgets) {
                foreach ($budgets as $budget) {
                    $month = (int)date('m', strtotime($budget->estimated_date));
                    $data['data'][$month-1] += $budget->estimated_amount;
                    $data['backgroundColor'][$month-1] = 'rgba(221, 161, 152, 0.8)';
                }
            }
        }
        $this->_return_json_success('OK', $data);
    }

    /**
     * @param int $id Project ID
     */
    public function actual_budget($id)
    {
        $this->load->model('project_brand_model');
        $data = array(
            'labels'          => array(),
            'data'            => array(),
            'backgroundColor' => array(),
        );

        $budgets = $this->project_brand_model->get_amounts($id);
        if ($budgets) {
            foreach ($budgets as $i => $brand) {
                $data['labels'][] = (strlen($brand->brands_name) > 20 ) ? substr($brand->brands_name,0,20).'...' : str_pad($brand->brands_name,23);;
                $data['labelskey'][$brand->brand_id] = (strlen($brand->brands_name) > 20 ) ? substr($brand->brands_name,0,20).'...' : str_pad($brand->brands_name,23);
                $data['data'][] = $brand->actual_amount;
                $data['backgroundColor'][] = $this->_colors[$i%count($this->_colors)];
            }
        }
        $this->_return_json_success('OK', $data);
    }

    /**
     * @param $id
     * @param $brand
     * @param string $month
     */
    public function actual_budget_date($id, $brand, $month='')
    {
        $this->load->model('project_brand_model');
        $brand = (string)strtolower($brand);
        $id = (int)$id;

        if ( $month !== '' ){
            $data = array(
                'data'              => array_fill(0,31,0),
                'backgroundColor'   => array_fill(0,31,0)
            );
            $month = (int)$month;
            $budgets = $this->project_brand_model->find(array('where' => array('projects_brands.project_id' => $id, 'brands.id' => $brand, 'MONTH(actual_date)' => $month)));
            if ( $budgets ) {
                foreach ($budgets as $budget) {
                    $day = (int)date('d', strtotime($budget->actual_date));
                    $data['data'][$day-1] += $budget->actual_amount;
                    $data['backgroundColor'][$day-1] = 'rgba(93, 78, 252, 0.8)';
                }
            }
        } else {
            $data = array(
                'data'              => array_fill(0,12,0),
                'backgroundColor'   => array_fill(0,12,0),
            );
            $budgets = $this->project_brand_model->find(array('where' => array('projects_brands.project_id' => $id, 'brands.id' => $brand)));
            if ( $budgets )
            {
                foreach ($budgets as $budget) {
                    $month = (int)date('m', strtotime($budget->actual_date));
                    $data['data'][$month-1] += $budget->actual_amount;
                    $data['backgroundColor'][$month-1] = 'rgba(93, 78, 252, 0.8)';
                }
            }
        }
        $this->_return_json_success('OK', $data);
    }

    /**
     * @param int $id
     * @param $year
     * @param int|string $brand_id Budget ID
     */
    public function sales_funnel($id, $year, $brand_id = 0)
    {
        $this->load->model('project_brand_model');
        $id = (int)$id;
        $brand_id = (int)$brand_id;
        $year = (int)$year;
        $data = array(
            array(
                'label'                => array(lang('estimated_amount')),
                'data'                 => array_fill(0, 12, 0),
                'borderColor'          => 'rgba(221, 161, 152, 0.8)',
                'pointBackgroundColor' => 'rgba(221, 161, 152, 0.8)',
                'backgroundColor'      => 'rgba(221, 161, 152, 0)',
                'pointBorderColor'     => 'rgba(221, 161, 152, 0.8)',
                'pointBorderWidth'     => 2,
                'fill'                 => FALSE,
            ),
            array(
                'label'                => array(lang('actual_amount')),
                'data'                 => array_fill(0, 12, 0),
                'borderColor'          => 'rgba(93, 78, 252, 0.8)',
                'pointBackgroundColor' => 'rgba(93, 78, 252, 0.8)',
                'backgroundColor'      => 'rgba(93, 78, 252, 0)',
                'pointBorderColor'     => 'rgba(0, 188, 212, 0.8)',
                'pointBorderWidth'     => 2,
                'fill'                 => FALSE,
            ),
        );
        if ($brand_id <= 0) {
            $estimateds = $this->project_brand_model->find(array('where' => array('YEAR(estimated_date)' => $year, 'project_id' => $id)));
            if ($estimateds) {
                foreach ($estimateds as $estimated) {
                    $month = (int)date('m', strtotime($estimated->estimated_date));
                    $data[0]['data'][$month - 1] = $data[0]['data'][$month - 1] + $estimated->estimated_amount;
                }
            }

            $actuals = $this->project_brand_model->find(array('where' => array('YEAR(actual_date)' => $year, 'project_id' => $id)));
            if ($actuals) {
                foreach ($actuals as $actual) {
                    $month = (int)date('m', strtotime($actual->actual_date));
                    $data[1]['data'][$month - 1] = $data[1]['data'][$month - 1] + $actual->actual_amount;
                }
            }
        } else {
            $estimateds = $this->project_brand_model->find(array('where' => array('YEAR(estimated_date)' => $year, 'project_id' => $id, 'brand_id' => $brand_id)));
            if ($estimateds) {
                foreach ($estimateds as $estimated) {
                    $month = (int)date('m', strtotime($estimated->estimated_date));
                    $data[0]['data'][$month - 1] = $data[0]['data'][$month - 1] + $estimated->estimated_amount;
                }
            }

            $actuals = $this->project_brand_model->find(array('where' => array('YEAR(actual_date)' => $year, 'project_id' => $id, 'brand_id' => $brand_id)));
            if ($actuals) {
                foreach ($actuals as $actual) {
                    $month = (int)date('m', strtotime($actual->actual_date));
                    $data[1]['data'][$month - 1] = $data[1]['data'][$month - 1] + $actual->actual_amount;
                }
            }
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param $id
     * @param $year
     * @param $month
     * @param int $brand_id
     */
    public function get_sales_funnel_by_date($id, $year, $month, $brand_id = 0)
    {
        $this->load->model('project_brand_model');
        $id = (int)$id;
        $year = (int)$year;
        $month = (int)$month;
        $data = array(
            array(
                'label'                => array(lang('estimated_amount')),
                'data'                 => array_fill(0, 31, 0),
                'borderColor'          => 'rgba(221, 161, 152, 0.8)',
                'pointBackgroundColor' => 'rgba(221, 161, 152, 0.8)',
                'backgroundColor'      => 'rgba(221, 161, 152, 0)',
                'pointBorderColor'     => 'rgba(221, 161, 152, 0.8)',
                'pointBorderWidth'     => 2,
                'fill'                 => FALSE,
            ),
            array(
                'label'                => array(lang('actual_amount')),
                'data'                 => array_fill(0, 31, 0),
                'borderColor'          => 'rgba(93, 78, 252, 0.8)',
                'pointBackgroundColor' => 'rgba(93, 78, 252, 0.8)',
                'backgroundColor'      => 'rgba(93, 78, 252, 0)',
                'pointBorderColor'     => 'rgba(0, 188, 212, 0.8)',
                'pointBorderWidth'     => 2,
                'fill'                 => FALSE,
            ),
        );

        if ($brand_id <= 0) {
            $estimateds = $this->project_brand_model->find(array('where' => array('YEAR(estimated_date)' => $year, 'MONTH(estimated_date)' => $month, 'project_id' => $id)));
            if ($estimateds) {
                foreach ($estimateds as $estimated) {
                    $day = (int)date('d', strtotime($estimated->estimated_date));
                    $data[0]['data'][$day - 1] = $data[0]['data'][$day - 1] + $estimated->estimated_amount;
                }
            }

            $actuals = $this->project_brand_model->find(array('where' => array('YEAR(actual_date)' => $year, 'MONTH(actual_date)' => $month, 'project_id' => $id)));
            if ($actuals) {
                foreach ($actuals as $actual) {
                    $day = (int)date('d', strtotime($actual->actual_date));
                    $data[1]['data'][$day - 1] = $data[1]['data'][$day - 1] + $actual->actual_amount;
                }
            }
        } else {
            $estimateds = $this->project_brand_model->find(array('where' => array('YEAR(estimated_date)' => $year, 'MONTH(estimated_date)' => $month, 'project_id' => $id, 'brand_id' => $brand_id)));
            if ($estimateds) {
                foreach ($estimateds as $estimated) {
                    $day = (int)date('d', strtotime($estimated->estimated_date));
                    $data[0]['data'][$day - 1] = $data[0]['data'][$day - 1] + $estimated->estimated_amount;
                }
            }

            $actuals = $this->project_brand_model->find(array('where' => array('YEAR(actual_date)' => $year, 'MONTH(actual_date)' => $month, 'project_id' => $id, 'brand_id' => $brand_id)));
            if ($actuals) {
                foreach ($actuals as $actual) {
                    $day = (int)date('d', strtotime($actual->actual_date));
                    $data[1]['data'][$day - 1] = $data[1]['data'][$day - 1] + $actual->actual_amount;
                }
            }
        }
        $this->_return_json_success('OK', $data);
    }

    /**
     * @param string $year
     * @param string $status
     * @param string|int $company
     * @param string|int $category
     * @param string $my_projects
     * @param string $delayed
     */
    public function status_projects($year='all', $status='all', $company='all', $category='all', $my_projects = 'false', $delayed='false')
    {
        $this->load->model('project_model');
        $data = array(
            'label'           => array(),
            'data'            => array(),
            'backgroundColor' => array(),
        );
        $color = array(
            'progreso'  => 'rgba(31, 117, 254, 0.8)',
            'finalizado' => 'rgba(28, 172, 120, 0.8)',
            'anulado'    => 'rgba(238,32 ,77, 0.8)',
        );
        $projects = NULL;

        if ( $my_projects === 'false' ) {
            $projects = $this->project_model->status_data($year,$status,$company,$category,FALSE, $delayed);
        } else {
            $projects = $this->project_model->status_data($year,$status,$company,$category,$this->session->userdata('user_id'), $delayed);
        }
        foreach ($projects as $id => $project) {
            $data['data'][] = $project->total_status;
            $data['label'][] = ucfirst($project->status);
            $data['backgroundColor'][] = $color[$project->status];
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param string $year
     * @param string $status
     * @param string $company
     * @param string $category
     * @param string $month
     * @param string $my_projects
     * @param string $delayed
     */
    public function status_projects_bar_chart($year='all', $status='all', $company='all', $category='all', $month='all', $my_projects = 'false', $delayed='false')
    {
        $this->load->model('project_model');
        $projects = NULL;
        $status = strtolower($status);

        if ( $my_projects === 'false' )
        {
            $projects = $this->project_model->status_data_bar_chart($year,$status,$company,$category,$month,FALSE, $delayed);
        }
        else
        {
            $projects = $this->project_model->status_data_bar_chart($year,$status,$company,$category,$month,$this->session->userdata('user_id'), $delayed);
        }

        if ($month === 'all')
        {
            $data = array(
                'data'              => array_fill(0,12,0),
                'backgroundColor'   => array_fill(0,12,0)
            );

            foreach($projects as $project)
            {
                $my_month = (int)date('m', strtotime($project->start_date));
                $data['data'][$my_month-1]++;
                $data['backgroundColor'][$my_month-1] = 'rgba(221, 161, 152, 0.8)';
            }
        }
        else
        {
            $data = array(
                'data'              => array_fill(0,31,0),
                'backgroundColor'   => array_fill(0,31,0)
            );

            foreach($projects as $project)
            {
                $day = (int)date('d', strtotime($project->start_date));
                $data['data'][$day-1]++;
                $data['backgroundColor'][$day-1] = 'rgba(221, 161, 152, 0.8)';
            }
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param string $year
     * @param string $status
     * @param string|int $company
     * @param string|int $category
     * @param string $my_projects
     */
    public function categories_projects($year='all', $status='all', $company='all', $category='all', $my_projects = 'false', $delayed='false')
    {
        $this->load->model('project_model');
        $data = array(
            'labels'          => array(),
            'data'            => array(),
            'backgroundColor' => array(),
        );
        $categories = NULL;

        if ( $my_projects === 'false' ) {
            $categories = $this->project_model->categories_data($year,$status,$company,$category,FALSE, $delayed);
        } else {
            $categories = $this->project_model->categories_data($year,$status,$company,$category,$this->session->userdata('user_id'), $delayed);
        }
        foreach ($categories as $i => $category) {
            $data['labels'][] = $category->category_name;
            $data['data'][] = $category->total_category;
            $data['backgroundColor'][] = $this->_colors[$i%count($this->_colors)];
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param string $year
     * @param string $status
     * @param string $company
     * @param string $category
     * @param string $month
     * @param string $my_projects
     */
    public function categories_projects_bar_chart($year='all', $status='all', $company='all', $category='all', $month='all', $my_projects = 'false', $delayed='false')
    {
        $this->load->model('project_model');
        $projects = NULL;
//        $month = ($month === '') ? 'all' : (int)$month;
        $category = strtolower($category);

        if ( $my_projects === 'false' )
        {
            $projects = $this->project_model->categories_data_bar_chart($year,$status,$company,$category,$month,FALSE, $delayed);
        }
        else
        {
            $projects = $this->project_model->categories_data_bar_chart($year,$status,$company,$category,$month,$this->session->userdata('user_id'), $delayed);
        }

        if ( $month === 'all' )
        {
            $data = array (
                'data'              =>  array_fill(0,12,0),
                'backgroundColor'   =>  array_fill(0,12,0)
            );

            foreach ($projects as $project)
            {
                $my_month = (int)date('m', strtotime($project->start_date));
                $data['data'][$my_month-1]++;
                $data['backgroundColor'][$my_month-1] = 'rgba(221, 161, 152, 0.8)';
            }
        }
        else
        {
            $data = array(
                'data'              => array_fill(0,31,0),
                'backgroundColor'   =>  array_fill(0,31,0)
            );

            foreach ($projects as $project)
            {
                $day = (int)date('d', strtotime($project->start_date));
                $data['data'][$day-1]++;
                $data['backgroundColor'][$day-1] = 'rgba(221, 161, 152, 0.8)';
            }
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param string $year
     * @param string $status
     * @param string|int $company
     * @param string|int $category
     * @param string $my_projects
     */
    public function companies_projects($year='all', $status='all', $company='all', $category='all', $my_projects = 'false', $delayed='false')
    {
        $this->load->model('project_model');
        $data = array(
            'label'             => array(),
            'data'              => array(),
            'backgroundColor'   => array(),
            'porcent'           => array(),
        );
        $companies = NULL;

        if ( $my_projects === 'false' ) {
            $companies = $this->project_model->companies_data($year,$status,$company,$category,FALSE, $delayed);
        } else {
            $companies = $this->project_model->companies_data($year,$status,$company,$category,$this->session->userdata('user_id'), $delayed);
        }
        foreach($companies as $i=>$company_project){
            $data['data'][] = $company_project->total_company;
            $data['label'][] = $company_project->company_name;
            $data['backgroundColor'][] = $this->_colors[$i%count($this->_colors)];
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param string $year
     * @param string $status
     * @param string $company
     * @param string $category
     * @param string $month
     * @param string $my_projects
     */
    public function companies_projects_bar_chart($year='all', $status='all', $company='all', $category='all', $month='all', $my_projects = 'false', $delayed='false')
    {
        $this->load->model('project_model');
        $companies = NULL;

        if ( $my_projects === 'false' )
        {
            $companies = $this->project_model->companies_data_bar_chart($year,$status,$company,$category,$month,FALSE, $delayed);
        }
        else
        {
            $companies = $this->project_model->companies_data_bar_chart($year,$status,$company,$category,$month,$this->session->userdata('user_id'), $delayed);
        }

        if ( $month === 'all')
        {
            $data = array (
                'data'              =>  array_fill(0,12,0),
                'backgroundColor'   =>  array_fill(0,12,0)
            );

            foreach ($companies as $company)
            {
                $my_month = (int)date('m', strtotime($company->start_date));
                $data['data'][$my_month-1]++;
                $data['backgroundColor'][$my_month-1] = 'rgba(221, 161, 152, 0.8)';
            }
        }
        else
        {
            $data = array(
                'data'              => array_fill(0,31,0),
                'backgroundColor'   =>  array_fill(0,31,0)
            );

            foreach ($companies as $company)
            {
                $day = (int)date('d', strtotime($company->start_date));
                $data['data'][$day-1]++;
                $data['backgroundColor'][$day-1] = 'rgba(221, 161, 152, 0.8)';
            }
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param string $year
     * @param string $status
     * @param string|int $company
     * @param string|int $category
     * @param string|int $brand
     * @param string $my_projects
     */
    public function budget_projects($year='all', $status='all', $company='all', $category='all', $brand='all', $my_projects='false', $delayed='false')
    {
        $this->load->model('project_brand_model');
        $data = array(
            array(
                'label'                => array(lang('estimated_amount')),
                'data'                 => array_fill(0, 12, 0),
                'borderColor'          => 'rgba(221, 161, 152, 0.8)',
                'pointBackgroundColor' => 'rgba(221, 161, 152, 0.8)',
                'backgroundColor'      => 'rgba(221, 161, 152, 0)',
                'pointBorderColor'     => 'rgba(221, 161, 152, 0.8)',
                'pointBorderWidth'     => 2,
                'fill'                 => FALSE,
            ),
            array(
                'label'                => array(lang('actual_amount')),
                'data'                 => array_fill(0, 12, 0),
                'borderColor'          => 'rgba(93, 78, 252, 0.8)',
                'pointBackgroundColor' => 'rgba(93, 78, 252, 0.8)',
                'backgroundColor'      => 'rgba(93, 78, 252, 0)',
                'pointBorderColor'     => 'rgba(0, 188, 212, 0.8)',
                'pointBorderWidth'     => 2,
                'fill'                 => FALSE,
            ),
        );

        if ( $my_projects === 'false' ) {
            $budgets = $this->project_brand_model->budget_data($year,'all',$status,$company,$category,$brand,FALSE, $delayed);
        } else {

            $budgets = $this->project_brand_model->budget_data($year,'all',$status,$company,$category,$brand,$this->session->userdata('user_id'), $delayed);
        }

        if ($budgets) {
            foreach ($budgets as $estimated) {
                $month = (int)date('m', strtotime($estimated->estimated_date));
                $data[0]['data'][$month - 1] = $data[0]['data'][$month - 1] + $estimated->estimated_amount;
            }

            foreach ($budgets as $actual) {
                $month = (int)date('m', strtotime($actual->actual_date));
                $data[1]['data'][$month - 1] = $data[1]['data'][$month - 1] + $actual->actual_amount;
            }
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param $year
     * @param $month
     * @param string $status
     * @param string $company
     * @param string $category
     * @param string $brand
     * @param bool|string $my_projects
     */
    public function get_budget_projects_by_day($year, $month, $status='all', $company='all', $category='all', $brand='all', $my_projects='false', $delayed='false')
    {
        $this->load->model('project_brand_model');
        $data = array(
            array(
                'label'                => array(lang('estimated_amount')),
                'data'                 => array_fill(0, 31, 0),
                'borderColor'          => 'rgba(221, 161, 152, 0.8)',
                'pointBackgroundColor' => 'rgba(221, 161, 152, 0.8)',
                'backgroundColor'      => 'rgba(221, 161, 152, 0)',
                'pointBorderColor'     => 'rgba(221, 161, 152, 0.8)',
                'pointBorderWidth'     => 2,
                'fill'                 => FALSE,
            ),
            array(
                'label'                => array(lang('actual_amount')),
                'data'                 => array_fill(0, 31, 0),
                'borderColor'          => 'rgba(93, 78, 252, 0.8)',
                'pointBackgroundColor' => 'rgba(93, 78, 252, 0.8)',
                'backgroundColor'      => 'rgba(93, 78, 252, 0)',
                'pointBorderColor'     => 'rgba(0, 188, 212, 0.8)',
                'pointBorderWidth'     => 2,
                'fill'                 => FALSE,
            ),
        );

        if ( $my_projects === 'false' ) {
            $budgets = $this->project_brand_model->budget_data($year, $month, $status,$company,$category,$brand,FALSE, $delayed);
        } else {

            $budgets = $this->project_brand_model->budget_data($year, $month, $status,$company,$category,$brand,$this->session->userdata('user_id'), $delayed);
        }

        if ($budgets) {
            foreach ($budgets as $estimated) {
                $day = (int)date('d', strtotime($estimated->estimated_date));
                $data[0]['data'][$day - 1] = $data[0]['data'][$day - 1] + $estimated->estimated_amount;
            }

            foreach ($budgets as $actual) {
                $day = (int)date('d', strtotime($actual->actual_date));
                $data[1]['data'][$day - 1] = $data[1]['data'][$day - 1] + $actual->actual_amount;
            }
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param string $year
     * @param string $status
     * @param string $company
     * @param string $category
     * @param string $stage_status
     * @param string $my_projects
     * @param string $delayed
     */
    public function get_internal_status_pie_chart($year='all', $status='all', $company='all', $category='all', $stage_status='all', $my_projects = 'false', $delayed='false')
    {
        $this->load->model('project_model');
        $projects = NULL;
        $data = array(
            'labels'             => array(),
            'data'              => array(),
            'backgroundColor'   => array(),
        );
        $status = strtolower($status);

        if ( $my_projects === 'false' )
        {
            $projects = $this->project_model->projects_by_stage_status_pie_chart($year,$status,$company,$category,$stage_status,FALSE, $delayed);
        }
        else
        {
            $projects = $this->project_model->projects_by_stage_status_pie_chart($year,$status,$company,$category,$stage_status,$this->session->userdata('user_id'), $delayed);
        }

        if ($projects)
        {
            foreach($projects as $i=> $project)
            {
                $data['data'][] = $project->total_projects;
                $data['labels'][] = (strlen($project->stage_status) > 20 ) ? substr($project->stage_status,0,20).'...' : $project->stage_status;
                $data['labelskey'][$project->stage_status_id] = (strlen($project->stage_status) > 20 ) ? substr($project->stage_status,0,20).'...' : str_pad($project->stage_status,25);
                $data['backgroundColor'][] = $this->_colors[$i%count($this->_colors)];
            }
        }

        $this->_return_json_success('OK', $data);
    }

    /**
     * @param string $year
     * @param string $status
     * @param string $company
     * @param string $category
     * @param string $stage_status
     * @param string $my_projects
     * @param string $delayed
     */
    public function get_reason_pie_chart($year='all', $status='all', $company='all', $category='all', $stage_status='all', $my_projects = 'false', $delayed='false')
    {
        $this->load->model('project_model');
        $projects = NULL;
        $data = array(
            'labels'             => array(),
            'data'              => array(),
            'backgroundColor'   => array(),
        );
        $status = strtolower($status);

        if ( $my_projects === 'false' )
        {
            $projects = $this->project_model->projects_by_reasons_pie_chart($year,$status,$company,$category,$stage_status,FALSE, $delayed);
        }
        else
        {
            $projects = $this->project_model->projects_by_reasons_pie_chart($year,$status,$company,$category,$stage_status,$this->session->userdata('user_id'), $delayed);
        }

        if ($projects)
        {
            foreach($projects as $i=> $project)
            {
                $data['data'][] = $project->total_projects;
                $data['labels'][] = (strlen($project->reason) > 20 ) ? substr($project->reason,0,20).'...' : $project->reason;
                $data['labelskey'][$project->reason_id] = (strlen($project->reason) > 20 ) ? substr($project->reason,0,20).'...' : str_pad($project->reason,25);
                $data['backgroundColor'][] = $this->_colors[$i%count($this->_colors)];
            }
        }

        $this->_return_json_success('OK', $data);
    }
}