<div class="card">
    <div class="header">
        <h2><i class="material-icons">edit</i><?= lang('method_create') ?> <?= lang('Calendar_lib') ?></h2>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="role-new-form" class="form-validate" method="post">
            <div class="row">
                <div class="col-sm-4 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="name" value="<?= set_value('name') ?>" required minlength="4" maxlength="100" />
                            <label class="form-label"><?= lang('name') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="description" value="<?= set_value('description') ?>" maxlength="250" />
                            <label class="form-label"><?= lang('description') ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 m-b-0 align-right">
                    <div class="switch">
                        <input type="hidden" name="active" value="false">
                        <label><?= lang('active') ?> <input type="checkbox" name="active" value="true" <?= set_checkbox('active', 'true', TRUE) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="number" class="form-control" name="working_hours" value="<?= set_value('working_hours', 8) ?>" required min="1" max="16" />
                            <label class="form-label"><?= lang('working_hours') ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <h5 class="card-title right hidden-xs"><?= lang('working_days') ?></h5>
                    <h5 class="card-title visible-xs"><?= lang('working_days') ?></h5>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_sunday" value="false">
                        <label><?= lang('sunday') ?> <input type="checkbox" name="wd_sunday" value="true" <?= set_checkbox('wd_sunday', 'true', FALSE) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_monday" value="false">
                        <label><?= lang('monday') ?> <input type="checkbox" name="wd_monday" value="true" <?= set_checkbox('wd_monday', 'true', TRUE) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_tuesday" value="false">
                        <label><?= lang('tuesday') ?> <input type="checkbox" name="wd_tuesday" value="true" <?= set_checkbox('wd_tuesday', 'true', TRUE) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_wednesday" value="false">
                        <label><?= lang('wednesday') ?> <input type="checkbox" name="wd_wednesday" value="true" <?= set_checkbox('wd_wednesday', 'true', TRUE) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_thursday" value="false">
                        <label><?= lang('thursday') ?> <input type="checkbox" name="wd_thursday" value="true" <?= set_checkbox('wd_thursday', 'true', TRUE) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_friday" value="false">
                        <label><?= lang('friday') ?> <input type="checkbox" name="wd_friday" value="true" <?= set_checkbox('wd_friday', 'true', TRUE) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_saturday" value="false">
                        <label><?= lang('saturday') ?> <input type="checkbox" name="wd_saturday" value="true" <?= set_checkbox('wd_saturday', 'true', FALSE) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
