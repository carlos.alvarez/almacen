<div class="card">
    <div class="header">
        <h2><i class="material-icons">edit</i><?= lang('method_edit') ?> <?= lang('Calendar_lib') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('calendar/view/').$calendar->id ?>" class="btn btn-info btn-xs <?= grant_show('Calendar_lib', 'view') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_view') ?>">
                    <i class="material-icons col-white">search</i>
                </a>
            </li>
        </ul>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="role-edit-form" class="form-validate" method="post">
            <div class="row">
                <div class="col-sm-4 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="name" value="<?= set_value('name', $calendar->name) ?>" required minlength="4" maxlength="100" />
                            <label class="form-label"><?= lang('name') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="description" value="<?= set_value('description', $calendar->description) ?>" maxlength="250" />
                            <label class="form-label"><?= lang('description') ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 m-b-0 align-right">
                    <div class="switch">
                        <input type="hidden" name="active" value="false">
                        <label><?= lang('active') ?> <input type="checkbox" name="active" value="true" <?= set_checkbox('active', 'true', ($calendar->active === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="number" class="form-control" name="working_hours" value="<?= set_value('working_hours', $calendar->working_hours) ?>" required min="1" max="16" />
                            <label class="form-label"><?= lang('working_hours') ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <h5 class="card-title right hidden-xs"><?= lang('working_days') ?></h5>
                    <h5 class="card-title visible-xs"><?= lang('working_days') ?></h5>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_sunday" value="false">
                        <label><?= lang('sunday') ?> <input type="checkbox" name="wd_sunday" value="true" <?= set_checkbox('wd_sunday', 'true', ($calendar->wd_sunday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_monday" value="false">
                        <label><?= lang('monday') ?> <input type="checkbox" name="wd_monday" value="true" <?= set_checkbox('wd_monday', 'true', ($calendar->wd_monday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_tuesday" value="false">
                        <label><?= lang('tuesday') ?> <input type="checkbox" name="wd_tuesday" value="true" <?= set_checkbox('wd_tuesday', 'true', ($calendar->wd_tuesday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_wednesday" value="false">
                        <label><?= lang('wednesday') ?> <input type="checkbox" name="wd_wednesday" value="true" <?= set_checkbox('wd_wednesday', 'true', ($calendar->wd_wednesday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_thursday" value="false">
                        <label><?= lang('thursday') ?> <input type="checkbox" name="wd_thursday" value="true" <?= set_checkbox('wd_thursday', 'true', ($calendar->wd_thursday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_friday" value="false">
                        <label><?= lang('friday') ?> <input type="checkbox" name="wd_friday" value="true" <?= set_checkbox('wd_friday', 'true', ($calendar->wd_friday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_saturday" value="false">
                        <label><?= lang('saturday') ?> <input type="checkbox" name="wd_saturday" value="true" <?= set_checkbox('wd_saturday', 'true', ($calendar->wd_saturday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="header">
        <h2><?= lang('holidays') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="javascript:void(0);" data-toggle="modal" data-target="#add-holiday-modal" class="btn btn-info btn-xs <?= grant_show('calendar', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_create') ?> <?= lang('holiday') ?>">
                    <i class="material-icons col-white">add</i>
                </a>
            </li>
        </ul>
    </div>
    <div class="body">
        <div class="row">
            <div class="col-sm-2 m-b-0">
                <div class="form-group form-group-sm form-float">
                    <div class="form-line focused">
                        <input type="number" class="form-control" id="year" value="<?= date('Y') ?>" required min="2015" max="2040" />
                        <label class="form-label"><?= lang('year') ?></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table id="holidays-table" data-calendar-id="<?= $calendar->id ?>" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="">
                            <label class="m-b-0" for="check-all"></label>
                        </th>
                        <th class="all"><?= lang('date') ?></th>
                        <th class="all"><?= lang('name') ?></th>
                        <th class="all"><?= lang('description') ?></th>
                        <th><?= lang('options') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-holiday-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Agregar Día Feriado</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="add-holiday-form" class="form-validate" onsubmit="return false;">
                    <input type="hidden" name="calendar_id" value="<?= $calendar->id ?>">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="date" class="form-control" name="date" value="" required />
                                    <label class="form-label">Fecha *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="name" value="" required />
                                    <label class="form-label">Nombre *</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <textarea name="description" rows="2" class="form-control"></textarea>
                                    <label class="form-label">Descripción</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" id="save-new-holiday" >Guardar</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-holiday-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Editar Día Feriado</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="edit-holiday-form" class="form-validate" onsubmit="return false;">
                    <input type="hidden" name="id" value="" required>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="date" class="form-control" name="date" value="" required />
                                    <label class="form-label">Fecha *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="name" value="" required />
                                    <label class="form-label">Nombre *</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <textarea name="description" rows="2" class="form-control"></textarea>
                                    <label class="form-label">Descripción</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" id="save-edit-holiday" >Guardar</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

