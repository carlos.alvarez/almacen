<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('Calendar_lib') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('calendar/edit/').$calendar->id ?>" class="btn btn-info btn-xs <?= grant_show('Calendar_lib', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="role-edit-form" class="form-validate" method="post">
            <div class="row">
                <div class="col-sm-4 m-b-0">
                    <small><?= lang('name') ?></small>
                    <h5><?= $calendar->name ?></h5>
                </div>
                <div class="col-sm-6 m-b-0">
                    <small><?= lang('description') ?></small>
                    <h5><?= $calendar->description ?></h5>
                </div>
                <div class="col-sm-2 m-b-0 align-right">
                    <div class="switch">
                        <input type="hidden" name="active" value="false">
                        <label><?= lang('active') ?> <input type="checkbox" name="active" disabled value="true" <?= set_checkbox('active', 'true', ($calendar->active === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <small><?= lang('working_hours') ?></small>
                    <h5><?= $calendar->working_hours ?></h5>
                </div>
                <div class="col-sm-3">
                    <h5 class="card-title right hidden-xs"><?= lang('working_days') ?></h5>
                    <h5 class="card-title visible-xs"><?= lang('working_days') ?></h5>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_sunday" value="false">
                        <label><?= lang('sunday') ?> <input disabled type="checkbox" name="wd_sunday" value="true" <?= set_checkbox('wd_sunday', 'true', ($calendar->wd_sunday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_monday" value="false">
                        <label><?= lang('monday') ?> <input disabled type="checkbox" name="wd_monday" value="true" <?= set_checkbox('wd_monday', 'true', ($calendar->wd_monday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_tuesday" value="false">
                        <label><?= lang('tuesday') ?> <input disabled type="checkbox" name="wd_tuesday" value="true" <?= set_checkbox('wd_tuesday', 'true', ($calendar->wd_tuesday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_wednesday" value="false">
                        <label><?= lang('wednesday') ?> <input disabled type="checkbox" name="wd_wednesday" value="true" <?= set_checkbox('wd_wednesday', 'true', ($calendar->wd_wednesday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_thursday" value="false">
                        <label><?= lang('thursday') ?> <input disabled type="checkbox" name="wd_thursday" value="true" <?= set_checkbox('wd_thursday', 'true', ($calendar->wd_thursday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_friday" value="false">
                        <label><?= lang('friday') ?> <input disabled type="checkbox" name="wd_friday" value="true" <?= set_checkbox('wd_friday', 'true', ($calendar->wd_friday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="switch">
                        <input type="hidden" name="wd_saturday" value="false">
                        <label><?= lang('saturday') ?> <input disabled type="checkbox" name="wd_saturday" value="true" <?= set_checkbox('wd_saturday', 'true', ($calendar->wd_saturday === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="header">
        <h2><?= lang('holidays') ?></h2>
    </div>
    <div class="body">
        <div class="row">
            <div class="col-sm-2 m-b-0">
                <div class="form-group form-group-sm form-float">
                    <div class="form-line focused">
                        <input type="number" class="form-control" id="year" value="<?= date('Y') ?>" required min="2015" max="2040" />
                        <label class="form-label"><?= lang('year') ?></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table id="holidays-table" data-calendar-id="<?= $calendar->id ?>" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="">
                            <label class="m-b-0" for="check-all"></label>
                        </th>
                        <th class="all"><?= lang('date') ?></th>
                        <th class="all"><?= lang('name') ?></th>
                        <th class="all"><?= lang('description') ?></th>
                        <th><?= lang('options') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
