<div class="card">
    <form id="client-new-form" class="form-validate" method="post" enctype="multipart/form-data">
        <div class="header">
            <h2 class="card-title"><i class="material-icons">add</i><?= lang('method_create') ?> <?= lang('client') ?></h2>
            <ul class="header-dropdown">
                <li>
                    <div class="switch">
                        <input type="hidden" name="active" value="false">
                        <label><?= lang('active') ?> <input type="checkbox" name="active" value="true" <?= set_checkbox('active', 'true', TRUE) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </li>
            </ul>
        </div>

        <?php if ( validation_errors() ): ?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <?= validation_errors() ?>
            </div>
        <?php endif; ?>

        <div class="body">
            <div class="row">
                <div class="col-md-2 col-md-offset-0 col-sm-3 col-sm-offset-5 col-xs-5 col-xs-offset-4 m-b-0">
                    <div class="avatar" style="background-image: url('<?= img_src('public/img/clients/default.jpg') ?>')">
                        <a class="avatar-edit" href="javascript:void(0);"><i class="material-icons">edit</i></a>
                        <input class="avatar-input" type="file" accept=".jpg,.jpeg" name="picture_file" value="">
                    </div>
                </div>

                <div class="col-md-10 col-sm-12 m-b-0">
                    <div class="row">
                        <div class="col-sm-3 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="ref_code" value="<?= set_value('ref_code') ?>" maxlength="12" />
                                    <label class="form-label"><?= lang('ref_code') ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="name" value="<?= set_value('name') ?>" required maxlength="100" />
                                    <label class="form-label"><?= lang('name') ?> *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="short_name" value="<?= set_value('short_name') ?>" maxlength="30" />
                                    <label class="form-label"><?= lang('short_name') ?></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <?= form_dropdown('client_category_id', $categories, set_value('client_category_id'), 'class="form-control" required') ?>
                                    <label class="form-label"><?= lang('category') ?> *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="tax_number" value="<?= set_value('tax_number') ?>" maxlength="11" />
                                    <label class="form-label"><?= lang('tax_number') ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <select class="btn-group bootstrap-select form-control show-tick" name="account_manager">
                                        <option value=""><?= lang('choose_an_option') ?></option>
                                        <?php foreach ($employees as $employee): ?>
                                            <option value="<?= $employee->user_id ?>"><?= $employee->full_name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <label class="form-label"><?= lang('account_manager') ?> *</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="address" value="<?= set_value('address') ?>" maxlength="250" />
                                    <label class="form-label"><?= lang('address') ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h4 class="card-title"><?= lang('contacts') ?></h4><br>
            <div id="contactsForm">
                <!-- Form template-->
                <div class="row" id="contactsForm_template">
                    <input id="contactsForm_#index#_id" type="hidden" name="contacts[#index#][id]" value="new"/>
                    <div class="col-sm-2 m-b-0">
                        <div class="form-group form-group-sm form-float">
                            <div class="form-line focused">
                                <select id="contactsForm_#index#_contact_type_id" class="btn-group ms bootstrap-select form-control show-tick" name="contacts[#index#][contact_type_id]" required value="">
                                    <option value=""><?= lang('choose_an_option') ?></option>
                                    <?php foreach ($contact_types as $type): ?>
                                        <option value="<?= $type->id ?>"><?= $type->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label class="form-label"><?= lang('contact_type') ?> *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 m-b-0">
                        <div class="form-group form-group-sm form-float">
                            <div class="form-line">
                                <input id="contactsForm_#index#_name" type="text" class="form-control" name="contacts[#index#][name]" value="" minlength="4" maxlength="250" required/>
                                <label class="form-label"><?= lang('name') ?> *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 m-b-0">
                        <div class="form-group form-group-sm form-float">
                            <div class="form-line">
                                <input id="contactsForm_#index#_position" type="text" class="form-control" name="contacts[#index#][position]" value="" minlength="2" maxlength="250" required/>
                                <label class="form-label"><?= lang('position') ?> *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 m-b-0">
                        <div class="form-group form-group-sm form-float">
                            <div class="form-line">
                                <input id="contactsForm_#index#_phone" type="text" class="form-control" name="contacts[#index#][phone]" value="" minlength="4" maxlength="30" />
                                <label class="form-label"><?= lang('phone') ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 m-b-0">
                        <div class="form-group form-group-sm form-float">
                            <div class="form-line">
                                <input id="contactsForm_#index#_email" type="email" class="form-control" name="contacts[#index#][email]" value="" minlength="4" maxlength="250" required/>
                                <label class="form-label"><?= lang('email') ?> *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1 m-b-0">
                        <a href="javascript:void(0);" id="contactsForm_remove_current">
                            <i class="material-icons col-red">close</i>
                        </a>
                    </div>
                </div>
                <!-- /Form template-->

                <!-- No forms template -->
                <div id="contactsForm_noforms_template"></div>
                <!-- /No forms template-->

                <!-- Controls -->
                <div id="contactsForm_controls">
                    <span id="contactsForm_add"><a href="javascript:void(0);"><i class="material-icons col-blue">add</i> <?= lang('add_contact') ?></a></span>
                </div>
                <!-- /Controls -->
            </div>

            <div class="row">
                <div class="col-xs-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </form>
</div>

<span id="contactsPre" data-contacts='<?= json_encode($contacts) ?>' ></span>
