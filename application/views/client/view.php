<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('client') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('client/edit/').$client->id ?>" class="btn btn-info btn-xs <?= grant_show('client', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-md-offset-0 col-xs-6 col-xs-offset-3">
                <div class="avatar" style="background-image: url('<?= img_src("public/img/clients/{$client->id}.jpg") ?>')">
                </div>
                <span class="col-grey font-italic"><?= $client->short_name ?></span>
            </div>

            <div class="col-lg-10 col-md-9 col-xs-12 m-b-0">
                <div class="row">
                    <div class="col-sm-9 col-xs-12 m-b-0">
                        <h3><?= $client->name ?></h3>
                    </div>
                    <div class="col-sm-3 col-xs-12 m-b-0 align-right">
                        <div class="switch">
                            <label><?= lang('active') ?> <input type="checkbox" value="true" <?= ($client->active === 'true') ? 'checked' : '' ?> disabled ><span class="lever switch-col-green"></span></label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <ul class="list-unstyled">
                            <li class="<?= $client->ref_code ? '' : 'hidden' ?>">
                                <span class="col-grey"><?= lang('ref_code') ?>:</span> <?= $client->ref_code ?>
                            </li>
                            <li class="<?= $client->tax_number ? '' : 'hidden' ?>">
                                <span class="col-grey"><?= lang('tax_number') ?>:</span> <?= $client->tax_number ?>
                            </li>
                            <li class="<?= $client->address ? '' : 'hidden' ?>">
                                <span class="col-grey"><i class="material-icons font-14">place</i></span> <?= $client->address ?>
                            </li>
                            <li class="<?= $client->account_manager_name ? '' : 'hidden' ?>">
                                <span class="col-grey"><?= lang('account_manager') ?>:</span> <?= $client->account_manager_name ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="card-title"><?= lang('contacts') ?></h4>

        <table class="table table-condensed">
            <thead>
            <tr>
                <th><?= lang('contact_type') ?></th>
                <th><?= lang('name') ?></th>
                <th><?= lang('position') ?></th>
                <th><?= lang('phone') ?></th>
                <th><?= lang('email') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($contacts as $contact): ?>
                <tr>
                    <td><?= $contact->contact_type ?></td>
                    <td><?= $contact->name ?></td>
                    <td><?= $contact->position ?></td>
                    <td><?= $contact->phone ?></td>
                    <td><?= $contact->email ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>
