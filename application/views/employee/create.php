<div class="card">
    <div class="header">
        <h2><i class="material-icons">add</i><?= lang('method_create') ?> <?= lang('employee') ?></h2>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="employee-new-form" class="form-validate" method="post">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12 m-b-0">
                            <input type="radio" name="creation_mode" id="creation-mode-new" value="new" checked class="radio-col-blue">
                            <label for="creation-mode-new">Crear empleado desde cero</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 m-b-0">
                            <input type="radio" name="creation_mode" id="creation-mode-existing" value="existing" class="radio-col-blue">
                            <label for="creation-mode-existing">Crear empleado a partir de usuario existente</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 m-b-0">
                    <div class="form-group form-float m-b-0 hidden" id="user-select-div">
                        <div class="form-line focused">
                            <?= form_dropdown('user_id', $users, set_value('user_id', ''), 'class="btn-group bootstrap-select form-control show-tick"') ?>
                            <label class="form-label"><?= lang('user') ?></label>
                        </div>
                    </div>
                </div>
            </div>

            <h5 class="card-inside-title"><?= lang('user_data') ?></h5>
            <div class="row optional-fields">
                <div class="col-sm-6">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="first_name" value="<?= set_value('first_name') ?>" autofocus required minlength="2" maxlength="100" />
                            <label class="form-label"><?= lang('first_name') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line">
                            <input type="text" class="form-control" name="last_name" value="<?= set_value('last_name') ?>" required minlength="2" maxlength="100" />
                            <label class="form-label"><?= lang('last_name') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row optional-fields">
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" value="<?= set_value('username') ?>" required minlength="4" maxlength="30" />
                            <label class="form-label"><?= lang('username') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" id="password" value="<?= set_value('password') ?>" required minlength="4" maxlength="72" />
                            <label class="form-label"><?= lang('password') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line">
                            <input type="password" class="form-control" name="repeat_password" value="<?= set_value('repeat_password') ?>" required minlength="4" maxlength="72" />
                            <label class="form-label"><?= lang('repeat_password') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row optional-fields">
                <div class="col-sm-12">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" value="<?= set_value('email') ?>" required maxlength="250" />
                            <label class="form-label"><?= lang('email') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>

            <h5 class="card-inside-title"><?= lang('employee_data') ?></h5>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <select class="btn-group bootstrap-select form-control show-tick" name="company_id" required>
                                <option value="" selected disabled><?= lang('choose_an_option') ?></option>
                                <?php foreach ($companies as $element): ?>
                                    <option value="<?= $element->id ?>" <?= set_select('company_id', $element->id) ?> data-subtext="[<?= $element->short_name ?>]"><?= $element->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('company') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused disabled">
                            <select class="btn-group bootstrap-select form-control show-tick" name="department_id" required>
                                <option value="" selected disabled><?= lang('choose_an_option') ?></option>
                                <?php foreach ($departments as $element): ?>
                                    <option data-company="<?= $element->company_id ?>" value="<?= $element->id ?>" <?= set_select('department_id', $element->id) ?> style="display: none;" data-subtext="[<?= $element->short_name ?>]"><?= $element->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('department') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused disabled">
                            <select class="btn-group bootstrap-select form-control show-tick" name="area_id" required>
                                <option value="" selected disabled><?= lang('choose_an_option') ?></option>
                                <?php foreach ($areas as $element): ?>
                                    <option data-department="<?= $element->department_id ?>" value="<?= $element->id ?>" <?= set_select('area_id', $element->id) ?> style="display: none;" data-subtext="[<?= $element->short_name ?>]"><?= $element->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('area') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <select name="supervisor_id" class="btn-group bootstrap-select form-control show-tick">
                                <option value="" selected><?= lang('choose_an_option'); ?></option>
                                <?php foreach($supervisors as $supervisor): ?>
                                    <option value="<?= $supervisor->user_id?>" <?= set_select('supervisor_id', $supervisor->user_id)?> ><?= $supervisor->full_name ?></option>
                                <?php endforeach;?>
                            </select>
                            <label class="form-label"><?= lang('supervisor') ?> </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused disabled">
                            <select class="btn-group bootstrap-select form-control show-tick" name="position_id" required>
                                <option value="" selected disabled><?= lang('choose_an_option') ?></option>
                                <?php foreach ($positions as $element): ?>
                                    <option data-company="<?= $element->company_id ?>" value="<?= $element->id ?>" <?= set_select('position_id', $element->id) ?> style="display: none;"><?= $element->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('position') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="phone" value="<?= set_value('phone') ?>" minlength="2" maxlength="20" />
                            <label class="form-label"><?= lang('phone') ?> (XXX) XXX-XXXX</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="ext" value="<?= set_value('ext') ?>" minlength="1" maxlength="8" />
                            <label class="form-label"><?= lang('ext') ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="mobile" value="<?= set_value('mobile') ?>" minlength="2" maxlength="20" />
                            <label class="form-label"><?= lang('mobile') ?> (XXX) XXX-XXXX</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
