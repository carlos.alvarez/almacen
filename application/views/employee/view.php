<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('employee') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('employee/edit/'.$employee->user_id) ?>" class="btn btn-info btn-xs <?= grant_show('employee', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-md-offset-0 col-xs-6 col-xs-offset-3">
                <div class="avatar" style="background-image: url('<?= img_src("public/img/users/{$employee->user_id}.jpg") ?>')">
                </div>
            </div>

            <div class="col-lg-10 col-md-9 col-xs-12 m-b-0">
                <h4 class="card-title">
                    <?= lang('user_data') ?>
                    <a href="<?= base_url('user/edit/'.$employee->user_id) ?>" class="right <?= grant_show('user', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?> <?= lang('user') ?>"><i class="material-icons col-blue">edit</i></a>
                    <a href="<?= base_url('user/view/'.$employee->user_id) ?>" class="right <?= grant_show('user', 'view') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_view') ?> <?= lang('user') ?>"><i class="material-icons col-blue">search</i></a>
                </h4>
                <div class="row">
                    <div class="col-sm-3">
                        <small><?= lang('name') ?></small>
                        <h5><?= $user->first_name.' '.$user->last_name ?></h5>
                    </div>
                    <div class="col-sm-3">
                        <small><?= lang('username') ?></small>
                        <h5><?= $user->username ?></h5>
                    </div>
                    <div class="col-sm-6">
                        <small><?= lang('email') ?></small>
                        <h5><?= $user->email ?></h5>
                    </div>
                </div>

                <h4 class="card-title">
                    <?= lang('employee_data') ?></h4>
                <div class="row">
                    <div class="col-sm-3">
                        <small><?= lang('company') ?></small>
                        <h5><?= $employee->company ?></h5>
                    </div>
                    <div class="col-sm-3">
                        <small><?= lang('department') ?></small>
                        <h5><?= $employee->department ?></h5>
                    </div>
                    <div class="col-sm-3">
                        <small><?= lang('area') ?></small>
                        <h5><?= $employee->area ?></h5>
                    </div>
                    <div class="col-sm-3">
                        <small><?= lang('position') ?></small>
                        <h5><?= $employee->position ?></h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <small><?= lang('supervisor') ?></small>
                        <h5><?= isset($supervisor->full_name) ? $supervisor->full_name : 'No Tiene supervisor' ?></h5>
                    </div>
                    <div class="col-sm-3">
                        <small><?= lang('phone') ?></small>
                        <h5><?= isset($employee->phone) ? $employee->phone : '' ?></h5>
                    </div>
                    <div class="col-sm-3">
                        <small><?= lang('ext') ?></small>
                        <h5><?= isset($employee->ext) ? $employee->ext : '' ?></h5>
                    </div>
                    <div class="col-sm-3">
                        <small><?= lang('mobile') ?></small>
                        <h5><?= isset($employee->mobile) ? $employee->mobile : '' ?></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>