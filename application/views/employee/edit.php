<div class="card">
    <div class="header">
        <h2><i class="material-icons">edit</i><?= lang('method_edit') ?> <?= lang('employee') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('employee/view/').$employee->user_id ?>" class="btn btn-info btn-xs <?= grant_show('employee', 'view') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_view') ?>">
                    <i class="material-icons col-white">search</i>
                </a>
            </li>
        </ul>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="employee-edit-form" class="form-validate" method="post">
            <h4 class="card-title">
                <?= lang('user_data') ?>
                <a href="<?= base_url('user/edit/'.$employee->user_id) ?>" class="right <?= grant_show('user', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?> <?= lang('user') ?>"><i class="material-icons col-blue">edit</i></a>
                <a href="<?= base_url('user/view/'.$employee->user_id) ?>" class="right <?= grant_show('user', 'view') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_view') ?> <?= lang('user') ?>"><i class="material-icons col-blue">search</i></a>
            </h4>
            <div class="row">
                <div class="col-sm-3">
                    <small><?= lang('name') ?></small>
                    <h5><?= $user->first_name.' '.$user->last_name ?></h5>
                </div>
                <div class="col-sm-3">
                    <small><?= lang('username') ?></small>
                    <h5><?= $user->username ?></h5>
                </div>
                <div class="col-sm-6">
                    <small><?= lang('email') ?></small>
                    <h5><?= $user->email ?></h5>
                </div>
            </div>

            <h4 class="card-title"><?= lang('employee_data') ?></h4>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <select class="btn-group bootstrap-select form-control show-tick" name="company_id" required>
                                <option value="" selected disabled><?= lang('choose_an_option') ?></option>
                                <?php foreach ($companies as $element): ?>
                                    <option value="<?= $element->id ?>" <?= set_select('company_id', $element->id, ($employee->company_id === $element->id)) ?> data-subtext="[<?= $element->short_name ?>]"><?= $element->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('company') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <select class="btn-group bootstrap-select form-control show-tick" name="department_id" required>
                                <option value=""><?= lang('choose_an_option') ?></option>
                                <?php foreach ($departments as $element): ?>
                                    <option data-company="<?= $element->company_id ?>" value="<?= $element->id ?>" <?= set_select('department_id', $element->id, ($employee->department_id === $element->id)) ?> style="<?= ($employee->company_id === $element->company_id) ? '' : 'display: none;' ?>" data-subtext="[<?= $element->short_name ?>]"><?= $element->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('department') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <select class="btn-group bootstrap-select form-control show-tick" name="area_id" required>
                                <option value="" selected disabled><?= lang('choose_an_option') ?></option>
                                <?php foreach ($areas as $element): ?>
                                    <option data-department="<?= $element->department_id ?>" value="<?= $element->id ?>" <?= set_select('area_id', $element->id, ($employee->area_id === $element->id)) ?> style="<?= ($employee->department_id === $element->department_id) ? '' : 'display: none;' ?>" data-subtext="[<?= $element->short_name ?>]"><?= $element->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('area') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <select name="supervisor_id" class="btn-group bootstrap-select form-control show-tick">
                                <option value="" selected><?= lang('choose_an_option'); ?></option>
                                <?php foreach($supervisors as $supervisor): ?>
                                    <option value="<?= $supervisor->user_id?>" <?= set_select('supervisor_id', $supervisor->user_id, ($employee->supervisor_id === $supervisor->user_id))?> ><?= $supervisor->full_name ?></option>
                                <?php endforeach;?>
                            </select>
                            <label class="form-label"><?= lang('supervisor') ?> </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <select class="btn-group bootstrap-select form-control show-tick" name="position_id" required>
                                <option value="" selected disabled><?= lang('choose_an_option') ?></option>
                                <?php foreach ($positions as $element): ?>
                                    <option data-company="<?= $element->company_id ?>" value="<?= $element->id ?>" <?= set_select('position_id', $element->id, ($employee->position_id === $element->id)) ?> style="<?= ($employee->company_id === $element->company_id) ? '' : 'display: none;' ?>"><?= $element->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('position') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="phone" value="<?= set_value('phone', $employee->phone) ?>" minlength="2" maxlength="20" />
                            <label class="form-label"><?= lang('phone') ?> (XXX) XXX-XXXX</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="ext" value="<?= set_value('ext', $employee->ext) ?>" minlength="1" maxlength="8" />
                            <label class="form-label"><?= lang('ext') ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="mobile" value="<?= set_value('mobile', $employee->mobile) ?>" minlength="2" maxlength="20" />
                            <label class="form-label"><?= lang('mobile') ?> (XXX) XXX-XXXX</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
