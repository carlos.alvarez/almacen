<div class="card">
    <div class="header">
        <h2><i class="material-icons">edit</i><?= lang('method_edit') ?> <?= lang('template') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('template/view/').$template_id ?>" class="btn btn-info btn-xs <?= grant_show('template', 'view') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_view') ?>">
                    <i class="material-icons col-white">search</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" name="tree-search-text" id="tree-search-text" class="form-control">
                        <label class="form-label"><i class='material-icons font-16'>search</i> Buscar</label>
                    </div>
                </div>
                <div id="template-tree" data-id="<?= $template_id ?>"></div>
            </div>
            <div class="col-sm-8">
                <div id="details-container"></div>
            </div>
        </div>
    </div>
</div>

<div class="card" id="default-details" style="display: none;">
    <div class="header">
        <h2>Seleccione un elemento</h2>
        <small>Seleccione un elemento en el árbol de la izquierda para ver sus detalles.</small>
    </div>
</div>

<div class="card" id="template-details" style="display: none;">
    <div class="header">
        <h2></h2>
        <small><i class="glyphicon glyphicon-modal-window col-blue"></i> Plantilla</small>
    </div>
    <div class="body">
        <h5 class="card-inside-title"><i class="material-icons font-18 col-grey">description</i> Descripción</h5>
        <p  id="template_descripcion"></p>
        <h5 class="card-inside-title">Categoría</h5>
        <p id="project_category"></p>
    </div>
</div>

<div class="card" id="stage-details" style="display: none;">
    <div class="header">
        <i class="material-icons col-grey right notification" data-toggle="tooltip" data-original-title="No notificar al cliente">notifications_off</i>
        <i class="material-icons col-grey right visibility" data-toggle="tooltip" data-original-title="Oculto para el cliente">visibility_off</i>
        <h2></h2>
        <small><i class="glyphicon glyphicon-briefcase col-purple"></i> Fase</small>
    </div>
    <div class="body">
        <h5 class="card-inside-title"><i class="material-icons font-18 col-grey">description</i> Descripción</h5>
        <p class="description"></p>

        <h5 class="card-inside-title"><i class="material-icons font-18 col-green">donut_small</i> Peso</h5>
        <div class="progress">
            <div class="progress-bar bg-purple progress-bar-striped weight" role="progressbar" style="width: 5%">5%</div>
        </div>

    </div>
</div>

<div class="card" id="task-details" style="display: none;">
    <div class="header">
        <i class="material-icons col-grey right notification" data-toggle="tooltip" data-original-title="No notificar al cliente">notifications_off</i>
        <i class="material-icons col-grey right visibility" data-toggle="tooltip" data-original-title="Oculto para el cliente">visibility_off</i>
        <h2></h2>
        <small><i class="glyphicon glyphicon-tasks col-green"></i> Tarea</small>
    </div>
    <div class="body">

        <div class="row">
            <div class="col-sm-12 m-b-0">
                <h5 class="card-inside-title"><i class="material-icons font-18 col-grey">description</i> Descripción</h5>
                <p class="description"></p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-7 m-b-0">
                <h5 class="card-inside-title"><i class="material-icons font-18 col-orange">assignment_turned_in</i> Prerequisitos</h5>
                <div class="row">
                    <div class="col-sm-12 m-b-0 prerequisites"></div>
                </div>

                <h5 class="card-inside-title"><i class="material-icons font-18 col-purple">person</i> Cargos</h5>
                <div class="row">
                    <div class="col-sm-12 m-b-0 positions"></div>
                </div>

                <h5 class="card-inside-title"><i class="material-icons font-18 col-cyan">local_offer</i> Etiquetas</h5>
                <div class="row">
                    <div class="col-sm-12 m-b-0 tags"></div>
                </div>

                <h5 class="card-inside-title"><i class="material-icons font-18 col-green">playlist_add_check</i> Requerimientos de cierre</h5>
                <div class="row">
                    <div class="col-sm-12 m-l-10 requirements"></div>
                </div>
            </div>
            <div class="col-sm-5 m-b-0">
                <div class="info-box hover-zoom-effect">
                    <div class="icon bg-light-blue">
                        <i class="material-icons day_type_icon">access_alarm</i>
                    </div>
                    <div class="content">
                        <div class="text day_type"></div>
                        <div class="number duration"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="status-changer" style="display: none;">
            <h5 class="card-inside-title"><i class="material-icons font-18 col-yellow">flash_on</i> Cambia Estado Interno</h5>
            <div class="row">
                <div class="col-sm-5">
                    <small>Estado</small>
                    <br>
                    <span class="stage-status" style="font-weight: bold;"></span>
                </div>
                <div class="col-sm-7">
                    <small>Causa</small>
                    <br>
                    <span class="reason"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h5 class="card-inside-title"><i class="material-icons font-18 col-green">donut_small</i> Peso</h5>
                <div class="progress">
                    <div class="progress-bar bg-green progress-bar-striped weight" role="progressbar" style="width: 5%">5%</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card" id="template-edit" style="display: none;">
    <div class="header">
        <h2><i class="glyphicon glyphicon-modal-window col-blue"></i> Editar Plantilla</h2>
        <small>Presione "Guardar" para salvar los cambios.</small>
    </div>
    <div class="body">
        <form class="form-validate">
            <input type="hidden" name="id" value="">
            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" name="title" class="form-control" required minlength="4" maxlength="150">
                            <label class="form-label">Título</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <h5 class="card-inside-title">Descripción</h5>
                    <div class="form-group">
                        <div class="form-line focused">
                            <textarea rows="1" class="form-control no-resize" name="description" placeholder="Descripción"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <h5 class="card-inside-title">Categoría de proyecto</h5>
                    <div class="form-group">
                        <div class="form-line focused">
                            <?= form_dropdown('project_category_id', $categories, set_value('project_category_id'), 'class="form-control ms" required') ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 m-b-0 align-center">
                    <input type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect" value="Guardar">
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card" id="stage-edit" style="display: none;">
    <form class="form-validate">
        <input type="hidden" name="id" value="">
        <div class="header">
            <input type="hidden" name="client_notification" value="false">
            <a href="javascript:void(0);" class="right">
                <i class="material-icons col-grey notification" data-toggle="tooltip" data-original-title="No notificar al cliente">notifications_off</i>
            </a>
            <input type="hidden" name="client_visibility" value="false">
            <a href="javascript:void(0);" class="right">
                <i class="material-icons col-grey visibility" data-toggle="tooltip" data-original-title="Oculto para el cliente">visibility_off</i>
            </a>
            <h2><i class="glyphicon glyphicon-briefcase col-purple"></i> Editar Fase</h2>
            <small>Presione "Guardar" para salvar los cambios.</small>
        </div>
        <div class="body">
            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" name="title" class="form-control" required minlength="4" maxlength="150">
                            <label class="form-label">Título</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <h5 class="card-inside-title">Descripción</h5>
                    <div class="form-group">
                        <div class="form-line focused">
                            <textarea rows="1" class="form-control no-resize auto-growth" name="description" placeholder="Descripción"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-9">
                    <h5 class="card-inside-title">Peso</h5>
                    <div class="weight-slider"></div>
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <div class="form-line focused">
                            <input type="number" name="weight" class="form-control" min="0.01" max="100" step="0.01" value="50">
                        </div>
                        <span class="input-group-addon">%</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 m-b-0 align-center">
                    <input type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect" value="Guardar">
                </div>
            </div>
        </div>
    </form>
</div>

<div class="card" id="task-edit" style="display: none;">
    <div class="header">
        <a href="javascript:void(0);" class="right">
            <i class="material-icons col-grey notification" data-toggle="tooltip" data-original-title="No notificar al cliente">notifications_off</i>
        </a>
        <a href="javascript:void(0);" class="right">
            <i class="material-icons col-grey visibility" data-toggle="tooltip" data-original-title="Oculto para el cliente">visibility_off</i>
        </a>
        <h2><i class="glyphicon glyphicon-tasks col-green"></i> Editar Tarea</h2>
        <small>Presione "Guardar" para salvar los cambios.</small>
    </div>
    <div class="body">
        <form class="form-validate">
            <input type="hidden" name="id" value="">
            <input type="hidden" name="client_visibility" value="false">
            <input type="hidden" name="client_notification" value="false">
            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" name="title" class="form-control" required minlength="4" maxlength="150">
                            <label class="form-label">Título</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <h5 class="card-inside-title"><i class="material-icons font-18 col-grey">description</i> Descripción</h5>
                    <div class="form-group">
                        <div class="form-line focused">
                            <textarea rows="1" class="form-control no-resize auto-growth" name="description" placeholder="Descripción"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" name="duration" style="height: auto;" required>
                            <label class="form-label">Duración</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-6 m-b-0">
                    <div class="form-group">
                        <input id="radio1" type="radio" class="radio-col-green" checked="" name="day_type" value="laborable">
                        <label for="radio1"><i class="material-icons col-green font-16">work</i> Laborables</label>
                        <br>
                        <input id="radio2" type="radio" class="radio-col-blue" name="day_type" value="calendar">
                        <label for="radio2"><i class="material-icons col-blue font-16">today</i> Calendario</label>
                    </div>
                </div>
            </div>

            <h5><i class="material-icons font-18 col-orange">assignment_turned_in</i> Prerequisitos</h5>
            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <select name="prerequisites[]" title="Seleccione los prerequisitos" data-live-search="true" multiple class="form-control ms dropup" style="display: none;">
                    </select>
                </div>
            </div>

            <h5><i class="material-icons font-18 col-purple">person</i> Cargos</h5>
            <div class="row">
                <div class="col-sm-12">
                    <select name="positions[]" title="Seleccione los cargos" data-live-search="true" multiple class="form-control ms dropup" style="display: none;">

                        <?php $last_company = ''; ?>
                        <?php foreach ($positions as $comp_id => $pos): ?>
                            <optgroup label="<?= $companies[$comp_id] ?>">
                                <?php foreach ($pos as $pos_id => $pos_name): ?>
                                    <option value="<?= $pos_id ?>" data-content="<span class='badge bg-purple'><?= $pos_name ?></span>"><?= $pos_name ?></option>
                                <?php endforeach; ?>
                            </optgroup>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-9">
                    <h5 class="card-inside-title"><i class="material-icons font-18 col-green">donut_small</i> Peso</h5>
                    <div class="weight-slider"></div>
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <div class="form-line focused">
                            <input type="number" name="weight" class="form-control" min="0.01" max="100" step="0.01" value="50">
                        </div>
                        <span class="input-group-addon">%</span>
                    </div>
                </div>
            </div>

            <h5><i class="material-icons font-18 col-cyan">local_offer</i> Etiquetas</h5>
            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" name="tags" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

            <h5><i class="material-icons font-18 col-green">playlist_add_check</i> Requerimientos de cierre</h5>
            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <div class="input-group input-group-sm m-b-0 add-req-group">
                        <span class="input-group-addon">
                            <a href="javascript:void(0);" class="add-req"><i class="material-icons">add</i> Agregar requerimiento</a>
                        </span>
                        <div>
                            <input type="text" class="form-control" value="" disabled>
                        </div>
                    </div>
                </div>
            </div>

            <h5><i class="material-icons font-18 col-yellow">flash_on</i> Cambia Estado Interno</h5>
            <div class="row">
                <div class="col-sm-2">
                    <div class="switch">
                        <input type="hidden" name="status_changer" value="false">
                        <label><input type="checkbox" name="status_changer" value="true"><span class="lever switch-col-amber"></span></label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-sm form-float m-b-0">
                        <div class="form-line focused disabled">
                            <select name="stage_status_id" class="form-control ms" disabled>
                                <?php foreach ($status as $st) : ?>
                                    <option value="<?= $st->id ?>"><?= $st->name ?></option>
                                <?php endforeach;?>
                            </select>
                            <label class="form-label">Estado</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-group-sm form-float m-b-0">
                        <div class="form-line focused disabled">
                            <select name="reason_id" class="form-control ms" disabled>
                                <?php foreach ($reasons as $reason) : ?>
                                    <option value="<?= $reason->id ?>" data-stage_status="<?= $reason->stage_status_id ?>"><?= $reason->name ?></option>
                                <?php endforeach;?>
                            </select>
                            <label class="form-label">Causa</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 align-center">
                    <input type="submit" class="btn btn-primary m-t-15 waves-effect center" value="Guardar">
                </div>
            </div>
        </form>

        <div class="input-group input-group-sm m-b-0 req-input-model hidden">
            <span class="input-group-addon">
                <i class="material-icons">check_box_outline_blank</i>
            </span>
            <div class="form-line">
                <input type="text" class="form-control" value="" maxlength="250">
            </div>
            <span class="input-group-addon">
                <a href="javascript:void(0);" class="delete-req" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons col-red">delete</i></a>
            </span>
        </div>
    </div>
</div>
