<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('template') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('template/edit/').$template_id ?>" class="btn btn-info btn-xs <?= grant_show('template', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <div id="readonly-view" class="body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" name="tree-search-text" id="tree-search-text" class="form-control">
                        <label class="form-label"><i class='material-icons font-16'>search</i> Buscar</label>
                    </div>
                </div>
                <div id="template-tree" data-id="<?= $template_id ?>"></div>
            </div>
            <div class="col-sm-8">
                <div id="details-container"></div>
            </div>
        </div>
    </div>
</div>

<div class="card" id="default-details" style="display: none;">
    <div class="header">
        <h2>Seleccione un elemento</h2>
        <small>Seleccione un elemento en el árbol de la izquierda para ver sus detalles.</small>
    </div>
</div>

<div class="card" id="template-details" style="display: none;">
    <div class="header">
        <h2></h2>
        <small><i class="glyphicon glyphicon-modal-window col-blue"></i> Plantilla</small>
    </div>
    <div class="body">
        <h5 class="card-inside-title"><i class="material-icons font-18 col-grey">description</i> Descripción</h5>
        <p id="template_descripcion"></p>
        <h5 class="card-inside-title">Categoría</h5>
        <p  id="project_category"></p>
    </div>
</div>

<div class="card" id="stage-details" style="display: none;">
    <div class="header">
        <i class="material-icons col-grey right notification" data-toggle="tooltip" data-original-title="No notificar al cliente">notifications_off</i>
        <i class="material-icons col-grey right visibility" data-toggle="tooltip" data-original-title="Oculto para el cliente">visibility_off</i>
        <h2></h2>
        <small><i class="glyphicon glyphicon-briefcase col-purple"></i> Fase</small>
    </div>
    <div class="body">
        <h5 class="card-inside-title"><i class="material-icons font-18 col-grey">description</i> Descripción</h5>
        <p class="description"></p>

        <h5 class="card-inside-title"><i class="material-icons font-18 col-green">donut_small</i> Peso</h5>
        <div class="progress">
            <div class="progress-bar bg-purple progress-bar-striped weight" role="progressbar" style="width: 5%">5%</div>
        </div>

    </div>
</div>

<div class="card" id="task-details" style="display: none;">
    <div class="header">
        <i class="material-icons col-grey right notification" data-toggle="tooltip" data-original-title="No notificar al cliente">notifications_off</i>
        <i class="material-icons col-grey right visibility" data-toggle="tooltip" data-original-title="Oculto para el cliente">visibility_off</i>
        <h2></h2>
        <small><i class="glyphicon glyphicon-tasks col-green"></i> Tarea</small>
    </div>
    <div class="body">

        <div class="row">
            <div class="col-sm-12 m-b-0">
                <h5 class="card-inside-title"><i class="material-icons font-18 col-grey">description</i> Descripción</h5>
                <p class="description"></p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-7 m-b-0">
                <h5 class="card-inside-title"><i class="material-icons font-18 col-orange">assignment_turned_in</i> Prerequisitos</h5>
                <div class="row">
                    <div class="col-sm-12 m-b-0 prerequisites"></div>
                </div>

                <h5 class="card-inside-title"><i class="material-icons font-18 col-purple">person</i> Cargos</h5>
                <div class="row">
                    <div class="col-sm-12 m-b-0 positions"></div>
                </div>

                <h5 class="card-inside-title"><i class="material-icons font-18 col-cyan">local_offer</i> Etiquetas</h5>
                <div class="row">
                    <div class="col-sm-12 m-b-0 tags"></div>
                </div>

                <h5 class="card-inside-title"><i class="material-icons font-18 col-green">playlist_add_check</i> Requerimientos de cierre</h5>
                <div class="row">
                    <div class="col-sm-12 m-l-10 requirements"></div>
                </div>
            </div>
            <div class="col-sm-5 m-b-0">
                <div class="info-box hover-zoom-effect">
                    <div class="icon bg-light-blue">
                        <i class="material-icons day_type_icon">access_alarm</i>
                    </div>
                    <div class="content">
                        <div class="text day_type"></div>
                        <div class="number duration"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="status-changer" style="display: none;">
            <h5 class="card-inside-title"><i class="material-icons font-18 col-yellow">flash_on</i> Cambia Estado Interno</h5>
            <div class="row">
                <div class="col-sm-5">
                    <small>Estado</small>
                    <br>
                    <span class="stage-status" style="font-weight: bold;"></span>
                </div>
                <div class="col-sm-7">
                    <small>Causa</small>
                    <br>
                    <span class="reason"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h5 class="card-inside-title"><i class="material-icons font-18 col-green">donut_small</i> Peso</h5>
                <div class="progress">
                    <div class="progress-bar bg-green progress-bar-striped weight" role="progressbar" style="width: 5%">5%</div>
                </div>
            </div>
        </div>
    </div>
</div>
