<div class="card">
    <div class="header">
        <h2><i class="material-icons">add</i><?= lang('method_create') ?> <?= lang('template') ?></h2>
    </div>
    <div class="body" id="new-template-body"></div>
</div>
