<div class="card">
    <div class="header">
        <h2>Bienvenido <?= $this->session->userdata('full_name') ?> | <?= $this->session->userdata('client_name') ?></h2>
    </div>
    <div class="body">
        <div class="row m-t-50 m-b-50">
            <div class="col-sm-12 align-center">
                <img src="<?= img_src('public/img/logo_slogan.jpg') ?>" alt="Ergotec" style="max-width: 200px;">
                <?php if ( file_exists('./public/img/clients/'.$this->session->userdata('client_id').'.jpg') ): ?>
                    <img src="<?= img_src('public/img/clients/'.$this->session->userdata('client_id').'.jpg') ?>" alt="<?= $this->session->userdata('client_name') ?>" style="max-width: 200px;">
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
