<div class="card">
    <div class="header">
        <h2><i class="material-icons">add</i><?= lang('method_create') ?> <?= lang('department') ?></h2>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="department-new-form" class="form-validate" method="post">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <?= form_dropdown('company_id', $companies, set_value('company_id', ''), 'class="btn-group bootstrap-select form-control show-tick" data-live-search="true" required') ?>
                            <label class="form-label"><?= lang('company') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" value="<?= set_value('name') ?>" required />
                            <label class="form-label"><?= lang('name') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="short_name" value="<?= set_value('short_name') ?>" required />
                            <label class="form-label"><?= lang('short_name') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6 align-right">
                    <div class="switch">
                        <input type="hidden" name="active" value="false">
                        <label><?= lang('active') ?> <input type="checkbox" name="active" value="true" <?= set_checkbox('active', 'true', TRUE) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <?= form_dropdown('manager_id', $employees, set_value('manager_id', ''), 'class="btn-group bootstrap-select form-control show-tick" data-live-search="true"') ?>
                            <label class="form-label"><?= lang('manager') ?></label>
                        </div>
                    </div>
                </div>

            </div>

            <div class="align-center">
                <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
            </div>
        </form>
    </div>
</div>
