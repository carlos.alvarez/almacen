<div class="title">¡Bienvenido a Ergospace!</div>
<br>

<div class="body-text">
    <p>Estimado <?= isset($client_name) ? $client_name : 'CLIENTE' ?>,</p>
    <p>Gracias por formar parte de nuestra familia, a continuación detallamos los datos de acceso a su cuenta:</p>

    <table>
        <tr>
            <th style="padding-right: 10px;">Usuario:</th>
            <td><?= isset($username) ? $username : 'usuario' ?></td>
        </tr>
        <tr>
            <th style="padding-right: 10px;">Contraseña:</th>
            <td><?= isset($password) ? $password : 'contraseña' ?></td>
        </tr>
    </table>

    <p>Accede a nuestra plataforma haciendo click <a href="<?= base_url() ?>">AQUÍ</a></p>

    <p>Para cualquier información que requiera no dude en contactarnos a través de nuestro chat en línea ó al <span style="white-space: nowrap;">809-412-7575</span> <span style="white-space: nowrap;">ext. 228</span> con Atención al Cliente.</p>

    <p class="goodbye">Estaremos complacidos de asistirle.</p>
</div>
