<div class="title">¡Bienvenido a Ergospace!</div>
<br>

<div class="body-text">
    <p>Gracias por formar parte de nuestro equipo, a continuación detallamos los datos de acceso a tu cuenta:</p>

    <table>
        <tr>
            <th style="padding-right: 10px;">Usuario:</th>
            <td><?= isset($username) ? $username : 'usuario' ?></td>
        </tr>
        <tr>
            <th style="padding-right: 10px;">Contraseña:</th>
            <td><?= isset($password) ? $password : 'contraseña' ?></td>
        </tr>
    </table>
    <br>
    <p>Para cualquier información o soporte que requieras no dudes en contactarnos al <span style="white-space: nowrap;">809-412-7575</span> <span style="white-space: nowrap;">ext. 326</span> con Tecnología.</p>

    <p class="goodbye">¡Juntos logramos más!</p>
</div>
