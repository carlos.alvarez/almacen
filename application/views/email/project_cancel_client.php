<div class="title">Proyecto Anulado</div>
<br>

<div class="body-text">
    <p>Estimado <?= isset($client->name) ? $client->name : 'CLIENTE' ?>,</p>

    <p>Se ha anulado el proyecto: <strong><?= isset($project->name) ? $project->name : 'Proyecto 1' ?></strong>.</p>

    <p>¡Gracias por contactarnos!</p>

    <p>Esperamos poder estrechar nuestros lazos comerciales en una próxima ocasión.</p>

    <p>Para nosotros será un gusto atenderle.</p>

    <p class="footer-text" align="right">¿Qué pudimos haber hecho mejor?<br>
        Déjanos tu comentario y ayúdanos a mejorar.<br>
        <a href="mailto:atencionalcliente@ergotec.com.do">atencionalcliente@ergotec.com.do</a></p>
</div>
