<div class="title">Proyecto Finalizado</div>
<br>

<div class="body-text">
    <p>Estimado Colaborador,</p>

    <p>El proyecto <strong><a href="<?= isset($project->id) ? base_url("project_client/view/{$project->id}") : 'javascript: void(0);' ?>"><?= isset($project->name) ? $project->name : 'Proyecto 1' ?></a></strong> ha concluido con todas las etapas de instalación.</p>

    <p class="goodbye">¡Juntos logramos más!</p>
</div>
