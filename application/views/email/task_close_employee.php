<div class="title">Cierre de Tarea</div>
<br>

<div class="body-text">
    <p>Estimado Colaborador,</p>

    <p>Esta tarea ha concluido satisfactoriamente.</p>

    <p class="goodbye">¡Juntos logramos más!</p>
</div>
