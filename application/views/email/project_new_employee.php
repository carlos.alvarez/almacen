<div class="title">¡Bienvenido a este nuevo proyecto!</div>
<br>

<div class="body-text">
    <p>Estimado Colaborador,</p>

    <p>Se ha creado el proyecto: <strong><?= isset($project->name) ? $project->name : 'Proyecto 1' ?></strong></p>

    <p>Ahora formas parte de este equipo de trabajo. Ingresa a <a href="<?= base_url() ?>">Ergospace</a> para verificar y actualizar el estado de todas tus tareas e interacciones.</p>

    <p class="goodbye">¡Juntos logramos más!</p>
</div>
