<div class="title">Proyecto Anulado</div>
<br>

<div class="body-text">
    <p>Estimado Colaborador,</p>

    <p>Se ha anulado el proyecto: <strong><?= isset($project->name) ? $project->name : 'Proyecto 1' ?></strong>.</p>

    <p class="goodbye">¡Juntos logramos más!</p>
</div>
