<div class="title">¡Bienvenido a este nuevo proyecto!</div>
<br>

<div class="body-text">
    <p>Estimado <?= isset($client->name) ? $client->name : 'CLIENTE' ?>,</p>

    <p>Se ha creado el proyecto: <strong><a href="<?= isset($project->id) ? base_url("project_client/view/{$project->id}") : 'javascript: void(0);' ?>"><?= isset($project->name) ? $project->name : 'Proyecto 1' ?></a></strong></p>

    <p>Gracias por registrarte en nuestra plataforma digital. Ingresando a <a href="<?= base_url() ?>">Ergospace</a> podrás verificar el estado de todas tus solicitudes y recibir asistencia inmediata.</p>

    <p class="goodbye">¡Esperamos satisfacer todas tus expectativas!</p>
</div>
