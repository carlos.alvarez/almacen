<div class="card">
    <div class="header">
        <h2><i class="material-icons">add</i><?= lang('method_create') ?> <?= lang('company') ?></h2>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="company-new-form" class="form-validate" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-md-offset-0 col-sm-3 col-sm-offset-5 col-xs-5 col-xs-offset-4">
                    <div class="avatar" style="background-image: url('<?= img_src('public/img/companies/default.jpg') ?>')">
                        <a class="avatar-edit" href="javascript:void(0);"><i class="material-icons">edit</i></a>
                        <input class="avatar-input" type="file" accept=".jpg,.jpeg" name="picture_file" value="">
                    </div>
                </div>

                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="name" value="<?= set_value('name') ?>" required maxlength="100" />
                                    <label class="form-label"><?= lang('name') ?> *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="short_name" value="<?= set_value('short_name') ?>" maxlength="30" />
                                    <label class="form-label"><?= lang('short_name') ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 col-xs-12 align-center">
                            <div class="switch">
                                <input type="hidden" name="active" value="false">
                                <label><?= lang('active') ?> <input type="checkbox" name="active" value="true" <?= set_checkbox('active', 'true', TRUE) ?>><span class="lever switch-col-green"></span></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="tax_number" value="<?= set_value('tax_number') ?>" maxlength="11" />
                                    <label class="form-label"><?= lang('tax_number') ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line">
                                    <?= form_dropdown('calendar_id', $calendars, set_value('calendar_id'), 'class="form-control" required') ?>
                                    <label class="form-label"><?= lang('Calendar_lib') ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="address" value="<?= set_value('address') ?>" maxlength="250" />
                                    <label class="form-label"><?= lang('address') ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
