<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('company') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('company/edit/').$company->id ?>" class="btn btn-info btn-xs <?= grant_show('company', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-md-offset-0 col-xs-6 col-xs-offset-3">
                <div class="avatar" style="background-image: url('<?= img_src("public/img/companies/{$company->id}.jpg") ?>')">
                </div>
                <span class="col-grey font-italic"><?= $company->short_name ?></span>
            </div>

            <div class="col-lg-10 col-md-9 col-xs-12 m-b-0">
                <div class="row">
                    <div class="col-sm-9 col-xs-12 m-b-0">
                        <h3><?= $company->name ?></h3>
                    </div>
                    <div class="col-sm-3 col-xs-12 m-b-0 align-right">
                        <div class="switch">
                            <label><?= lang('active') ?> <input type="checkbox" value="true" <?= ($company->active === 'true') ? 'checked' : '' ?> disabled ><span class="lever switch-col-green"></span></label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <ul class="list-unstyled">
                            <li>
                                <span class="col-grey"><?= lang('tax_number') ?>:</span> <?= $company->tax_number ?>
                            </li>
                            <li>
                                <span class="col-grey"><i class="material-icons font-14">place</i></span> <?= $company->address ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
