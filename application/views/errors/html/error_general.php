<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?><?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Error</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="/public/fonts/roboto_regular/stylesheet.css" rel="stylesheet">
    <link href="/public/plugins/material-design-icons/material-icons.css" rel="stylesheet">

    <!-- Bootstrap Core Css -->
    <link href="/public/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/public/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/public/css/style.css" rel="stylesheet">
</head>

<body class="five-zero-zero">
<div class="five-zero-zero-container">
    <div class="error-code">500</div>
    <div class="error-message">
        <h1><?= $heading ?></h1>
        <?= $message ?>
    </div>
    <div class="button-place">
        <a href="/" class="btn btn-default btn-lg waves-effect">GO TO HOMEPAGE</a>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="/public/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/public/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/public/plugins/node-waves/waves.js"></script>
</body>

</html>
