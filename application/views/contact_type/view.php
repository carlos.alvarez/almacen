<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('contact_type') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('contact_type/edit/').$contact_type->id ?>" class="btn btn-info btn-xs <?= grant_show('contact_type', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-sm-12">
                <h2><?= $contact_type->name ?></h2>
            </div>
        </div>
    </div>
</div>
