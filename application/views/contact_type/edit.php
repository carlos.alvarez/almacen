<div class="card">
    <div class="header">
        <h2><i class="material-icons">edit</i><?= lang('method_edit') ?> <?= lang('contact_type') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('contact_type/view/').$contact_type->id ?>" class="btn btn-info btn-xs <?= grant_show('contact_type', 'view') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_view') ?>">
                    <i class="material-icons col-white">search</i>
                </a>
            </li>
        </ul>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="contact_type-edit-form" class="form-validate" method="post">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="name" value="<?= set_value('name', $contact_type->name) ?>" required minlength="2" maxlength="100" />
                            <label class="form-label"><?= lang('name') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="align-center">
                <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
            </div>
        </form>
    </div>
</div>
