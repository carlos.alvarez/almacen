<div class="card">
    <div class="body">

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#projects" data-toggle="tab" aria-expanded="false">
                    <i class="material-icons">bubble_chart</i> PROYECTOS
                </a>
            </li>
            <!--li role="presentation" class="">
                <a href="#tasks" data-toggle="tab" aria-expanded="false">
                    <i class="material-icons">playlist_add_check</i> TAREAS
                </a>
            </li-->
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="projects">
                <table id="projects-table" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <!--                    <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">-->
                            <input type="checkbox" id="check-all" class="filled-in chk-col-teal">
                            <label class="m-b-0" for="check-all"></label>
                        </th>
                        <th class="all">ID</th>
                        <th class="all"><?= lang('name') ?></th>
                        <th class="all" data-toggle="tooltip" title="Fecha de Inicio">F.Inicio</th>
                        <th class="all" data-toggle="tooltip" title="Fecha Estimada">F.Estimada</th>
                        <th class="all" data-toggle="tooltip" title="Fecha Estimada">F.Fin</th>
                        <th class="all" data-toggle="tooltip" title="Tiempo Estimado">T.</th>
                        <th class="all">Estado</th>
                        <th class="all">Cliente</th>
                        <th class="all">Empresa</th>
                        <th class="all">Responsable</th>
                        <th class="all">Etiquetas</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tasks">
                <table id="tasks-table" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">
                            <label class="m-b-0" for="check-all"></label>
                        </th>
                        <th class="all">ID</th>
                        <th class="all">Fase</th>
                        <th class="all">Título</th>
                        <th class="all">Descripcion</th>
                        <th class="all"><?= lang('start_date') ?></th>
                        <th class="all"><?= lang('estimated_date') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
