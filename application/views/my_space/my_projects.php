<div class="card">
    <div class="header" style="border-bottom: none;">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#dashboard" data-toggle="tab" aria-expanded="true">
                    <i class="material-icons">dashboard</i> DASHBOARD
                </a>
            </li>
            <li role="presentation" class="">
                <a href="#projects" data-toggle="tab" aria-expanded="false">
                    <i class="material-icons">bubble_chart</i> PROYECTOS
                </a>
            </li>
            <li role="presentation" class="">
                <a href="#tasks" data-toggle="tab" aria-expanded="false">
                    <i class="material-icons">playlist_add_check</i> TAREAS
                </a>
            </li>
            <!-- li role="presentation" class="">
                <a href="#calendar" data-toggle="tab" aria-expanded="false">
                    <i class="material-icons">today</i> CALENDARIO
                </a>
            </li>
            <li role="presentation" class="">
                <a href="#files" data-toggle="tab" aria-expanded="false">
                    <i class="material-icons">insert_drive_file</i> ARCHIVOS
                </a>
            </li>
            <li role="presentation" class="">
                <a href="#blog" data-toggle="tab" aria-expanded="false">
                    <i class="material-icons">forum</i> BLOG
                </a>
            </li -->
        </ul>

        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('project/create') ?>" class="btn btn-info btn-xs <?= grant_show('project', 'create') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_create') ?> Proyecto">
                    <i class="material-icons col-white">add</i>
                </a>
            </li>
        </ul>
    </div>
    <div class="body">
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="dashboard">
                <div class="card" id="filters">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-2 col-md-2  m-b-5">
                                <small>Año</small>
                                <select id="year_chart" class="form-control">
                                    <option value="/all" selected>Todos los años</option>
                                    <option value="/2017">2017</option>
                                    <option value="/2018">2018</option>
                                    <option value="/2019">2019</option>
                                    <option value="/2020">2020</option>
                                    <option value="/2021">2021</option>
                                    <option value="/2022">2022</option>
                                    <option value="/2023">2023</option>
                                    <option value="/2024">2024</option>
                                    <option value="/2025">2025</option>
                                    <option value="/2026">2026</option>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2  m-b-5">
                                <small>Categoría</small>
                                <select id="category_chart" class="form-control">
                                    <option value="/all" selected>Todas las categorías</option>
                                    <?php foreach($categories as $category): ?>
                                        <option value="/<?= $category->id?>"><?= $category->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3  m-b-5">
                                <small>Estado</small>
                                <select id="status_chart" class="form-control">
                                    <option value="/all" selected>Todos los estados</option>
                                    <option value="/progreso">Progreso</option>
                                    <option value="/finalizado">Finalizado</option>
                                    <option value="/anulado">Anulado</option>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3  m-b-5">
                                <small>Empresa</small>
                                <select id="company_chart" class="form-control">
                                    <option value="/all" selected>Todas las empresas</option>
                                    <?php foreach($companies as $company): ?>
                                        <option value="/<?= $company->id ?>"><?= $company->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2  m-b-5">
                                <small>Marca</small>
                                <select id="brand_chart" class="form-control">
                                    <option value="/all" selected>Todas las marcas</option>
                                    <?php foreach($brands as $brand): ?>
                                        <option value="/<?= $brand->id ?>"><?= $brand->name ?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card m-t-5">
                    <div class="header align-center">
                        <h2>Proyectos por Estado</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div id="load-status-projects" class="loader text-center">
                                    <div class="preloader">
                                        <div class="spinner-layer pl-red">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>Cargando gr&aacute;fica...</p>
                                </div>
                                <canvas id="status-projects-chart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="header align-center">
                        <h2>Proyectos por Categoría</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div id="load-categories-projects" class="loader text-center">
                                    <div class="preloader">
                                        <div class="spinner-layer pl-red">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>Cargando gr&aacute;fica...</p>
                                </div>
                                <canvas id="categories-projects-chart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="header align-center">
                        <h2>Proyectos por Empresa</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div id="load-companies-projects" class="loader text-center">
                                    <div class="preloader">
                                        <div class="spinner-layer pl-red">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>Cargando gr&aacute;fica...</p>
                                </div>
                                <canvas id="companies-projects-chart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="header align-center">
                        <h2>Embudo de Ventas</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div id="load-sales-funnel" class="loader text-center">
                                    <div class="preloader">
                                        <div class="spinner-layer pl-red">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>Cargando gr&aacute;fica...</p>
                                </div>
                                <canvas id="sales-funnel-chart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="projects">
                <div class="row">
                    <div class="col-sm-12 align-right">
                        <div class="switch">
                            <label>Ver Projectos Atrasados <input type="checkbox" id="delayed_projects_filter"><span class="lever switch-col-red"></span></label>
                        </div>
                    </div>
                </div>
                <table id="projects-table" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <!--                    <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">-->
                            <input type="checkbox" id="check-all" class="filled-in chk-col-teal">
                            <label class="m-b-0" for="check-all"></label>
                        </th>
                        <th class="all">ID</th>
                        <th class="all"><?= lang('name') ?></th>
                        <th class="all" data-toggle="tooltip" title="Fecha de Inicio">F.Inicio</th>
                        <th class="all" data-toggle="tooltip" title="Fecha Estimada">F.Estimada</th>
                        <th class="all" data-toggle="tooltip" title="Fecha Fin">F.Fin</th>
                        <th class="all" data-toggle="tooltip" title="Tiempo Estimado">T.</th>
                        <th class="all">Estado</th>
                        <th class="all" data-toggle="tooltip" title="Estado Interno">E.Interno</th>
                        <th class="all">Causa</th>
                        <th class="all">Cliente</th>
                        <th class="all">Empresa</th>
                        <th class="all">Responsable</th>
                        <th class="all">Etiquetas</th>
                        <th><?= lang('options') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tasks">
                <div class="row">
                    <div class="col-sm-12 align-right">
                        <div class="switch">
                            <label>Ver Tareas Atrasadas <input type="checkbox" id="delayed_tasks_filter"><span class="lever switch-col-red"></span></label>
                        </div>
                    </div>
                </div>
                <table id="tasks-table" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="">
                            <label class="m-b-0" for="check-all"></label>
                        </th>
                        <th class="all">ID</th>
                        <th class="all"><?= lang('project') ?></th>
                        <th class="all"><?= lang('template') ?></th>
                        <th class="all"><?= lang('stage') ?></th>
                        <th class="all"><?= lang('title') ?></th>
                        <th class="all">Estado</th>
                        <th class="all" data-toggle="tooltip" title="Cambia Estado">C.Estado</th>
                        <th class="all" data-toggle="tooltip" title="Estado Interno">E.Interno</th>
                        <th class="all">Causa</th>
                        <th class="all">Progreso</th>
                        <th class="all">Recursos</th>
                        <th class="all" data-toggle="tooltip" title="Fecha de Inicio">F.Inicio</th>
                        <th class="all" data-toggle="tooltip" title="Fecha Estimada">F.Estimada</th>
                        <th class="all" data-toggle="tooltip" title="Fecha de Cierre">F.Cierre</th>
                        <th class="all" data-toggle="tooltip" title="Tiempo Estimado">T.</th>
                        <th class="all">Etiquetas</th>
                        <th class="all" data-toggle="tooltip" title="Visibilidad">Visib.</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!--div role="tabpanel" class="tab-pane fade" id="calendar">

            </div>
            <div role="tabpanel" class="tab-pane fade" id="files">

            </div>
            <div role="tabpanel" class="tab-pane fade" id="blog">

            </div-->
        </div>

    </div>
</div>
