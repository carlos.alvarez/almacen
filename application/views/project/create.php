<div class="card">
    <div class="header">
        <h2><i class="material-icons">add</i><?= lang('method_create') ?> <?= lang('project') ?></h2>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="project-new-form" method="POST">
            <h3>Información Inicial</h3>
            <fieldset>
                <div class="row">
                    <div class="col-sm-9 m-b-0">
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input type="text" class="form-control" name="name" value="<?= set_value('name') ?>" required minlength="4" maxlength="100" autofocus />
                                <label class="form-label">Nombre del Proyecto *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 m-b-0">
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input type="text" class="form-control" name="start_date" value="<?= set_value('start_date', date('m/d/Y')) ?>" required />
                                <label class="form-label">Fecha Inicio *</label>
                            </div>
                        </div>
                    </div>
                    <!--div class="col-sm-3 m-b-0">
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input type="text" class="form-control" name="estimated_end_date" value="<?= set_value('estimated_end_date', date('m/d/Y')) ?>" required />
                                <label class="form-label">Fecha de Finalización Estimada *</label>
                            </div>
                        </div>
                    </div-->
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <textarea name="description" rows="2" class="form-control" required minlength="10" maxlength="250"></textarea>
                                <label class="form-label"><?= lang('description') ?> *</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 m-b-0">
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <?= form_dropdown('client_id', array(''=>lang('choose_an_option'))+array_object_column($clients, 'name', 'id'), set_value('client_id'), 'class="form-control ms" required') ?>
                                <label class="form-label"><?= lang('client') ?> *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 m-b-0">
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <select name="company_id" class="form-control ms" required>
                                    <option value=""><?= lang('choose_an_option') ?></option>
                                    <?php foreach ( $companies as $company ): ?>
                                        <option value="<?= $company->id ?>"><?= $company->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label class="form-label"><?= lang('company') ?> *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 m-b-0">
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <select name="manager_id" class="form-control ms" required>
                                    <option value=""><?= lang('choose_an_option') ?></option>
                                    <?php foreach( $companies as $company ):?>
                                        <optgroup label="<?= $company->name ?>">
                                        <?php foreach ($employees as $employee): ?>
                                            <?php if ($employee->company_id === $company->id): ?>
                                            <option value="<?= $employee->user_id ?>" data-company="<?= $employee->company_id ?>"><?= $employee->full_name ?></option>
                                            <?php endif;?>
                                        <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach;?>
                                </select>
                                <label class="form-label"><?= lang('manager') ?> *</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 m-b-0">
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <?= form_dropdown('invoice_company_id', array(''=>lang('choose_an_option'))+array_object_column($companies, 'name', 'id'), set_value('invoice_company_id'), 'class="form-control ms" required') ?>
                                <label class="form-label"><?= lang('invoiced_by') ?> *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 m-b-0">
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <select name="client_level_visibility" id="client_level_visibility" class="form-control ms" required>
                                    <option value="stage">Fases</option>
                                    <option value="task">Tareas</option>
                                </select>
                                <label class="form-label">Nivel de visibilidad del cliente *</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 m-b-0">
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input type="text" name="tags" class="form-control">
                                <label class="form-label">Etiquetas</label>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <h3>Plantillas</h3>
            <fieldset>
                <div class="row">
                    <div class="col-sm-8 m-b-0">
                        <h4 class="card-title">Categorías de plantillas</h4><br>
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <?= form_dropdown('project_category_id', $categories, set_value('project_category_id'), 'class="form-control ms" required') ?>
                                <label class="form-label"><?= lang('category') ?> *</label>
                            </div>
                        </div>
                    </div>
                    <!--div class="col-sm-4 m-b-0"><br><br><br>
                        <div class="form-group form-float">
                            <input type="checkbox" class="filled-in chk-col-teal" id="budget" value="yes" name="budget">
                            <label for="budget" class="form-label">Presupuesto de ventas</label>
                        </div>
                    </div-->
                    <div class="col-sm-6">
                        <h4 class="card-title">Plantillas disponibles <span id="title-category"></span></h4>
                        <div class="dd" id="available-templates">
                            <ol class="dd-list">
                            </ol>
                        </div>
                        <div id="loader-template" style="display: none;">
                            <div class="preloader">
                                <div class="spinner-layer pl-red">
                                    <div class="circle-clipper left">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="circle-clipper right">
                                        <div class="circle"></div>
                                    </div>
                                </div>
                            </div>
                            <p>Cargando plantillas...</p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h4 class="card-title">
                            Plantillas a utilizar
                            <small>
                                Arrastre al menos una plantilla.
                            </small>
                        </h4>
                        <div class="dd" id="template-list">
                            <div class="dd-empty"></div>
                        </div>
                        <input type="hidden" name="templates" value="" required>
                    </div>
                </div>
            </fieldset>

            <h3>Acoplamientos</h3>
            <fieldset>
                <div id="coupling"></div>
            </fieldset>

            <h3>Personal</h3>
            <fieldset>
                <div id="personal"></div>
            </fieldset>
        </form>

        <div class="row" id="loanding-project-new" style="display: none;">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="loader text-center">
                    <div class="preloader pl-size-xl">
                        <div class="spinner-layer pl-red">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                    <p>Creando proyecto, por favor espere...</p>
                </div>
            </div>
        </div> <!-- #loanding-project-new -->
    </div>
</div>

<div class="row" id="coupling-model" style="display: none;">
    <div class="col-sm-2">
        <a href="javascript:void(0);" class="former" data-toggle="tooltip" data-original-title="FORMER" data-placement="right"></a> - <a href="javascript:void(0);" class="latter" data-toggle="tooltip" data-original-title="LATTER" data-placement="right"></a>
    </div>
    <div class="col-sm-4 m-b-0">
        <div class="form-group form-float">
            <div class="form-line focused">
                <select class="form-control ms former" required>
                    <option value=""><?= lang('choose_an_option') ?></option>
                </select>
                <label class="form-label">Tarea *</label>
            </div>
        </div>
    </div>
    <div class="col-sm-4 m-b-0">
        <div class="form-group form-float">
            <div class="form-line focused">
                <select class="form-control ms latter" required>
                    <option value=""><?= lang('choose_an_option') ?></option>
                </select>
                <label class="form-label">Tarea *</label>
            </div>
        </div>
    </div>
    <div class="col-sm-2 m-b-0">
        <div class="form-group form-float">
            <div class="form-line focused">
                <select class="form-control ms concurrency" required>
                    <option value="start">Inicio</option>
                    <option value="end" selected>Fin</option>
                </select>
                <label class="form-label">Concurrencia *</label>
            </div>
        </div>
    </div>
</div>

