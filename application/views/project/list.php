<div class="card">
    <div class="header">
        <h2><i class="material-icons">list</i> <?= lang('cont_project') ?></h2>
        <ul class="header-dropdown">
            <li>
                <div class="switch">
                    <label>Ver Projectos Atrasados <input type="checkbox" id="delayed_projects_filter"><span class="lever switch-col-red"></span></label>
                </div>
            </li>
            <li>
                <a href="<?= base_url('project/create') ?>" class="btn btn-info btn-xs <?= grant_show('project', 'create') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_create') ?>">
                    <i class="material-icons col-white">add</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <table id="projects-table" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
            <thead>
            <tr>
                <th>
<!--                    <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">-->
                    <input type="checkbox" id="check-all" class="filled-in chk-col-teal">
                    <label class="m-b-0" for="check-all"></label>
                </th>
                <th class="all">ID</th>
                <th class="all"><?= lang('name') ?></th>
                <th class="all" data-toggle="tooltip" title="Fecha de Inicio">F.Inicio</th>
                <th class="all" data-toggle="tooltip" title="Fecha Estimada">F.Estimada</th>
                <th class="all" data-toggle="tooltip" title="Fecha Fin">F.Fin</th>
                <th class="all" data-toggle="tooltip" title="Tiempo Estimado">T.</th>
                <th class="all">Estado</th>
                <th class="all" data-toggle="tooltip" title="Estado Interno">E.Interno</th>
                <th class="all">Causa</th>
                <th class="all">Cliente</th>
                <th class="all">Empresa</th>
                <th class="all">Responsable</th>
                <th class="all">Etiquetas</th>
                <th><?= lang('options') ?></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
