<div class="card">
    <div class="header">
        <h2><i class="material-icons">add_to_photos</i> Añadir Plantilla a Proyecto</h2>
        <!--ul class="header-dropdown">
            <li>
                <a href="<?= base_url('project/create') ?>" class="btn btn-info btn-xs <?= grant_show('project', 'create') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_create') ?>">
                    <i class="material-icons col-white">add</i>
                </a>
            </li>
        </ul-->
    </div>

    <div class="body">
        <form id="project-new-form" data-id="<?= $project->id ?>" method="POST">
            <h3>Plantillas</h3>
            <fieldset>
                <div class="row">
                    <div class="col-sm-8 m-b-0">
                        <h4 class="card-title">Categorías de plantillas</h4><br>
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <?= form_dropdown('project_category_id', $categories, set_value('project_category_id', $project->project_category_id), 'class="form-control ms" required') ?>
                                <label class="form-label"><?= lang('category') ?> *</label>
                            </div>
                        </div>
                    </div>
                    <!--div class="col-sm-4 m-b-0"><br><br><br>
                        <div class="form-group form-float">
                            <input type="checkbox" class="filled-in chk-col-teal" id="budget" value="yes" name="budget">
                            <label for="budget" class="form-label">Presupuesto de ventas</label>
                        </div>
                    </div-->
                    <div class="col-sm-6">
                        <h4 class="card-title">Plantillas disponibles <span id="title-category"></span></h4>
                        <div class="dd" id="available-templates">
                            <ol class="dd-list">
                            </ol>
                        </div>
                        <div id="loader-template" style="display: none;">
                            <div class="preloader">
                                <div class="spinner-layer pl-red">
                                    <div class="circle-clipper left">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="circle-clipper right">
                                        <div class="circle"></div>
                                    </div>
                                </div>
                            </div>
                            <p>Cargando plantillas...</p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h4 class="card-title">
                            Plantillas a utilizar
                            <small>
                                Arrastre al menos una plantilla.
                            </small>
                        </h4>
                        <div class="dd" id="template-list">
                            <div class="dd-empty"></div>
                        </div>
                        <input type="hidden" name="templates" value="" required>
                    </div>
                </div>
            </fieldset>

            <h3>Acoplamientos</h3>
            <fieldset>
                <div id="coupling"></div>
            </fieldset>

            <h3>Personal</h3>
            <fieldset>
                <div id="personal"></div>
            </fieldset>
        </form>

        <div class="row" id="loanding-project-new" style="display: none;">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="loader text-center">
                    <div class="preloader pl-size-xl">
                        <div class="spinner-layer pl-red">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                    <p>Creando proyecto, por favor espere...</p>
                </div>
            </div>
        </div> <!-- #loanding-project-new -->
    </div>
</div>

<div class="row" id="coupling-model" style="display: none;">
    <div class="col-sm-2">
        <a href="javascript:void(0);" class="former" data-toggle="tooltip" data-original-title="FORMER" data-placement="right"></a> - <a href="javascript:void(0);" class="latter" data-toggle="tooltip" data-original-title="LATTER" data-placement="right"></a>
    </div>
    <div class="col-sm-4 m-b-0">
        <div class="form-group form-float">
            <div class="form-line focused">
                <select class="form-control ms former" required>
                    <option value=""><?= lang('choose_an_option') ?></option>
                </select>
                <label class="form-label">Tarea *</label>
            </div>
        </div>
    </div>
    <div class="col-sm-4 m-b-0">
        <div class="form-group form-float">
            <div class="form-line focused">
                <select class="form-control ms latter" required>
                    <option value=""><?= lang('choose_an_option') ?></option>
                </select>
                <label class="form-label">Tarea *</label>
            </div>
        </div>
    </div>
    <div class="col-sm-2 m-b-0">
        <div class="form-group form-float">
            <div class="form-line focused">
                <select class="form-control ms concurrency" required>
                    <option value="start">Inicio</option>
                    <option value="end" selected>Fin</option>
                </select>
                <label class="form-label">Concurrencia *</label>
            </div>
        </div>
    </div>
</div>
