<div class="card">
    <div class="header">
        <h2><i class="material-icons">list</i> <?= lang('project') ?></h2>
    </div>
    <input type="hidden" class="hide" name="day" id="day" value="<?= isset($_GET['day']) ? $_GET['day'] : 'all' ?>">
    <input type="hidden" class="hide" name="month" id="month" value="<?= isset($_GET['month']) ? $_GET['month'] : 'all' ?>">
    <input type="hidden" class="hide" name="year" id="year" value="<?= isset($_GET['year']) ? $_GET['year'] : 'all' ?>">
    <input type="hidden" class="hide" name="status" id="status" value="<?= isset($_GET['status']) ? $_GET['status'] : 'all' ?>">
    <input type="hidden" class="hide" name="company" id="company" value="<?= isset($_GET['company']) ? $_GET['company'] : 'all' ?>">
    <input type="hidden" class="hide" name="category" id="category" value="<?= isset($_GET['category']) ? $_GET['category'] : 'all' ?>">
    <div class="body">
        <table id="project-filter-table" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
            <thead>
            <tr>
                <th>
                    <!--                    <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">-->
                    <input type="checkbox" id="check-all" class="filled-in chk-col-teal">
                    <label class="m-b-0" for="check-all"></label>
                </th>
                <th class="all">ID</th>
                <th class="all"><?= lang('name') ?></th>
                <th class="all" data-toggle="tooltip" title="Fecha de Inicio">F.Inicio</th>
                <th class="all" data-toggle="tooltip" title="Fecha Estimada">F.Estimada</th>
                <th class="all" data-toggle="tooltip" title="Fecha Estimada">F.Fin</th>
                <th class="all" data-toggle="tooltip" title="Tiempo Estimado">T.</th>
                <th class="all">Estado</th>
                <th class="all">Cliente</th>
                <th class="all">Empresa</th>
                <th class="all">Responsable</th>
                <th class="all">Etiquetas</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>