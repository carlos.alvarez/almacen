<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i> <?= lang('project') ?>: <?= $project->name ?></h2>
        <ul class="header-dropdown">
            <?php if( (grant_access('project', 'edit') || $project->manager_id === $this->session->userdata('user_id')) && $project->status === 'progreso' ): ?>
            <li>
                <a href="<?= base_url('project/f_edit/').$project->id ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
            <?php endif; ?>
            <?php if( $progress >= 100 && (grant_access('project', 'edit') || $project->manager_id === $this->session->userdata('user_id')) && $project->status === 'progreso' ): ?>
            <li>
                <a id="close_project_btn" data-project="<?= $project->id ?>" href="javascript: void(0);" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Finalizar Proyecto">
                    <i class="material-icons col-white">lock</i>
                </a>
            </li>
            <?php endif; ?>
            <?php if( (grant_access('project', 'edit') || $project->manager_id === $this->session->userdata('user_id')) && $project->status === 'progreso' ): ?>
            <li>
                <a href="javascript: void(0);" data-project="<?= $project->id ?>" id="cancel_project_btn" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Anular Proyecto">
                    <i class="material-icons col-white">close</i>
                </a>
            </li>
            <?php endif; ?>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-sm-12 m-b-0">
                <small><?= lang('description') ?></small>
                <h4><?= $project->description ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 m-b-0">
                <small><?= lang('client') ?></small>
                <h4><?= $project->client ?></h4>
            </div>
            <div class="col-sm-4 m-b-0">
                <small><?= lang('company') ?></small>
                <h4><?= $project->company ?></h4>
            </div>
            <div class="col-sm-4 m-b-0">
                <small><?= lang('invoiced_by') ?></small>
                <h4><?= $project->invoice_company ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 m-b-0">
                <small><?= lang('manager') ?></small>
                <h4><?= $project->manager ?></h4>
            </div>
            <div class="col-sm-4 m-b-0">
                <small><?= lang('start_date') ?></small>
                <h4><?= nice_date($project->start_date) ?></h4>
            </div>
            <div class="col-sm-4 m-b-0">
                <small><?= lang('estimated_end_date') ?></small>
                <h4><?= nice_date($project->estimated_end_date) ?></h4>
            </div>
            <div class="col-sm-4 m-b-0">
                <small><?= lang('end_date') ?></small>
                <h4><?= nice_date($project->end_date) ?></h4>
            </div>
            <div class="col-sm-4 m-b-0">
                <small><?= lang('status') ?></small>
                <h4><?= strtoupper($project->status) ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 m-b-0">
                <small>Estado Interno</small>
                <h4><?= $project->stage_status ?></h4>
            </div>
            <div class="col-sm-8 m-b-0">
                <small>Causa</small>
                <h4><?= $project->reason ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <small>Etiquetas</small><br>
                <?php foreach ($tags as $tag): ?>
                    <span class="badge bg-cyan"><?= $tag ?></span>
                <?php endforeach; ?>
            </div>
            <!--div class="col-sm-6">
                <small>Progreso</small><br>
                <h4><?= $progress ?>%</h4>
            </div-->
        </div>
    </div>
</div>

<div class="card">
    <div class="body">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#gantter-panel" data-toggle="tab">
                    <span class="col-grey"><i class="material-icons">event_note</i></span> <span>Gantter</span>
                </a>
            </li>
            <li role="presentation" class="<?= ($project->budget === 'yes') ? '' : 'hide'?>">
                <a href="#dashboard-panel" data-toggle="tab">
                    <span class="col-grey"><i class="material-icons">insert_chart</i></span> <span>Tablero</span>
                </a>
            </li>
            <li role="presentation">
                <a href="#tasks-panel" data-toggle="tab">
                    <span class="col-grey"><i class="material-icons">playlist_add_check</i></span>  <span><?= lang('cont_task') ?></span>
                </a>
            </li>
            <li role="presentation">
                <a href="#chat-panel" data-toggle="tab">
                    <span class="col-grey"><i class="material-icons">chat</i></span>  <span>Chat</span>
                </a>
            </li>
            <li role="presentation">
                <a href="#files-panel" data-toggle="tab">
                    <span class="col-grey"><i class="material-icons">insert_drive_file</i></span> <span>Archivos</span>
                </a>
            </li>
            <li role="presentation" <?= ($project->budget === 'yes') ? '' : 'class="hide"'?>>
                <a href="#brand-panel" id="brand_tab_header" data-toggle="tab">
                    <span class="col-grey"><i class="material-icons">monetization_on</i></span> <span>Presupuesto Ventas</span>
                </a>
            </li>
            <li role="presentation">
                <a href="#logs-panel" id="log_tab_header" data-toggle="tab">
                    <span class="col-grey"><i class="material-icons">history</i></span> <span>Logs</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in" id="dashboard-panel">
                <!-- Pie Chart -->
                <div class="card m-t-5">
                    <div class="header align-center">
                        <h2>Tareas por Fase</h2>
                    </div>
                    <br>
                    <div class="row clearfix">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div id="load-stage_task" class="text-center">
                                <div class="preloader">
                                    <div class="spinner-layer pl-red">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div>
                                <p>Cargando gr&aacute;fica...</p>
                            </div>
                            <canvas id="stage_task_chart" height="150"></canvas>
                        </div>
                    </div>
                </div>
                <div class="card m-t-5">
                    <div class="header align-center">
                        <h2>Tareas por Estado</h2>
                    </div>
                    <br>
                    <div class="row clearfix">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div id="load-status_task" class="text-center"><br>
                                <div class="preloader">
                                    <div class="spinner-layer pl-red">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div>
                                <p>Cargando gr&aacute;fica...</p>
                            </div>
                            <canvas id="status_task_chart" height="150"></canvas>
                        </div>
                    </div>
                </div>
                <div class="card m-t-5">
                    <div class="header align-center">
                        <h2>Presupuesto por Marca (Monto Estimado)</h2>
                    </div>
                    <br>
                    <div class="row clearfix">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div id="load-estimated-budget" class="text-center">
                                <div class="preloader">
                                    <div class="spinner-layer pl-red">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div>
                                <p>Cargando gr&aacute;fica...</p>
                            </div>
                            <canvas id="estimated-budget-chart" height="150"></canvas>
                        </div>
                    </div>
                </div>
                <div class="card m-t-5">
                    <div class="header align-center">
                        <h2>Presupuesto por Marca (Monto Real)</h2>
                    </div>
                    <br>
                    <div class="row clearfix">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div id="load-actual-budget" class="text-center">
                                <div class="preloader">
                                    <div class="spinner-layer pl-red">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div>
                                <p>Cargando gr&aacute;fica...</p>
                            </div>
                            <canvas id="actual-budget-chart" height="150"></canvas>
                        </div>
                    </div>
                </div>
                <!-- #END# Pie Chart -->
                <!-- Line Chart -->
                <div class="card m-t-5">
                    <div class="header align-center">
                        <h2>Embudo de Ventas</h2>
                    </div>
                    <br>
                    <div class="row clearfix">
                        <div class="col-sm-10 col-sm-offset-1">
                            Año
                            <select id="chart_year" style="display:none">
                                <option value="2017" selected>2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                            </select>&nbsp;&nbsp;&nbsp;
                            Marca
                            <select id="chart_brand">
                                <option value="0" selected>Todas las marcas</option>
                                <?php foreach($brands as $brand): ?>
                                    <option value="<?= $brand->id ?>"><?= $brand->name ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                            <div id="load-sales-funnel" class="text-center">
                                <div class="preloader">
                                    <div class="spinner-layer pl-red">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div>
                                <p>Cargando gr&aacute;fica...</p>
                            </div>
                            <canvas id="sales-funnel-chart" height="100"></canvas>
                        </div>
                    </div>
                </div>
                <!-- #END# Line Chart -->
            </div>

            <div role="tabpanel" class="tab-pane fade in active" style="overflow-x: auto;" id="gantter-panel">
                <div style="position:relative; width: 100%;" data-project_id="<?= $project->id ?>" class="gantt" id="gantter"></div>
            </div>
            <div role="tabpanel" class="tab-pane fade in" id="tasks-panel">
                <div class="row">
                    <div class="col-sm-12 align-right">
                        <div class="switch">
                            <label>Ver Tareas Atrasadas <input type="checkbox" id="delayed_tasks_filter"><span class="lever switch-col-red"></span></label>
                        </div>
                    </div>
                </div>
                <table id="tasks-table" data-project_id="<?= $project->id ?>" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="">
                            <label class="m-b-0" for="check-all"></label>
                        </th>
                        <th class="all">ID</th>
                        <th class="all"><?= lang('project') ?></th>
                        <th class="all"><?= lang('template') ?></th>
                        <th class="all"><?= lang('stage') ?></th>
                        <th class="all"><?= lang('title') ?></th>
                        <th class="all">Estado</th>
                        <th class="all" data-toggle="tooltip" title="Cambia Estado">C.Estado</th>
                        <th class="all" data-toggle="tooltip" title="Estado Interno">E.Interno</th>
                        <th class="all">Causa</th>
                        <th class="all">Progreso</th>
                        <th class="all">Recursos</th>
                        <th class="all" data-toggle="tooltip" title="Fecha de Inicio">F.Inicio</th>
                        <th class="all" data-toggle="tooltip" title="Fecha Estimada">F.Estimada</th>
                        <th class="all" data-toggle="tooltip" title="Fecha de Cierre">F.Cierre</th>
                        <th class="all" data-toggle="tooltip" title="Tiempo Estimado">T.</th>
                        <th class="all">Etiquetas</th>
                        <th class="all" data-toggle="tooltip" title="Visibilidad">Visib.</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane fade in" id="chat-panel">
                <form id="comment-form" method="post" action="javascript: void(0);">
                    <input type="hidden" name="project_id" value="<?= $project->id ?>">
                    <div class="row clearfix">
                        <div class="col-xs-10 col-sm-11">
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="message" class="form-control" placeholder="Escriba un comentario...">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            <a href="javascript:void(0);" id="send-comment" data-toggle="tooltip" title="Enviar"><i class="material-icons">send</i></a>
                            <?php if(grant_access('project', 'edit') || $project->manager_id === $this->session->userdata('user_id')): ?>
                            <a href="javascript:void(0);" id="client-visibility-toggle" data-toggle="tooltip" title="Visibilidad para el cliente">
                                <input type="hidden" name="client_visibility" value="false">
                                <i class="material-icons col-grey">visibility_off</i>
                            </a>
                            <?php else: ?>
                                <input type="hidden" name="client_visibility" value="false">
                            <?php endif;?>
                        </div>
                    </div>
                </form>
                <div id="comments-panel" data-project_id="<?= $project->id ?>"></div>
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="files-panel">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-4 m-b-0 align-right">
                        <div id="file-spinner" class="preloader pl-size-xs" style="display: none;">
                            <div class="spinner-layer pl-cyan">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div>
                                <div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                        <span class="label label-info" id="file-name" style="display: none;"></span>
                        <input type="file" id="project_file" name="project_file" style="display: none;">
                        <a href="javascript:void(0);" class="btn btn-info btn-xs waves-effect" id="file-input" data-toggle="tooltip" data-original-title="Seleccionar Archivo"><i class="material-icons">file_upload</i></a>
                    </div>
                    <div class="col-xs-10 col-sm-7 m-b-0">
                        <div class="form-group">
                            <div class="form-line">
                                <input class="form-control" id="project_file_description" name="project_file_description" placeholder="Escriba una breve descripción del archivo">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-1 m-b-0">
                        <a href="javascript:void(0);" id="save-file" data-project_id="<?= $project->id ?>" data-toggle="tooltip" data-original-title="Subir"><i class="material-icons">send</i></a>
                        <?php if(grant_access('project', 'edit') || $project->manager_id === $this->session->userdata('user_id')): ?>
                            <a href="javascript:void(0);" id="client-visibility-file-toggle" data-toggle="tooltip" title="Visibilidad para el cliente">
                                <input type="hidden" id="client_visibility_file" name="client_visibility" value="false">
                                <i class="material-icons col-grey">visibility_off</i>
                            </a>
                        <?php endif;?>
                    </div>
                </div>
                <table id="files-table" data-project_id="<?= $project->id ?>" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox" id="check-all-file" class="filled-in chk-col-teal" name="check[]" value="">
                            <label class="m-b-0" for="check-all-file"></label>
                        </th>
                        <th class="all">ID</th>
                        <th class="all"><?= lang('name') ?></th>
                        <th class="all"><?= lang('description') ?></th>
                        <th class="all"><?= lang('employee') ?></th>
                        <th class="all"><?= lang('created_at') ?></th>
                        <th class="all"><?= lang('updated_at') ?></th>
                        <th class="all"><?= lang('options') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane fade in" id="brand-panel">

                <div class="row clearfix">
                    <!--div class="col-sm-12 m-b-0 text-right">
                        <a href="/project_brand/create_budget/" class="btn btn-info btn-xs " data-toggle="tooltip" data-original-title="Agregar Marca Presupuesto">
                            <i class="material-icons col-white">add</i>
                        </a>
                    </div-->
                    <div class="col-sm-12 m-b-0">
                        <form class="form-validate" id="budget-new-form" method="post">
                            <input type="hidden" class="hide" id="budget-project-id" value="<?= $project->id ?>" name="project_id">
                            <div class="row">
                                <div class="col-sm-4 m-b-0">
                                    <div class="form-group form-group-sm form-float">
                                        <div class="form-line focused">
                                            <select name="brand_id" class="btn-group bootstrap-select form-control show-tick ms" required id="brand">
                                                <option value="" disabled selected><?= lang('choose_an_option') ?></option>
                                                <?php foreach($brands as $brand):?>
                                                    <option value="<?= $brand->id ?>" <?= $brand->id === set_value('brand_id') ? 'selected' : '' ?>><?= $brand->name ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <label class="form-label"><?= lang('brand') ?> *</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 m-b-0">
                                    <div class="form-group form-group-sm form-float">
                                        <div class="form-line <?= set_value('estimated_amount') != '' ? 'focused':'' ?>">
                                            <input type="text" class="form-control" name="estimated_amount" value="<?= set_value('estimated_amount') ?>" required maxlength="30" />
                                            <label class="form-label"><?= lang('estimated_amount') ?>(US$) *</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 m-b-0">
                                    <div class="form-group form-group-sm form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" name="estimated_date" id="estimated-date" value="<?= set_value('estimated_date', date('d/m/Y')) ?>" required  />
                                            <label class="form-label"><?= lang('estimated_date') ?> *</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 m-b-0 align-right">
                                    <button type="submit" id="budget-submit" class="btn btn-primary waves-effect"><?= lang('submit') ?></button>
                                    <button type="reset" id="budget-reset" class="hide btn btn-default waves-effect">Cancelar</button>
                                </div>
                            </div>
                            <div class="row hide">
                                <div class="col-sm-6 m-b-0">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" name="actual_amount" value="<?= number_format(set_value('actual_amount','0'), 2,'.',',') ?>" required maxlength="30" />
                                            <label class="form-label"><?= lang('actual_amount') ?>(US$) *</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 m-b-0">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" name="actual_date" id="actual-date" value="<?= set_value('actual_date', date('d/m/Y'))?>" required/>
                                            <label class="form-label"><?= lang('actual_date') ?> *</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-10 col-sm-offset-1 m-b-0">
                                <div id="budget-spinner" class="text-center" style="display: none;">
                                    <div class="preloader">
                                        <div class="spinner-layer pl-cyan">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>Creando presupuesto...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body col-sm-12 m-b-0">
                        <table id="brand-list" class="table table-striped table-hover" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><?= lang('brand'); ?></th>
                                <th><?= lang('estimated_amount') ?> (US$)</th>
                                <th><?= lang('estimated_date') ?></th>
                                <th><?= lang('actual_amount') ?> (US$)</th>
                                <th><?= lang('actual_date') ?></th>
                                <th>Opción</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($budgets as $budget): ?>
                                <tr>
                                    <td><?= $budget->id ?></td>
                                    <td><?= $budget->brands_name ?></td>
                                    <td class="text-center">$<span class="estimated-amount" data-amount="<?= $budget->estimated_amount ?>"><?= number_format($budget->estimated_amount, 2,'.',',') ?></span></td>
                                    <td><?= date('d/M/Y', strtotime($budget->estimated_date)) ?></td>
                                    <td class="text-center">$<span class="actual-amount" data-amount="<?= $budget->actual_amount ?>"><?= number_format($budget->actual_amount, 2,'.',',') ?></span></td>
                                    <td><?= date('d/M/Y', strtotime($budget->actual_date)) ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a data-id="<?= $budget->id?>" class="btn btn-info btn-xs waves-effect <?= grant_show('project_brand','edit')?> budget-edit" data-toggle="tooltip" data-original-title="Editar" aria-describedby="tooltip62885"><i class="material-icons">edit</i></a>
                                            <a href="#" class="btn btn-danger btn-xs waves-effect delete-budget <?= grant_show('project_brand', 'delete')?>" data-control="project_budget" data-id="<?= $budget->id ?>" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th><strong>TOTAL</strong></th>
                                    <th class="text-center"><span class="total-estimated">$0</span></th>
                                    <th></th>
                                    <th class="text-center"><span class="total-actual">$0</span> </th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade in" id="logs-panel">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-4 m-b-0 align-right">
                        <div id="log-spinner" class="preloader pl-size-xs" style="display: none;">
                            <div class="spinner-layer pl-cyan">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div>
                                <div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table id="logs-table" data-project_id="<?= $project->id ?>" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox" id="check-all-logs" class="filled-in chk-col-teal" name="check[]" value="">
                            <label class="m-b-0" for="check-all-logs"></label>
                        </th>
                        <th class="all"><?= lang('date') ?></th>
                        <th class="all"><?= lang('message') ?></th>
                        <th class="all"><?= lang('user') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div> <!-- /#logs-panel -->
        </div>
    </div>
</div>

<!-- Modal Dialogs ====================================================================================================================== -->
<!-- Default Size -->
<div class="modal fade" id="budget-edit-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel"><i class="material-icons">edit</i> Editar presupuesto</h4>
            </div>
            <div class="modal-body">
                <form id="budget-edit-form" method="post">
                    <input type="hidden" value="" id="budget-id" name="id">
                    <input type="hidden" value="" id="project-id" name="project_id">
                    <div class="row">
                        <div class="row">
                            <div class="col-sm-4 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <select name="brand_id" class="bootstrap-select form-control show-tick" required id="brand-id-edit">
                                            <?php foreach($brands as $brand):?>
                                                <option value="<?= $brand->id ?>"><?= $brand->name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <label class="form-label"><?= lang('brand') ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" class="form-control" id="estimated-amount-edit" name="estimated_amount" value="" required step=".01" min="0" />
                                        <label class="form-label"><?= lang('estimated_amount') ?> *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="date" class="form-control" id="estimated-date-edit" name="estimated_date" value="" required  />
                                        <label class="form-label"><?= lang('estimated_date') ?> *</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4 m-b-0"></div>
                            <div class="col-sm-4 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" class="form-control" id="actual-amount-edit" name="actual_amount" value="" required step=".01" min="0" />
                                        <label class="form-label"><?= lang('actual_amount') ?> *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="date" class="form-control" id="actual-date-edit" name="actual_date" value="" required/>
                                        <label class="form-label"><?= lang('actual_date') ?> *</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <div id="loader-template" style="display: none;">
                    <div class="preloader">
                        <div class="spinner-layer pl-blue">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                    <p>Guardando presupuesto...</p>
                </div>
                <button type="button" class="btn btn-primary waves-effect btn-modal" id="edit-budget"><?= lang('submit') ?></button>
                <button type="button" class="btn btn-link waves-effect btn-modal" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>