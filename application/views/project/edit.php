<div class="card">
    <div class="header">
        <h2><i class="material-icons">edit</i><?= lang('method_edit') ?> <?= lang('project') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('project/view/').$project->id ?>" class="btn btn-info btn-xs <?= grant_show('project', 'view') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_view') ?>">
                    <i class="material-icons col-white">search</i>
                </a>
            </li>
            <li>
                <a href="<?= base_url('project/add_template/').$project->id ?>" class="btn btn-info btn-xs <?= grant_show('project', 'edit') ?>" data-toggle="tooltip" data-original-title="Añadir Plantilla">
                    <i class="material-icons col-white">add_to_photos</i>
                </a>
            </li>
        </ul>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="project-edit-form" class="form-validate" method="post">
            <div class="row">
                <div class="col-sm-6 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="name" value="<?= set_value('name', $project->name) ?>" required minlength="4" maxlength="100" autofocus />
                            <label class="form-label"><?= lang('name') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="start_date" value="<?= set_value('start_date', date('d/m/Y', strtotime($project->start_date))) ?>" />
                            <label class="form-label"><?= lang('start_date') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="estimated_end_date" value="<?= set_value('estimated_end_date', date('d/m/Y', strtotime($project->estimated_end_date))) ?>" />
                            <label class="form-label"><?= lang('estimated_end_date') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <textarea name="description" class="form-control" rows="2" required minlength="10"><?= set_value('description', $project->description) ?></textarea>
                            <label class="form-label"><?= lang('description') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <?= form_dropdown('client_id', array(''=>lang('choose_an_option'))+array_object_column($clients, 'name', 'id'), set_value('client_id', $project->client_id), 'class="form-control" data-live-search="true" required') ?>
                            <label class="form-label"><?= lang('client') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <select name="company_id" class="form-control ms" required>
                                <option value=""><?= lang('choose_an_option') ?></option>
                                <?php foreach ( $companies as $company ): ?>
                                    <option value="<?= $company->id ?>" <?= $company->id === $project->company_id ? 'selected' : '' ?> ><?= $company->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('company') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <select name="manager_id" class="form-control" data-live-search="true" required>
                                <option value=""><?= lang('choose_an_option') ?></option>
                                <?php foreach ($employees as $employee): ?>
                                    <option value="<?= $employee->user_id ?>" data-company="<?= $employee->company_id ?>" <?= $employee->user_id === $project->manager_id ? 'selected' : '' ?> ><?= $employee->full_name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('manager') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <?= form_dropdown('invoice_company_id', array(''=>lang('choose_an_option'))+array_object_column($companies, 'name', 'id'), set_value('invoice_company_id', $project->invoice_company_id), 'class="form-control ms" required') ?>
                            <label class="form-label"><?= lang('invoiced_by') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <?= form_dropdown('project_category_id', $categories, set_value('project_category_id', $project->project_category_id), 'class="form-control ms" required') ?>
                            <label class="form-label"><?= lang('category') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" name="tags" value="<?= set_value('tags', $tags) ?>" class="form-control">
                            <label class="form-label">Etiquetas</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="header">
        <h2><i class="glyphicon glyphicon-tasks col-green"></i> <?= lang('cont_task') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('task/f_create?project_id='.$project->id) ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-original-title="<?= lang('method_create') ?> Tarea">
                    <i class="material-icons col-white">add</i>
                </a>
            </li>
        </ul>
    </div>
    <div class="body">
        <table id="tasks-edit-table" data-project_id="<?= $project->id ?>" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="">
                    <label class="m-b-0" for="check-all"></label>
                </th>
                <th class="all">ID</th>
                <th class="all"><?= lang('stage') ?></th>
                <th class="all"><?= lang('title') ?></th>
                <th class="all">Estado</th>
                <th class="all" data-toggle="tooltip" title="Cambia Estado">C.Estado</th>
                <th class="all" data-toggle="tooltip" title="Estado Interno">E.Interno</th>
                <th class="all">Causa</th>
                <th class="all">Progreso</th>
                <th class="all">Recursos</th>
                <th class="all" data-toggle="tooltip" title="Fecha de Inicio">F.Inicio</th>
                <th class="all" data-toggle="tooltip" title="Fecha Estimada">F.Estimada</th>
                <th class="all" data-toggle="tooltip" title="Fecha de Finalización">F.Finaliz.</th>
                <th class="all" data-toggle="tooltip" title="Visibilidad">Visib.</th>
                <th><?= lang('options') ?></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
