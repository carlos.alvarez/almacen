<div class="card">
    <div class="header">
        <h2><i class="material-icons">edit</i><?= lang('method_edit') ?> <?= lang('client_category') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('client_category/view/').$client_category->id ?>" class="btn btn-info btn-xs <?= grant_show('client_category', 'view') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_view') ?>">
                    <i class="material-icons col-white">search</i>
                </a>
            </li>
        </ul>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="client_category-edit-form" class="form-validate" method="post">
            <div class="row">
                <div class="col-sm-4 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="name" value="<?= set_value('name', $client_category->name) ?>" required minlength="4" maxlength="30" />
                            <label class="form-label"><?= lang('name') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="description" value="<?= set_value('description', $client_category->description) ?>" minlength="4" maxlength="250" />
                            <label class="form-label"><?= lang('description') ?></label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
