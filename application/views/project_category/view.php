<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('project_category') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('project_category/edit/').$project_category->id ?>" class="btn btn-info btn-xs <?= grant_show('project_category', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-sm-12">
                <h2><?= $project_category->name ?></h2>
                <span class="col-grey"><?= $project_category->description ?></span>
            </div>
        </div>
    </div>
</div>
