<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('area') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('area/edit/').$area->id ?>" class="btn btn-info btn-xs <?= grant_show('area', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-sm-4">
                <small><?= lang('name') ?></small>
                <h4><?= $area->name ?></h4>
            </div>
            <div class="col-sm-5">
                <small><?= lang('short_name') ?></small>
                <h4><?= $area->short_name ?></h4>
            </div>
            <div class="col-sm-3 align-right">
                <div class="switch">
                    <label><?= lang('active') ?> <input type="checkbox" value="true" <?= ($area->active === 'true') ? 'checked' : '' ?> disabled ><span class="lever switch-col-green"></span></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <small><?= lang('company') ?></small>
                <h4><?= $area->company ?></h4>
            </div>
            <div class="col-sm-4">
                <small><?= lang('department') ?></small>
                <h4><?= $area->department ?></h4>
            </div>
            <div class="col-sm-4">
                <small><?= lang('manager') ?></small>
                <h4><?= ($manager) ? $manager->full_name : '' ?></h4>
            </div>
        </div>
    </div>
</div>
