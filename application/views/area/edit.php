<div class="card">
    <div class="header">
        <h2><i class="material-icons">edit</i><?= lang('method_edit') ?> <?= lang('area') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('area/view/').$area->id ?>" class="btn btn-info btn-xs <?= grant_show('area', 'view') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_view') ?>">
                    <i class="material-icons col-white">search</i>
                </a>
            </li>
        </ul>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="area-edit-form" class="form-validate" method="post">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="name" value="<?= set_value('name', $area->name) ?>" required />
                            <label class="form-label"><?= lang('name') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="short_name" value="<?= set_value('short_name', $area->short_name) ?>" required />
                            <label class="form-label"><?= lang('short_name') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 align-right">
                    <div class="switch">
                        <input type="hidden" name="active" value="false">
                        <label><?= lang('active') ?> <input type="checkbox" name="active" value="true" <?= set_checkbox('active', 'true', ($area->active === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <select class="btn-group bootstrap-select form-control show-tick" name="company_id" required>
                                <option value=""><?= lang('choose_an_option') ?></option>
                                <?php foreach ($companies as $element): ?>
                                    <option value="<?= $element->id ?>" <?= set_select('company_id', $element->id, ($area->company_id === $element->id)) ?> data-subtext="[<?= $element->short_name ?>]"><?= $element->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('company') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <select class="btn-group bootstrap-select form-control show-tick" name="department_id" required>
                                <option value=""><?= lang('choose_an_option') ?></option>
                                <?php foreach ($departments as $element): ?>
                                    <option data-company="<?= $element->company_id ?>" value="<?= $element->id ?>" <?= set_select('department_id', $element->id, ($area->department_id === $element->id)) ?> style="<?= ($area->company_id === $element->company_id) ? '' : 'display: none;' ?>" data-subtext="[<?= $element->short_name ?>]"><?= $element->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('department') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <select class="btn-group bootstrap-select form-control show-tick" data-live-search="true" name="manager_id">
                                <option value=""><?= lang('choose_an_option') ?></option>
                                <?php foreach ($employees as $element): ?>
                                    <option value="<?= $element->user_id ?>" <?= set_select('manager_id', $element->user_id, ($area->manager_id === $element->user_id)) ?>><?= $element->full_name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label class="form-label"><?= lang('manager') ?></label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 align-center">
                    <button type="submit" class="btn btn-primary waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
