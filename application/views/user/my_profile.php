<div class="card">
    <div class="header">
        <h2><i class="material-icons">account_box</i><?= ucwords(lang('my_profile')) ?></h2>
        <ul class="header-dropdown">
            <li>
                <div class="switch js-push-btn">
                    <label><span class="pushText"></span><input type="checkbox" class="pushButton" checked><span class="lever"></span></label>
                </div>
            </li>
        </ul>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="myProfile-edit-form" method="post" enctype="multipart/form-data">

            <div class="row">
                <div class="col-lg-2 col-md-2 col-md-offset-0 col-sm-3 col-sm-offset-5 col-xs-5 col-xs-offset-4">
                    <div class="avatar" style="background-image: url('<?= img_src("public/img/users/{$user->id}.jpg") ?>')">
                        <a class="avatar-edit" href="javascript:void(0);"><i class="material-icons">edit</i></a>
                        <input class="avatar-input" type="file" accept=".jpg,.jpeg" name="picture_file" value="">
                    </div>
                </div>

                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 ">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line focused disabled">
                                    <input type="text" disabled class="form-control" name="first_namen" value="<?= $user->first_name ?>" autofocus required minlength="2" maxlength="100" />
                                    <label class="form-label"><?= lang('first_name') ?> *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line focused disabled">
                                    <input type="text" disabled class="form-control" name="last_namen" value="<?= $user->last_name ?>" required minlength="2" maxlength="100" />
                                    <label class="form-label"><?= lang('last_name') ?> *</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line focused">
                                    <input type="email" class="form-control" name="email" value="<?= set_value('email', $user->email) ?>" required maxlength="250" />
                                    <label class="form-label"><?= lang('email') ?> *</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line focused disabled">
                                    <input type="text" disabled class="form-control" name="usernamen" value="<?= $user->username ?>" required minlength="4" maxlength="30" />
                                    <label class="form-label"><?= lang('username') ?> *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" id="password" minlength="4" maxlength="72" />
                                    <label class="form-label"><?= lang('password') ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line">
                                    <input type="password" class="form-control" name="repeat_password" minlength="4" maxlength="72" />
                                    <label class="form-label"><?= lang('repeat_password') ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 align-center">
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>

</div>