<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('user') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('user/edit/').$user->id ?>" class="btn btn-info btn-xs <?= grant_show('user', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-sm-4">
                <h2><?= $user->username ?></h2>
                <span class="col-grey"><a href="<?= base_url("person/view/{$user->id}") ?>"><?= "{$user->first_name} {$user->last_name}" ?></a></span>
                <br><span class="col-grey"><?= $user->email ?></span>
            </div>
            <div class="col-sm-5">
                <h4><?= lang('cont_role') ?></h4>
                <ul class="list-unstyled">
                    <?php foreach ($user_roles as $role): ?>
                        <li>
                            <span class="col-grey"><i class="material-icons font-14">face</i></span>
                            <?= $role->role ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-sm-3 align-right">
                <div class="switch">
                    <label><?= lang('active') ?> <input type="checkbox" value="true" <?= ($user->active === 'true') ? 'checked' : '' ?> disabled ><span class="lever switch-col-green"></span></label>
                </div>
            </div>
        </div>
    </div>
</div>
