<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Iniciar Sesión | <?= $this->config->item('app_name'); ?></title>
    <!-- Favicon-->
    <link href="<?= base_url('favicon.ico') ?>" rel="icon" type="text/css">

    <!-- Google Fonts -->
    <link href="<?= base_url('public/fonts/roboto_regular/stylesheet.css') ?>" rel="stylesheet">
    <link href="<?= base_url('public/fonts/material_icons/stylesheet.css') ?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Parisienne" rel="stylesheet">

    <!-- Bootstrap Core Css -->
    <link href="<?= base_url('public/plugins/bootstrap/css/bootstrap.css') ?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= base_url('public/plugins/node-waves/waves.css') ?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= base_url('public/plugins/animate-css/animate.css') ?>" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= base_url('public/css/style.css') ?>" rel="stylesheet">
    <link href="<?= base_url(resource_link('public/css/custom.css')) ?>" rel="stylesheet">

    <style>
        html, body {
            height: 100%;
        }

        .login-carousel {
            height: 100%;
        }

        .login-carousel .carousel-inner {
            height: 100%;
        }

        .item {
            height: 100%;
            overflow: hidden;
            position: relative;
        }

        .item img {
            vertical-align: middle;
            min-height: 100%;
            min-width: 100%;
            object-fit: cover;
            position: absolute;
            top: 0;
            bottom: 0;
            margin: auto;
        }

        .login-header {
            position: absolute;
            left: 15px;
            top: 0;
            padding: 20px 20px 0px 20px;
            width: 100%;
            /*border-bottom: 1px solid #343434 !important;*/
        }

        .logo-container img {
            width: 100%;
        }

        .login-button {
            position: absolute;
            right: 0px;
            bottom: 0px;
        }

        .login-button .btn-group {
            box-shadow: none !important;
            width: 100%;
        }
        .login-button .btn-group button {
            border-radius: 0px !important;
            float: right;
        }

        .login-button .btn-group .dropdown-menu {
            width: 100%;
            top: 78px;
        }
    </style>

</head>

<body>
<div id="myCarousel" class="carousel slide login-carousel" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php foreach ( $images as $key => $image ): ?>
        <li data-target="#myCarousel" data-slide-to="<?= $key ?>" class="<?= ($key == 0) ? 'active' : '' ?>"></li>
        <?php endforeach; ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox"  >
        <?php foreach ( $images as $key => $image ): ?>
        <div class="item <?= ($key == 0) ? 'active' : '' ?>">
            <img src="/public/img/gallery/<?= $image ?>" alt="<?= $this->config->item('app_name'); ?>">
        </div>
        <?php endforeach; ?>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Anterior</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Siguiente</span>
    </a>
</div>

<div class="row login-header">
    <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4 logo-container">
        <img src="/public/img/logo.png" alt="<?= $this->config->item('app_name'); ?>">
    </div>
    <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12 align-right login-button">
        <div class="btn-group">
            <button id="open-login" type="button" class="btn btn-lg waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: #343434; color: white;">
                <i class="material-icons">keyboard_arrow_down</i>
            </button>
            <button id="open-login2" type="button" class="btn btn-success btn-lg waves-effect">
                Acceder al portal
                <i class="material-icons">exit_to_app</i>
            </button>
            <div class="dropdown-menu" style="padding: 20px;">
                <!-- Mesages -->
                <?php if ( isset($_SESSION['alerts']) ): ?>
                    <?php foreach ( $_SESSION['alerts'] as $alert ): ?>
                        <div class="alert alert-<?= $alert['type'] ?> alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <?= $alert['message'] ?>
                        </div>
                    <?php endforeach; ?>
                    <?php unset($_SESSION['alerts']); ?>
                <?php endif; ?>
                <!-- /Mesages -->
                <?php if ( validation_errors() ): ?>
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?= validation_errors() ?>
                    </div>
                <?php endif; ?>
                <form id="log-in-form" method="post">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="material-icons">person</i></span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="username" placeholder="<?= lang('username') ?>" required autofocus minlength="4" value="<?= set_value('username') ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="material-icons">lock</i></span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>" required minlength="4">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="align-center">
                        <button class="btn btn-block bg-cyan waves-effect" type="submit"><?= lang('login') ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row" style="position: absolute; bottom: 0; right: 15px; width: 100%;">
    <div class="col-sm-6 col-xs-12 align-left">
        <span style="font-size: 12px; color: white; bottom: 0px;">Copyright © <span id="year"><?= date('Y') ?></span>. Powered by <a>DTE</a>.</span>
    </div>
    <div class="col-sm-6 col-xs-12 align-right">
        <!--span style="font-family: 'Parisienne', cursive; font-size: 30px; color: white;"><span class="glyphicon glyphicon-exclamation-sign" style="color: darkorange;"></span> Extranet Corporativa</span-->
    </div>
</div>

<!-- Jquery Core Js -->
<script src="<?= base_url('public/plugins/jquery/jquery.min.js') ?>"></script>

<!-- Bootstrap Core Js -->
<script src="<?= base_url('public/plugins/bootstrap/js/bootstrap.js') ?>"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?= base_url('public/plugins/node-waves/waves.js') ?>"></script>

<!-- Bootstrap Notify Js -->
<script src="<?= base_url('public/plugins/bootstrap-notify/bootstrap-notify.js') ?>"></script>

<!-- Custom Js -->
<script src="<?= base_url(resource_link('public/js/admin.js')) ?>"></script>
<script src="<?= base_url(resource_link('public/js/src/app.js')) ?>"></script>

<!-- Validation Plugin Js -->
<script src="<?= base_url('public/plugins/jquery-validation/jquery.validate.js') ?>"></script>
<script src="<?= base_url('public/plugins/jquery-validation/localization/messages_es.js') ?>"></script>

<script type="text/javascript">
    function load_captcha() {
        $('#refresh-captcha').find('i.material-icons').addClass('animated rotateIn infinite');
        $.get('/user/get_captcha', function (json) {
            var obj = JSON.parse(json);

            if (obj.error) {
                notification(obj.message, 'danger');
            } else {
                $('#captcha-img').attr('src', '/public/img/captchas/'+obj.data);
            }

            $('#refresh-captcha').find('i.material-icons').removeClass('animated rotateIn infinite');
        });
        setTimeout(function(){$('#refresh-captcha').click();},480000); // 480,000 Ms = 8 Min
    }

    load_captcha();

    $('#refresh-captcha').on('click', function () {
        load_captcha();
    });

    $(document).on('click', '.login-button .btn-group .dropdown-menu', function (e) {
        e.stopPropagation();
    });

    $('#log-in-form').validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.input-group').append(error);
        }
    });

    $('#open-login2').click(function (event) {
        event.stopPropagation();
        $('#open-login').click();
    });

    <?php if( set_value('username') ): ?>
    $('#open-login').click();
    <?php endif; ?>

</script>

</body>

</html>
