<div class="card">
    <div class="header">
        <h2><i class="material-icons">add</i><?= lang('method_create') ?> <?= lang('user') ?></h2>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="user-new-form" method="post" enctype="multipart/form-data">

            <div class="row">
                <div class="col-lg-2 col-md-2 col-md-offset-0 col-sm-3 col-sm-offset-5 col-xs-5 col-xs-offset-4">
                    <div class="avatar" style="background-image: url('<?= img_src('public/img/users/default.jpg') ?>')">
                        <a class="avatar-edit" href="javascript:void(0);"><i class="material-icons">edit</i></a>
                        <input class="avatar-input" type="file" accept=".jpg,.jpeg" name="picture_file" value="">
                    </div>
                </div>

                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12 ">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line <?= set_value('first_name') ? 'focused' : '' ?>">
                                    <input type="text" class="form-control" name="first_name" value="<?= set_value('first_name') ?>" autofocus required minlength="2" maxlength="100" />
                                    <label class="form-label"><?= lang('first_name') ?> *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line <?= set_value('last_name') ? 'focused' : '' ?>">
                                    <input type="text" class="form-control" name="last_name" value="<?= set_value('last_name') ?>" required minlength="2" maxlength="100" />
                                    <label class="form-label"><?= lang('last_name') ?> *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12 align-right">
                            <div class="switch">
                                <input type="hidden" name="active" value="false">
                                <label><?= lang('active') ?> <input type="checkbox" name="active" value="true" <?= set_checkbox('active', 'true', TRUE) ?>><span class="lever switch-col-green"></span></label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line <?= set_value('email') ? 'focused' : '' ?>">
                                    <input type="email" class="form-control" name="email" value="<?= set_value('email') ?>" required maxlength="250" />
                                    <label class="form-label"><?= lang('email') ?> *</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line <?= set_value('username') ? 'focused' : '' ?>">
                                    <input type="text" class="form-control" name="username" value="<?= set_value('username') ?>" required minlength="4" maxlength="30" />
                                    <label class="form-label"><?= lang('username') ?> *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" id="password" required minlength="4" maxlength="72" />
                                    <label class="form-label"><?= lang('password') ?> *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group form-float m-b-0">
                                <div class="form-line">
                                    <input type="password" class="form-control" name="repeat_password" required minlength="4" maxlength="72" />
                                    <label class="form-label"><?= lang('repeat_password') ?> *</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-xs-offset-0 col-xs-12">
                    <h5><?= lang('cont_role') ?> *</h5>
                    <div class="form-group form-float m-b-0">
                        <div class="form-line">
                            <select id="user_roles" name="user_roles[]" class="ms" multiple="multiple" required>
                                <?php foreach ($roles as $role): ?>
                                    <option value="<?= $role->id ?>" <?= ($role->active !== 'true') ? 'disabled' : '' ?> <?= in_array($role->id, $post_roles) ? 'selected' : '' ?>><?= $role->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>

        </form>
    </div>

</div>