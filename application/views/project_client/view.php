<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i> <?= lang('project') ?>: <?= $project_client->name ?></h2>
        <ul class="header-dropdown">
            <li>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-sm-12 m-b-0">
                <small><?= lang('description') ?></small>
                <h4><?= $project_client->description ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 m-b-0">
                <small><?= lang('client') ?></small>
                <h4><?= $project_client->client ?></h4>
            </div>
            <div class="col-sm-4 m-b-0">
                <small><?= lang('company') ?></small>
                <h4><?= $project_client->company ?></h4>
            </div>
            <div class="col-sm-4 m-b-0">
                <small><?= lang('invoiced_by') ?></small>
                <h4><?= $project_client->invoice_company ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 m-b-0">
                <small><?= lang('manager') ?></small>
                <h4><?= $project_client->manager ?></h4>
            </div>
            <div class="col-sm-4 m-b-0">
                <small><?= lang('start_date') ?></small>
                <h4><?= nice_date($project_client->start_date) ?></h4>
            </div>
            <div class="col-sm-4 m-b-0">
                <small><?= lang('estimated_end_date') ?></small>
                <h4><?= nice_date($project_client->estimated_end_date) ?></h4>
            </div>
            <div class="col-sm-4 m-b-0">
                <small><?= lang('status') ?></small>
                <h4><?= strtoupper($project_client->status) ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <small>Etiquetas</small><br>
                <?php foreach ($tags as $tag): ?>
                    <span class="badge bg-cyan"><?= $tag ?></span>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="body">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#gantter-panel" data-toggle="tab">
                    <span class="col-grey"><i class="material-icons">event_note</i></span> <span>Gantter</span>
                </a>
            </li>
            <li role="presentation">
                <a href="#chat-panel" data-toggle="tab">
                    <span class="col-grey"><i class="material-icons">chat</i></span>  <span>Chat</span>
                </a>
            </li>
            <li role="presentation">
                <a href="#files-panel" data-toggle="tab">
                    <span class="col-grey"><i class="material-icons">insert_drive_file</i></span> <span>Archivos</span>
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" style="overflow-x: auto;" id="gantter-panel">
                <div style="position:relative; width: 100%;" data-project_id="<?= $project_client->id ?>" class="gantt" id="gantter"></div>
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="chat-panel">
                <form id="comment-form" method="post" action="javascript: void(0);">
                    <input type="hidden" name="project_id" value="<?= $project_client->id ?>">
                    <div class="row clearfix">
                        <div class="col-xs-10 col-sm-11">
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="message" class="form-control" placeholder="Escriba un comentario...">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            <a href="javascript:void(0);" id="send-comment" data-toggle="tooltip" title="Enviar"><i class="material-icons">send</i></a>
                            <input type="hidden" name="client_visibility" value="true">
                        </div>
                    </div>
                </form>
                <div id="comments-panel" data-project_id="<?= $project_client->id ?>"></div>
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="files-panel">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-4 m-b-0 align-right">
                        <div id="file-spinner" class="preloader pl-size-xs" style="display: none;">
                            <div class="spinner-layer pl-cyan">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div>
                                <div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                        <span class="label label-info" id="file-name" style="display: none;"></span>
                        <input type="file" id="project_file" name="project_file" style="display: none;">
                        <a href="javascript:void(0);" class="btn btn-info btn-xs waves-effect" id="file-input" data-toggle="tooltip" data-original-title="Seleccionar Archivo"><i class="material-icons">file_upload</i></a>
                    </div>
                    <div class="col-xs-10 col-sm-7 m-b-0">
                        <div class="form-group">
                            <div class="form-line">
                                <input class="form-control" id="project_file_description" name="project_file_description" placeholder="Escriba una breve descripción del archivo">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-1 m-b-0">
                        <a href="javascript:void(0);" id="save-file" data-project_id="<?= $project_client->id ?>" data-toggle="tooltip" data-original-title="Subir"><i class="material-icons">send</i></a>
                        <input type="hidden" name="client_visibility" value="true">
                    </div>
                </div>
                <table id="files-table" data-project_id="<?= $project_client->id ?>" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox" id="check-all-file" class="filled-in chk-col-teal" name="check[]" value="">
                            <label class="m-b-0" for="check-all-file"></label>
                        </th>
                        <th class="all">ID</th>
                        <th class="all"><?= lang('name') ?></th>
                        <th class="all"><?= lang('description') ?></th>
                        <th class="all"><?= lang('employee') ?></th>
                        <th class="all"><?= lang('created_at') ?></th>
                        <th class="all"><?= lang('updated_at') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
