<?php
    $title = '';
    $crud = array('create' => 'crear','edit' => 'editar','view' => 'ver');
    if ( $this->session->flashdata('window_title') ){
        $title = $this->session->flashdata('window_title');
    } else {
        if ( isset($crud[$this->router->method]) ){
            $title = lang('method_'.$this->router->method).' '.lang('cont_'.$this->router->class);
        } else {
            $title = lang('cont_'.$this->router->class);
        }
    }

    $title = trim($title);
    if ( $title !== '' ) { $title .= ' | '; }
?>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title> <?= $title ?><?= $this->config->item('app_name'); ?></title>
    <!-- Favicon-->
    <link href="<?= base_url('favicon.ico') ?>" rel="icon" type="text/css">

    <!-- Google Fonts -->
    <link href="<?= base_url('public/fonts/roboto_regular/stylesheet.css') ?>" rel="stylesheet">
    <link href="<?= base_url('public/fonts/material_icons/stylesheet.css') ?>" rel="stylesheet">

    <!-- Bootstrap Core Css -->
    <link href="<?= base_url('public/plugins/bootstrap/css/bootstrap.css') ?>" rel="stylesheet">

    <!-- Bootstrap Select -->
    <link href="<?= base_url('public/plugins/bootstrap-select/css/bootstrap-select.min.css') ?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= base_url('public/plugins/node-waves/waves.css') ?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= base_url('public/plugins/animate-css/animate.css') ?>" rel="stylesheet" />

    <!-- SweetAlert -->
    <link href="<?= base_url('public/plugins/sweetalert/sweetalert.css') ?>" rel="stylesheet">

    <!-- Datepicker -->
    <link href="<?= base_url('public/plugins/jquery-datepicker/datepicker.css') ?>" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?= base_url(resource_link('public/css/style.css')) ?>" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= base_url('public/css/themes/theme-blue.min.css') ?>" rel="stylesheet" />

	<?php if ( isset($styles) ): ?>
		<?php foreach ($styles as $style): ?>
			<link href="<?= base_url(resource_link($style)) ?>" rel="stylesheet">
		<?php endforeach; ?>
	<?php endif; ?>

    <link rel="manifest" href="/public/pwa/manifest.json">

	<link href="<?= base_url(resource_link('public/css/custom.css')) ?>" rel="stylesheet">
</head>
