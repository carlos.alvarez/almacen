<!DOCTYPE html>
<html>

<?php $this->load->view('layout/header'); ?>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p><?= lang('please_wait') ?></p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <?php $this->load->view('layout/navbar'); ?>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <?php $this->load->view('layout/sidebar'); ?>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">

            <!-- Mesages -->
            <?php if ( isset($_SESSION['alerts']) ): ?>
                <?php foreach ( $_SESSION['alerts'] as $alert ): ?>
                    <div class="alert alert-<?= $alert['type'] ?> alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?= $alert['message'] ?>
                    </div>
                <?php endforeach; ?>
                <?php unset($_SESSION['alerts']); ?>
            <?php endif; ?>
            <!-- /Mesages -->

            <!-- Content area -->
            <?php if( isset($content) && $content != '' ) { $this->load->view($content); } ?>
            <!-- /Content area -->

        </div>
    </section>

    <?php if ( $this->session->userdata('acceso') == 'advertencia' ): ?>
        <?php $this->session->set_userdata(array('acceso' => 'advertido')); ?>
        <div class="modal modal-message modal-warning fade in" id="modal-mensaje-acceso" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        ADVERTENCIA!
                    </div>
                    <div class="modal-title" style="font-size: 40px;">
                        <i class="fa fa-warning warning"></i>
                    </div>
                    <div class="modal-body">
                        <?= $this->session->userdata('acceso_mensaje') ?>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" class="btn btn-warning" data-dismiss="modal" style="">
                            Cerrar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <!-- Jquery Core Js -->
    <script src="<?= base_url('public/plugins/jquery/jquery.min.js') ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= base_url('public/plugins/bootstrap/js/bootstrap.js') ?>"></script>

    <!-- Bootstrap Notify Js -->
    <script src="<?= base_url('public/plugins/bootstrap-notify/bootstrap-notify.js') ?>"></script>

    <!-- SweetAlert Js -->
    <script src="<?= base_url('public/plugins/sweetalert/sweetalert.min.js') ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?= base_url('public/plugins/bootstrap-select/js/bootstrap-select.js') ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= base_url('public/plugins/jquery-slimscroll/jquery.slimscroll.js') ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= base_url('public/plugins/node-waves/waves.js') ?>"></script>

    <!-- Custom Js -->
    <script src="<?= base_url(resource_link('public/js/admin.js')) ?>"></script>

    <!-- Datepicker -->
    <script src="<?= base_url('public/plugins/jquery-datepicker/datepicker.js') ?>"></script>

    <script src="<?= base_url(resource_link('public/js/src/app.js')) ?>"></script>

	<?php if ( isset($scripts) ): ?>
		<?php foreach ($scripts as $script): ?>
			<script src="<?= base_url(resource_link($script)) ?>"></script>
		<?php endforeach; ?>
	<?php endif; ?>

	<script>
        // Muestra el modal para el mensaje de acceso
        $('#modal-mensaje-acceso').modal('show');
    </script>

</body>

</html>
