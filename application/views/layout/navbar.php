<nav class="navbar">
	<div class="container-fluid">
		<div class="navbar-header">
            <!--button class="btn btn-sm btn-info visible-xs right m-t-10 m-r-10 go_back_button" data-toggle="tooltip" data-original-title="<?= lang('back') ?>" data-placement="bottom"><i class="material-icons">arrow_back</i></button-->
            <a href="javascript:void(0);" class="col-cyan navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
			<a href="javascript:void(0);" class="bars"></a>
			<div class="">
                <a class="navbar-brand" href="<?= base_url() ?>"><?= $this->config->item('app_logo') ?></a>
			</div>
		</div>

        <div class="collapse navbar-collapse" id="navbar-collapse">

            <ol class="breadcrumb breadcrumb-col-cyan navbar-left m-t-20 hidden-xs">
                <li><a href="<?= base_url() ?>"><i class="material-icons">home</i> <?= lang('home'); ?></a></li>
                <?php if ($this->router->class && $this->router->class !== 'welcome'): ?>
                    <li><a href="<?= base_url($this->router->class) ?>"><?= lang($this->router->class) ?></a></li>
                <?php endif; ?>
                <?php if ($this->router->class && $this->router->class !== 'welcome' && $this->router->method !== 'index'): ?>
                    <?php $icon = array('create'=>'add','edit'=>'edit','view'=>'search','my_profile' => 'person'); ?>
                    <li class="active"><i class="material-icons"><?= isset($icon[$this->router->method]) ? $icon[$this->router->method] : '' ?></i><?= lang('method_'.$this->router->method) ?></li>
                <?php endif; ?>
            </ol>

            <!--button class="btn btn-sm btn-info right m-t-20 m-l-40 go_back_button hidden-xs" data-toggle="tooltip" data-original-title="<?= lang('back') ?>" data-placement="bottom"><i class="material-icons">arrow_back</i></button-->
            <ul class="nav navbar-nav navbar-right">
                <!-- Tasks -->
				<!--li class="dropdown">
					<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
						<i class="material-icons">flag</i>
						<span class="label-count">0</span>
					</a>
					<ul class="dropdown-menu">
						<li class="header">TASKS</li>
						<li class="body">
							<ul class="menu tasks">
								<li>
									<a href="javascript:void(0);">
										<h4>
											Loading...
											<small>0%</small>
										</h4>
										<div class="progress">
											<div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
											</div>
										</div>
									</a>
								</li>
							</ul>
						</li>
						<li class="footer">
							<a href="javascript:void(0);">View All Tasks</a>
						</li>
					</ul>
				</li-->
				<!-- #END# Tasks -->
                <li class="hidden-xs">
                    <a href="<?= base_url('user/my_profile') ?>"><i class="material-icons">person</i> <span class="hidden-sm visible-md-inline visible-lg-inline"><?= lang('my_profile') ?></span></a>
                </li>
                <li class="hidden-xs">
                    <a href="<?= base_url('user/logout') ?>"><i class="material-icons">input</i> <span class="hidden-sm visible-md-inline visible-lg-inline"><?= lang('logout') ?></span></a>
                </li>
			</ul>
		</div>
	</div>
</nav>
