<aside id="leftsidebar" class="sidebar">
	<!-- User Info -->
	<div class="user-info" style="background: url(<?= resource_link('/public/img/user-img-background.jpg') ?>) no-repeat no-repeat;">
		<div class="image">
			<img src="<?= img_src('public/img/users/'.$_SESSION['user_id'].'.jpg') ?>" width="48" height="48" alt="<?= $_SESSION['full_name'] ?>" />
		</div>
		<div class="info-container">
			<div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $_SESSION['full_name'] ?></div>
			<div class="email"><?= $_SESSION['email'] ?></div>
			<div class="btn-group user-helper-dropdown visible-xs hidden-sm">
				<i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
				<ul class="dropdown-menu pull-right">
					<li><a href="<?= base_url('user/my_profile') ?>"><i class="material-icons">person</i><?= lang('profile') ?></a></li>
                    <li role="separator" class="divider"></li>
					<li><a href="<?= base_url('user/logout') ?>"><i class="material-icons">input</i><?= lang('logout') ?></a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- #User Info -->
	<!-- Menu -->
	<div class="menu">
		<ul class="list">
            <li>
				<a href="<?= base_url() ?>">
					<i class="material-icons col-cyan">home</i>
					<span><?= lang('home'); ?></span>
				</a>
			</li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block">
                    <i class="material-icons">account_balance</i>
                    <span><?= lang('organization') ?></span>
                </a>
                <ul class="ml-menu">
                    <li class="<?= grant_show('company', 'view') ?>">
                        <a href="<?= base_url('company') ?>">
                            <i class="material-icons col-green">business</i>
                            <span><?= lang('cont_company') ?></span>
                        </a>
                    </li>
                    <li class="<?= grant_show('department', 'view') ?>">
                        <a href="<?= base_url('department') ?>">
                            <i class="material-icons col-brown">business_center</i>
                            <span><?= lang('cont_department') ?></span>
                        </a>
                    </li>
                    <li class="<?= grant_show('area', 'view') ?>">
                        <a href="<?= base_url('area') ?>">
                            <i class="material-icons col-blue">fullscreen</i>
                            <span><?= lang('cont_area') ?></span>
                        </a>
                    </li>
                    <li class="<?= grant_show('position', 'view') ?>">
                        <a href="<?= base_url('position') ?>">
                            <i class="material-icons col-deep-orange">person_outline</i>
                            <span><?= lang('cont_position') ?></span>
                        </a>
                    </li>
                    <li class="<?= grant_show('employee', 'view') ?>">
                        <a href="<?= base_url('employee') ?>">
                            <i class="material-icons col-brown">people</i>
                            <span><?= lang('cont_employee') ?></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block">
                    <i class="material-icons">settings</i>
                    <span><?= lang('user_settings') ?></span>
                </a>
                <ul class="ml-menu">
                    <li class="<?= grant_show('role', 'view') ?>">
                        <a href="<?= base_url('role') ?>">
                            <i class="material-icons col-deep-orange">account_box</i>
                            <span><?= lang('cont_role') ?></span>
                        </a>
                    </li>
                    <li class="<?= grant_show('user', 'view') ?>">
                        <a href="<?= base_url('user') ?>">
                            <i class="material-icons col-light-blue">person</i>
                            <span><?= lang('cont_user') ?></span>
                        </a>
                    </li>
                    <!-- li class="<?= grant_show('module', 'view') ?>">
                        <a href="<?= base_url('module') ?>">
                            <i class="material-icons col-orange">extension</i>
                            <span><?= lang('cont_module') ?></span>
                        </a>
                    </li -->
                    <li class="<?= grant_show('grant', 'view') ?>">
                        <a href="<?= base_url('grant') ?>">
                            <i class="material-icons col-green">security</i>
                            <span><?= lang('cont_grant') ?></span>
                        </a>
                    </li>
                </ul>
            </li>
		</ul>
	</div>
	<!-- #Menu -->
	<!-- Footer -->
	<div class="legal">
		<div class="copyright">
			&copy; <?= date('Y') ?> <a href="<?= $this->config->item('powered_by_url') ?>"><?= $this->config->item('app_name') ?> - <?= $this->config->item('powered_by') ?></a>.
		</div>
        <script type="text/javascript" src="https://cdn.ywxi﻿.net/js/1.js" async></script>
		<!--div class="version">
			<b>Version: </b> 1.0.4
		</div-->
	</div>
	<!-- #Footer -->
</aside>
