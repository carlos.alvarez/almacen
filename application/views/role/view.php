<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('role') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('role/edit/').$role->id ?>" class="btn btn-info btn-xs <?= grant_show('role', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-sm-9 col-xs-12 ">
                <h2><?= $role->name ?></h2>
                <span class="col-grey"><?= $role->description ?></span>
            </div>
            <div class="col-sm-3 col-xs-12 align-right">
                <div class="switch">
                    <label><?= lang('active') ?> <input type="checkbox" value="true" <?= ($role->active === 'true') ? 'checked' : '' ?> disabled ><span class="lever switch-col-green"></span></label>
                </div>
            </div>
        </div>
    </div>
</div>
