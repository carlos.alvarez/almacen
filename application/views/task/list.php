<div class="card">
    <div class="header">
        <h2><i class="material-icons">list</i> <?= lang('cont_task') ?></h2>

        <ul class="header-dropdown">
            <li>
                <div class="switch">
                    <label>Ver Tareas Atrasadas <input type="checkbox" id="delayed_tasks_filter"><span class="lever switch-col-red"></span></label>
                </div>
            </li>
        </ul>
    </div>

    <div class="body">
        <table id="tasks-table" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="">
                    <label class="m-b-0" for="check-all"></label>
                </th>
                <th class="all">ID</th>
                <th class="all"><?= lang('project') ?></th>
                <th class="all"><?= lang('template') ?></th>
                <th class="all"><?= lang('stage') ?></th>
                <th class="all"><?= lang('title') ?></th>
                <th class="all">Estado</th>
                <th class="all" data-toggle="tooltip" title="Cambia Estado">C.Estado</th>
                <th class="all" data-toggle="tooltip" title="Estado Interno">E.Interno</th>
                <th class="all">Causa</th>
                <th class="all">Progreso</th>
                <th class="all">Recursos</th>
                <th class="all" data-toggle="tooltip" title="Fecha de Inicio">F.Inicio</th>
                <th class="all" data-toggle="tooltip" title="Fecha Estimada">F.Estimada</th>
                <th class="all" data-toggle="tooltip" title="Fecha de Cierre">F.Cierre</th>
                <th class="all" data-toggle="tooltip" title="Tiempo Estimado">T.</th>
                <th class="all">Etiquetas</th>
                <th class="all" data-toggle="tooltip" title="Visibilidad">Visib.</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
