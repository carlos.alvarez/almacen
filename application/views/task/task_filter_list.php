<div class="card">
    <div class="header">
        <h2><i class="material-icons">list</i> <?= lang('task') ?></h2>
        <ul class="header-dropdown">
            <li style="margin-right: 15px;">
                <div class="form-group form-float">
                    <div class="form-line focused">
                        <input type="hidden" class="form-control" id="day" name="day" value="<?= isset($_GET['day']) ? $_GET['day'] : 'all' ?>"/>
                        <input type="hidden" class="form-control" id="month" name="month" value="<?= isset($_GET['month']) ? $_GET['month'] : 'all' ?>"/>
                        <input type="hidden" class="form-control" id="project_id" name="project_id" value="<?= isset($_GET['project_id']) ? $_GET['project_id'] : 'all' ?>"/>
                        <input type="hidden" class="form-control" id="stage" name="stage" value="<?= isset($_GET['stage']) ? $_GET['stage'] : 'all' ?>"/>
                        <input type="hidden" class="form-control" id="status" name="status" value="<?= isset($_GET['status']) ? $_GET['status'] : 'all' ?>"/>
                        <input type="hidden" class="form-control" id="brand" name="brand" value="<?= isset($_GET['brand']) ? $_GET['brand'] : 'all' ?>"/>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="body">
        <table id="tasks-filter-table" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="">
                    <label class="m-b-0" for="check-all"></label>
                </th>
                <th class="all">ID</th>
                <th class="all"><?= lang('stage') ?></th>
                <th class="all"><?= lang('title') ?></th>
                <th class="all"><?= lang('description') ?></th>
                <th class="all">Estado</th>
                <th class="all">Progreso</th>
                <th class="all">Fecha Inicio</th>
                <th class="all"><?= lang('estimated_date') ?></th>
                <th class="all">Fecha Cierre</th>
                <th class="all" data-toggle="tooltip" title="Cambia Estado">C.Estado</th>
                <th class="all" data-toggle="tooltip" title="Estado Interno">E.Interno</th>
                <th class="all">Causa</th>
                <th class="all">Visibilidad</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>