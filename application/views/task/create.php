<div class="card">
    <div class="header">
        <h2><i class="material-icons">add</i><?= lang('method_create') ?> <?= lang('task') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="javascript: void(0);" id="toggle-visibility" class="btn btn-default btn-xs" data-toggle="tooltip" data-original-title="No visible para el cliente">
                    <i class="material-icons col-gray right visibility">visibility_off</i>
                </a>
            </li>
            <li>
                <a href="javascript: void(0);" id="toggle-notification" class="btn btn-default btn-xs" data-toggle="tooltip" data-original-title="No notificar al cliente">
                    <i class="material-icons col-gray right notification">notifications_off</i>
                </a>
            </li>
        </ul>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="task-new-form" class="form-validate" method="post">
            <input type="hidden" name="client_visibility" value="false">
            <input type="hidden" name="weight" value="10">
            <input type="hidden" name="client_notification" value="false">
            <div class="row">
                <div class="col-sm-4 m-b-0">
                    <small class="col-grey"><?= lang('project') ?></small>
                    <br>
                    <a href="<?= base_url("project/f_view/{$project->id}") ?>">
                        <strong><?= $project->name ?></strong>
                    </a>
                </div>
                <div class="col-sm-3 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <?= form_dropdown('stage_id', $stages, set_value('stage_id'), 'class="form-control ms" required') ?>
                            <label class="form-label"><?= lang('stage') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="title" value="<?= set_value('title') ?>" required />
                            <label class="form-label"><?= lang('title') ?> *</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <textarea name="description" class="form-control" rows="2"><?= set_value('description') ?></textarea>
                            <label class="form-label"><?= lang('description') ?></label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <select name="status" class="form-control ms">
                                <option value="pendiente" <?= set_select('status', 'pendiente') ?>>PENDIENTE</option>
                            </select>
                            <label class="form-label">Estado *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="hidden" id="start_date" name="start_date" value="<?= set_value('start_date', $project->start_date) ?>">
                            <input type="text" class="form-control" name="start_date_n" value="<?= set_value('start_date_n', date('d/m/Y', strtotime($project->start_date))) ?>" required />
                            <label class="form-label"><?= lang('start_date') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="hidden" id="estimated_end_date" name="estimated_end_date" value="<?= set_value('estimated_end_date', $project->start_date) ?>">
                            <input type="text" class="form-control" name="estimated_end_date_n" value="<?= set_value('estimated_end_date_n', date('d/m/Y', strtotime($project->start_date))) ?>" required />
                            <label class="form-label"><?= lang('estimated_end_date') ?></label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 m-b-0">
                    <div class="row">
                        <div class="col-sm-12 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <select name="prerequisites[]" class="form-control ms" id="prerequisites" multiple>
                                        <?php foreach($tasks as $pre): ?>
                                            <?php if ( $pre->id === $task->id ) { continue; } ?>
                                            <option value="<?= $pre->id ?>" <?= in_array($pre->id, $prerequisites) ? 'selected' : '' ?>><?= $pre->title ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <label class="form-label">Prerequisitos</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <select name="resources[]" class="form-control" id="resources" data-live-search="true" multiple required>
                                        <?php if ( count($employees) > 0 ): ?>
                                            <?php $optgroup = ''; $start = FALSE; ?>
                                            <?php foreach($employees as $employee): ?>
                                                <?php if($start && $optgroup !== $employee->company) { echo '</optgroup>'; $start = FALSE; } ?>
                                                <?php if( !$start && $optgroup !== $employee->company) { echo '<optgroup label="'.$employee->company.'" data-subtext="">'; $optgroup = $employee->company; $start = TRUE; } ?>
                                                <option data-subtext="<?= $employee->position ?>" value="<?= $employee->user_id ?>" <?= in_array($employee->user_id, $resources) ? 'selected' : '' ?>><?= $employee->full_name ?></option>
                                            <?php endforeach; ?>
                                            <?= '</optgroup>' ?>
                                        <?php endif; ?>
                                    </select>
                                    <label class="form-label">Recursos *</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="tags" id="tags" value="<?= $tags ?>" />
                                    <label class="form-label">Etiquetas</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 m-b-0">
                    <div class="card">
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-12 m-b-0">
                                    <div id="requirementsForm">
                                        <!-- Form template-->
                                        <div class="row" id="requirementsForm_template">
                                            <input id="requirementsForm_#index#_id" type="hidden" name="requirements[#index#][id]" class="requirement_id" value="new"/>
                                            <div class="col-sm-11 m-b-0">
                                                <div class="form-group form-group-sm form-float">
                                                    <div class="form-line focused">
                                                        <input id="requirementsForm_#index#_requirement" type="text" class="form-control requirement_requirement" name="requirements[#index#][requirement]" value="" minlength="4" maxlength="250" required/>
                                                        <label class="form-label">Requerimiento *</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 m-b-0">
                                                <a href="javascript:void(0);" id="requirementsForm_remove_current">
                                                    <i class="material-icons col-red">close</i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- /Form template-->

                                        <!-- No forms template -->
                                        <div id="requirementsForm_noforms_template"></div>
                                        <!-- /No forms template-->

                                        <!-- Controls -->
                                        <div id="requirementsForm_controls">
                                            <span id="requirementsForm_add"><a href="javascript:void(0);"><i class="material-icons col-blue">add</i> Agregar Requerimiento</a></span>
                                        </div>
                                        <!-- /Controls -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h5><i class="material-icons font-18 col-yellow">flash_on</i> Cambia Estado Interno</h5>
            <div class="row">
                <div class="col-sm-2">
                    <div class="switch">
                        <input type="hidden" name="status_changer" value="false">
                        <label><input type="checkbox" name="status_changer" value="true" <?= set_checkbox('status_changer', 'true', FALSE) ?>><span class="lever switch-col-amber"></span></label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-sm form-float m-b-0">
                        <div class="form-line focused disabled">
                            <select name="stage_status_id" class="form-control ms" disabled>
                                <?php foreach ($status as $st) : ?>
                                    <option value="<?= $st->id ?>" <?= set_select('stage_status_id', $st->id) ?>><?= $st->name ?></option>
                                <?php endforeach;?>
                            </select>
                            <label class="form-label">Estado</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-group-sm form-float m-b-0">
                        <div class="form-line focused disabled">
                            <select name="reason_id" class="form-control ms" disabled>
                                <?php foreach ($reasons as $reason) : ?>
                                    <option value="<?= $reason->id ?>" data-stage_status="<?= $reason->stage_status_id ?>" <?= set_select('reason_id', $reason->id) ?>><?= $reason->name ?></option>
                                <?php endforeach;?>
                            </select>
                            <label class="form-label">Causa</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>

<span id="requirementsPre" data-requirements='<?= json_encode($requirements) ?>' ></span>
