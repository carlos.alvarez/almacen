<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i> <?= lang('task') ?>: <?= $task->title ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="javascript: void(0);" class="btn btn-default btn-xs" data-toggle="tooltip" data-original-title="<?= ($task->client_visibility==='true') ? 'Visible para el cliente' : 'No visible para el cliente' ?>">
                    <i class="material-icons <?= ($task->client_visibility==='true') ? 'col-blue' : 'col-gray' ?> right visibility">visibility<?= ($task->client_visibility==='true') ? '' : '_off' ?></i>
                </a>
            </li>
            <li>
                <a href="javascript: void(0);" class="btn btn-default btn-xs" data-toggle="tooltip" data-original-title="<?= ($task->client_notification==='true') ? 'Notificar al cliente' : 'No notificar al cliente' ?>">
                    <i class="material-icons <?= ($task->client_notification==='true') ? 'col-yellow' : 'col-gray' ?> right notification">notifications_<?= ($task->client_notification==='true') ? 'on' : 'off' ?></i>
                </a>
            </li>
            <?php if( (grant_access('task', 'edit') || $project->manager_id === $this->session->userdata('user_id')) ): ?>
                <li>
                    <a href="<?= base_url("task/f_edit/{$task->id}") ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                        <i class="material-icons col-white">edit</i>
                    </a>
                </li>
            <?php endif; ?>
                <li>
                    <a href="javascript: void(0);" data-task="<?= $task->id ?>" id="close_task_btn" class="btn btn-success btn-xs <?= ( $close_btn && (grant_access('task', 'edit') || $project->manager_id === $this->session->userdata('user_id')) && in_array($task->status, array('pendiente','proceso')) ) ? '' : 'hide' ?>" data-toggle="tooltip" data-original-title="Cerrar Tarea">
                        <i class="material-icons col-white">lock</i>
                    </a>
                </li>
            <?php if( (grant_access('task', 'edit') || $project->manager_id === $this->session->userdata('user_id')) && in_array($task->status, array('pendiente','proceso')) ): ?>
                <li>
                    <a href="javascript: void(0);" data-task="<?= $task->id ?>" id="cancel_task_btn" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Anular Tarea">
                        <i class="material-icons col-white">close</i>
                    </a>
                </li>
            <?php endif; ?>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-sm-12">
                <small><?= lang('description') ?></small>
                <h4><?= $task->description ?></h4>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <small><?= lang('project') ?></small>
                <h4><a href="<?= base_url("project/f_view/{$project->id}") ?>"><?= $project->name ?></a></h4>
            </div>
            <div class="col-sm-4">
                <small><?= lang('stage') ?></small>
                <h4><?= $stage->title ?></a></h4>
            </div>
            <div class="col-sm-4">
                <small>Estado</small>
                <h4><?= strtoupper($task->status) ?></h4>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <small>Fecha de Inicio</small>
                <h4><?= nice_date($task->start_date) ?></h4>
            </div>
            <div class="col-sm-4">
                <small>Fecha de Cierre Estimada</small>
                <h4><?= nice_date($task->estimated_end_date) ?></h4>
            </div>
            <div class="col-sm-2">
                <small>Fecha de Cierre</small>
                <h4><?= nice_date($task->end_date) ?></h4>
            </div>
            <div class="col-sm-2">
                <small>Duración</small>
                <h4><?= ($task->duration_days > 0) ? "{$task->duration_days}d" : '' ?> <?= ($task->duration_hours > 0) ? "{$task->duration_hours}h" : '' ?></h4>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <small>Cambia Estado Interno</small>
                <div class="switch">
                    <label><input type="checkbox" disabled name="status_changer" value="true" <?= $task->status_changer === 'true' ? 'checked' : '' ?>><span class="lever switch-col-amber"></span></label>
                </div>
            </div>
            <div class="col-sm-4">
                <small>Estado Interno</small>
                <h4><?= $task->stage_status ?></h4>
            </div>
            <div class="col-sm-4">
                <small>Causa</small>
                <h4><?= $task->reason ?></a></h4>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <small><i class="material-icons font-18 col-orange">assignment_turned_in</i> Prerequisitos</small>
                        <div>
                            <?php if (isset($task_prerequisite['stage'])): ?>
                                <?php foreach($task_prerequisite['stage'] as $stage => $val): ?>
                                    <?php foreach($val as $prerequisite): ?>
                                        <span class="label bg-orange" data-toggle="tooltip" data-original-title="Fase: <?= $stage?>"><?= $prerequisite?></span>
                                    <?php endforeach;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <small><i class="material-icons font-18 col-purple">person</i> Recursos</small>
                        <div>
                            <?php if(isset($task_employee['company'])): ?>
                                <?php foreach($employees as $employeee): ?>
                                    <span class="label bg-purple" data-toggle="tooltip" data-original-title=""><?= $employeee->employee ?></span>
                                <?php endforeach;?>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-12">
                        <small>Requerimientos</small>
                        <div>
                            <?php foreach($requirements as $requirement):?>
                                <input type="checkbox" id="requirement_check_<?= $requirement->id ?>" class="filled-in chk-col-teal requirement_check" data-id="<?= $requirement->id ?>" <?= ($requirement->done == 'true') ? 'checked' : '' ?> <?= (grant_access('task', 'edit') || $project->manager_id === $_SESSION['user_id']) && in_array($task->status, array('pendiente', 'proceso')) ? '' : 'disabled' ?>>
                                <label class="m-b-0" for="requirement_check_<?= $requirement->id ?>"></label>
                                <span><?= $requirement->requirement ?></span><br>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <small><i class="material-icons font-18 col-cyan">local_offer</i> Etiquetas</small>
                <div>
                    <?php foreach($tags as $tag):?>
                        <span class="label label-info"><?= $tag->tag ?></span>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>
