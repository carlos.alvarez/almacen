<div class="card">
    <form id="projectBrand-new-form" class="form-validate" method="post">
        <input type="hidden" value="<?= $project_id ?>" name="project_id">
        <div class="header">
            <h2 class="card-title"><i class="material-icons">add</i><?= lang('method_create') ?> Marca presupuesto</h2>

        </div>

        <?php if ( validation_errors() ): ?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <?= validation_errors() ?>
            </div>
        <?php endif; ?>

        <div class="body">
            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 m-b-0">
                            <h5 class="card-title"><?= lang('brand') ?></h5>
                            <div class="form-group form-float">
                                <div class="form-line form-focused">
                                    <select name="brand_id" class="btn-group bootstrap-select form-control show-tick" required id="brand">
                                        <option value="" disabled selected><?= lang('choose_an_option') ?></option>
                                        <?php foreach($brands as $brand):?>
                                            <option value="<?= $brand->id ?>" <?= $brand->id === set_value('brand_id') ? 'selected' : '' ?>><?= $brand->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <label class="form-label"><?= lang('brand') ?></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line <?= set_value('estimated_amount') != '' ? 'focused':'' ?>">
                                    <input type="text" class="form-control" name="estimated_amount" value="<?= set_value('estimated_amount') ?>" required maxlength="30" />
                                    <label class="form-label"><?= lang('estimated_amount') ?>(US$) *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="estimated_date" value="<?= set_value('estimated_date', date('d/m/Y')) ?>" required  />
                                    <label class="form-label"><?= lang('estimated_date') ?> *</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="actual_amount" value="<?= number_format(set_value('actual_amount','0'), 2,'.',',') ?>" required maxlength="30" />
                                    <label class="form-label"><?= lang('actual_amount') ?>(US$) *</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 m-b-0">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="text" class="form-control" name="actual_date" value="<?= set_value('actual_date', date('d/m/Y'))?>" required/>
                                    <label class="form-label"><?= lang('actual_date') ?> *</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </form>
</div>
