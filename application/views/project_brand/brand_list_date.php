<div class="card">
    <div class="header">
        <h2><i class="material-icons">list</i> <?= lang('project_brand') ?></h2>
        <ul class="header-dropdown">
            <li style="margin-right: 15px;">
                <div class="form-group form-float">
                    <div class="form-line focused">
                        <input type="hidden" class="form-control" id="budget_date" name="budget_date" value="<?= isset($_GET['date']) ? $_GET['date'] : date('Y-m-d') ?>"/>
                        <input type="hidden" class="form-control" id="status" name="status" value="<?= isset($_GET['status']) ? $_GET['status'] : 'all' ?>"/>
                        <input type="hidden" class="form-control" id="company" name="company" value="<?= isset($_GET['company']) ? $_GET['company'] : 'all' ?>"/>
                        <input type="hidden" class="form-control" id="category" name="category" value="<?= isset($_GET['category']) ? $_GET['category'] : 'all' ?>"/>
                        <input type="hidden" class="form-control" id="brand" name="brand" value="<?= isset($_GET['brand']) ? $_GET['brand'] : 'all' ?>"/>
                        <input type="hidden" class="form-control" id="project_id" name="project_id" value="<?= isset($_GET['project_id']) ? $_GET['project_id'] : 'all' ?>"/>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="body">
        <table id="project-brand-table" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">
                    <label class="m-b-0" for="check-all"></label>
                </th>
                <th class="all">ID</th>
                <th class="all"><?= lang('project') ?></th>
                <th class="all"><?= lang('brand') ?></th>
                <th class="all"><?= lang('estimated_amount') ?> (US$)</th>
                <th class="all"><?= lang('estimated_date') ?></th>
                <th class="all"><?= lang('actual_amount') ?> (US$)</th>
                <th class="all"><?= lang('actual_date') ?></th>
                <th><?= lang('created_at') ?></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>