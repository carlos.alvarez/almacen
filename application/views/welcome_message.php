<div class="card" id="filters">
    <div class="header">
        <h2><i class="material-icons">dashboard</i> TABLERO [<?= getHostByName(getHostName()) ?>]</h2>
        <ul class="header-dropdown">

        </ul>
    </div>
    <div class="body">
        <div class="row">
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-1">
                        <div id="bar_chart" class="flot-chart"></div>
                    </div>
                    <div class="col-sm-3">
                        <div id="legend" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3" id="log">

            </div>
        </div>
    </div>
</div>

