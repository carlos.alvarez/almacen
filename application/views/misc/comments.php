<div>
    <?php foreach ($comments as $comment): ?>
        <?php $orientation = ($comment->user_id === $_SESSION['user_id']) ? 'right' : 'left'; ?>
        <div class="row">
            <div class="col-xs-2 col-sm-1 <?= ($orientation === 'right') ? 'col-xs-push-10 col-sm-push-11' : '' ?>">
                <a href="javascript:void(0);">
                    <img class="media-object" src="<?= img_src("public/img/users/{$comment->user_id}.jpg") ?>" style="border-radius: 50%; width: 100%;">
                </a>
            </div>
            <div class="col-xs-10 col-sm-11 <?= ($orientation === 'right') ? 'col-xs-pull-2 col-sm-pull-1' : '' ?> align-<?= $orientation ?>">
                <p>
                    <?php if ( $orientation === 'right' ): ?>
                        <span class="font-10"><?= date('d/M/Y h:i A', strtotime($comment->created_at)) ?></span>
                        <span class="font-14"><?= $comment->user ?></span>
                        <?php if(grant_access('project', 'edit') || isset($project->manager_id) === $this->session->userdata('user_id') || isset($project_client->manager_id) === $this->session->userdata('user_id')): ?>
                            <a href="javascript:void(0);" class="client-visibility-comment-toggle" data-id="<?= $comment->id ?>" data-toggle="tooltip" title="Visibilidad para el cliente">
                                <input type="hidden" name="client_visibility" value="<?= $comment->client_visibility ?>">
                                <i class="material-icons <?= ($comment->client_visibility === 'false') ? 'col-grey' : 'col-blue' ?>"><?= ($comment->client_visibility === 'false') ? 'visibility_off' : 'visibility'?></i>
                            </a>
                        <?php endif;?>
                    <?php endif; ?>

                    <?php if ( $orientation === 'left' ): ?>
                        <?php if(grant_access('project', 'edit') || isset($project->manager_id) === $this->session->userdata('user_id') || isset($project_client->manager_id) === $this->session->userdata('user_id')): ?>
                            <a href="javascript:void(0);" class="client-visibility-comment-toggle" data-id="<?= $comment->id ?>" data-toggle="tooltip" title="Visibilidad para el cliente">
                                <input type="hidden" name="client_visibility" value="false">
                                <i class="material-icons <?= ($comment->client_visibility === 'false') ? 'col-grey' : 'col-blue' ?>"><?= ($comment->client_visibility === 'false') ? 'visibility_off' : 'visibility'?></i>
                            </a>
                        <?php endif;?>
                        <span class="font-14"><?= $comment->user ?></span>

                        <span class="font-10"><?= date('d/M/Y h:i A', strtotime($comment->created_at)) ?></span>

                    <?php endif; ?>
                </p>
                <p><?= $comment->message ?></p>
            </div>
        </div>
    <?php endforeach; ?>
</div>