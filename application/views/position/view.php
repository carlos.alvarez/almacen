<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('position') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('position/edit/').$position->id ?>" class="btn btn-info btn-xs <?= grant_show('position', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-sm-4 m-b-0">
                <small><?= lang('name') ?></small>
                <h4><?= $position->name ?></h4>
            </div>
            <div class="col-sm-6 m-b-0">
                <small><?= lang('company') ?></small>
                <h4><?= $company->name ?></h4>
            </div>
            <div class="col-sm-2">
                <small><?= lang('manage_clients') ?></small>
                <div class="switch">
                    <label><input type="checkbox" value="true" <?= ($position->manage_clients === 'true') ? 'checked' : '' ?> disabled><span class="lever switch-col-green"></span></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <small><?= lang('description') ?></small>
                <h5><?= $position->description ?></h5>
            </div>
        </div>

    </div>

</div>