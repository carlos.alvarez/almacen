<div class="card">
    <div class="header">
        <h2><i class="material-icons">add</i><?= lang('method_create') ?> <?= lang('position') ?></h2>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="position-new-form" class="form-validate" method="post">
            <div class="row">
                <div class="col-sm-4 m-b-0">
                    <div class="form-group form-float m-b-0">
                        <div class="form-line focused">
                            <?= form_dropdown('company_id', $companies, set_value('company_id', ''), 'class="btn-group bootstrap-select form-control show-tick" data-live-search="true" required') ?>
                            <label class="form-label"><?= lang('company') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" value="<?= set_value('name') ?>" required maxlength="100" />
                            <label class="form-label"><?= lang('name') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 m-b-0 align-right">
                    <div class="switch">
                        <input type="hidden" name="manage_clients" value="false">
                        <label><?= lang('manage_clients') ?> <input type="checkbox" name="manage_clients" value="true" <?= set_checkbox('manage_clients', 'true', FALSE) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 m-b-0">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="description" value="<?= set_value('description') ?>" />
                            <label class="form-label"><?= lang('description') ?></label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 align-center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
