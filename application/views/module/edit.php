<div class="card">
    <div class="header">
        <h2><i class="material-icons">edit</i><?= lang('method_edit') ?> <?= lang('module') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('module/view/').$module->id ?>" class="btn btn-info btn-xs <?= grant_show('module', 'view') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_view') ?>">
                    <i class="material-icons col-white">search</i>
                </a>
            </li>
        </ul>
    </div>

    <?php if ( validation_errors() ): ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?= validation_errors() ?>
        </div>
    <?php endif; ?>

    <div class="body">
        <form id="module-edit-form" class="form-validate" method="post">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="name" value="<?= set_value('name', $module->name) ?>" required />
                            <label class="form-label"><?= lang('name') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="description" value="<?= set_value('description', $module->description) ?>" maxlength="250" />
                            <label class="form-label"><?= lang('description') ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12 align-right">
                    <div class="switch">
                        <input type="hidden" name="active" value="false">
                        <label><?= lang('active') ?> <input type="checkbox" name="active" value="true" <?= set_checkbox('active', 'true', ($module->active === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" class="form-control" name="controller" value="<?= set_value('controller', $module->controller) ?>" required minlength="2" maxlength="30" />
                            <label class="form-label"><?= lang('controller') ?> *</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12 align-right">
                    <div class="switch">
                        <input type="hidden" name="action_create" value="false">
                        <label><?= lang('method_create') ?> <input type="checkbox" name="action_create" value="true" <?= set_checkbox('action_create', 'true', ($module->action_create === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12 align-right">
                    <div class="switch">
                        <input type="hidden" name="action_view" value="false">
                        <label><?= lang('method_view') ?> <input type="checkbox" name="action_view" value="true" <?= set_checkbox('action_view', 'true', ($module->action_view === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12 align-right">
                    <div class="switch">
                        <input type="hidden" name="action_edit" value="false">
                        <label><?= lang('method_edit') ?> <input type="checkbox" name="action_edit" value="true" <?= set_checkbox('action_edit', 'true', ($module->action_edit === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12 align-right">
                    <div class="switch">
                        <input type="hidden" name="action_delete" value="false">
                        <label><?= lang('method_delete') ?> <input type="checkbox" name="action_delete" value="true" <?= set_checkbox('action_delete', 'true', ($module->action_delete === 'true')) ?>><span class="lever switch-col-green"></span></label>
                    </div>
                </div>
            </div>

            <div class="align-center">
                <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
            </div>
        </form>
    </div>
</div>
