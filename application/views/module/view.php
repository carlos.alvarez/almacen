<div class="card">
    <div class="header">
        <h2><i class="material-icons">search</i><?= lang('method_view') ?> <?= lang('module') ?></h2>
        <ul class="header-dropdown">
            <li>
                <a href="<?= base_url('module/edit/').$module->id ?>" class="btn btn-info btn-xs <?= grant_show('module', 'edit') ?>" data-toggle="tooltip" data-original-title="<?= lang('method_edit') ?>">
                    <i class="material-icons col-white">edit</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="body">
        <div class="row">
            <div class="col-sm-9 col-xs-12 ">
                <h2><?= $module->name ?></h2>
                <span class="col-grey"><?= $module->controller ?></span>
            </div>
            <div class="col-sm-3 col-xs-12 align-right">
                <div class="switch">
                    <label><?= lang('active') ?> <input type="checkbox" value="true" <?= ($module->active === 'true') ? 'checked' : '' ?> disabled><span class="lever switch-col-green"></span></label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3 col-xs-12">
                <div class="switch">
                    <label><?= lang('method_create') ?> <input type="checkbox" value="true" <?= ($module->action_create === 'true') ? 'checked' : '' ?> disabled><span class="lever switch-col-green"></span></label>
                </div>
            </div>
            <div class="col-sm-3 col-xs-12">
                <div class="switch">
                    <label><?= lang('method_view') ?> <input type="checkbox" value="true" <?= ($module->action_view === 'true') ? 'checked' : '' ?> disabled><span class="lever switch-col-green"></span></label>
                </div>
            </div>
            <div class="col-sm-3 col-xs-12">
                <div class="switch">
                    <label><?= lang('method_edit') ?> <input type="checkbox" value="true" <?= ($module->action_edit === 'true') ? 'checked' : '' ?> disabled><span class="lever switch-col-green"></span></label>
                </div>
            </div>
            <div class="col-sm-3 col-xs-12">
                <div class="switch">
                    <label><?= lang('method_delete') ?> <input type="checkbox" value="true" <?= ($module->action_delete === 'true') ? 'checked' : '' ?> disabled><span class="lever switch-col-green"></span></label>
                </div>
            </div>
        </div>
    </div>
</div>
