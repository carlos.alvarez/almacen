<div class="header">
    <a href="<?= base_url('action/create') ?>" class="btn btn-success right <?= grant_show('action', 'create') ?>"><i class="material-icons">add</i> <?= lang('method_create') ?> <?= lang('action') ?></a>
    <h2><?= lang('cont_action') ?></h2>
</div>

<div class="body">
    <table id="action-table" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width: 100%;">
        <thead>
        <tr>
            <th>
                <input type="checkbox" id="check-all" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">
                <label class="m-b-0" for="check-all"></label>
            </th>
            <th class="all">ID</th>
            <th class="all"><?= lang('name') ?></th>
            <th class="all"><?= lang('method') ?></th>
            <th class="all"><?= lang('active') ?></th>
            <th><?= lang('options') ?></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>
