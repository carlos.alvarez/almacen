
<div class="header">
    <a href="<?= base_url("action/edit/{$action->id}") ?>" class="btn btn-info right <?= grant_show('action', 'edit') ?>"><i class="material-icons">edit</i> <?= lang('method_edit') ?> <?= lang('action') ?></a>
    <h2><?= lang('method_view') ?> <?= lang('action') ?></h2>
</div>

<div class="body">
    <div class="row">
        <div class="col-sm-9 col-xs-12 ">
            <h2><?= $action->name ?></h2>
            <span class="col-grey"><?= $action->method ?></span>
        </div>
        <div class="col-sm-3 col-xs-12 align-right">
            <div class="switch">
                <label><?= lang('active') ?> <input type="checkbox" value="true" <?= ($action->active === 'true') ? 'checked' : '' ?> disabled ><span class="lever switch-col-green"></span></label>
            </div>
        </div>
    </div>
</div>
