
<?php if ( validation_errors() ): ?>
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <?= validation_errors() ?>
    </div>
<?php endif; ?>

<div class="header">
    <h2><?= lang('method_create') ?> <?= lang('action') ?></h2>
</div>

<div class="body">
    <form id="action-new-form" class="form-validate" method="post">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="name" value="<?= set_value('name') ?>" required />
                        <label class="form-label"><?= lang('name') ?> *</label>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="method" value="<?= set_value('method') ?>" required />
                        <label class="form-label"><?= lang('method') ?> *</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 col-xs-12 align-right">
                <div class="switch">
                    <input type="hidden" name="active" value="false">
                    <label><?= lang('active') ?> <input type="checkbox" name="active" value="true" <?= set_checkbox('active', 'true', TRUE) ?>><span class="lever switch-col-green"></span></label>
                </div>
            </div>
        </div>

        <div class="align-center">
            <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('submit') ?></button>
        </div>
    </form>
</div>

