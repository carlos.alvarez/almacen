<?php

class Migration_Add_templates_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'project_category_id'   =>  array(
                    'type'              =>  'INT',
                    'unsigned'          =>  TRUE,
                    'null'              =>  FALSE
                ),
                'title' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 150,
                    'null'              => FALSE
                ),
                'description' => array(
                    'type'              => 'TEXT',
                    'null'              => TRUE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('templates', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('templates', TRUE);
    }
}
