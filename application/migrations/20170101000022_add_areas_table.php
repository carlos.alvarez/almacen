<?php

class Migration_Add_areas_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'department_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 100,
                    'null'              => FALSE
                ),
                'short_name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 30,
                    'null'              => TRUE
                ),
                'manager_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => TRUE
                ),
                'active' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
				'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('department_id');
		$this->dbforge->create_table('areas', FALSE, array('ENGINE'=>'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('areas', TRUE);
    }
}
