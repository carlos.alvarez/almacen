<?php
/**
 * User: cristian.rodriguez
 * Date: 9/7/2017
 * Time: 10:06 AM
 */

class Migration_add_notifications_table extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(array(
            'id'   => array(
                'type'              => 'INT',
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE,
                'null'              => FALSE
            ),
            'user_id'        => array(
                'type'          => 'INT',
                'unsigned'      => TRUE,
                'null'          => FALSE
            ),
            'message'       => array(
                'type'          => 'TEXT',
                'null'          => FALSE
            ),
            'read'          => array(
                'type'              => 'ENUM("true","false")',
                'default'           => 'false',
                'null'              => FALSE
            ),
            'actions'       => array(
                'type'          => 'TEXT',
                'null'          => TRUE
            ),
            'tag'       => array(
                'type'          => 'VARCHAR',
                'constraint'    => 30,
                'null'          => TRUE
            ),
            'created_at'    => array(
                'type'          => 'TIMESTAMP',
                'null'          => TRUE
            ),
            'updated_at'    => array(
                'type'          => 'TIMESTAMP',
                'null'          => TRUE
            )
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('user_id');
        $this->dbforge->add_key('tag');
        $this->dbforge->create_table('notifications', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('notifications');
    }
}