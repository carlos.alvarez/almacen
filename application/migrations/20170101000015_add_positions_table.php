<?php

class Migration_Add_positions_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'company_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 100,
                    'null'              => FALSE
                ),
                'description' => array(
                    'type'              => 'TEXT',
                    'null'              => TRUE
                ),
                'manage_clients' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'false',
                    'null'              => FALSE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('company_id');
        $this->dbforge->create_table('positions', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('positions', TRUE);
    }
}
