<?php

class Migration_Add_projects_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'client_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'company_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'invoice_company_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'manager_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'project_category_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'stage_status_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'reason_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 100,
                    'null'              => FALSE
                ),
                'description' => array(
                    'type'              => 'TEXT',
                    'null'              => TRUE
                ),
                'start_date' => array(
                    'type'              => 'DATE',
                    'null'              => FALSE
                ),
                'estimated_end_date' => array(
                    'type'              => 'DATE',
                    'null'              => FALSE
                ),
                'end_date' => array(
                    'type'              => 'DATE',
                    'null'              => TRUE
                ),
                'status' => array(
                    'type'              => 'ENUM("progreso","finalizado","anulado")',
                    'default'           => 'progreso',
                    'null'              => FALSE
                ),
				'budget' => array(
				    'type'              => 'ENUNM("yes","no")',
                    'default'           => 'yes',
                    'null'              => FALSE
                ),
				'client_level_visibility' => array(
				    'type'              => 'ENUM("stage","task")',
                    'default'           => 'stage',
                    'null'              => FALSE
                ),
				'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('client_id');
        $this->dbforge->add_key('company_id');
        $this->dbforge->add_key('invoice_company_id');
        $this->dbforge->add_key('manager_id');
        $this->dbforge->add_key('project_category_id');
        $this->dbforge->add_key('stage_status_id');
        $this->dbforge->add_key('reason_id');
		$this->dbforge->create_table('projects', FALSE, array('ENGINE'=>'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('projects', TRUE);
    }
}
