<?php

class Migration_Add_users_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'username' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 30,
                    'null'              => FALSE,
                    'unique'            => TRUE
                ),
                'password' => array(
                    'type'              => 'CHAR',
                    'constraint'        => 60,
                    'null'              => FALSE
                ),
                'email' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 250,
                    'null'              => FALSE
                ),
                'first_name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 100,
                    'null'              => FALSE
                ),
                'last_name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 100,
                    'null'              => FALSE
                ),
                'active' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
				'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('users', FALSE, array('ENGINE'=>'InnoDB'));

		$this->db->insert('users', array(
			'username'		=> 'admin',
			'password'		=> password_hash('123456', PASSWORD_BCRYPT),
            'first_name'    => 'System',
            'last_name'     => 'Administrator',
			'email'			=> 'admin@test.tst',
			'created_at'	=> date('Y-m-d H:i:s')
		));
    }

    public function down()
    {
        $this->dbforge->drop_table('users', TRUE);
    }
}
