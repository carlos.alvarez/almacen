<?php

class Migration_Add_user_roles_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'user_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'role_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
				'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('user_id', TRUE);
        $this->dbforge->add_key('role_id', TRUE);
        $this->dbforge->create_table('user_roles', FALSE, array('ENGINE'=>'InnoDB'));

		$this->db->insert('user_roles', array(
			'user_id'		=> 1,
			'role_id'		=> 1,
			'created_at'	=> date('Y-m-d H:i:s')
		));
    }

    public function down()
    {
        $this->dbforge->drop_table('user_roles', TRUE);
    }
}
