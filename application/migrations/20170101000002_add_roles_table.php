<?php

class Migration_Add_roles_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 30,
                    'null'              => FALSE,
                    'unique'            => TRUE
                ),
                'description' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 250,
                    'null'              => FALSE
                ),
                'active' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
				'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('roles', FALSE, array('ENGINE'=>'InnoDB'));

		$this->db->insert('roles', array(
			'name'			=> 'Administrator',
			'description'	=> 'Manages the platform.',
			'created_at'	=> date('Y-m-d H:i:s')
		));
    }

    public function down()
    {
        $this->dbforge->drop_table('roles', TRUE);
    }
}
