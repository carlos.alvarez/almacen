<?php

class Migration_Add_employees_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'user_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'area_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'position_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'supervisor_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => TRUE,
                ),
                'phone' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 20,
                    'null'              => TRUE
                ),
                'ext' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 8,
                    'null'              => TRUE
                ),
                'mobile' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 20,
                    'null'              => TRUE
                ),
                'active' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('user_id', TRUE);
        $this->dbforge->add_key('department_id');
        $this->dbforge->add_key('position_id');
        $this->dbforge->create_table('employees', FALSE, array('ENGINE'=>'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('employees', TRUE);
    }
}
