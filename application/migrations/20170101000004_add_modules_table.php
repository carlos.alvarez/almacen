<?php

class Migration_Add_modules_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 30,
                    'null'              => FALSE
                ),
                'description' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 250,
                    'null'              => TRUE
                ),
                'controller' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 30,
                    'null'              => FALSE,
                    'unique'            => TRUE
                ),
                'active' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
                'action_create' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
                'action_view' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
                'action_edit' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
                'action_delete' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
				'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('modules', FALSE, array('ENGINE'=>'InnoDB'));

		$created_at = date('Y-m-d H:i:s');
		$this->db->insert_batch('modules', array(
			array(
				'name'			=> lang('cont_user'),
				'controller'	=> 'user',
				'created_at'	=> $created_at
			),
			array(
				'name'			=> lang('cont_role'),
				'controller'	=> 'role',
				'created_at'	=> $created_at
			),
			array(
				'name'			=> lang('cont_module'),
				'controller'	=> 'module',
				'created_at'	=> $created_at
			),
			array(
				'name'			=> lang('cont_action'),
				'controller'	=> 'action',
				'created_at'	=> $created_at
			),
			array(
				'name'			=> lang('cont_grant'),
				'controller'	=> 'grant',
				'created_at'	=> $created_at
			),
			array(
				'name'			=> lang('cont_company'),
				'controller'	=> 'company',
				'created_at'	=> $created_at
			),
			array(
				'name'			=> lang('cont_department'),
				'controller'	=> 'department',
				'created_at'	=> $created_at
			),
			array(
				'name'			=> lang('cont_employee'),
				'controller'	=> 'employee',
				'created_at'	=> $created_at
			),
            array(
                'name'			=> lang('cont_template'),
                'controller'	=> 'template',
                'created_at'	=> $created_at
            ),
            array(
                'name'			=> lang('cont_position'),
                'controller'	=> 'position',
                'created_at'	=> $created_at
            ),
            array(
                'name'			=> lang('cont_client'),
                'controller'	=> 'client',
                'created_at'	=> $created_at
            ),
            array(
                'name'			=> lang('cont_area'),
                'controller'	=> 'area',
                'created_at'	=> $created_at
            )
		));
    }

    public function down()
    {
        $this->dbforge->drop_table('modules', TRUE);
    }
}
