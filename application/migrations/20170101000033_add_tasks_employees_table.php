<?php

class Migration_Add_tasks_employees_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'task_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'user_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('task_id', TRUE);
        $this->dbforge->add_key('user_id', TRUE);
        $this->dbforge->add_key('user_id');
        $this->dbforge->create_table('tasks_employees', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('tasks_employees', TRUE);
    }
}
