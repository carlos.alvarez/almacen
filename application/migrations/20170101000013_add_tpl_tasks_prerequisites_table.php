<?php

class Migration_Add_tpl_tasks_prerequisites_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'tpl_task_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'tpl_task_prerequisite' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('tpl_task_id', TRUE);
        $this->dbforge->add_key('tpl_task_prerequisite', TRUE);
        $this->dbforge->add_key('tpl_task_prerequisite');
        $this->dbforge->create_table('tpl_tasks_prerequisites', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('tpl_tasks_prerequisites', TRUE);
    }
}
