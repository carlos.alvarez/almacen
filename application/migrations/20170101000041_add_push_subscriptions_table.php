<?php
/**
 * User: cristian.rodriguez
 * Date: 9/7/2017
 * Time: 10:06 AM
 */

class Migration_add_push_subscriptions_table extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(array(
            'id'   => array(
                'type'              => 'INT',
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE,
                'null'              => FALSE
            ),
            'user_id'        => array(
                'type'          => 'INT',
                'unsigned'      => TRUE,
                'null'          => FALSE
            ),
            'endpoint'  => array(
                'type'          => 'VARCHAR',
                'constraint'    => 200,
                'null'          => FALSE
            ),
            'expirationTime'    => array(
                'type'          => 'DATETIME',
                'null'          => TRUE
            ),
            'created_at'    => array(
                'type'          => 'TIMESTAMP',
                'null'          => TRUE
            ),
            'updated_at'    => array(
                'type'          => 'TIMESTAMP',
                'null'          => TRUE
            )
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('user_id');
        $this->dbforge->create_table('push_subscriptions', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('push_subscriptions');
    }
}