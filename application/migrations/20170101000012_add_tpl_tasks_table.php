<?php

class Migration_Add_tpl_tasks_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'tpl_stage_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'title' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 150,
                    'null'              => FALSE
                ),
                'description' => array(
                    'type'              => 'TEXT',
                    'null'              => TRUE
                ),
                'duration_days' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'duration_hours' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'day_type' => array(
                    'type'              => 'ENUM("laborable","calendar")',
                    'default'           => 'laborable',
                    'null'              => FALSE
                ),
                'weight' => array(
                    'type'              => 'DECIMAL',
                    'constraint'        => '6,3',
                    'null'              => FALSE
                ),
                'client_visibility' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'false',
                    'null'              => FALSE
                ),
                'client_notification' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'false',
                    'null'              => FALSE
                ),
                'order' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => TRUE
                ),
                'status_changer' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'false',
                    'null'              => FALSE
                ),
                'stage_status_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => TRUE
                ),
                'reason_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => TRUE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('tpl_stage_id');
        $this->dbforge->add_key('stage_status_id');
        $this->dbforge->add_key('reason_id');
        $this->dbforge->create_table('tpl_tasks', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('tpl_tasks', TRUE);
    }
}
