<?php

class Migration_Add_calendars_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 100,
                    'null'              => FALSE
                ),
                'description' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 250,
                    'null'              => TRUE
                ),
                'working_hours' => array(
                    'type'              => 'TINYINT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE,
                    'default'           => 8
                ),
                'wd_sunday' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'false',
                    'null'              => FALSE
                ),
                'wd_monday' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
                'wd_tuesday' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
                'wd_wednesday' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
                'wd_thursday' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
                'wd_friday' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
                'wd_saturday' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'false',
                    'null'              => FALSE
                ),
                'active' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
				'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('calendars', FALSE, array('ENGINE'=>'InnoDB'));

		$this->db->insert('calendars', array(
			'name'			=> lang('default'),
			'created_at'	=> date('Y-m-d H:i:s')
		));
    }

    public function down()
    {
        $this->dbforge->drop_table('calendars', TRUE);
    }
}
