<?php

class Migration_Add_contact_types_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 100,
                    'null'              => FALSE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('contact_types', FALSE, array('ENGINE'=>'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('contact_types', TRUE);
    }
}
