<?php

class Migration_Add_calendar_days_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'date' => array(
                    'type'              => 'DATE'
                ),
                'week_day' => array(
                    'type'              => 'TINYINT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                )
            )
        );

        $this->dbforge->add_key('date', TRUE);
        $this->dbforge->add_key('week_day');
		$this->dbforge->create_table('calendar_days', FALSE, array('ENGINE'=>'InnoDB'));

        $period = new DatePeriod(new DateTime('2014-01-01'), new DateInterval('P1D'), new DateTime('2040-12-01 +1 day'));

        /** @var DateTime $date */
        foreach ($period as $date)
        {
            $this->db->insert('calendar_days', array(
                'date'			=> $date->format('Y-m-d'),
                'week_day'      => $date->format('w')
            ));
        }
    }

    public function down()
    {
        $this->dbforge->drop_table('calendar_days', TRUE);
    }
}
