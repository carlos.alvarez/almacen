<?php
/**
 * User: cristian.rodriguez
 * Date: 9/7/2017
 * Time: 10:06 AM
 */

class Migration_add_brands_table extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(array(
            'id'   => array(
                'type'              => 'INT',
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE,
                'null'              => FALSE
            ),
            'name'  => array(
                'type'          => 'VARCHAR',
                'constraint'    => 200,
                'null'          => FALSE
            ),
            'created_at'    => array(
                'type'          => 'TIMESTAMP',
                'null'          => FALSE
            ),
            'updated_at'    => array(
                'type'          => 'TIMESTAMP',
                'null'          => FALSE
            )
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('brands', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('brands');
    }
}