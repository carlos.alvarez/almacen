<?php
/**
 * User: cristian.rodriguez
 * Date: 9/7/2017
 * Time: 9:29 AM
 */

class Migration_add_projects_brands_table extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(array(
            'id'                => array(
                'type'              => 'INT',
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE,
                'null'              => FALSE
            ),
            'project_id'        => array(
                'type'          => 'INT',
                'unsigned'      => TRUE,
                'null'          => FALSE
            ),
            'brand_id'          => array(
                'type'          => 'INT',
                'unsigned'      => TRUE,
                'null'          => FALSE
            ),
            'estimated_amount'  => array(
                'type'          => 'DOUBLE',
                'constraint'    => '13,2',
                'null'          => FALSE
            ),
            'estimated_date' => array(
                'type'       => 'TIMESTAMP',
                'null'       => TRUE
            ),
            'actual_amount'     => array(
                'type'          => 'DOUBLE',
                'constraint'    => '13,2',
                'null'          => FALSE
            ),
            'actual_date'   => array(
                'type'      => 'TIMESTAMP',
                'null'      => TRUE
            ),
            'created_at'    => array(
                'type'      => 'TIMESTAMP',
                'null'      => TRUE
            ),
            'updated_at'    => array(
                'type'      => 'TIMESTAMP',
                'null'      => TRUE,
            )
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('project_id');
        $this->dbforge->add_key('brand_id');
        $this->dbforge->create_table('projects_brands', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('projects_brands', TRUE);
    }
}