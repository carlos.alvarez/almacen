<?php

class Migration_Add_tpl_tasks_requirements_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'tpl_task_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'requirement' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 250,
                    'null'              => FALSE
                ),
                'weight' => array(
                    'type'              => 'DECIMAL',
                    'constraint'        => '6,3',
                    'null'              => FALSE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('tpl_task_id');
        $this->dbforge->create_table('tpl_tasks_requirements', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('tpl_tasks_requirements', TRUE);
    }
}
