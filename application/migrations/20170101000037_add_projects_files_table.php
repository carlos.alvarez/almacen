<?php

class Migration_Add_projects_files_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'project_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'user_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 100,
                    'null'              => FALSE
                ),
                'path' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 250,
                    'null'              => FALSE
                ),
                'description' => array(
                    'type'              => 'TEXT',
                    'null'              => TRUE
                ),
                'client_visibility' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'false',
                    'null'              => FALSE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('project_id');
        $this->dbforge->add_key('user_id');
        $this->dbforge->create_table('projects_files', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('projects_files', TRUE);
    }
}
