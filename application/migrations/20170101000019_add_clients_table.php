<?php

class Migration_Add_clients_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'ref_code' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 12,
                    'null'              => TRUE
                ),
                'name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 100,
                    'null'              => FALSE
                ),
                'short_name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 30,
                    'null'              => TRUE
                ),
                'tax_number' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 30,
                    'null'              => TRUE
                ),
                'client_category_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => TRUE
                ),
                'account_manager' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => TRUE
                ),
                'active' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'true',
                    'null'              => FALSE
                ),
                'address' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 250,
                    'null'              => TRUE
                ),
				'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('ref_code');
        $this->dbforge->add_key('account_manager');
		$this->dbforge->create_table('clients', FALSE, array('ENGINE'=>'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('clients', TRUE);
    }
}
