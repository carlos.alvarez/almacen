<?php

class Migration_Add_stages_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'project_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'template_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'title' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 150,
                    'null'              => FALSE
                ),
                'description' => array(
                    'type'              => 'TEXT',
                    'null'              => TRUE
                ),
                'weight' => array(
                    'type'              => 'DECIMAL',
                    'constraint'        => '6,3',
                    'null'              => FALSE
                ),
                'client_visibility' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'false',
                    'null'              => FALSE
                ),
                'client_notification' => array(
                    'type'              => 'ENUM("true","false")',
                    'default'           => 'false',
                    'null'              => FALSE
                ),
                'order' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => TRUE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('project_id');
        $this->dbforge->add_key('template_id');
        $this->dbforge->create_table('stages', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('stages', TRUE);
    }
}
