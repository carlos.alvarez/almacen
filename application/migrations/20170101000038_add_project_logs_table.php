<?php
/**
 * User: Christian Rodriguez
 * Date: 8/23/2017
 * Time: 11:07 PM
 */

class Migration_Add_project_logs_table extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(array(
                'id' => array(
                    'type'              =>  'INT',
                    'unsigned'          =>  TRUE,
                    'auto_increment'    =>  TRUE,
                    'null'              =>  FALSE
                ),
                'project_id' => array(
                    'type'              =>  'INT',
                    'unsigned'          =>  TRUE,
                    'null'              =>  FALSE
                ),
                'user_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'message' => array(
                    'type'              =>  'TEXT',
                    'null'              =>  FALSE
                ),
                'created_at' => array(
                    'type'              =>  'TIMESTAMP',
                    'null'              =>  FALSE
                )
            ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('project_id');
        $this->dbforge->add_key('user_id');
        $this->dbforge->create_table('project_logs', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('project_logs', TRUE);
    }
}