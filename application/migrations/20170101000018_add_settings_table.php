<?php

class Migration_Add_settings_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 50,
                    'null'              => FALSE
                ),
                'value' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 250,
                    'null'              => FALSE
                ),
                'type' => array(
                    'type'              => 'ENUM("int","float","string","date","bool")',
                    'null'              => FALSE
                ),
				'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('name', TRUE);
		$this->dbforge->create_table('settings', FALSE, array('ENGINE'=>'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('settings', TRUE);
    }
}
