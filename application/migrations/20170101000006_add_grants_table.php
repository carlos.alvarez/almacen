<?php

class Migration_Add_grants_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'role_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'module_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'method' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 60,
                    'null'              => FALSE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('role_id', TRUE);
        $this->dbforge->add_key('module_id', TRUE);
        $this->dbforge->add_key('method', TRUE);
		$this->dbforge->create_table('grants', FALSE, array('ENGINE'=>'InnoDB'));

		$modules = $this->db->get('modules')->result();

		$actions = array(
		    'create',
            'view',
            'edit',
            'delete'
        );

		foreach ($modules as $module)
		{
			foreach ($actions as $action)
			{
				$this->db->insert('grants', array(
					'role_id'		=> 1, //Admin
					'module_id'		=> $module->id,
					'method'		=> $action,
					'created_at' 	=> date('Y-m-d H:i:s')
				));
			}
		}

    }

    public function down()
    {
        $this->dbforge->drop_table('grants', TRUE);
    }
}
