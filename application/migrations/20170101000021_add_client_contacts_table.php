<?php

class Migration_Add_client_contacts_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'client_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'user_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => TRUE
                ),
                'contact_type_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'name' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 250,
                    'null'              => FALSE
                ),
                'position' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 250,
                    'null'              => FALSE
                ),
                'phone' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 30,
                    'null'              => FALSE
                ),
                'email' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 250,
                    'null'              => FALSE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                ),
                'updated_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('client_id');
        $this->dbforge->add_key('user_id');
        $this->dbforge->add_key('contact_type_id');
		$this->dbforge->create_table('client_contacts', FALSE, array('ENGINE'=>'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('client_contacts', TRUE);
    }
}
