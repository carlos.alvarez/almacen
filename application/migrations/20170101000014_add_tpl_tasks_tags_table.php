<?php

class Migration_Add_tpl_tasks_tags_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'auto_increment'    => TRUE,
                    'null'              => FALSE
                ),
                'tpl_task_id' => array(
                    'type'              => 'INT',
                    'unsigned'          => TRUE,
                    'null'              => FALSE
                ),
                'tag' => array(
                    'type'              => 'VARCHAR',
                    'constraint'        => 60,
                    'null'              => FALSE
                ),
                'color' => array(
                    'type'              => 'CHAR',
                    'constraint'        => 7,
                    'null'              => FALSE
                ),
                'created_at' => array(
                    'type'              => 'TIMESTAMP',
                    'null'              => TRUE
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('tpl_task_id');
        $this->dbforge->add_key('tag');
        $this->dbforge->create_table('tpl_tasks_tags', FALSE, array('ENGINE' => 'InnoDB'));
    }

    public function down()
    {
        $this->dbforge->drop_table('tpl_tasks_tags', TRUE);
    }
}
