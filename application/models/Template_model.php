<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Template_model
 *
 * @property Tpl_stage_model $tpl_stage_model
 * @property Tpl_task_model $tpl_task_model
 * @property Tpl_task_position_model $tpl_task_position_model
 * @property Tpl_task_prerequisite_model $tpl_task_prerequisite_model
 * @property Tpl_task_requirement_model $tpl_task_requirement_model
 * @property Tpl_task_tag_model $tpl_task_tag_model
 */
class Template_model extends MY_Model
{

    /**
     * @var string
     */
    protected $_table = 'templates';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field' => 'id',
            'label' => 'ID',
            'rules' => 'is_natural_no_zero'
        ),
        array(
            'field' => 'project_category_id',
            'label' => 'lang:project',
            'rules' => 'trim|required|is_natural_no_zero|exist[project_categories.id]'
        ),
        array( // title
            'field' => 'title',
            'label' => 'lang:title',
            'rules' => 'trim|required|max_length[150]'
        ),
        array( // description
            'field' => 'description',
            'label' => 'lang:description',
            'rules' => 'trim'
        ),
        array( // created_at
            'field' => 'created_at',
            'label' => 'created_at',
            'rules' => 'trim'
        ),
        array( // updated_at
            'field' => 'updated_at',
            'label' => 'updated_at',
            'rules' => 'trim'
        )
    );

    /**
     * @return string
     */
    public function datatable_json()
    {
        $this->load->library('datatables');
        $grant_edit = grant_access('template', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('template', 'delete') ? 'true' : 'false';
        $grant_duplicate = grant_access('template', 'create') ? 'true' : 'false';

        $this->datatables->select('
		    templates.id,
		    templates.title,
		    project_categories.name as project_category,
		    project_categories.id as project_category_id,
		    templates.created_at,
		    DATE_FORMAT(templates.created_at, "%d/%b/%y %h:%i %p") AS created_at_formatted,
		    templates.updated_at,
		    DATE_FORMAT(templates.updated_at, "%d/%b/%y %h:%i %p") AS updated_at_formatted,
		')->from('templates')
            ->join('project_categories', 'project_categories.id = templates.project_category_id')
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete)
            ->add_column('duplicate', '$1', $grant_duplicate);

        return $this->datatables->generate();
    }

    /**
     * @param array|int|string $pk
     * @return bool
     */
    public function delete($pk)
    {
        $this->load->model('tpl_stage_model');
        $this->db->trans_start();
            $this->tpl_stage_model->delete_where(array('template_id' => $pk));
            parent::delete($pk);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){ return FALSE; }

        return TRUE;
    }

    /**
     * @param array|null $options
     * @param bool $array_result
     * @return object|array|null
     */
    public function find($options = NULL, $array_result = FALSE)
    {
        $this->db->select("{$this->_table}.*,
                            project_categories.name as project_category,
                            project_categories.id as project_category_id")
                ->from($this->_table)
                ->join('project_categories','project_categories.id = project_category_id');
        if ( isset($options['select']) )    { $this->db->select($options['select'], FALSE); }
        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->group_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @param $pk
     * @param $title
     *
     * @return bool
     */
    public function duplicate($pk, $title)
    {
        $template = $this->get($pk);
        $insert_result = $this->insert(array(
            'title' => $title,
            'project_category_id'   => $template->project_category_id,
            'description'           => $template->description
        ));

        if ( $insert_result ){
            $this->load->model('tpl_stage_model');
            $this->load->model('tpl_task_model');
            $this->load->model('tpl_task_position_model');
            $this->load->model('tpl_task_prerequisite_model');
            $this->load->model('tpl_task_requirement_model');
            $this->load->model('tpl_task_tag_model');

            $task_prerequisites = array();  // CREATING ASSOCIATIVE ARRAY FOR PREREQUISITES

            $stages = $this->tpl_stage_model->find(array('where' => array('template_id' => $pk)));
            foreach($stages as $stage){
                $stage_id = $this->tpl_stage_model->insert(array(
                    'template_id'           => $insert_result,
                    'title'                 => $stage->title,
                    'descripcion'           => $stage->description,
                    'weight'                => $stage->weight,
                    'client_visibility'     => $stage->client_visibility,
                    'client_notificacion'   => $stage->client_notification,
                    'order'                 => $stage->order
                ));

                $tpl_task = $this->tpl_task_model->find(array('where' => array('tpl_stage_id' => $stage->id)));
                foreach($tpl_task as $task){
                    $task_id = $this->tpl_task_model->insert(array(
                        'tpl_stage_id'          => $stage_id,
                        'title'                 => $task->title,
                        'description'           => $task->description,
                        'duration_days'         => $task->duration_days,
                        'duration_hours'        => $task->duration_hours,
                        'day_type'              => $task->day_type,
                        'weight'                => $task->weight,
                        'client_visibility'     => $task->client_visibility,
                        'client_notification'   => $task->client_notification,
                        'order'                 => $task->order
                    ));

                   if( $task_id ) $task_prerequisites[$task->id] = $task_id; // CREATING ASSOCIATIVE ARRAY FOR PREREQUISITES

                    // TPL TASK POSITION
                    $tpl_task_positions = $this->tpl_task_position_model->find(array('where' => array('tpl_task_id' => $task->id)));
                    if( count($tpl_task_positions) > 0 ){
                        foreach($tpl_task_positions as $position){
                            $this->tpl_task_position_model->insert(array('tpl_task_id'   => $task_id, 'position_id'   => $position->position_id));
                        }
                    }
                    // TPL TASK REQUIREMENT
                    $tpl_task_requirements = $this->tpl_task_requirement_model->find(array('where' => array('tpl_task_id' => $task->id)));
                    if( $tpl_task_requirements ){
                        foreach($tpl_task_requirements as $requirement){
                            $this->tpl_task_requirement_model->insert(array(
                                'tpl_task_id'   => $task_id,
                                'requirement'   => $requirement->requirement,
                                'weight'        => $requirement->weight
                            ));
                        }
                    }
                    // TPL TASK TAG
                    $tpl_task_tags = $this->tpl_task_tag_model->find(array('where' => array('tpl_task_id' => $task->id)));
                    if( $tpl_task_tags ){
                        foreach ($tpl_task_tags as $tag){
                            $this->tpl_task_tag_model->insert(array('tpl_task_id' => $task_id, 'tag' => $tag->tag, 'color' => $tag->color));
                        }
                    }
                }
            }
            // TPL TASK PREREQUISITES
            foreach ($task_prerequisites as $task_id_old => $task_id_new){
                $tpl_task_prerequisites = $this->tpl_task_prerequisite_model->find(array('where' => array('tpl_task_id' => $task_id_old)));

                if( $tpl_task_prerequisites ){
                    foreach($tpl_task_prerequisites as $prerequisite){
                            $this->tpl_task_prerequisite_model->insert(array(
                                'tpl_task_id' => $task_id_new,
                                'tpl_task_prerequisite' => $task_prerequisites[$prerequisite->tpl_task_prerequisite]
                            ));
                    }
                }
            }

            return TRUE;
        }
        return FALSE;

    }

    /**
     * @param int $template_id
     * @param int $task_id
     * @return array
     */
    public function get_task_prerequisites_candidates($template_id, $task_id)
    {
        return $this->db->select('
            tpl_stages.id AS stage_id,
            tpl_stages.title AS stage,
            tpl_tasks.id AS task_id,
            tpl_tasks.title AS task_title,
            SUM(IF(t1.tpl_task_prerequisite = ' . $task_id . ', 1, 0)) AS t1,
            SUM(IF(t2.tpl_task_prerequisite = ' . $task_id . ', 1, 0)) AS t2,
            SUM(IF(t3.tpl_task_prerequisite = ' . $task_id . ', 1, 0)) AS t3,
            SUM(IF(t4.tpl_task_prerequisite = ' . $task_id . ', 1, 0)) AS t4,
            SUM(IF(t5.tpl_task_prerequisite = ' . $task_id . ', 1, 0)) AS t5
        ')
            ->from('templates')
            ->join('tpl_stages', 'tpl_stages.template_id = templates.id')
            ->join('tpl_tasks', 'tpl_tasks.tpl_stage_id = tpl_stages.id')
            ->join('tpl_tasks_prerequisites t1', 't1.tpl_task_id = tpl_tasks.id', 'left')
            ->join('tpl_tasks_prerequisites t2', 't2.tpl_task_id = t1.tpl_task_prerequisite', 'left')
            ->join('tpl_tasks_prerequisites t3', 't3.tpl_task_id = t2.tpl_task_prerequisite', 'left')
            ->join('tpl_tasks_prerequisites t4', 't4.tpl_task_id = t3.tpl_task_prerequisite', 'left')
            ->join('tpl_tasks_prerequisites t5', 't5.tpl_task_id = t4.tpl_task_prerequisite', 'left')
            ->where(array('templates.id' => $template_id, 'tpl_tasks.id !=' => $task_id))
            ->order_by('tpl_stages.order, tpl_tasks.order ASC')
            ->group_by('tpl_tasks.id')
            ->get()->result();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get_structure($id)
    {
        return $this->db->select('
            tpl_stages.id AS stage_id,
            tpl_stages.title AS stage,
            tpl_tasks.id AS task_id,
            tpl_tasks.title AS task
        ')
            ->from('templates')
            ->join('tpl_stages', 'tpl_stages.template_id = templates.id')
            ->join('tpl_tasks', 'tpl_tasks.tpl_stage_id = tpl_stages.id')
            ->where(array('templates.id' => $id))
            ->order_by('tpl_stages.order, tpl_tasks.order ASC')
            ->get()->result();
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function get_positions($id)
    {
        return $this->db->select('
            templates.title as template,
            templates.description AS template_description,
            project_categories.id as project_category_id,
            project_categories.name as project_category,
            tpl_stages.id AS stage_id,
            tpl_stages.title AS stage,
            tpl_tasks.id AS task_id,
            tpl_tasks.title AS task,
            positions.id AS position_id,
            positions.name AS position,
            positions.description AS position_description,
            positions.company_id AS position_company_id
        ')
            ->from('templates')
            ->join('tpl_stages', 'tpl_stages.template_id = templates.id')
            ->join('tpl_tasks', 'tpl_tasks.tpl_stage_id = tpl_stages.id')
            ->join('tpl_tasks_positions', 'tpl_tasks_positions.tpl_task_id = tpl_tasks.id')
            ->join('positions', 'tpl_tasks_positions.position_id = positions.id')
            ->join('project_categories', 'project_categories.id = project_category_id')
            ->where(array('templates.id' => $id))
            ->order_by('tpl_stages.order, tpl_tasks.order, positions.name ASC')
            ->get()->result();
    }

}
