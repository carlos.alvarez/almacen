<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Project_model
 * @property Project_tag_model $project_tag_model
 * @property Template_model $template_model
 * @property Tpl_stage_model $tpl_stage_model
 * @property Stage_model $stage_model
 * @property Tpl_task_model $tpl_task_model
 * @property Calendar_model $calendar_model
 * @property Task_model $task_model
 * @property Tpl_task_requirement_model $tpl_task_requirement_model
 * @property Tpl_task_prerequisite_model $tpl_task_prerequisite_model
 * @property Task_tag_model $task_tag_model
 * @property Tpl_task_tag_model $tpl_task_tag_model
 * @property Task_employee_model $task_employee_model
 * @property Task_prerequisite_model $task_prerequisite_model
 * @property Task_requirement_model $task_requirement_model
 * @property Project_log_model $project_log_model
 * @property Project_file_model $project_file_model
 * @property Project_comment_model $project_comment_model
 * @property Project_brand_model $project_brand_model
 * @property Project_model $project_model
 */
class Project_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'projects';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( //
            'field'     => 'client_id',
            'label'     => 'Cliente',
            'rules'     => 'trim|required|is_natural_no_zero|exist[clients.id]'
        ),
        array( //
            'field'     => 'company_id',
            'label'     => 'Empresa',
            'rules'     => 'trim|required|is_natural_no_zero|exist[companies.id]'
        ),
        array( //
            'field'     => 'invoice_company_id',
            'label'     => 'Facturado por',
            'rules'     => 'trim|required|is_natural_no_zero|exist[companies.id]'
        ),
        array( //
            'field'     => 'manager_id',
            'label'     => 'lang:manager',
            'rules'     => 'trim|required|is_natural_no_zero|exist[employees.user_id]'
        ),
        array( //
            'field'     => 'project_category_id',
            'label'     => 'Categoría',
            'rules'     => 'trim|required|is_natural_no_zero|exist[project_categories.id]'
        ),
        array( // stage_status_id
            'field'     => 'stage_status_id',
            'label'     => 'Estado Interno',
            'rules'     => 'trim|required|is_natural_no_zero|exist[stages_status.id]'
        ),
        array( // reason_id
            'field'     => 'reason_id',
            'label'     => 'Causa',
            'rules'     => 'trim|required|is_natural_no_zero|exist[reasons.id]'
        ),
        array( // name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|min_length[4]|max_length[100]'
		),
        array( // Description
            'field'     => 'description',
            'label'     => 'lang:description',
            'rules'     => 'trim|min_length[4]|max_length[250]'
        ),
        array( // start_date
            'field'     => 'start_date',
            'label'     => 'lang:start_date',
            'rules'     => 'trim|required'
        ),
		array( // estimated_end_date
		    'field'     => 'estimated_end_date',
            'label'     => 'lang:estimated_end_date',
            'rules'     => 'trim|required'
        ),
        array( // end_date
            'field'     => 'end_date',
            'label'     => 'lang:end_date',
            'rules'     => 'trim'
        ),
		array( // status
			'field'     => 'status',
			'label'     => 'Estado',
			'rules'     => 'trim|in_list[progreso,finalizado,anulado]'
		),
        /*array(
            'field'     => 'budget',
            'label'     => 'Presupuesto',
            'rules'     => 'trim|in_list[yes,no]'
        ),*/
        array(
            'field'     => 'client_level_visibility',
            'label'     => 'Nivel de visibilidad del cliente',
            'rules'     => 'trim|in_list[stage,task]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param null $options
     * @param bool $array_result
     * @return mixed
     */
	public function find($options = NULL, $array_result = FALSE)
    {
        if ( ! isset($options['select']) )
        {
            $this->db->select('
                projects.*,
                clients.name as client,
                companies.name as company,
                ic.name as invoice_company,
                CONCAT(users.first_name," ",users.last_name) as manager,
                stages_status.name AS stage_status,
                reasons.name AS reason
            ');
        }
        else { $this->db->select($options['select'], FALSE); }

        $this->db->from($this->_table)
            ->join('clients', 'projects.client_id = clients.id')
            ->join('companies', 'projects.company_id = companies.id')
            ->join('companies ic', 'projects.invoice_company_id = ic.id')
            ->join('users', 'projects.manager_id = users.id')
            ->join('stages_status', 'projects.stage_status_id = stages_status.id', 'left')
            ->join('reasons', 'projects.reason_id = reasons.id', 'left');

        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->group_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @param $data
     * @param bool $skip_validation
     * @return int|false
     */
    public function insert($data, $skip_validation = FALSE)
    {
        $id = parent::insert($data, $skip_validation);

        if ( $id && isset($data['tags']) )
        {
            $this->update_tags($id, $data['tags']);
        }

        if ( $id ){ // INSERT LOG BY PROJECT ACTIONS
            $this->load->model('project_log_model');
            $this->project_log_model->insert(array(
                'project_id'    =>  $id,
                'user_id'       =>  $this->session->userdata('user_id'),
                'message'       =>  "El usuario {$this->session->userdata('full_name')} ha creado el proyecto {$data['name']}."
            ));
        }

        return $id;
    }

    /**
     * @param $pk
     * @param $data
     * @param bool $skip_validation
     * @return bool
     */
    public function update($pk, $data, $skip_validation = FALSE)
    {

        if ( isset($data['start_date']) )
        {
            $temp = explode('/', $data['start_date']);
            if ( count($temp) === 3 )
            {
                $data['start_date'] = "{$temp[2]}-{$temp[1]}-$temp[0]";
            }
        }

        if ( isset($data['estimated_end_date']) )
        {
            $estimated = explode('/', $data['estimated_end_date']);
            if ( count($estimated) === 3 )
            {
                $data['estimated_end_date'] = "{$estimated[2]}-{$estimated[1]}-$estimated[0]";
            }
        }

        if ( isset($data['end_date']) )
        {
            $estimated = explode('/', $data['end_date']);
            if ( count($estimated) === 3 )
            {
                $data['end_date'] = "{$temp[2]}-{$temp[1]}-$temp[0]";
            }
        }

        $project_before = $this->get($pk);
        $result = parent::update($pk, $data, $skip_validation);

        if ( $result && isset($data['tags']) )
        {
            $this->update_tags($pk, $data['tags']);
        }
        $project_after = $this->get($pk);

        if ( $project_before->start_date !== $project_after->start_date )
        {
            $this->reschedule_tasks($pk);
        }

        if ( $result && isset($data['name']) ){ // INSERT LOG BY PROJECT ACTIONS
            $this->load->model('project_log_model');
            $this->project_log_model->insert(array(
                'project_id'    =>  $pk,
                'user_id'       =>  $this->session->userdata('user_id'),
                'message'       =>  "El usuario {$this->session->userdata('full_name')} ha actualizado el proyecto {$data['name']}."
            ));
        }

        return $result;
    }

    /**
     * @param int $pk
     *
     * @return bool
     */
    public function delete($pk)
    {

        $this->load->model('stage_model');
        $this->load->model('project_tag_model');
        $this->load->model('project_log_model');
        $this->load->model('project_file_model');
        $this->load->model('project_comment_model');
        $this->load->model('project_brand_model');

        $this->db->trans_start();
            $this->stage_model->delete_where(array('project_id' => $pk));
            $this->project_tag_model->delete_where(array('project_id' => $pk));
            $this->project_file_model->delete_where(array('project_id' => $pk));
            $this->project_log_model->delete_where(array('project_id' => $pk));
            $this->project_comment_model->delete_where(array('project_id' => $pk));
            $this->project_brand_model->delete_where(array('project_id' => $pk));
            parent::delete($pk);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){ return FALSE; }

        return TRUE;
    }

    /**
     * @return string
     */
	public function datatable_json()
	{
		$this->load->library('datatables');
        $this->load->helper('date_helper');
        $grant_edit = grant_access('project','edit') ? 'true' : 'false';
        $grant_delete = grant_access('project','delete') ? 'true' : 'false';
		$this->datatables->select('
			projects.id,
			projects.name,
			projects.status,
			projects.start_date,
			projects.estimated_end_date,
			projects.end_date,
			clients.name AS client,
            companies.name AS company,
            ic.name as invoice_company,
            CONCAT(users.first_name," ",users.last_name) as manager,
            projects_tags.tag,
            GROUP_CONCAT(DISTINCT projects_tags.tag ORDER BY projects_tags.tag SEPARATOR ",") AS tags,
            projects.stage_status_id,
            projects.reason_id,
            stages_status.name AS stage_status,
            reasons.name AS reason
		')->from('projects')
        ->join('projects_tags', 'projects_tags.project_id = projects.id', 'left')
        ->join('clients', 'projects.client_id = clients.id')
        ->join('companies', 'projects.company_id = companies.id')
        ->join('companies ic', 'projects.invoice_company_id = ic.id', 'left')
        ->join('users', 'projects.manager_id = users.id')
        ->join('stages_status', 'projects.stage_status_id = stages_status.id', 'left')
        ->join('reasons', 'projects.reason_id = reasons.id', 'left')
        ->group_by('projects.id')
        ->add_column('delay', '$1', 'date_difference(estimated_end_date)')
        ->add_column('start_date_nice', '$1', 'nice_date(start_date, "d-M-Y")')
		->add_column('estimated_end_date_nice', '$1', 'nice_date(estimated_end_date, "d-M-Y")')
        ->add_column('end_date_nice', '$1', 'nice_date(end_date, "d-M-Y")')
        ->add_column('edit', '$1', $grant_edit)
        ->add_column('delete', '$1', $grant_delete);

        if ( $this->input->get('delayed') )
        {
            $this->datatables->where_in('projects.status', array('progreso'))
                ->where('DATE(projects.estimated_end_date) != "0000-00-00" AND DATE(projects.estimated_end_date) < DATE(NOW())');
        }

		return $this->datatables->generate();
	}

    /**
     * My_projects method
     *
     * @param $pk
     *
     * @param bool|int $client
     *
     * @return string
     */
    public function my_datatable_json($pk, $client=FALSE)
    {
        $this->load->library('datatables');
        $this->load->helper('date_helper');
        $grant_edit = grant_access('project','edit') ? 'true' : 'false';
        $grant_delete = grant_access('project','delete') ? 'true' : 'false';
        $this->datatables->select('
			projects.id,
			projects.name,
			projects.status,
			projects.start_date,
			projects.estimated_end_date,
			projects.end_date,
			clients.name as client,
            companies.name as company,
            ic.name as invoice_company,
            CONCAT(users.first_name," ",users.last_name) as manager,
            projects_tags.tag,
            GROUP_CONCAT(DISTINCT projects_tags.tag ORDER BY projects_tags.tag SEPARATOR ",") AS tags,
            projects.stage_status_id,
            projects.reason_id,
            stages_status.name AS stage_status,
            reasons.name AS reason
		')->from('projects')
            ->join('projects_tags', 'projects_tags.project_id = projects.id', 'left')
            ->join('clients', 'projects.client_id = clients.id')
            ->join('companies', 'projects.company_id = companies.id')
            ->join('companies ic', 'projects.invoice_company_id = ic.id', 'left')
            ->join('users', 'projects.manager_id = users.id')
            ->join('stages','stages.project_id = projects.id', 'left')
            ->join('tasks','tasks.stage_id = stages.id', 'left')
            ->join('tasks_employees', 'tasks_employees.task_id = tasks.id', 'left')
            ->join('stages_status', 'projects.stage_status_id = stages_status.id', 'left')
            ->join('reasons', 'projects.reason_id = reasons.id', 'left')
            ->group_by('projects.id')
            ->add_column('delay', '$1', 'date_difference(estimated_end_date)')
            ->add_column('start_date_nice', '$1', 'nice_date(start_date, "d-M-Y")')
            ->add_column('estimated_end_date_nice', '$1', 'nice_date(estimated_end_date, "d-M-Y")')
            ->add_column('end_date_nice', '$1', 'nice_date(end_date, "d-M-Y")')
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

        if ( !$client )
        {
            // BY PROJECT MANAGER
            $this->datatables->join('employees e1', 'projects.manager_id = e1.user_id','left')
                ->join('employees e2', 'e1.user_id = e2.supervisor_id','left')
                ->join('employees e3', 'e2.user_id = e3.supervisor_id','left');

            // BY TASK EMPLOYEE
            $this->datatables->join('employees te1', 'tasks_employees.user_id = te1.user_id','left')
                ->join('employees te2', 'te1.user_id = te2.supervisor_id','left')
                ->join('employees te3', 'te2.user_id = te3.supervisor_id','left');

            $this->datatables->where('projects.manager_id = '.$pk.' OR tasks_employees.user_id ='.$pk)
                ->or_where('e1.supervisor_id ='.$pk.' OR e2.supervisor_id ='.$pk.' OR e3.supervisor_id ='.$pk)
                ->or_where('te1.supervisor_id ='.$pk.' OR te2.supervisor_id ='.$pk.' OR te3.supervisor_id ='.$pk);
        }
        else
        {
            $this->datatables->where('client_id = '.$client);
        }

        if ( $this->input->get('delayed') )
        {
            $this->datatables->where_in('projects.status', array('progreso'))
                ->where('DATE(projects.estimated_end_date) != "0000-00-00" AND DATE(projects.estimated_end_date) < DATE(NOW())');
        }

        return $this->datatables->generate();
    }

    public function projects_filter_json($day,$month,$year,$status,$company,$category)
    {
        $this->load->library('datatables');
        $this->datatables->select('
			projects.id,
			projects.name,
			projects.status,
			projects.start_date,
			projects.estimated_end_date,
			projects.end_date,
			clients.name as client,
            companies.name as company,
            ic.name as invoice_company,
            CONCAT(users.first_name," ",users.last_name) as manager,
            projects_tags.tag,
            GROUP_CONCAT(DISTINCT projects_tags.tag ORDER BY projects_tags.tag SEPARATOR ",") AS tags
		')->from('projects')
            ->join('projects_tags', 'projects_tags.project_id = projects.id', 'left')
            ->join('clients', 'projects.client_id = clients.id')
            ->join('companies', 'projects.company_id = companies.id')
            ->join('companies ic', 'projects.invoice_company_id = ic.id', 'left')
            ->join('users', 'projects.manager_id = users.id')
            ->group_by('projects.id')
            ->add_column('delay', '$1', 'date_difference(estimated_end_date)')
            ->add_column('start_date_nice', '$1', 'nice_date(start_date, "d-M-Y")')
            ->add_column('estimated_end_date_nice', '$1', 'nice_date(estimated_end_date, "d-M-Y")')
            ->add_column('end_date_nice', '$1', 'nice_date(end_date, "d-M-Y")')
            ->where("DAY(projects.start_date) ={$day}")
            ->where("MONTH(projects.start_date) ={$month}");

        if ( $year !== 'all' ) { $year = (int)$year; $this->datatables->where("YEAR(projects.start_date) = '{$year}'"); }
        if ( $status !== 'all' ) { $status = strtolower($status); $this->datatables->where("projects.status = '{$status}'"); }
        if ( $company !== 'all' ) { $company = (int)$company; $this->datatables->where("projects.company_id = {$company}"); }
        if ( $category !== 'all' ) { $category = (int)$category; $this->datatables->where("projects.project_category_id = {$category}"); }

        return $this->datatables->generate();
    }

    /**
     * @param $id
     * @param bool $array_result
     * @return array|bool
     */
	public function get_tags($id, $array_result = FALSE)
    {
        $this->load->model('project_tag_model');
        return $this->project_tag_model->find(array('where'=>array('project_id'=>$id)), $array_result);
    }

    /**
     * @param $id
     * @return array|bool
     */
    public function get_employees($id)
    {
        $project = $this->get($id);
        if ( !$project ) { return FALSE; }

        $users = $this->db->select('tasks_employees.user_id')
            ->from('tasks_employees')
            ->join('tasks', 'tasks_employees.task_id = tasks.id')
            ->join('stages', 'tasks.stage_id = stages.id')
            ->where(array(
                'stages.project_id'     => $id
            ))->group_by('tasks_employees.user_id')
            ->get()->result_array();

        if ( $users ) { $users = array_column($users, 'user_id'); }
        else { $users = array(); }

        if ( !in_array($project->manager_id, $users) ) { $users[] = $project->manager_id; }

        return $users;
    }

    /**
     * @param $id
     * @param $tags
     */
	public function update_tags($id, $tags)
    {
        $this->load->model('project_tag_model');

        if ( !is_array($tags) ) { $tags = explode(',', $tags); }

        $temp = $this->project_tag_model->find(array('where'=>array('project_id'=>$id)));
        $actions = array();

        foreach ( $temp as $tag ) { $actions[$tag->tag] = 'delete'; }
        foreach ( $tags as $tag ) { $actions[$tag] = (isset($actions[$tag])) ? 'nothing' : 'insert'; }

        foreach ($actions as $tag => $action)
        {
            switch ($action)
            {
                case 'delete':
                    $this->project_tag_model->delete_where(array(
                        'project_id'                => $id,
                        'tag'                       => $tag
                    ));
                    break;
                case 'insert':
                    if ( $tag === '' ) { continue; }
                    $this->project_tag_model->insert(array(
                        'project_id'                => $id,
                        'tag'                       => $tag
                    ));
                    break;
            }
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function get_tasks($id)
    {
        return $this->db->select('
            tasks.*
        ')->from('tasks')
        ->join('stages', 'tasks.stage_id = stages.id')
        ->where(array(
            'stages.project_id' => $id
        ))
        ->group_by('tasks.id')->get()->result();
    }

    /**
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public function create_from_template($data)
    {
        $this->load->model('task_employee_model');
        $this->load->model('calendar_model');

        $temp = explode('/', $data['start_date']);
        $data['start_date'] = "{$temp[2]}-{$temp[0]}-{$temp[1]}";

        //$estimated = explode('/', $data['estimated_end_date']);
        //$data['estimated_end_date'] = "{$estimated[2]}-{$estimated[0]}-{$estimated[1]}";

        if ( !isset($data['budget']) ) { $data['budget'] = 'yes'; }

        if ( !$data['templates'] )
        {
            throw new Exception('No se definieron plantillas.');
        }
        $data_templates = json_decode($data['templates']);

        $this->db->trans_start();
        // Creamos la estructura básica
        $project_id = $this->_generate_project($data);
        $stages = array();
        $tasks = array();
        foreach ($data_templates as $data_template)
        {
            $_stages = $this->_generate_stages($data_template->id, $project_id);
            $stages = array_merge($stages, $_stages);
            foreach ($_stages as $_stage)
            {
                $_tasks = $this->_generate_tasks($_stage['tpl'], $_stage['id']);
                $tasks = array_merge($tasks, $_tasks);
            }
        }
        // Añadimos los empleados
        foreach ($tasks as $task)
        {
            if ( isset($data['positions'][$task['tpl']]) )
            {
                $positions = $data['positions'][$task['tpl']];
                foreach ($positions as $position)
                {
                    $res = $this->task_employee_model->insert(array(
                        'task_id'       => $task['id'],
                        'user_id'       => $position
                    ));

                    if ( !$res )
                    {
                        throw new Exception('Error al agregar empleado. '.validation_errors());
                    }
                }
            }
        }
        // Añadimos los prerequisitos
        $tpl_tasks = array();
        foreach ($tasks as $task) { $tpl_tasks[$task['tpl']] = $task['id']; }
        foreach ($tasks as $kt => $task)
        {
            foreach ($task['prerequisites'] as $kp => $prerequisite)
            {
                $tasks[$kt]['prerequisites'][$kp]['id'] = $tpl_tasks[$prerequisite['tpl']];
                $res = $this->task_prerequisite_model->insert(array(
                    'task_id'               => $task['id'],
                    'task_prerequisite'     => $tpl_tasks[$prerequisite['tpl']]
                ));

                if ( !$res )
                {
                    throw new Exception('Error al agregar prerequisito. '.validation_errors());
                }
            }
        }

        /*$no_dated_tasks = array();
        $dated_tasks = array();
        foreach ($tasks as $task)
        {
            $no_dated_tasks[$task['id']] = $task;
        }
        while ( count($no_dated_tasks) > 0 )
        {
            foreach ($no_dated_tasks as $no_dated_task_id => $no_dated_task)
            {
                $start_date = $data['start_date'];
                if ( count($no_dated_task['prerequisites']) > 0 )
                {
                    $req_pend = FALSE;
                    foreach ( $no_dated_task['prerequisites'] as $prerequisite )
                    {
                        if ( !isset($dated_tasks[$prerequisite['id']]) )
                        {
                            $req_pend = TRUE;
                            break;
                        }
                        if ( $dated_tasks[$prerequisite['id']]['end_date'] >= $start_date )
                        {
                            $start_date = $this->calendar_model->calc_end_date(1, $dated_tasks[$prerequisite['id']]['end_date'], 2);
                        }
                    }
                    if ( $req_pend ) { continue; }
                }
                $end_date = $this->calendar_model->calc_end_date(1, $start_date, $no_dated_task['duration_days'], $no_dated_task['duration_hours'], $no_dated_task['day_type'] === 'laborable');
                if ( !$end_date ) { throw new Exception('Error al calcular fecha final de la tarea.'); }

                $res = $this->task_model->update($no_dated_task_id, array(
                    'start_date'            => $start_date,
                    'estimated_end_date'    => $end_date
                ));
                if ( !$res ) { throw new Exception('Error al guardar fechas de la tarea. '.validation_errors());}

                $dated_tasks[$no_dated_task_id] = array(
                    'start_date'    => $start_date,
                    'end_date'      => $end_date
                );
                unset($no_dated_tasks[$no_dated_task_id]);
            }
        }*/

        $this->reschedule_tasks($project_id);

        $this->db->trans_complete();

        if ( !$this->db->trans_status() ) { return FALSE; }

        return $project_id;
    }

    /**
     * @param $project_id
     * @return bool
     * @throws Exception
     */
    public function reschedule_tasks($project_id)
    {
        $this->load->model('calendar_model');
        $this->load->model('task_model');

        $project = $this->get($project_id);
        if ( !$project ) { throw new Exception('No existe un proyecto con este ID: '.$project_id); }

        $tasks = $this->db->select('
            tasks.*,
            IFNULL(tasks.end_date, tasks.estimated_end_date) AS end_date_r
        ')
            ->from('tasks')
            ->join('stages', 'tasks.stage_id = stages.id')
            ->join('projects', 'stages.project_id = projects.id')
            ->where(array('projects.id'=>$project_id))->get()->result_array();

        $prerequisites = $this->db->select('tasks_prerequisites.*')
            ->from('tasks_prerequisites')
            ->join('tasks', 'tasks_prerequisites.task_id = tasks.id')
            ->join('stages', 'tasks.stage_id = stages.id')
            ->join('projects', 'stages.project_id = projects.id')
            ->where(array('projects.id'=>$project_id))->get()->result_array();

        $no_dated_tasks = array();
        $dated_tasks = array();
        foreach ($tasks as $task)
        {
            $task['prerequisites'] = array();
            $no_dated_tasks[$task['id']] = $task;
        }
        foreach ($prerequisites as $prerequisite)
        {
            $no_dated_tasks[$prerequisite['task_id']]['prerequisites'][] = array('id'=>$prerequisite['task_prerequisite']);
        }

        while ( count($no_dated_tasks) > 0 )
        {
            foreach ($no_dated_tasks as $no_dated_task_id => $no_dated_task)
            {
                $start_date = $project->start_date;
                if ( count($no_dated_task['prerequisites']) > 0 )
                {
                    $req_pend = FALSE;
                    foreach ( $no_dated_task['prerequisites'] as $prerequisite )
                    {
                        if ( !isset($dated_tasks[$prerequisite['id']]) )
                        {
                            $req_pend = TRUE;
                            break;
                        }
                        if ( $dated_tasks[$prerequisite['id']]['end_date'] >= $start_date && $dated_tasks[$prerequisite['id']]['status'] !== 'anulado' )
                        {
                            $start_date = $this->calendar_model->calc_end_date(1, $dated_tasks[$prerequisite['id']]['end_date'], 2);
                        }
                    }
                    if ( $req_pend ) { continue; }
                }

                if ( $no_dated_task['status'] === 'cerrado' )
                {
                    $dated_tasks[$no_dated_task_id] = array(
                        'status'        => $no_dated_task['status'],
                        'start_date'    => $no_dated_task['start_date'],
                        'end_date'      => $no_dated_task['end_date_r']
                    );
                    unset($no_dated_tasks[$no_dated_task_id]);
                    continue;
                }

                if ( $no_dated_task['status'] !== 'pendiente' )
                {
                    $start_date = $no_dated_task['start_date'];
                }

                $end_date = $this->calendar_model->calc_end_date(1, $start_date, $no_dated_task['duration_days'], $no_dated_task['duration_hours'], $no_dated_task['day_type'] === 'laborable');
                if ( !$end_date ) { throw new Exception('Error al calcular fecha final de la tarea.'); }

                $res = $this->task_model->update($no_dated_task_id, array(
                    'start_date'            => $start_date,
                    'estimated_end_date'    => $end_date,
                    'no_reschedule'         => TRUE
                ));
                if ( !$res ) { throw new Exception('Error al guardar fechas de la tarea. '.validation_errors());}

                $dated_tasks[$no_dated_task_id] = array(
                    'status'        => $no_dated_task['status'],
                    'start_date'    => $start_date,
                    'end_date'      => $end_date
                );
                unset($no_dated_tasks[$no_dated_task_id]);
            }
        }
        $this->update_estimated_end_date($project_id);

        return TRUE;
    }

    /**
     * @param array $data
     * @return int
     * @throws Exception
     */
    private function _generate_project($data)
    {
        $project_id = $this->insert(array(
            'client_id'             => $data['client_id'],
            'company_id'            => $data['company_id'],
            'invoice_company_id'    => $data['invoice_company_id'],
            'manager_id'            => $data['manager_id'],
            'stage_status_id'       => 1, // Pendiente
            'reason_id'             => 1, // En proceso
            'name'                  => $data['name'],
            'description'           => $data['description'],
            'start_date'            => $data['start_date'],
            'budget'                => $data['budget'],
            'estimated_end_date'    => $data['start_date'], //$data['estimated_end_date'],
            'project_category_id'   => $data['project_category_id'],
            'status'                => 'progreso',
            'tags'                  => $data['tags']
        ));
        if ( !$project_id )
        {
            throw new Exception('No se pudo crear el projecto. '.validation_errors());
        }

        return $project_id;
    }

    /**
     * @param int $template_id
     * @param int $project_id
     * @return array
     * @throws Exception
     */
    private function _generate_stages($template_id, $project_id)
    {
        $this->load->model('tpl_stage_model');
        $this->load->model('stage_model');

        $last_stage = $this->stage_model->find(array(
            'where'     => array(
                'stages.project_id' => $project_id
            ),
            'order_by'  => 'stages.order DESC',
            'limit'     => 1
        ));

        $order = 0;
        if ( $last_stage )
        {
            $order = $last_stage->order;
        }

        $tpl_stages = $this->tpl_stage_model->find(array('where'=>array('template_id'=>$template_id)));
        $stages = array();
        foreach ($tpl_stages as $tpl_stage)
        {
            $stage_id = $this->stage_model->insert(array(
                'project_id'            => $project_id,
                'template_id'           => $template_id,
                'title'                 => $tpl_stage->title,
                'description'           => $tpl_stage->description,
                'weight'                => $tpl_stage->weight,
                'client_visibility'     => $tpl_stage->client_visibility,
                'client_notification'   => $tpl_stage->client_notification,
                'order'                 => $order + $tpl_stage->order
            ));
            if ( !$stage_id )
            {
                throw new Exception('No se pudo crear la fase ['.$tpl_stage->id.']. '.validation_errors());
            }

            $stages[] = array(
                'tpl'   => $tpl_stage->id,
                'id'    => $stage_id
            );
        }

        return $stages;
    }

    /**
     * @param int $tpl_stage_id
     * @param int $stage_id
     * @return array
     * @throws Exception
     */
    private function _generate_tasks($tpl_stage_id, $stage_id)
    {
        $this->load->model('tpl_task_model');
        $this->load->model('tpl_task_prerequisite_model');
        $this->load->model('tpl_task_requirement_model');
        $this->load->model('tpl_task_tag_model');
        $this->load->model('task_model');
        $this->load->model('task_prerequisite_model');
        $this->load->model('task_requirement_model');
        $this->load->model('task_employee_model');
        $this->load->model('task_tag_model');

        $tpl_tasks = $this->tpl_task_model->find(array('where'=>array('tpl_stage_id'=>$tpl_stage_id)));

        // Creacion de las tareas
        $tasks = array();
        foreach ($tpl_tasks as $tpl_task)
        {
            $task_id = $this->task_model->insert(array(
                'no_reschedule'         => TRUE,
                'stage_id'              => $stage_id,
                'title'                 => $tpl_task->title,
                'description'           => $tpl_task->description,
                'start_date'            => date('Y-m-d'),
                'estimated_end_date'    => date('Y-m-d'),
                'duration_days'         => $tpl_task->duration_days,
                'duration_hours'        => $tpl_task->duration_hours,
                'day_type'              => $tpl_task->day_type,
                'weight'                => $tpl_task->weight,
                'client_visibility'     => $tpl_task->client_visibility,
                'client_notification'   => $tpl_task->client_notification,
                'order'                 => $tpl_task->order,
                'status'                => 'pendiente',
                'status_changer'        => $tpl_task->status_changer,
                'stage_status_id'       => $tpl_task->stage_status_id,
                'reason_id'             => $tpl_task->reason_id
            ));
            if ( !$task_id )
            {
                throw new Exception('Error al crear tarea ['.$tpl_task->id.']. '.validation_errors());
            }

            // Creacion de requerimientos
            $tpl_requirements = $this->tpl_task_requirement_model->find(array('where'=>array('tpl_task_id'=>$tpl_task->id)));
            if ( !$tpl_requirements )
            {
                $req = new stdClass();
                $req->id = 0;
                $req->requirement = $tpl_task->title;
                $req->weight = 100.0;
                $tpl_requirements = array($req);
            }
            $requirements = array();
            foreach ($tpl_requirements as $tpl_requirement)
            {
                $requirement_id = $this->task_requirement_model->insert(array(
                    'task_id'       => $task_id,
                    'requirement'   => $tpl_requirement->requirement,
                    'weight'        => $tpl_requirement->weight
                ));
                if ( !$requirement_id )
                {
                    throw new Exception('Error al crear requerimiento de tarea ['.$tpl_requirement->id.']. '.validation_errors());
                }
                $requirements[] = array(
                    'tpl'   => $tpl_requirement->id,
                    'id'    => $requirement_id
                );
            }

            // Creacion de tags
            $tpl_tags = $this->tpl_task_tag_model->find(array('where'=>array('tpl_task_id'=>$tpl_task->id)));
            $tags = array();
            foreach ($tpl_tags as $tpl_tag)
            {
                $tag_id = $this->task_tag_model->insert(array(
                    'task_id'       => $task_id,
                    'tag'           => $tpl_tag->tag,
                    'color'         => '#BBBBBB'
                ));
                if ( !$tag_id )
                {
                    throw new Exception('Error al crear tag de tarea ['.$tpl_tag->id.']. '.validation_errors());
                }
                $tags[] = array(
                    'tpl'   => $tpl_tag->id,
                    'id'    => $tag_id
                );
            }

            // Prerequisitos
            $tpl_prerequisites = $this->tpl_task_prerequisite_model->find(array('where'=>array('tpl_task_id'=>$tpl_task->id)));
            $prerequisites = array();
            foreach ($tpl_prerequisites as $tpl_prerequisite)
            {
                $prerequisites[] = array(
                    'tpl'   => $tpl_prerequisite->tpl_task_prerequisite
                );
            }

            $tasks[] = array(
                'tpl'               => $tpl_task->id,
                'id'                => $task_id,
                'duration_days'     => $tpl_task->duration_days,
                'duration_hours'    => $tpl_task->duration_hours,
                'day_type'          => $tpl_task->day_type,
                'requirements'      => $requirements,
                'tags'              => $tags,
                'prerequisites'     => $prerequisites
            );
        }

        return $tasks;
    }

    /**
     * @param int $project_id
     * @return array
     */
    public function gantter_data($project_id)
    {
        $project_id = (int) $project_id;

        $result = $this->db->query(/** @lang sql */
            "
            SELECT
                stages.id AS stage_id,
                stages.title AS stage_title,
                stages.description AS stage_description,
                tasks.id AS task_id,
                tasks.title AS task_title,
                tasks.description AS task_description,
                tasks.start_date AS task_start_date,
                tasks.estimated_end_date AS task_estimated_end_date,
                tasks.end_date AS task_end_date
            FROM tasks
            JOIN stages ON (tasks.stage_id = stages.id)
            WHERE 
                stages.project_id = {$project_id}
            ORDER BY stages.id, tasks.id ASC;
        ")->result();


        $data = array();
        $stages = array();
        foreach ($result as $task)
        {
            $data[$task->task_id] = array(
                'name'      => (!isset($stages[$task->stage_id])) ? $task->stage_title : '',
                'desc'      => $task->task_title,
                'values'    => array(
                    array(
                        'to'        => ($task->task_end_date) ? '/Date('.(strtotime($task->task_end_date)*1000).')/' : '/Date('.(strtotime($task->task_estimated_end_date)*1000).')/',
                        'from'      => '/Date('.(strtotime($task->task_start_date)*1000).')/',
                        'label'     => $task->task_title,
                        'desc'      => $task->task_description
                    )
                )
            );
            $stages[$task->stage_id] = TRUE;
        }

        return array_values($data);
    }

    /**
     * @param string $year
     * @param string $status
     * @param string|int $company
     * @param string|int $category
     * @param boolean|int $my_projects
     *
     * @param string $delayed
     * @return array
     */
    public function status_data($year, $status, $company, $category, $my_projects, $delayed='false')
    {
        $this->db->from($this->_table)->select('
            COUNT(projects.id) as total_status,
            status
        ');

        if ( $year !== 'all' ) { $year = (int)$year; $this->db->where("YEAR(start_date) = '{$year}'"); }
        if ( $status !== 'all' ) { $status = (string)$status; $this->db->where("status = '{$status}'"); }
        if ( $company !== 'all' ) { $company = (int)$company; $this->db->where("company_id = {$company}"); }
        if ( $category !== 'all' ) { $category = (int)$category; $this->db->where("project_category_id = {$category} "); }
        if ( $my_projects !== FALSE ) { $my_projects = (int)$my_projects; $this->db->where("manager_id = {$my_projects} "); }
        if ( $delayed === 'true' ) { $this->db->where('DATE(projects.estimated_end_date) < DATE(NOW()) AND projects.status = "progreso"'); }

        $this->db->group_by('status')->order_by('status ASC');

        return $this->db->get()->result();
    }
    public function status_data_bar_chart($year, $status, $company, $category, $month, $my_projects, $delayed)
    {
        $this->db->from($this->_table)->select('
            status,
            start_date,
            created_at,
        ');

        if ( $year !== 'all' ) { $year = (int)$year; $this->db->where("YEAR(start_date) = '{$year}'"); }
        if ( $status !== 'all' ) { $status = (string)$status; $this->db->where("status = '{$status}'"); }
        if ( $company !== 'all' ) { $company = (int)$company; $this->db->where("company_id = {$company}"); }
        if ( $category !== 'all' ) { $category = (int)$category; $this->db->where("project_category_id = {$category} "); }
        if ( $month !== 'all' ) { $month = (int)$month; $this->db->where("MONTH(start_date) = {$month} "); }
        if ( $my_projects !== FALSE ) { $my_projects = (int)$my_projects; $this->db->where("manager_id = {$my_projects} "); }
        if ( $delayed === 'true' ) { $this->db->where('DATE(projects.estimated_end_date) < DATE(NOW()) AND projects.status = "progreso"'); }

//        $this->db->group_by('status')->order_by('status ASC');

        return $this->db->get()->result();
    }

    /**
     * @param string $year
     * @param string $status
     * @param string|int $company
     * @param string|int $category
     * @param boolean|int $my_projects
     *
     * @return array
     */
    public function categories_data($year='all', $status='all', $company='all', $category='all', $my_projects, $delayed = 'false')
    {
        $this->db->from($this->_table)->select('
            COUNT(projects.id) as total_category,
            projects.project_category_id,
            project_categories.name as category_name
        ')->join('project_categories','project_categories.id = projects.project_category_id');

        if ( $year !== 'all' ) { $year = (int)$year; $this->db->where("YEAR({$this->_table}.created_at) = '{$year}'"); }
        if ( $status !== 'all' ) { $status = (string)$status; $this->db->where("{$this->_table}.status = '{$status}'"); }
        if ( $company !== 'all' ) { $company = (int)$company; $this->db->where("{$this->_table}.company_id = {$company}"); }
        if ( $category !== 'all' ) { $category = (int)$category; $this->db->where("{$this->_table}.project_category_id = {$category} "); }
        if ( $my_projects !== FALSE ) { $my_projects = (int)$my_projects; $this->db->where("manager_id = {$my_projects} "); }
        if ( $delayed === 'true' ) { $this->db->where('DATE(projects.estimated_end_date) < DATE(NOW()) AND projects.status = "progreso"'); }

        $this->db->group_by('project_categories.name')->order_by('project_categories.name ASC');

        return $this->db->get()->result();
    }

    /**
     * @param $year
     * @param $status
     * @param $company
     * @param $category
     * @param $month
     * @param $my_projects
     * @param string $delayed
     * @return array
     */
    public function categories_data_bar_chart($year, $status, $company, $category, $month, $my_projects, $delayed='false')
    {
        $this->db->from($this->_table)->select('
            projects.project_category_id,
            projects.start_date,
            projects.created_at,
            project_categories.name as category_name
        ')->join('project_categories','project_categories.id = projects.project_category_id');

        if ( $year !== 'all' ) { $year = (int)$year; $this->db->where("YEAR({$this->_table}.start_date) = '{$year}'"); }
        if ( $status !== 'all' ) { $status = (string)$status; $this->db->where("{$this->_table}.status = '{$status}'"); }
        if ( $company !== 'all' ) { $company = (int)$company; $this->db->where("{$this->_table}.company_id = {$company}"); }
        if ( $category !== 'all' ) { $category = (string)$category; $this->db->where("project_categories.name like '%{$category}%'"); }
        if ( $month !== 'all' ) { $month = (int)$month; $this->db->where("MONTH({$this->_table}.start_date) = {$month} "); }
        if ( $my_projects !== FALSE ) { $my_projects = (int)$my_projects; $this->db->where("manager_id = {$my_projects} "); }
        if ( $delayed === 'true' ) { $this->db->where('DATE(projects.estimated_end_date) < DATE(NOW()) AND projects.status = "progreso"'); }

//        $this->db->group_by('project_categories.name')->order_by('project_categories.name ASC');

        return $this->db->get()->result();
    }

    /**
     * @param $year
     * @param $status
     * @param $company
     * @param $category
     * @param $stage_status
     * @param $my_projects
     * @param string $delayed
     * @return array
     */
    public function projects_by_stage_status_pie_chart($year, $status, $company, $category, $stage_status, $my_projects, $delayed='false')
    {
        $this->db->from($this->_table)->select('
            COUNT(projects.id) as total_projects,
            projects.stage_status_id,
            stages_status.name as stage_status,
            projects.start_date,
            projects.created_at
        ')->join('stages_status', 'stages_status.id = projects.stage_status_id');

        if ( $year !== 'all' ) { $year = (int)$year; $this->db->where("YEAR({$this->_table}.start_date) = '{$year}'"); }
        if ( $status !== 'all' ) { $status = (string)$status; $this->db->where("{$this->_table}.status = '{$status}'"); }
        if ( $company !== 'all' ) { $company = (int)$company; $this->db->where("{$this->_table}.company_id = {$company}"); }
        if ( $category !== 'all' ) { $category = (string)$category; $this->db->where("project_categories.name like '%{$category}%'"); }
        if ( $stage_status !== 'all' ) {$stage_status = (int)$stage_status; $this->db->where("stage_status_id = {$stage_status}"); }
        if ( $my_projects !== FALSE ) { $my_projects = (int)$my_projects; $this->db->where("manager_id = {$my_projects} "); }
        if ( $delayed === 'true' ) { $this->db->where('DATE(projects.estimated_end_date) < DATE(NOW()) AND projects.status = "progreso"'); }

        $this->db->group_by('projects.stage_status_id')->order_by('stages_status.name ASC');

        return $this->db->get()->result();
    }
    public function projects_by_reasons_pie_chart($year, $status, $company, $category, $stage_status, $my_projects,$delayed='false')
    {
        $this->db->from($this->_table)->select('
            COUNT(projects.id) as total_projects,
            projects.reason_id,
            reasons.name as reason,
            projects.start_date,
            projects.created_at
        ')->join('reasons', 'reasons.id = projects.reason_id');

        if ( $year !== 'all' ) { $year = (int)$year; $this->db->where("YEAR({$this->_table}.start_date) = '{$year}'"); }
        if ( $status !== 'all' ) { $status = (string)$status; $this->db->where("{$this->_table}.status = '{$status}'"); }
        if ( $company !== 'all' ) { $company = (int)$company; $this->db->where("{$this->_table}.company_id = {$company}"); }
        if ( $category !== 'all' ) { $category = (string)$category; $this->db->where("project_categories.name like '%{$category}%'"); }
        if ( $stage_status !== 'all' ) {$stage_status = (int)$stage_status; $this->db->where("projects.stage_status_id = {$stage_status}"); }
        if ( $my_projects !== FALSE ) { $my_projects = (int)$my_projects; $this->db->where("manager_id = {$my_projects} "); }
        if ( $delayed === 'true' ) { $this->db->where('DATE(projects.estimated_end_date) < DATE(NOW()) AND projects.status = "progreso"'); }

        $this->db->group_by('projects.reason_id')->order_by('reasons.name ASC');

        return $this->db->get()->result();
    }

    /**
     * @param string $year
     * @param string $status
     * @param string|int $company
     * @param string|int $category
     * @param boolean|int $my_projects
     *
     * @return array
     */
    public function companies_data($year, $status, $company, $category, $my_projects, $delayed='false')
    {
        $this->db->from($this->_table)->select('
            COUNT(projects.id) as total_company,
            projects.company_id,
            companies.name as company_name
        ')->join('companies', 'companies.id = projects.company_id')
            ->group_by('companies.name')
            ->order_by('companies.name ASC');

        if ( $year !== 'all' ) { $year = (int)$year; $this->db->where("YEAR({$this->_table}.created_at) = '{$year}'"); }
        if ( $status !== 'all' ) { $status = (string)$status; $this->db->where("{$this->_table}.status = '{$status}'"); }
        if ( $company !== 'all' ) { $company = (int)$company; $this->db->where("{$this->_table}.company_id = {$company} "); }
        if ( $category !== 'all' ) { $category = (int)$category; $this->db->where("{$this->_table}.project_category_id = {$category} "); }
        if ( $my_projects !== FALSE ) { $my_projects = (int)$my_projects; $this->db->where("manager_id = {$my_projects} "); }

        return $this->db->get()->result();
    }

    /**
     * @param $year
     * @param $status
     * @param $company
     * @param $category
     * @param $month
     * @param $my_projects
     * @return array
     */
    public function companies_data_bar_chart($year, $status, $company, $category, $month, $my_projects)
    {
        $this->db->from($this->_table)->select('
            projects.company_id,
            projects.start_date,
            projects.created_at,
            projects.id,
            companies.name as company_name
        ')->join('companies', 'companies.id = projects.company_id');

        if ( $year !== 'all' ) { $year = (int)$year; $this->db->where("YEAR({$this->_table}.created_at) = '{$year}'"); }
        if ( $status !== 'all' ) { $status = (string)$status; $this->db->where("{$this->_table}.status = '{$status}'"); }
        if ( $company !== 'all' ) { $company = (string)$company; $this->db->where("companies.name like '%{$company}%'"); }
        if ( $category !== 'all' ) { $category = (int)$category; $this->db->where("{$this->_table}.project_category_id = {$category}"); }
        if ( $month !== 'all' ) { $month = (int)$month; $this->db->where("MONTH({$this->_table}.start_date) = '{$month}'"); }
        if ( $my_projects !== FALSE ) { $my_projects = (int)$my_projects; $this->db->where("manager_id = {$my_projects} "); }

        return $this->db->get()->result();
    }

    /**
     * @param $project_id
     * @return array
     */
    public function gantt_data($project_id)
    {
        $this->load->model('stage_model');
        $this->load->model('template_model');

        $data = array();
        $data['project'] = $this->get($project_id);
        $data['stages'] = $this->stage_model->find(array('where'=>array('stages.project_id'=>$project_id), 'order_by'=>'stages.order ASC'));

        $templates = array();
        foreach ($data['stages'] as $stage)
        {
            if ( $stage->template_id && !isset($templates[$stage->template_id]) )
            {
                $templates[$stage->template_id] = $this->template_model->get($stage->template_id);
            }
        }
        $data['templates'] = array_values($templates);

        $data['tasks'] = $this->db->query(/** @lang sql */
            "
            SELECT
                tasks.*,
                IFNULL(tasks.end_date, tasks.estimated_end_date) AS g_end_date,
                IF(ISNULL(users.id), '', CONCAT(users.first_name,' ',users.last_name)) AS resources,
                GROUP_CONCAT(tasks_prerequisites.task_prerequisite SEPARATOR ',') AS prerequisites,
                IF(COUNT(tasks_requirements.id) > 0, ROUND((SUM(IF(tasks_requirements.done = 'true', 1, 0))/COUNT(tasks_requirements.id)) * 100, 0), 0) AS progress
            FROM tasks
            JOIN stages ON (tasks.stage_id = stages.id)
            LEFT JOIN tasks_employees ON (tasks_employees.task_id = tasks.id)
            LEFT JOIN users ON (tasks_employees.user_id = users.id)
            LEFT JOIN tasks_prerequisites ON (tasks_prerequisites.task_id = tasks.id)
            LEFT JOIN tasks_requirements ON (tasks_requirements.task_id = tasks.id)
            WHERE
                stages.project_id = {$project_id}
            GROUP BY tasks.id
            ORDER BY tasks.order ASC;
        ")->result();

        return $data;
    }

    /**
     * @param $id
     * @return bool|float
     */
    public function progress($id)
    {
        $id = (int) $id;
        $progress = $this->db->query("
            SELECT
                IF(COUNT(tasks_requirements.id)>0, (SUM(IF(tasks_requirements.done='true',1,0))/COUNT(tasks_requirements.id))*100, 0) AS progress
            FROM projects
            JOIN stages ON (stages.project_id = projects.id)
            JOIN tasks ON (tasks.stage_id = stages.id)
            LEFT JOIN tasks_requirements ON (tasks_requirements.task_id = tasks.id)
            WHERE 
                projects.id = {$id}
            GROUP BY projects.id
        ")->result();

        if ( $progress )
        {
            return $progress[0]->progress;
        }

        return FALSE;
    }

    /**
     * @param $project_id
     * @return array
     */
    public function gantt_client_data($project_id)
    {
        $this->load->model('stage_model');
        $this->load->model('template_model');
        $project_id = (int) $project_id;

        $data = array();

        $data['project'] = $this->get($project_id);
        $data['stages'] = $this->db->query("
            SELECT
                stages.*,
                MIN(tasks.start_date) AS start_date,
                MAX(IFNULL(tasks.end_date, tasks.estimated_end_date)) as g_end_date
            FROM stages
            JOIN tasks ON (tasks.stage_id = stages.id)
            WHERE
                stages.project_id = {$project_id}
            GROUP BY stages.id
            ORDER BY stages.order ASC
        ")->result();

        $templates = array();
        foreach ($data['stages'] as $stage)
        {
            if ( $stage->template_id && !isset($templates[$stage->template_id]) )
            {
                $templates[$stage->template_id] = $this->template_model->get($stage->template_id);
            }
        }
        $data['templates'] = array_values($templates);

        $data['tasks'] = $this->db->query("
            SELECT
                tasks.*,
                IFNULL(tasks.end_date, tasks.estimated_end_date) AS g_end_date,
                CONCAT(users.first_name,' ',users.last_name) AS resources,
                GROUP_CONCAT(tasks_prerequisites.task_prerequisite SEPARATOR ',') AS prerequisites,
                IF(COUNT(tasks_requirements.id) > 0, ROUND((SUM(IF(tasks_requirements.done = 'true', 1, 0))/COUNT(tasks_requirements.id)) * 100, 0), 0) AS progress
            FROM tasks
            JOIN stages ON (tasks.stage_id = stages.id)
            JOIN tasks_employees ON (tasks_employees.task_id = tasks.id)
            JOIN users ON (tasks_employees.user_id = users.id)
            LEFT JOIN tasks_prerequisites ON (tasks_prerequisites.task_id = tasks.id)
            LEFT JOIN tasks_requirements ON (tasks_requirements.task_id = tasks.id)
            WHERE
                stages.project_id = {$project_id} AND
                tasks.client_visibility = 'true'
            GROUP BY tasks.id
            ORDER BY tasks.order ASC;
        ")->result();

        return $data;
    }

    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function cancel($id)
    {
        $this->load->model('project_log_model');
        $this->load->model('task_model');

        $this->db->trans_start();
        if ( !$this->update($id, array('status'=>'anulado')) )
        {
            throw new Exception(validation_errors());
        }

        $tasks = $this->get_tasks($id);
        foreach ($tasks as $task)
        {
            if ( !$this->task_model->update($task->id, array('status' => 'anulado')) )
            {
                throw new Exception("Error al anular la Tarea {$task->id}: ".validation_errors());
            }
        }

        $this->project_log_model->insert(array(
            'project_id'        => $id,
            'user_id'           => $this->session->userdata('user_id'),
            'message'           => $this->session->userdata('full_name').' ha anulado el proyecto.'
        ));

        // TODO: Enviar correos

        $this->db->trans_complete();

        if ( !$this->db->trans_status() )
        {
            throw new Exception('Ha ocurrido un error al intentar anular este proyecto.');
        }

        // LOG ENTRY
        $this->load->model('project_log_model');
        $this->project_log_model->insert(array(
            'project_id'    =>  $id,
            'user_id'       =>  $this->session->userdata('user_id'),
            'message'       =>  "El usuario {$this->session->userdata('full_name')} ha anulado el proyecto."
        ));

        return TRUE;
    }

    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function close($id)
    {
        $this->load->model('project_log_model');

        $this->db->trans_start();
        if ( !$this->update($id, array('status'=>'finalizado','end_date'=>date('Y-m-d'))) )
        {
            throw new Exception(validation_errors());
        }

        $this->project_log_model->insert(array(
            'project_id'        => $id,
            'user_id'           => $this->session->userdata('user_id'),
            'message'           => $this->session->userdata('full_name').' ha finalizado el proyecto.'
        ));

        $this->db->trans_complete();

        if ( !$this->db->trans_status() )
        {
            throw new Exception('Ha ocurrido un error al intentar finalizar este proyecto.');
        }

        return TRUE;
    }

    /**
     * @param $id
     * @return array
     */
    public function get_structure($id)
    {
        return $this->db->select('
            stages.id AS stage_id,
            stages.title AS stage,
            tasks.id AS task_id,
            tasks.title AS task
        ')
            ->from('projects')
            ->join('stages', 'stages.project_id = projects.id')
            ->join('tasks', 'tasks.stage_id = stages.id')
            ->where(array('projects.id' => $id))
            ->where(array('tasks.status !=' => 'anulado'))
            ->order_by('stages.order, tasks.order ASC')
            ->get()->result();
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     * @throws Exception
     */
    public function add_template($id, $data)
    {
        $this->load->model('task_employee_model');
        $this->load->model('calendar_model');

        $project = $this->get($id);

        if ( !$data['templates'] )
        {
            throw new Exception('No se definieron plantillas.');
        }
        $data_templates = json_decode($data['templates']);

        $this->db->trans_start();
        // Creamos la estructura básica
        $project_id = $project->id;
        $stages = array();
        $tasks = array();
        foreach ($data_templates as $data_template)
        {
            $_stages = $this->_generate_stages($data_template->id, $project_id);
            $stages = array_merge($stages, $_stages);
            foreach ($_stages as $_stage)
            {
                $_tasks = $this->_generate_tasks($_stage['tpl'], $_stage['id']);
                $tasks = array_merge($tasks, $_tasks);
            }
        }
        // Añadimos los empleados
        foreach ($tasks as $task)
        {
            if ( isset($data['positions'][$task['tpl']]) )
            {
                $positions = $data['positions'][$task['tpl']];
                foreach ($positions as $position)
                {
                    $res = $this->task_employee_model->insert(array(
                        'task_id'       => $task['id'],
                        'user_id'       => $position
                    ));

                    if ( !$res )
                    {
                        throw new Exception('Error al agregar empleado. '.validation_errors());
                    }
                }
            }
        }
        // Añadimos los prerequisitos
        $tpl_tasks = array();
        foreach ($tasks as $task) { $tpl_tasks[$task['tpl']] = $task['id']; }
        foreach ($tasks as $kt => $task)
        {
            foreach ($task['prerequisites'] as $kp => $prerequisite)
            {
                $tasks[$kt]['prerequisites'][$kp]['id'] = $tpl_tasks[$prerequisite['tpl']];
                $res = $this->task_prerequisite_model->insert(array(
                    'task_id'               => $task['id'],
                    'task_prerequisite'     => $tpl_tasks[$prerequisite['tpl']]
                ));

                if ( !$res )
                {
                    throw new Exception('Error al agregar prerequisito. '.validation_errors());
                }
            }
        }


        $no_dated_tasks = array();
        $dated_tasks = array();
        foreach ($tasks as $task)
        {
            $no_dated_tasks[$task['id']] = $task;
        }

        foreach ($data['couplings_project'] as $coupling)
        {
            $res = $this->task_prerequisite_model->insert(array(
                'task_id'               => $tpl_tasks[$coupling['latter']],
                'task_prerequisite'     => $coupling['former']
            ));
            if ( !$res )
            {
                throw new Exception('Error al agregar prerequisito. '.validation_errors());
            }

            $task = $this->task_model->get($coupling['former']);
            $dated_tasks[$task->id] = array(
                'start_date'    => $task->start_date,
                'end_date'      => ($task->end_date) ? $task->end_date : $task->estimated_end_date
            );

            $no_dated_tasks[$tpl_tasks[$coupling['latter']]]['prerequisites'][] = array('id'=>$coupling['former']);
        }

        while ( count($no_dated_tasks) > 0 )
        {
            foreach ($no_dated_tasks as $no_dated_task_id => $no_dated_task)
            {
                $start_date = $project->start_date;
                if ( count($no_dated_task['prerequisites']) > 0 )
                {
                    $req_pend = FALSE;
                    foreach ( $no_dated_task['prerequisites'] as $prerequisite )
                    {
                        if ( !isset($dated_tasks[$prerequisite['id']]) )
                        {
                            $req_pend = TRUE;
                            break;
                        }
                        if ( $dated_tasks[$prerequisite['id']]['end_date'] >= $start_date )
                        {
                            $start_date = $this->calendar_model->calc_end_date(1, $dated_tasks[$prerequisite['id']]['end_date'], 2);
                        }
                    }
                    if ( $req_pend ) { continue; }
                }
                $end_date = $this->calendar_model->calc_end_date(1, $start_date, $no_dated_task['duration_days'], $no_dated_task['duration_hours'], $no_dated_task['day_type'] === 'laborable');
                if ( !$end_date ) { throw new Exception('Error al calcular fecha final de la tarea.'); }

                $res = $this->task_model->update($no_dated_task_id, array(
                    'start_date'            => $start_date,
                    'estimated_end_date'    => $end_date
                ));
                if ( !$res ) { throw new Exception('Error al guardar fechas de la tarea. '.validation_errors());}

                $dated_tasks[$no_dated_task_id] = array(
                    'start_date'    => $start_date,
                    'end_date'      => $end_date
                );
                unset($no_dated_tasks[$no_dated_task_id]);
            }
        }

        //$this->reschedule_tasks($project_id);

        $this->update_estimated_end_date($project_id);

        $this->db->trans_complete();

        if ( !$this->db->trans_status() ) { return FALSE; }

        return TRUE;
    }

    /**
     * @param $id
     * @return FALSE|string
     */
    public function calc_estimated_end_date($id)
    {
        $final_task = $this->db->select('
            IFNULL(tasks.end_date, tasks.estimated_end_date) AS end_date_r
        ')->from('tasks')
        ->join('stages', 'tasks.stage_id = stages.id')
        ->where(array(
            'stages.project_id' => $id
        ))
        ->order_by('end_date_r DESC')
        ->limit(1)->get()->row();

        if ( !$final_task ) { return FALSE; }

        return $final_task->end_date_r;
    }

    /**
     * @param $id
     */
    public function update_estimated_end_date($id)
    {
        $end_date = $this->calc_estimated_end_date($id);

        $this->update($id, array(
            'estimated_end_date'   => $end_date
        ));
    }

    /**
     * @param $project_id
     * @return array
     */
    public function get_templates($project_id)
    {
        $temp = $this->db->select('stages.template_id')->from('stages')
            ->where('stages.project_id', $project_id)->group_by('stages.template_id')->get()->result();

        $templates = array();

        foreach ($temp as $item)
        {
            if ( $item->template_id ) { $templates[] = $item->template_id; }
        }

        return $templates;
    }

}
