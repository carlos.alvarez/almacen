<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Acceso_model
 */
class Acceso_model extends CI_Model
{
    /**
     *
     */
    public function obtener()
    {
        return $this->db->get('acceso', 1)->row();
    }

}
