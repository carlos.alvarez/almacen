<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class User_model
 *
 * @property User_role_model $user_role_model
 * @property Employee_model $employee_model
 * @property Client_contact_model $client_contact_model
 * @property Push_subscription_model $push_subscription_model
 */
class User_model extends MY_Model {

    /**
     * @var string
     */
	protected $_table = 'users';

    /**
     * @var array
     */
	protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
		array( // Username
			'field'     => 'username',
			'label'     => 'lang:username',
			'rules'     => 'trim|required|min_length[4]|max_length[30]|is_unique[users.username]'
		),
		array( // Password
			'field'     => 'password',
			'label'     => 'lang:password',
			'rules'     => 'trim|required|min_length[4]|max_length[72]|prep_password_hash'
		),
		array( // Email
			'field'     => 'email',
			'label'     => 'lang:email',
			'rules'     => 'trim|required|valid_email|max_length[250]|strtolower'
		),
        array( // First name
            'field'     => 'first_name',
            'label'     => 'lang:first_name',
            'rules'     => 'trim|required|min_length[2]|max_length[100]|ucwords'
        ),
        array( // Last name
            'field'     => 'last_name',
            'label'     => 'lang:last_name',
            'rules'     => 'trim|required|min_length[2]|max_length[100]|ucwords'
        ),
		array( // Active
			'field'     => 'active',
			'label'     => 'lang:active',
			'rules'     => 'trim|in_list[true,false]'
		),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param array $data
     * @return object|bool
     */
	public function auth($data)
	{
		$user = $this->find(array(
			'where'		=> array('username' => $data['username']),
			'limit'		=> 1
		));

		if ( is_null($user) ) { return FALSE; }

		$res = password_verify($data['password'], $user->password);
		if ( !$res )
		{
            $user->password = substr_replace($user->password, '$2a', 0, 3);
            $res = password_verify($data['password'], $user->password);
        }
        return ($res === TRUE) ? $user : FALSE;
	}

    /**
     * @return string
     */
	public function datatable_json()
	{
		$this->load->library('datatables');
        $grant_edit = grant_access('user', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('user', 'delete') ? 'true' : 'false';

		$this->datatables->select('
			users.id,
			users.username,
			users.email,
			users.active,
			CONCAT(users.first_name," ",users.last_name) AS full_name
		')->from('users')
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

		return $this->datatables->generate();
	}

    /**
     * @param $data
     * @param bool $skip_validation
     * @return bool
     */
    public function insert($data, $skip_validation = FALSE)
    {
        if ( ! isset($data['user_roles']) ) { $data['user_roles'] = array(); }

        $this->db->trans_start();
        if ( ! $id = parent::insert($data, $skip_validation) )
        {
            $this->db->trans_rollback();
            return FALSE;
        }

        if ( ! $this->update_roles($id, $data['user_roles']) )
        {
            $this->db->trans_rollback();
            return FALSE;
        }

        $this->db->trans_complete();
        return $this->db->trans_status() ? $id : FALSE;
    }

    /**
     * @param $pk
     * @param $data
     * @param bool $skip_validation
     * @return bool
     */
    public function update($pk, $data, $skip_validation = FALSE)
    {
        $this->db->trans_start();

        if ( ! parent::update($pk, $data, $skip_validation) )
        {
            $this->db->trans_rollback();
            return FALSE;
        }

        if ( isset($data['user_roles']) )
        {
            if ( ! $this->update_roles($pk, $data['user_roles']) )
            {
                $this->db->trans_rollback();
                return FALSE;
            }
        }

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * @param $id
     * @param $roles
     * @return bool
     */
    public function update_roles($id, $roles)
    {
        $this->load->model('user_role_model');

        $e_roles = $this->user_role_model->get_by_user($id);

        $changes = array();
        foreach ($e_roles as $role)
        {
            $changes[$role->role_id] = 'delete';
        }

        foreach ($roles as $role)
        {
            if ( isset($changes[$role]) )
            {
                unset($changes[$role]);
            }
            else
            {
                $changes[$role] = 'add';
            }
        }

        $this->db->trans_start();
        foreach ($changes as $role_id => $change)
        {
            switch ($change)
            {
                case 'add':
                    $success = $this->user_role_model->insert(array(
                        'user_id'   => $id,
                        'role_id'   => $role_id
                    ));
                    break;
                case 'delete':
                    $success = $this->user_role_model->delete(array(
                        'user_id'   => $id,
                        'role_id'   => $role_id
                    ));
                    break;
                default:
                    $this->db->trans_rollback();
                    return FALSE;
            }

            if ( ! $success ) { $this->db->trans_rollback(); return FALSE; }
        }

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get_roles($id)
    {
        $this->load->model('user_role_model');

        $this->db->select('user_roles.*, roles.name as role')
            ->from('user_roles')
            ->join('roles', 'user_roles.role_id = roles.id')
            ->where(array('user_roles.user_id' => $id));

        return $this->db->get()->result();
    }

    /**
     * @param $id
     * @return array
     */
    public function get_grants($id)
    {
        $result = $this->db->select('modules.controller, grants.method')
        ->from('users')
        ->join('user_roles', 'user_roles.user_id = users.id')
        ->join('roles', 'user_roles.role_id = roles.id')
        ->join('grants', 'grants.role_id = roles.id')
        ->join('modules', 'grants.module_id = modules.id')
        ->where(array('users.id' => $id))
        ->group_by('grants.module_id,grants.method')
        ->get()->result();

        $grants = array();
        foreach ($result as $grant)
        {
            $grants[$grant->controller][$grant->method] = TRUE;
        }

        return $grants;
    }

    /**
     * @return array
     */
    public function get_no_employees()
    {
        return $this->db->select('
            users.*,
            CONCAT(users.first_name," ",users.last_name) AS full_name
        ')
        ->from('users')
        ->join('employees', 'employees.user_id = users.id', 'left')
        ->join('client_contacts', 'client_contacts.user_id = users.id', 'left')
        ->where('employees.user_id IS NULL')
        ->where('client_contacts.user_id IS NULL')
        ->get()->result();
    }

    /**
     * @param array|int|string $pk
     *
     * @return bool
     * @throws Exception
     */
    public function delete($pk)
    {
        $this->load->model('employee_model');
        $this->load->model('client_contact_model');

        if ($this->employee_model->exist_where(array('user_id' => $pk)))
        {
            throw new Exception('No se pudo eliminar el usuario porque tiene un empleado asociado.');
        }
        else if ($this->client_contact_model->exist_where(array('user_id' => $pk)))
        {
            throw new Exception('No se pudo eliminar el usuario porque tiene un contacto de cliente asociado.');
        }
        else
        {
            $this->load->model('user_role_model');
            $this->load->model('push_subscription_model');

            $this->db->trans_start();
            $this->push_subscription_model->delete_where(array('user_id' => $pk));
            $this->user_role_model->delete_by_user($pk);
            parent::delete($pk);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) { return FALSE; }

            return TRUE;
        }
    }
}
