<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Employee_model
 *
 * @property Datatables $datatables
 * @property Project_model $project_model
 * @property Task_employee_model $task_employee_model
 * @property Project_log_model $project_log_model
 * @property Project_comment_model $project_comment_model
 * @property Project_file_model $project_file_model
 */
class Employee_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'employees';
    /**
     * @var string
     */
    protected $_pk = 'user_id';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // user_id
            'field'     => 'user_id',
            'label'     => 'lang:user',
            'rules'     => 'trim|required|is_natural_no_zero|exist[users.id]'
        ),
        array( // area_id
            'field'     => 'area_id',
            'label'     => 'lang:area',
            'rules'     => 'trim|required|is_natural_no_zero|exist[areas.id]'
        ),
        array( // position_id
            'field'     => 'position_id',
            'label'     => 'lang:position',
            'rules'     => 'trim|required|is_natural_no_zero|exist[positions.id]'
        ),
		array(
		  'field'       => 'supervisor_id',
          'label'       => 'lang:supervisor',
          'rules'       => 'trim|is_natural_no_zero|exist[employees.user_id]'
        ),
        array( //
            'field'     => 'phone',
            'label'     => 'lang:phone',
            'rules'     => 'trim|min_length[2]|max_length[20]'
        ),
        array( //
            'field'     => 'ext',
            'label'     => 'lang:ext',
            'rules'     => 'trim|min_length[1]|max_length[8]'
        ),
        array( //
            'field'     => 'mobile',
            'label'     => 'lang:mobile',
            'rules'     => 'trim|min_length[2]|max_length[20]'
        ),
		array( // Active
			'field'     => 'active',
			'label'     => 'lang:active',
			'rules'     => 'trim|in_list[true,false]'
		),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param null $options
     * @param bool|string $array_result
     * @return mixed
     */
	public function find($options = NULL, $array_result = FALSE)
    {
        if ( !isset($options['select']) )
        {
            $this->db->select('
                employees.*,
                users.first_name,
                users.last_name,
                CONCAT(users.first_name, " ", users.last_name) AS full_name,
                positions.name AS position,
                areas.name AS area,
                departments.id AS department_id,
                departments.name AS department,
                companies.id AS company_id,
                companies.name AS company
            ');
        }
        else { $this->db->select($options['select']); }

        $this->db->from('employees')
            ->join('users', 'employees.user_id = users.id')
            ->join('positions', 'employees.position_id = positions.id')
            ->join('areas', 'employees.area_id = areas.id')
            ->join('departments', 'areas.department_id = departments.id')
            ->join('companies', 'departments.company_id = companies.id');

        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->order_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @return string
     */
	public function datatable_json()
	{
		$this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';

		$this->datatables->select('
            employees.user_id,
            employees.position_id,
            employees.area_id,
            users.username,
            users.first_name,
            users.last_name,
            users.active,
            CONCAT(users.first_name, " ", users.last_name) AS full_name,
            positions.name AS position,
            areas.name AS area,
            areas.short_name AS area_short,
            departments.id AS department_id,
            departments.name AS department,
            departments.short_name AS department_short,
            companies.name AS company,
            companies.short_name AS company_short
        ')
            ->from('employees')
            ->join('users', 'employees.user_id = users.id')
            ->join('positions', 'employees.position_id = positions.id')
            ->join('areas', 'employees.area_id = areas.id')
            ->join('departments', 'areas.department_id = departments.id')
            ->join('companies', 'departments.company_id = companies.id')
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

		return $this->datatables->generate();
	}

    /**
     * @param array|int|string $pk
     *
     * @return bool
     * @throws Exception
     */
    public function delete($pk)
    {
        $this->load->model('project_model');
        $this->load->model('project_log_model');
        $this->load->model('project_comment_model');
        $this->load->model('project_file_model');
        $this->load->model('task_employee_model');

        if ( $this->project_model->exist_where(array('manager_id' => $pk)) ||
                $this->project_log_model->exist_where(array('user_id' => $pk)) ||
                $this->project_comment_model->exist_where(array('user_id' => $pk)) ||
                $this->project_file_model->exist_where(array('user_id' => $pk)) ||
                $this->task_employee_model->exist_where(array('user_id' => $pk)) ||
                $this->exist_where(array('supervisor_id' => $pk))
            )
        {
            throw new Exception('No se puede eliminar al empleado porque está asociado a proyecto(s), tarea(s) y/o empleado(s).');
        }
        else
        {
            return parent::delete($pk);
        }
    }
}
