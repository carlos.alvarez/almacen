<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User: Christian Rodríguez
 * Date: 8/23/2017
 * Time: 11:41 PM
 */

class Project_log_model extends MY_Model
{
    /**
     * @var string
     */
    protected $_table = 'project_logs';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     =>  'id',
            'label'     =>  'ID',
            'rules'     =>  'is_natural_no_zero'
        ),
        array(
            'field'     =>  'project_id',
            'label'     =>  'lang:project',
            'rules'     =>  'trim|required|is_natural_no_zero|exist[projects.id]'
        ),
        array(
            'field'     =>  'user_id',
            'label'     =>  'lang:user',
            'rules'     =>  'trim|required|is_natural_no_zero|exist[users.id]'
        ),
        array(
            'field'     =>  'message',
            'label'     =>  'Mensaje',
            'rules'     =>  'trim|required|max_length[250]'
        ),
        array(
            'field'     =>  'created_at',
            'label'     =>  'created_at',
            'rules'     =>  'trim'
        ),
    );

    public function datatable_json($id)
    {
        $this->load->library('datatables');
        $this->load->helper('date_helper');

        $this->datatables->select('
            project_logs.id,
            project_logs.created_at,
            DATE_FORMAT(project_logs.created_at, "%d/%b/%y %h:%i %p") AS created_at_nice,
            projects.name as project_name,
            CONCAT(users.first_name, " ", users.last_name) as username,
            project_logs.message as message
        ')->from('project_logs')
            ->join('projects','projects.id = project_logs.project_id')
            ->join('users', 'users.id = project_logs.user_id')
            ->where(array('project_logs.project_id' => $id));

        return $this->datatables->generate();
    }
}