<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Role_model
 * @property User_role_model $user_role_model
 * @property Grant_model $grant_model
 */
class Push_subscription_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'push_subscriptions';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // user_id
            'field'     => 'user_id',
            'label'     => 'lang:user',
            'rules'     => 'trim|required|is_natural_no_zero|exist[users.id]'
        ),
        array( // endpoint
			'field'     => 'endpoint',
			'label'     => 'Endpoint',
			'rules'     => 'trim|required|min_length[10]|max_length[200]'
		),
		array( // expirationTime
            'field'     => 'expirationTime',
            'label'     => 'Expiration Time',
            'rules'     => 'trim'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param $user_id
     * @return array|null|object
     */
    public function get_by_user($user_id)
    {
        $subs = $this->find(array(
            'where'     => array(
                'user_id'   => $user_id
            ),
            'limit'     => 10,
            'order_by'  => 'expirationTime DESC, id DESC'
        ));

        $endpoints = array();
        foreach ($subs as $sub) {
            $endpoints[] = $sub->endpoint;
        }

        return $endpoints;
    }

    /**
     * @param array|object $data
     * @param bool $skip_validation
     * @return bool|int|string
     */
    public function insert($data, $skip_validation = FALSE)
    {
        $res = parent::insert($data, $skip_validation);
        if ( !$res ) { return $res; }
        $this->remove_excess($data['user_id']);

        return $res;
    }

    /**
     * @param $user_id
     * @return bool|mixed
     */
    public function remove_excess($user_id)
    {
        $subs = $this->find(array(
            'where'     => array(
                'user_id'   => $user_id
            ),
            'limit'     => 100,
            'offset'    => 10,
            'order_by'  => 'expirationTime DESC, id DESC'
        ));

        if ( !$subs ) { return TRUE; }

        $ids = array();
        foreach ($subs as $sub) { $ids[] = $sub->id; }

        return $this->db->where_in('id', $ids)->delete($this->_table);
    }
}
