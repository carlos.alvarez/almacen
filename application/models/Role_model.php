<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Role_model
 * @property User_role_model $user_role_model
 * @property Grant_model $grant_model
 */
class Role_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'roles';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|min_length[4]|max_length[30]|is_unique[roles.name]'
		),
		array( // Description
			'field'     => 'description',
			'label'     => 'lang:description',
			'rules'     => 'trim|min_length[4]|max_length[250]'
		),
		array( // Active
			'field'     => 'active',
			'label'     => 'lang:active',
			'rules'     => 'trim|in_list[true,false]'
		),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @return string
     */
	public function datatable_json()
	{
		$this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';

		$this->datatables->select('
			id,
			name,
			description,
			active
		')->from('roles')
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

		return $this->datatables->generate();
	}

    /**
     * @param array|int|string $pk
     *
     * @return bool
     * @throws Exception
     */
    public function delete($pk)
    {
        $this->load->model('user_role_model');

        if ( $this->user_role_model->exist_where(array('role_id' => $pk)) )
        {
            throw new Exception('No se puede eliminar el rol porque tiene usuario(s) asociado(s).');
        }
        else
        {
            $this->load->model('grant_model');

            $this->db->trans_start();
                $this->grant_model->delete_where(array('role_id' => $pk));
                parent::delete($pk);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) { return FALSE; }

            return TRUE;
        }
    }
}
