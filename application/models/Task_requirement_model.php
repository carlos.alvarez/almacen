<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Task_requirement_model
 */
class Task_requirement_model extends MY_Model {

    /**
     * @var string
     */
	protected $_table = 'tasks_requirements';

    /**
     * @var array
     */
	protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // task_id
            'field'     => 'task_id',
            'label'     => 'lang:task',
            'rules'     => 'trim|required|is_natural_no_zero|exist[tasks.id]'
        ),
        array( // requirement
            'field'     => 'requirement',
            'label'     => 'lang:requirement',
            'rules'     => 'trim|required|max_length[250]'
        ),
        array( // weight
            'field'     => 'weight',
            'label'     => 'lang:weight',
            'rules'     => 'trim|required|numeric|greater_than[0]|less_than_equal_to[100]'
        ),
        array( // done
            'field'     => 'done',
            'label'     => 'Estado',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);
}
