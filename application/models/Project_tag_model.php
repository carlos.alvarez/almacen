<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_tag_model extends MY_Model {

	protected $_table = 'projects_tags';

	protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // project_id
            'field'     => 'project_id',
            'label'     => 'lang:project',
            'rules'     => 'trim|required|is_natural_no_zero|exist[projects.id]'
        ),
        array( // tag
            'field'     => 'tag',
            'label'     => 'lang:tag',
            'rules'     => 'trim|strtoupper|required|max_length[60]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        )
	);
}
