<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Tpl_stage_model
 * @property Tpl_task_model $tpl_task_model
 */
class Tpl_stage_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'tpl_stages';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // template_id
            'field'     => 'template_id',
            'label'     => 'lang:template',
            'rules'     => 'trim|required|is_natural_no_zero|exist[templates.id]'
        ),
        array( // title
            'field'     => 'title',
            'label'     => 'lang:title',
            'rules'     => 'trim|required|max_length[150]'
        ),
        array( // description
            'field'     => 'description',
            'label'     => 'lang:description',
            'rules'     => 'trim'
        ),
        array( // weight
            'field'     => 'weight',
            'label'     => 'lang:weight',
            'rules'     => 'trim|required|numeric|greater_than[0]|less_than_equal_to[100]'
        ),
        array( // client_visibility
            'field'     => 'client_visibility',
            'label'     => 'lang:client_visibility',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // client_notification
            'field'     => 'client_notification',
            'label'     => 'lang:client_notification',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // order
            'field'     => 'order',
            'label'     => 'lang:order',
            'rules'     => 'trim|is_natural'
        ),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param $data
     * @param bool $skip_validation
     * @return bool
     */
	public function insert($data, $skip_validation = FALSE)
    {
        $result = parent::insert($data, $skip_validation);

        if ( $result )
        {
            $stages = $this->find(array('where' => array('template_id' => $data['template_id'])));
            $perc = 100.0 / count($stages);

            foreach ($stages as $stage)
            {
                $this->update($stage->id, array('weight'=>$perc));
            }
        }

        return $result;
    }

    /**
     * @param array|int|string $pk
     * @return bool
     */
    public function delete($pk)
    {
        $this->load->model('tpl_task_model');
        $this->db->trans_start();
            $this->tpl_task_model->delete_where(array('tpl_stage_id'=>$pk));
            parent::delete($pk);
        $this->db->trans_complete();

        if ( $this->db->trans_status() === FALSE ) { return FALSE; }

        return TRUE;
    }

}
