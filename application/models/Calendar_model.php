<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Calendar_model
 *
 * @property Datatables $datatables
 * @property Calendar_holiday_model $calendar_holiday_model
 * @property Company_model $company_model
 */
class Calendar_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'calendars';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|min_length[2]|max_length[100]'
		),
		array( // Description
			'field'     => 'description',
			'label'     => 'lang:description',
			'rules'     => 'trim|min_length[4]|max_length[250]'
		),
        array( // working_hours
            'field'     => 'working_hours',
            'label'     => 'lang:working_hours',
            'rules'     => 'trim|required|greater_than_equal_to[1]|less_than_equal_to[16]'
        ),
        array( // wd_sunday
            'field'     => 'wd_sunday',
            'label'     => 'lang:sunday',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // wd_monday
            'field'     => 'wd_monday',
            'label'     => 'lang:monday',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // wd_tuesday
            'field'     => 'wd_tuesday',
            'label'     => 'lang:tuesday',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // wd_wednesday
            'field'     => 'wd_wednesday',
            'label'     => 'lang:wednesday',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // wd_thursday
            'field'     => 'wd_thursday',
            'label'     => 'lang:thursday',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // wd_friday
            'field'     => 'wd_friday',
            'label'     => 'lang:friday',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // wd_saturday
            'field'     => 'wd_saturday',
            'label'     => 'lang:saturday',
            'rules'     => 'trim|in_list[true,false]'
        ),
		array( // Active
			'field'     => 'active',
			'label'     => 'lang:active',
			'rules'     => 'trim|in_list[true,false]'
		),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param unsigned int $calendar_id
     * @param string $start_date
     * @param unsigned int $days
     * @param unsigned int $hours
     * @param bool $working_days
     * @return false|string
     */
    public function calc_end_date($calendar_id, $start_date, $days, $hours = 0, $working_days = TRUE)
    {
        if ( !check_date($start_date) ) { return FALSE; }

        if ( $hours > 0 )
        {
            if ( $working_days )
            {
                $calendar = $this->get($calendar_id);
                if ( !$calendar ) { return FALSE; }
                $work_hours = $calendar->working_hours;
            }
            else { $work_hours = 24; }
            $extra_days = ceil((float)($hours/$work_hours));
            $days += (int)$extra_days;
        }

        $end_date = $this->find_dates($calendar_id, array(
            'where'     => "calendar_days.date >= '{$start_date}'",
            'order_by'  => 'calendar_days.date ASC',
            'limit'     => 1,
            'offset'    => $days - 1
        ), $working_days);

        return ($end_date) ? $end_date->date : $start_date;
    }

    /**
     * @param $calendar_id
     * @param $start_date
     * @param $end_date
     * @return int
     */
    public function days_between($calendar_id, $start_date, $end_date)
    {
        $days = $this->find_dates($calendar_id, array(
            'where'     => "calendar_days.date >= '{$start_date}' AND calendar_days.date <= '{$end_date}'",
        ));

        return count($days);
    }

    /**
     * @param int $calendar_id
     * @param null|array $options
     * @param bool $working_days
     * @param bool $array_result
     * @return array|mixed
     */
    public function find_dates($calendar_id, $options = NULL, $working_days = TRUE, $array_result = FALSE)
    {
        if ( !$this->exist($calendar_id) ) { return FALSE; }

        if ( $working_days )
        {
            $wd = $this->_working_days($calendar_id);
            $this->db->where_in('calendar_days.week_day', $wd)
                ->where('calendar_holidays.id IS NULL', NULL, FALSE);
        }

        $this->db->select('calendar_days.*')
            ->from('calendar_days')
            ->join('calendar_holidays', 'calendar_days.date = calendar_holidays.date AND calendar_holidays.calendar_id = '.$calendar_id, 'left');

        if ( isset($options['select']) )    { $this->db->select($options['select'], FALSE); }
        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->group_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @return mixed
     */
    public function datatable_json()
    {
        $this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';

        $this->datatables->select('
			id,
			name,
			description,
			working_hours,
			wd_sunday,
			wd_monday,
			wd_tuesday,
			wd_wednesday,
			wd_thursday,
			wd_friday,
			wd_saturday,
			active
		')->from($this->_table)
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

        return $this->datatables->generate();
    }

    /**
     * @param int $id
     * @return array|false
     */
    protected function _working_days($id)
    {
        $calendar = $this->get($id);
        if ( !$calendar ) { return FALSE; }

        $wd = array();
        $calendar->wd_sunday    === 'true' && $wd[] = 0;
        $calendar->wd_monday    === 'true' && $wd[] = 1;
        $calendar->wd_tuesday   === 'true' && $wd[] = 2;
        $calendar->wd_wednesday === 'true' && $wd[] = 3;
        $calendar->wd_thursday  === 'true' && $wd[] = 4;
        $calendar->wd_friday    === 'true' && $wd[] = 5;
        $calendar->wd_saturday  === 'true' && $wd[] = 6;

        return $wd;
    }

    /**
     * @param array|int|string $pk
     *
     * @return bool
     * @throws Exception
     */
    public function delete($pk)
    {
        $this->load->model('calendar_holiday_model');
        $this->load->model('company_model');

        if ( $this->company_model->exist_where(array('calendar_id' => $pk)) )
        {
            throw new Exception('No se puede eliminar el calendario porque está asociado a empresa(s).');
        }
        else
        {
            $this->db->trans_start();
                $this->calendar_holiday_model->delete_where(array('calendar_id' => $pk));
                parent::delete($pk);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) { return FALSE; }

            return TRUE;
        }

    }
}
