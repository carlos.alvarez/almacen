<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Contact_type_model
 *
 * @property Datatables $datatables
 * @property Client_contact_model $client_contact_model
 */
class Contact_type_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'contact_types';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|min_length[2]|max_length[100]'
		),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @return mixed
     */
    public function datatable_json()
    {
        $this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';

        $this->datatables->select('
			id,
			name,
			created_at
		')->from($this->_table)
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

        return $this->datatables->generate();
    }

    /**
     * @param array|int|string $pk
     *
     * @return bool
     * @throws Exception
     */
    public function delete($pk)
    {
        $this->load->model('client_contact_model');

        if ( $this->client_contact_model->exist_where(array('contact_type_id')) )
        {
            throw new Exception('No se puede eliminar el tipo de contacto porque tiene contacto(s) asociado(s) a este tipo de contacto.');
        }
        else
        {
            return parent::delete($pk);
        }
    }
}
