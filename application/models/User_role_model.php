<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_role_model extends MY_Model {

    protected $_pk = array('user_id','role_id');
    protected $_table = 'user_roles';
    protected $_validation_rules = array(
        array( // user_id
            'field'     => 'user_id',
            'label'     => 'User',
            'rules'     => 'trim|required|is_natural_no_zero|exist[users.id]'
        ),
        array( // role_id
            'field'     => 'role_id',
            'label'     => 'Role',
            'rules'     => 'trim|required|is_natural_no_zero|exist[roles.id]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        )
    );

    /**
     * @param $user
     * @param array $options
     * @return mixed
     */
    public function get_by_user($user, $options = array())
    {
        $options['where']['user_id'] = $user;
        return $this->find($options);
    }

    /**
     * @param $role
     * @param array $options
     * @return mixed
     */
    public function get_by_role($role, $options = array())
    {
        $options['where']['role_id'] = $role;
        return $this->find($options);
    }

    /**
     * @param $role
     * @return mixed
     */
    public function delete_by_role($role)
    {
        return $this->delete_where(array('role_id' => $role));
    }

    /**
     * @param $user
     * @return mixed
     */
    public function delete_by_user($user)
    {
        return $this->db->delete($this->_table, array('user_id' => $user));
    }

    public function insert($data, $skip_validation = FALSE)
    {
        $this->_filter_inputs($data);

        if ( isset($this->_fields['created_at']) )
        {
            $data['created_at'] = date('Y-m-d H:i:s');
        }

        if ( !$skip_validation && !$this->_validate($data) ) { return FALSE; }

        $result = $this->db->insert($this->_table, $data);
        if ( $result )
        {
            $result = (is_array($this->_pk)) ? $result : $this->db->insert_id();
        }

        return $result;
    }

}
