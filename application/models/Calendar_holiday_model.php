<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Calendar_holiday_model
 *
 * @property Datatables $datatables
 */
class Calendar_holiday_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'calendar_holidays';

    /**
     * @var string
     */
    protected $_pk = 'id';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // calendar_id
            'field'     => 'calendar_id',
            'label'     => 'lang:calendar',
            'rules'     => 'trim|required|is_natural_no_zero|exist[calendars.id]'
        ),
        array( // date
            'field'     => 'date',
            'label'     => 'lang:date',
            'rules'     => 'trim'
        ),
        array( // name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|min_length[2]|max_length[100]'
		),
		array( // Description
			'field'     => 'description',
			'label'     => 'lang:description',
			'rules'     => 'trim|min_length[4]|max_length[250]'
		),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param $calendar_id
     * @param null $year
     * @return mixed
     */
    public function datatable_json($calendar_id, $year = NULL)
    {
        $this->load->library('datatables');

        $this->datatables->select('
			id,
			date,
			name,
			description
		')->from($this->_table)
            ->where(array('calendar_id' => $calendar_id));

        if ( $year !== NULL )
        {
            $this->datatables->where("YEAR(date) = {$year}");
        }

        return $this->datatables->generate();
    }

}
