<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpl_task_tag_model extends MY_Model {

	protected $_table = 'tpl_tasks_tags';

	protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // tpl_task_id
            'field'     => 'tpl_task_id',
            'label'     => 'lang:task',
            'rules'     => 'trim|required|is_natural_no_zero|exist[tpl_tasks.id]'
        ),
        array( // tag
            'field'     => 'tag',
            'label'     => 'lang:tag',
            'rules'     => 'trim|strtoupper|required|max_length[60]'
        ),
        array( // color
            'field'     => 'color',
            'label'     => 'lang:color',
            'rules'     => 'trim|required|exact_length[7]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        )
	);
}
