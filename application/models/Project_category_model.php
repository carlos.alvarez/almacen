<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Project_category_model
 * @property Project_model $project_model
 * @property Template_model $template_model
 */
class Project_category_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'project_categories';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // name
            'field'     => 'name',
            'label'     => 'lang:name',
            'rules'     => 'trim|required|max_length[100]'
        ),
        array( // description
            'field'     => 'description',
            'label'     => 'lang:description',
            'rules'     => 'trim'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @return mixed
     */
    public function datatable_json()
    {
        $this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';

        $this->datatables->select('
			id,
			name,
			description
		')->from($this->_table)
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

        return $this->datatables->generate();
    }

    public function delete($pk)
    {
        $this->load->model('project_model');
        $this->load->model('template_model');

        if ($this->project_model->exist_where(array('project_category_id' => $pk)))
        {
            throw new Exception('No se puede eliminar la categoría porque tiene proyectos asociados a esta categoría.');
        }
        else
        {
            $this->db->trans_start();
                $this->template_model->delete_where(array('project_category_id' => $pk));
                parent::delete($pk);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) { return FALSE; }

            return TRUE;
        }
    }
}
