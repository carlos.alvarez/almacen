<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Department_model
 *
 * @property Datatables $datatables
 * @property Area_model $area_model
 */
class Department_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'departments';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // company_id
            'field'     => 'company_id',
            'label'     => 'lang:company',
            'rules'     => 'trim|required|is_natural_no_zero|exist[companies.id]'
        ),
		array( // Name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|min_length[4]|max_length[100]'
		),
		array( // Short Name
			'field'     => 'short_name',
			'label'     => 'lang:short_name',
			'rules'     => 'trim|max_length[30]'
		),
        array( // manager_id
            'field'     => 'manager_id',
            'label'     => 'lang:manager',
            'rules'     => 'trim|is_natural_no_zero|exist[employees.user_id]'
        ),
		array( // Active
			'field'     => 'active',
			'label'     => 'lang:active',
			'rules'     => 'trim|in_list[true,false]'
		),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param null|string|array $filters
     * @return string
     */
	public function datatable_json($filters = NULL)
	{
		$this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';

		$this->datatables->select('
		    departments.id,
            departments.name,
		    departments.short_name,
		    departments.active,
		    departments.company_id, 
		    companies.name as company_name,
		    CONCAT(users.first_name," ",users.last_name) AS manager'
        )
            ->from($this->_table)
            ->join('companies', 'departments.company_id = companies.id')
            ->join('users', 'departments.manager_id = users.id', 'left')
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

		if ( $filters )
        {
            $this->datatables->where($filters);
        }

		return $this->datatables->generate();
	}

    /**
     * @param array|int|string $pk
     *
     * @return bool
     * @throws Exception
     */
    public function delete($pk)
    {
        $this->load->model('area_model');

        if ( $this->area_model->exist_where(array('department_id' => $pk)) )
        {
            throw new Exception('No se puede eliminar el departamento porque tiene área(s) asociado(s).');
        }
        else
        {
            return parent::delete($pk);
        }
    }

}
