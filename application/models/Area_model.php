<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Area_model
 *
 * @property Datatables $datatables
 * @property Employee_model $employee_model
 */
class Area_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'areas';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // department_id
            'field'     => 'department_id',
            'label'     => 'lang:department',
            'rules'     => 'trim|required|is_natural_no_zero|exist[departments.id]'
        ),
		array( // Name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|min_length[4]|max_length[100]'
		),
		array( // Short Name
			'field'     => 'short_name',
			'label'     => 'lang:short_name',
			'rules'     => 'trim|max_length[30]'
		),
        array( // manager_id
            'field'     => 'manager_id',
            'label'     => 'lang:manager',
            'rules'     => 'trim|is_natural_no_zero|exist[employees.user_id]'
        ),
		array( // Active
			'field'     => 'active',
			'label'     => 'lang:active',
			'rules'     => 'trim|in_list[true,false]'
		),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param null $options
     * @param bool|string $array_result
     * @return mixed
     */
    public function find($options = NULL, $array_result = FALSE)
    {
        if ( !isset($options['select']) )
        {
            $this->db->select('
                areas.*,
                departments.name AS department,
                companies.id AS company_id,
                companies.name AS company
            ');
        }
        else { $this->db->select($options['select']); }

        $this->db->from('areas')
            ->join('departments', 'areas.department_id = departments.id')
            ->join('companies', 'departments.company_id = companies.id');

        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->order_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @return string
     */
	public function datatable_json()
	{
		$this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';

		$this->datatables->select('
		    areas.id,
            areas.name,
		    areas.short_name,
		    areas.active,
		    areas.department_id, 
		    departments.name as department_name,
		    companies.name as company_name,
		    CONCAT(users.first_name," ",users.last_name) AS manager'
        )
            ->from($this->_table)
            ->join('departments', 'areas.department_id = departments.id')
            ->join('companies', 'departments.company_id = companies.id')
            ->join('users', 'areas.manager_id = users.id', 'left')
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

		return $this->datatables->generate();
	}

    public function delete($pk)
    {
        $this->load->model('employee_model');

        if ( $this->employee_model->exist_where(array('area_id' => $pk)) )
        {
            throw new Exception('No se puede eliminar el área porque tiene empleado(s) asociado(s).');
        }
        else
        {
            return parent::delete($pk);
        }
    }
}
