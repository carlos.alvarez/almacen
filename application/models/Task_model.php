<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Task_model
 *
 * @property Task_employee_model $task_employee_model
 * @property Task_prerequisite_model $task_prerequisite_model
 * @property Task_tag_model $task_tag_model
 * @property Task_requirement_model $task_requirement_model
 * @property Project_log_model $project_log_model
 * @property Calendar_model $calendar_model
 * @property Task_model $task_model
 * @property Project_model $project_model
 * @property Notification_model $notification_model
 * @property Push_subscription_model $push_subscription_model
 * @property GCM $gcm
 */
class Task_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'tasks';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // stage_id
            'field'     => 'stage_id',
            'label'     => 'lang:stage',
            'rules'     => 'trim|required|is_natural_no_zero|exist[stages.id]'
        ),
        array( // title
            'field'     => 'title',
            'label'     => 'lang:title',
            'rules'     => 'trim|required|max_length[150]'
        ),
        array( // description
            'field'     => 'description',
            'label'     => 'lang:description',
            'rules'     => 'trim'
        ),
        array( // start_date
            'field'     => 'start_date',
            'label'     => 'lang:start_date',
            'rules'     => 'trim|required'
        ),
        array( // estimated_end_date
            'field'     => 'estimated_end_date',
            'label'     => 'lang:estimated_date',
            'rules'     => 'trim|required'
        ),
        array( // end_date
            'field'     => 'end_date',
            'label'     => 'lang:end_date',
            'rules'     => 'trim'
        ),
        array( // duration_days
            'field'     => 'duration_days',
            'label'     => 'lang:duration_days',
            'rules'     => 'trim|required|is_natural'
        ),
        array( // duration_hours
            'field'     => 'duration_hours',
            'label'     => 'lang:duration_hours',
            'rules'     => 'trim|required|is_natural'
        ),
        array( // day_type
            'field'     => 'day_type',
            'label'     => 'lang:day_type',
            'rules'     => 'trim|in_list[laborable,calendar]'
        ),
        array( // weight
            'field'     => 'weight',
            'label'     => 'lang:weight',
            'rules'     => 'trim|required|numeric|greater_than[0]|less_than_equal_to[100]'
        ),
        array( // client_visibility
            'field'     => 'client_visibility',
            'label'     => 'lang:client_visibility',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // client_notification
            'field'     => 'client_notification',
            'label'     => 'lang:client_notification',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // order
            'field'     => 'order',
            'label'     => 'lang:order',
            'rules'     => 'trim|is_natural'
        ),
        array( // status
            'field'     => 'status',
            'label'     => 'Estado',
            'rules'     => 'trim|in_list[pendiente,proceso,cerrado,anulado]'
        ),
        array( // status_changer
            'field'     => 'status_changer',
            'label'     => 'Cambia Estado',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // stage_status_id
            'field'     => 'stage_status_id',
            'label'     => 'Estado Interno',
            'rules'     => 'trim|is_natural_no_zero|exist[stages_status.id]'
        ),
        array( // reason_id
            'field'     => 'reason_id',
            'label'     => 'Causa',
            'rules'     => 'trim|is_natural_no_zero|exist[reasons.id]'
        ),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param null $options
     * @param bool $array_result
     * @return array|mixed
     */
    public function find($options = NULL, $array_result = FALSE)
    {
        $this->db->from($this->_table)
            ->join('stages_status', 'tasks.stage_status_id = stages_status.id', 'left')
            ->join('reasons', 'tasks.reason_id = reasons.id', 'left');
        if ( isset($options['select']) )    { $this->db->select($options['select'], FALSE); }
        else
        {
            $this->db->select('
                tasks.*,
                stages_status.name AS stage_status,
                reasons.name AS reason
            ');
        }
        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->group_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @param array|object $data
     * @param bool $skip_validation
     * @return bool|int|string
     */
    public function insert($data, $skip_validation = FALSE)
    {
        $this->load->model('project_model');

        $this->db->trans_start();

        $id = parent::insert($data, $skip_validation);

        if ( $id )
        {
            $project_id = $this->get_project($id);

            if ( isset($data['tags']) )
            {
                $this->update_tags($id, $data['tags']);
            }

            if ( isset($data['resources']) )
            {
                $this->update_resources($id, $data['resources']);
            }

            if ( isset($data['requirements']) )
            {
                $this->update_requirements($id, $data['requirements']);
            }

            if ( isset($data['prerequisites']) )
            {
                $this->update_prerequisites($id, $data['prerequisites']);
            }

            if ( !isset($data['no_reschedule']) )
            {
                $this->project_model->update_estimated_end_date($project_id);
            }

            $this->db->trans_complete();
        }

        return $this->db->trans_status() ? $id : FALSE;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get_ideal_start_date($id)
    {
        $this->load->model('project_model');
        $this->load->model('task_prerequisite_model');

        $project_id = $this->get_project($id);
        if ( !$project_id ) { return FALSE; }

        $project = $this->project_model->get($project_id);

        $start_date = $project->start_date;

        $prerequisites = $this->task_prerequisite_model->find(array('where'=>array('task_id'=>$id)));
        foreach ($prerequisites as $prerequisite)
        {
            $pre = $this->get($prerequisite);

            if ( $pre && $pre->status !== 'anulado' )
            {
                $pre_end_date = ($pre->status === 'cerrado') ? $pre->end_date : $pre->estimated_end_date;
                if ( $pre_end_date > $start_date ) { $start_date = $pre_end_date; }
            }
        }

        return $start_date;
    }

    /**
     * @param array|int|string $pk
     * @return bool
     */
    public function delete($pk)
    {
        $this->load->model('task_employee_model');
        $this->load->model('task_prerequisite_model');
        $this->load->model('task_tag_model');
        $this->load->model('task_requirement_model');

        $this->db->trans_start();
            $this->task_employee_model->delete_where(array('task_id'=>$pk));
            $this->task_prerequisite_model->delete_where(array('task_id'=>$pk));
            $this->task_prerequisite_model->delete_where(array('task_prerequisite'=>$pk));
            $this->task_tag_model->delete_where(array('task_id'=>$pk));
            $this->task_requirement_model->delete_where(array('task_id'=>$pk));
            parent::delete($pk);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){ return FALSE; }

        return TRUE;
    }

    /**
     * @param $id
     * @return int|bool
     */
    public function get_project($id)
    {
        $project = $this->db->select('projects.id')
            ->from('tasks')
            ->join('stages', 'tasks.stage_id = stages.id')
            ->join('projects', 'stages.project_id = projects.id')
            ->where(array('tasks.id'=>$id))->get()->row();

        if ( $project ) { return $project->id; }

        return FALSE;
    }

    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function cancel($id)
    {
        $this->load->model('project_log_model');

        $task = $this->get($id);

        $this->db->trans_start();
        if ( !$this->update($id, array('status'=>'anulado')) )
        {
            throw new Exception(validation_errors());
        }

        $this->project_log_model->insert(array(
            'project_id'        => $this->get_project($id),
            'user_id'           => $this->session->userdata('user_id'),
            'message'           => $this->session->userdata('full_name')." ha anulado la tarea [{$id}]: {$task->title}."
        ));

        $this->db->trans_complete();

        if ( !$this->db->trans_status() )
        {
            throw new Exception('Ha ocurrido un error al intentar anular esta tarea.');
        }

        return TRUE;
    }

    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function close($id)
    {
        $this->load->model('project_log_model');
        $this->load->model('project_model');

        $task = $this->get($id);

        $this->db->trans_start();
        if ( !$this->update($id, array('status'=>'cerrado', 'end_date'=>date('Y-m-d'))) )
        {
            throw new Exception(validation_errors());
        }

        $project_id = $this->get_project($id);

        $log_message = $this->session->userdata('full_name')." ha cerrado la tarea [{$id}]: {$task->title}.";
        if ( $task->status_changer === 'true' && $task->stage_status_id && $task->reason_id )
        {
            $this->project_model->update($project_id, array(
                'stage_status_id'   => $task->stage_status_id,
                'reason_id'         => $task->reason_id
            ));

            $log_message .= " Ha cambiado el estado interno del proyecto a: '{$task->stage_status}' por la causa: '{$task->reason}'.";
        }

        $this->project_log_model->insert(array(
            'project_id'        => $project_id,
            'user_id'           => $this->session->userdata('user_id'),
            'message'           => $log_message
        ));

        // TODO: Enviar correos

        $this->db->trans_complete();

        if ( !$this->db->trans_status() )
        {
            throw new Exception('Ha ocurrido un error al intentar cerrar esta tarea.');
        }

        return TRUE;
    }

    /**
     * @return string
     */
    public function datatable_json()
    {
        $this->load->library('datatables');

        $this->datatables->select('
			tasks.id,
			tasks.title,
			tasks.description,
			tasks.start_date,
			DATE_FORMAT(tasks.start_date, "%d/%b/%y") AS start_date_nice,
			tasks.estimated_end_date,
			DATE_FORMAT(tasks.estimated_end_date, "%d/%b/%y") AS estimated_end_date_nice,
			tasks.end_date,
			DATE_FORMAT(tasks.end_date, "%d/%b/%y") AS end_date_nice,
			tasks.status,
			tasks.client_visibility,
			tasks.client_notification,
			stages.title AS stage,
			FORMAT((SUM(IF(tasks_requirements.done="true",1,0))/COUNT(tasks_requirements.id))*100, 0) AS progress,
			tasks_tags.tag,
            GROUP_CONCAT(DISTINCT tasks_tags.tag ORDER BY tasks_tags.tag SEPARATOR ",") AS tags,
			users.first_name AS resource_fn,
			users.last_name AS resource_ln,
			stages.template_id,
			templates.title AS template,
			stages.project_id,
			projects.name AS project,
			projects.description AS project_description,
			tasks.status_changer,
			tasks.stage_status_id,
			tasks.reason_id,
			stages_status.name AS stage_status,
            reasons.name AS reason
		')->from('tasks')
            ->join('tasks_tags', 'tasks_tags.task_id = tasks.id', 'left')
            ->join('stages', 'tasks.stage_id = stages.id')
            ->join('templates', 'stages.template_id = templates.id', 'left')
            ->join('projects', 'stages.project_id = projects.id')
            ->join('tasks_requirements', 'tasks_requirements.task_id = tasks.id')
            ->join('tasks_employees', 'tasks_employees.task_id = tasks.id', 'left')
            ->join('users', 'tasks_employees.user_id = users.id', 'left')
            ->join('stages_status', 'tasks.stage_status_id = stages_status.id', 'left')
            ->join('reasons', 'tasks.reason_id = reasons.id', 'left')
            ->add_column('resources', '$1', 'short_name(resource_fn, resource_ln)')
            ->add_column('delay', '$1', 'date_difference(estimated_end_date)')
            ->unset_column('resource_fn')
            ->unset_column('resource_ln')
            ->group_by('tasks.id');

        $filtered = FALSE;
        if ( $this->input->get('project') )
        {
            $this->datatables->where(array('stages.project_id' => $this->input->get('project')));
            $filtered = TRUE;
        }
        if ( $this->input->get('template') )
        {
            $this->datatables->where(array('stages.template_id' => $this->input->get('template')));
            $filtered = TRUE;
        }
        if ( $this->input->get('stage') )
        {
            $this->datatables->where(array('stages.id' => $this->input->get('stage')));
            $filtered = TRUE;
        }
        if ( $this->input->get('task') )
        {
            $this->datatables->where(array('tasks.id' => $this->input->get('task')));
            $filtered = TRUE;
        }

        if ( !$filtered )
        {
            $this->datatables->where(array('tasks_employees.user_id' => $this->session->userdata('user_id')));
        }

        if ( $this->input->get('delayed') )
        {
            $this->datatables->where_in('tasks.status', array('pendiente','proceso'))
                ->where('DATE(tasks.estimated_end_date) != "0000-00-00" AND DATE(tasks.estimated_end_date) < DATE(NOW())');
        }

        return $this->datatables->generate();
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function project_datatable_json($id)
    {
        $this->load->library('datatables');

        $this->datatables->select('
			tasks.id,
			tasks.title,
			tasks.description,
			tasks.start_date,
			DATE_FORMAT(tasks.start_date, "%d/%b/%y") AS start_date_nice,
			tasks.estimated_end_date,
			DATE_FORMAT(tasks.estimated_end_date, "%d/%b/%y") AS estimated_end_date_nice,
			tasks.end_date,
			DATE_FORMAT(tasks.end_date, "%d/%b/%y") AS end_date_nice,
			tasks.status,
			tasks.client_visibility,
			tasks.client_notification,
			stages.title AS stage,
			FORMAT((SUM(IF(tasks_requirements.done="true",1,0))/COUNT(tasks_requirements.id))*100, 0) AS progress,
			tasks_tags.tag,
            GROUP_CONCAT(DISTINCT tasks_tags.tag ORDER BY tasks_tags.tag SEPARATOR ",") AS tags,
			users.first_name AS resource_fn,
			users.last_name AS resource_ln,
			stages.template_id,
			templates.title AS template,
			stages.project_id,
			projects.name AS project,
			projects.description AS project_description,
			CONCAT(LPAD(stages.order,3,"0"),"_",LPAD(tasks.order,3,"0")) AS `order`,
			tasks.status_changer,
			tasks.stage_status_id,
			tasks.reason_id,
			stages_status.name AS stage_status,
            reasons.name AS reason
		')->from('tasks')
            ->join('tasks_tags', 'tasks_tags.task_id = tasks.id', 'left')
            ->join('stages', 'tasks.stage_id = stages.id')
            ->join('templates', 'stages.template_id = templates.id', 'left')
            ->join('projects', 'stages.project_id = projects.id')
            ->join('tasks_requirements', 'tasks_requirements.task_id = tasks.id')
            ->join('tasks_employees', 'tasks_employees.task_id = tasks.id', 'left')
            ->join('users', 'tasks_employees.user_id = users.id', 'left')
            ->join('stages_status', 'tasks.stage_status_id = stages_status.id', 'left')
            ->join('reasons', 'tasks.reason_id = reasons.id', 'left')
            ->add_column('resources', '$1', 'short_name(resource_fn, resource_ln)')
            ->add_column('delay', '$1', 'date_difference(estimated_end_date)')
            ->unset_column('resource_fn')
            ->unset_column('resource_ln')
            ->group_by('tasks.id')
            ->where('stages.project_id', $id);

        if ( $this->session->userdata('client') )
        {
            $this->datatables->where('tasks.client_visibility', 'true');
        }

        if ( $this->input->get('delayed') )
        {
            $this->datatables->where_in('tasks.status', array('pendiente','proceso'))
                ->where('DATE(tasks.estimated_end_date) != "0000-00-00" AND DATE(tasks.estimated_end_date) < DATE(NOW())');
        }

        return $this->datatables->generate();
    }

    /**
     * @param $id
     *
     * @param bool|int $client
     *
     * @return string
     */
    public function my_datatable_json($id, $client=FALSE)
    {
        $this->load->library('datatables');

        $this->datatables->select('
			tasks.id,
			tasks.title,
			tasks.description,
			tasks.start_date,
			DATE_FORMAT(tasks.start_date, "%d/%b/%y") AS start_date_nice,
			tasks.estimated_end_date,
			DATE_FORMAT(tasks.estimated_end_date, "%d/%b/%y") AS estimated_end_date_nice,
			tasks.end_date,
			DATE_FORMAT(tasks.end_date, "%d/%b/%y") AS end_date_nice,
			tasks.status,
			tasks.client_visibility,
			tasks.client_notification,
			stages.title AS stage,
			FORMAT((SUM(IF(tasks_requirements.done="true",1,0))/COUNT(tasks_requirements.id))*100, 0) AS progress,
			tasks_tags.tag,
            GROUP_CONCAT(DISTINCT tasks_tags.tag ORDER BY tasks_tags.tag SEPARATOR ",") AS tags,
			users.first_name AS resource_fn,
			users.last_name AS resource_ln,
			stages.template_id,
			templates.title AS template,
			stages.project_id,
			projects.name AS project,
			projects.description AS project_description,
			tasks.status_changer,
			tasks.stage_status_id,
			tasks.reason_id,
			stages_status.name AS stage_status,
            reasons.name AS reason
		')->from('tasks')
            ->join('tasks_tags', 'tasks_tags.task_id = tasks.id', 'left')
            ->join('stages', 'tasks.stage_id = stages.id')
            ->join('templates', 'stages.template_id = templates.id', 'left')
            ->join('projects', 'stages.project_id = projects.id')
            ->join('tasks_requirements', 'tasks_requirements.task_id = tasks.id')
            ->join('tasks_employees', 'tasks_employees.task_id = tasks.id', 'left')
            ->join('users', 'tasks_employees.user_id = users.id', 'left')
            ->join('stages_status', 'tasks.stage_status_id = stages_status.id', 'left')
            ->join('reasons', 'tasks.reason_id = reasons.id', 'left')
            ->add_column('resources', '$1', 'short_name(resource_fn, resource_ln)')
            ->add_column('delay', '$1', 'date_difference(estimated_end_date)')
            ->unset_column('resource_fn')
            ->unset_column('resource_ln')
            ->group_by('tasks.id');

        if ( $this->input->get('delayed') )
        {
            $this->datatables->where_in('tasks.status', array('pendiente','proceso'))
                ->where('DATE(tasks.estimated_end_date) != "0000-00-00" AND DATE(tasks.estimated_end_date) < DATE(NOW())');
        }

        if ( !$client )
        {
            $this->datatables->where('tasks_employees.user_id = '.$id);
        }
        else
        {
            $this->datatables->where(array('projects.client_id' => $client, 'tasks.client_visibility' => 'true'));
        }

        return $this->datatables->generate();
    }

    /**
     * @param $id
     * @return bool
     */
    public function schedule_task($id)
    {
        $this->load->model('calendar_model');

        $task = $this->task_model->get($id);
        if ( !$task ) { return FALSE; }

        $start_date = $this->calculate_start_date($id);

        if ( !$this->update($id, array( 'start_date' => $start_date )) ) { return FALSE; }

        $end_date = $this->calculate_end_date($id);

        $this->update($id, array( 'estimated_end_date' => $end_date ));

        return TRUE;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function calculate_start_date($id)
    {
        $this->load->model('project_model');

        $id = (int) $id;
        $start_date = $this->db->query("
            SELECT
              IFNULL(tasks.end_date, tasks.estimated_end_date) AS end_date_r
            FROM tasks_prerequisites
            JOIN tasks ON (tasks_prerequisites.task_prerequisite = tasks.id)
            WHERE
              tasks_prerequisites.task_id = {$id}
            ORDER BY end_date_r DESC
            LIMIT 1;
        ")->result();

        if ( $start_date ) { return $start_date->end_date_r; }

        $project = $this->project_model->get($this->get_project($id));
        return $project->start_date;
    }

    /**
     * @param $id
     * @return false|string
     */
    public function calculate_end_date($id)
    {
        $this->load->model('calendar_model');

        $id = (int) $id;

        $task = $this->task_model->get($id);

        $end_date = $this->calendar_model->calc_end_date(1, $task->start_date, $task->duration_days);

        return $end_date;
    }

    /**
     * @param array|int|string $pk
     * @param array|object $data
     * @param bool $skip_validation
     * @return bool
     */
    public function update($pk, $data, $skip_validation = FALSE)
    {
        $this->load->model('project_model');
        $project_id = $this->get_project($pk);

        $this->db->trans_start();
        $before = $this->get($pk);
        $res = parent::update($pk, $data, $skip_validation);

        if ( $res )
        {
            $after = $this->get($pk);

            if ( isset($data['tags']) )
            {
                $this->update_tags($pk, $data['tags']);
            }

            if ( isset($data['resources']) )
            {
                $this->update_resources($pk, $data['resources']);
            }

            if ( isset($data['requirements']) )
            {
                $this->update_requirements($pk, $data['requirements']);
            }

            $changed_pre = FALSE;
            if ( isset($data['prerequisites']) )
            {
                $changed_pre = $this->update_prerequisites($pk, $data['prerequisites']);
            }

            if ( !isset($data['no_reschedule']) && ($before->start_date !== $after->start_date || $before->duration_days !== $after->duration_days || $changed_pre) )
            {
                //$this->project_model->reschedule_tasks($project_id);
                $this->reschedule($pk, $after->start_date, $after->estimated_end_date);
                $this->project_model->update_estimated_end_date($project_id);
            }

        }
        $this->db->trans_complete();

        return $res && $this->db->trans_status();
    }

    /**
     * @param $task_id
     * @param $prerequisites
     * @return bool
     */
    public function update_prerequisites($task_id, $prerequisites)
    {
        $this->load->model('task_prerequisite_model');

        $changed = FALSE;

        $prereqs = $this->task_prerequisite_model->find(array(
            'where' => array(
                'task_id'   => $task_id
            )
        ));

        $pre_saved = array();
        foreach ($prereqs as $prereq)
        {
            if ( !in_array($prereq->task_prerequisite, $prerequisites) )
            {
                $this->task_prerequisite_model->delete(array(
                    'task_id'               => $prereq->task_id,
                    'task_prerequisite'     => $prereq->task_prerequisite
                ));
                $changed = TRUE;
            }
            else
            {
                $pre_saved[] = $prereq->task_prerequisite;
            }
        }
        foreach ($prerequisites as $prerequisite)
        {
            if ( !in_array($prerequisite, $pre_saved) )
            {
                $this->task_prerequisite_model->insert(array(
                    'task_id'               => $task_id,
                    'task_prerequisite'     => $prerequisite
                ));
                $changed = TRUE;
            }
        }

        return $changed;
    }

    /**
     * @param $task_id
     * @param $resources
     */
    public function update_resources($task_id, $resources)
    {
        $this->load->model('task_employee_model');

        $res = $this->task_employee_model->find(array(
            'where' => array(
                'task_id'   => $task_id
            )
        ));

        $re_saved = array();
        foreach ($res as $re)
        {
            if ( !in_array($re->user_id, $resources) )
            {
                $this->task_employee_model->delete(array(
                    'task_id'   => $re->task_id,
                    'user_id'   => $re->user_id
                ));
            }
            else
            {
                $re_saved[] = $re->user_id;
            }
        }

        foreach ($resources as $resource)
        {
            if ( !in_array($resource, $re_saved) )
            {
                $this->task_employee_model->insert(array(
                    'task_id'   => $task_id,
                    'user_id'   => $resource
                ));
            }
        }
    }

    public function update_requirements($task_id, $requirements)
    {
        $this->load->model('task_requirement_model');

        $temp = $this->task_requirement_model->find(array(
            'where' => array(
                'task_id'   => $task_id
            )
        ));

        $reqs = array();
        foreach ($temp as $item) { $reqs[$item->id] = $item; }

        foreach ($requirements as $requirement)
        {
            if ( isset($reqs[$requirement['id']]) )
            {
                if ( $requirement['requirement'] !== $reqs[$requirement['id']]->requirement )
                {
                    $this->task_requirement_model->update($requirement['id'], array(
                        'requirement'   => $requirement['requirement']
                    ));
                }
                unset($reqs[$requirement['id']]);
            }
            else
            {
                $this->task_requirement_model->insert(array(
                    'task_id'       => $task_id,
                    'requirement'   => $requirement['requirement'],
                    'weight'        => 5
                ));
            }
        }

        foreach ($reqs as $id => $req)
        {
            $this->task_requirement_model->delete($id);
        }

    }

    /**
     * @param $task_id
     * @param $tags
     */
    public function update_tags($task_id, $tags)
    {
        $this->load->model('task_tag_model');

        if ( !is_array($tags) )
        {
            $tags = explode(',', $tags);
        }
        foreach ($tags as $i => $tag) { $tags[$i] = strtoupper(trim($tag)); }

        $tgs = $this->task_tag_model->get_by_task($task_id);

        $re_saved = array();
        foreach ($tgs as $tg)
        {
            if ( !in_array($tg->tag, $tags) )
            {
                $this->task_tag_model->delete($tg->id);
            }
            else
            {
                $re_saved[] = $tg->tag;
            }
        }

        foreach ($tags as $tag)
        {
            if ( !in_array($tag, $re_saved) )
            {
                $this->task_tag_model->insert(array(
                    'task_id'   => $task_id,
                    'tag'       => $tag,
                    'color'     => '#BBBBBB'
                ));
            }
        }
    }

    /**
     * @param $task_id
     * @return mixed
     */
    public function get_employees($task_id)
    {
        $this->load->model('task_employee_model');

        $temp = $this->task_employee_model->find(array(
            'where'     => array(
                'tasks_employees.task_id'   => $task_id
            )
        ));

        $users = array();
        foreach ($temp as $item) { $users[] = $item->user_id; }

        return $users;
    }

    public function tasks_filter_json($day,$month,$id,$stage,$status,$brand)
    {
        $this->load->library('datatables');
        $brand = (int)$brand;
        $this->datatables->select('
			tasks.id,
			tasks.title,
			tasks.description,
			tasks.start_date,
			DATE_FORMAT(tasks.start_date, "%d/%b/%Y") AS start_date_nice,
			tasks.estimated_end_date,
			DATE_FORMAT(tasks.estimated_end_date, "%d/%b/%Y") AS estimated_end_date_nice,
			tasks.end_date,
			DATE_FORMAT(tasks.end_date, "%d/%b/%Y") AS end_date_nice,
			tasks.status,
			tasks.client_visibility,
			tasks.client_notification,
			stages.title AS stage,
			FORMAT((SUM(IF(tasks_requirements.done="true",1,0))/COUNT(tasks_requirements.id))*100, 0) AS progress
		')->from('tasks')
            ->join('stages', 'tasks.stage_id = stages.id')
            ->join('tasks_requirements', 'tasks_requirements.task_id = tasks.id')
            ->join('projects_brands', 'projects_brands.project_id = stages.project_id','left')
            ->join('brands', 'brands.id = projects_brands.brand_id','left')
            ->group_by('tasks.id');

        $this->datatables->where(array('stages.project_id' => $id));

        if ( $month !== 'all' ) { $month = (int)$month; $this->datatables->where("MONTH(tasks.start_date) ='{$month}'"); }
        if ( $day !== 'all' ) { $day = (int)$day; $this->datatables->where("DAY(tasks.start_date) = '{$day}'"); }
        if ( $brand > 0 ) { $this->datatables->where("projects_brands.brand_id ={$brand}"); }
        if ( $status !== 'all' ) { $status = strtolower($status); $this->datatables->where("tasks.status LIKE '%{$status}%'"); }
        if ( $stage !== 'all' ) { $stage = (string)$stage; $this->datatables->where("stages.id ={$stage}"); }
//        if ( $brand !== 'all' ) { $brand = (string)$brand; $this->datatables->where("brands.name LIKE '%{$brand}%'"); }

        return $this->datatables->generate();
    }

    /**
     * @param $task_id
     * @param $new_start_date
     * @param $new_end_date
     * @return array|bool
     */
    public function analize_reschedule($task_id, $new_start_date, $new_end_date)
    {
        $this->load->model('task_prerequisite_model');
        $warnings = array();

        $temp = $this->task_prerequisite_model->find(array('where'=>array('task_id'=>$task_id)));
        $prerequisites = array();
        foreach ($temp as $item) { $prerequisites[] = $item->task_prerequisite; }

        foreach ($prerequisites as $p_task_id)
        {
            $task = $this->get($p_task_id);
            if ( !$task || $task->status === 'anulado' ) { continue; }
            $end_date = ($task->status === 'cerrado') ? $task->end_date : $task->estimated_end_date;

            if ( $new_start_date < $end_date )
            {
                $warnings[] = "A esta tarea le será removida de los prerequisitos la tarea '{$task->title}' [{$task->id}].";
            }
        }


        $temp = $this->task_prerequisite_model->find(array('where'=>array('task_prerequisite'=>$task_id)));
        $dependents = array();
        foreach ($temp as $item) { $dependents[] = $item->task_id; }

        foreach ($dependents as $d_task_id)
        {
            $task = $this->get($d_task_id);
            if ( !$task || $task->status === 'anulado' ) { continue; }
            if ( $task->start_date < $new_end_date )
            {
                if ( $task->status === 'pendiente' )
                {
                    $warnings[] = "La tarea '{$task->title}' [{$task->id}] tendrá que ser reagendada.";
                }
                else
                {
                    $warnings[] = "Esta tarea será removida de los prerequisitos de la tarea '{$task->title}' [{$task->id}].";
                }
            }
        }

        if ( count($warnings) > 0 )
        {
            $warnings[] = 'Es posible que este cambio afecte a más tareas e incluso cambie la fecha estimada del proyecto.';
            return $warnings;
        }

        return TRUE;
    }

    /**
     * @param $task_id
     * @param $new_start_date
     * @param null $new_end_date
     * @return bool
     * @throws Exception
     */
    public function reschedule($task_id, $new_start_date, $new_end_date = NULL)
    {
        $this->load->model('task_prerequisite_model');
        $this->load->model('project_model');
        $this->load->model('calendar_model');

        if ( !($this_task = $this->get($task_id)) ) { return FALSE; }

        if ( $new_end_date === NULL )
        {
            $days = $this->calendar_model->days_between(1, $this_task->start_date, $this_task->estimated_end_date);
            $new_end_date = $this->calendar_model->calc_end_date(1, $new_start_date, $days);
        }

        $days = $this->calendar_model->days_between(1, $new_start_date, $new_end_date);
        $res = $this->update($task_id, array(
            'start_date'            => $new_start_date,
            'estimated_end_date'    => $new_end_date,
            'duration_days'         => $days
        ));

        if ( !$res ) { throw new Exception(validation_errors()); }

        $temp = $this->task_prerequisite_model->find(array('where'=>array('task_id'=>$task_id)));
        $prerequisites = array();
        foreach ($temp as $item) { $prerequisites[] = $item->task_prerequisite; }
        foreach ($prerequisites as $p_task_id)
        {
            $task = $this->get($p_task_id);
            if ( !$task || $task->status === 'anulado' ) { continue; }
            $end_date = ($task->status === 'cerrado') ? $task->end_date : $task->estimated_end_date;

            if ( $new_start_date < $end_date )
            {
                $this->task_prerequisite_model->delete(array('task_id'=>$task_id,'task_prerequisite'=>$p_task_id));
            }
        }

        $temp = $this->task_prerequisite_model->find(array('where'=>array('task_prerequisite'=>$task_id)));
        $dependents = array();
        foreach ($temp as $item) { $dependents[] = $item->task_id; }

        foreach ($dependents as $d_task_id)
        {
            $task = $this->get($d_task_id);
            if ( !$task || $task->status === 'anulado' ) { continue; }
            if ( $task->start_date < $new_end_date )
            {
                if ( $task->status === 'pendiente' )
                {
                    $this->reschedule($d_task_id, $new_end_date);
                }
                else
                {
                    $this->task_prerequisite_model->delete(array('task_id'=>$d_task_id,'task_prerequisite'=>$task_id));
                }
            }
        }

        if ( $this_task->start_date !== $new_start_date || $this_task->estimated_end_date !== $new_end_date )
        {
            $this->_task_moved_notifications($task_id);
        }

        return TRUE;
    }

    protected function _task_moved_notifications($task_id)
    {
        $this->load->model('notification_model');
        $this->load->model('push_subscription_model');

        $task = $this->get($task_id);
        if ( !$task ) { return FALSE; }

        $project_id = $this->get_project($task_id);

        $project = $this->project_model->get($project_id);

        $notification = array(
            'message'   => "La tarea '{$task->title}' [ID:{$task_id}] ha sido re-agendada.",
            'actions'   => json_encode(array(array(
                'action'    => 'task/f_view/'.$task->id,
                'title'     => 'Ver Tarea'
            )))
        );
        $employees = $this->get_employees($task_id);
        if ( !in_array($project->manager_id, $employees) )
        {
            $employees[] = $project->manager_id;
        }

        foreach ($employees as $employee_id)
        {
            $notification['user_id'] = $employee_id;
            $this->notification_model->insert($notification);
        }

        $endpoints = array();
        foreach ($employees as $employee)
        {
            $endps = $this->push_subscription_model->get_by_user($employee);
            foreach ($endps as $endp) { $endpoints[] = $endp; }
        }

        if ( count($endpoints) === 0 ) { return TRUE; }

        $this->load->library('gcm');
        $this->gcm->setRecipients($endpoints);
        $this->gcm->setMessage('Nuevas notificaciones');
        $this->gcm->send();

        return TRUE;
    }
}
