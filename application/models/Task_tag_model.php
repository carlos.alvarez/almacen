<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Task_tag_model
 */
class Task_tag_model extends MY_Model {

    /**
     * @var string
     */
	protected $_table = 'tasks_tags';

    /**
     * @var array
     */
	protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // task_id
            'field'     => 'task_id',
            'label'     => 'lang:task',
            'rules'     => 'trim|required|is_natural_no_zero|exist[tasks.id]'
        ),
        array( // tag
            'field'     => 'tag',
            'label'     => 'lang:tag',
            'rules'     => 'trim|strtoupper|required|max_length[60]'
        ),
        array( // color
            'field'     => 'color',
            'label'     => 'lang:color',
            'rules'     => 'trim|required|exact_length[7]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param $task_id
     * @param bool $formatted
     * @return array|null|object|string
     */
	public function get_by_task($task_id, $formatted = FALSE)
    {
        $temp = $this->find(array(
            'where'     => array(
                'task_id' => $task_id
            )
        ));

        if ( $formatted )
        {
            $tags = '';
            foreach ($temp as $item)
            {
                $tags .= ($tags === '') ? $item->tag : ",{$item->tag}";
            }
            return $tags;
        }

        return $temp;
    }
}
