<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_comment_model extends MY_Model {

	protected $_table = 'projects_comments';

	protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // project_id
            'field'     => 'project_id',
            'label'     => 'lang:project',
            'rules'     => 'trim|required|is_natural_no_zero|exist[projects.id]'
        ),
        array( // user_id
            'field'     => 'user_id',
            'label'     => 'lang:user',
            'rules'     => 'trim|required|is_natural_no_zero|exist[users.id]'
        ),
        array( // message
            'field'     => 'message',
            'label'     => 'lang:message',
            'rules'     => 'trim|required'
        ),
        array( // client_visibility
            'field'     => 'client_visibility',
            'label'     => 'lang:client_visibility',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

	public function find($options = NULL, $array_result = FALSE)
    {
        if ( isset($options['select']) )    { $this->db->select($options['select'], FALSE); }
        else
        {
            $this->db->select('
                projects_comments.*,
                CONCAT(users.first_name," ",users.last_name) as user
            ', FALSE);
        }

        $this->db->from($this->_table)
            ->join('users', 'projects_comments.user_id = users.id');

        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->group_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }
}
