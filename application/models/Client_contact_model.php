<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Client_contact_model
 * @property User_model $user_model
 * @property Role_model $role_model
 * @property User_role_model $user_role_model
 * @property Correo $correo
 * @property Client_model $client_model
 */
class Client_contact_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'client_contacts';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // client_id
            'field'     => 'client_id',
            'label'     => 'lang:client',
            'rules'     => 'trim|required|is_natural_no_zero|exist[clients.id]'
        ),
        array( // user_id
            'field'     => 'user_id',
            'label'     => 'lang:user',
            'rules'     => 'trim|is_natural_no_zero|exist[users.id]'
        ),
        array( // contact_type_id
            'field'     => 'contact_type_id',
            'label'     => 'lang:contact_type',
            'rules'     => 'trim|required|is_natural_no_zero|exist[contact_types.id]'
        ),
        array( // name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|min_length[4]|max_length[250]'
		),
        array( // position
            'field'     => 'position',
            'label'     => 'lang:position',
            'rules'     => 'trim|required|min_length[2]|max_length[250]'
        ),
        array( // phone
            'field'     => 'phone',
            'label'     => 'lang:phone',
            'rules'     => 'trim|min_length[4]|max_length[30]'
        ),
        array( // email
            'field'     => 'email',
            'label'     => 'lang:email',
            'rules'     => 'trim|required|min_length[4]|max_length[250]'
        ),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param null $options
     * @param bool|string $array_result
     * @return mixed
     */
    public function find($options = NULL, $array_result = FALSE)
    {
        if ( isset($options['select']) )
        {
            $this->db->select($options['select'], FALSE);
        }
        else
        {
            $this->db->select('
                client_contacts.*,
                contact_types.name as contact_type
            ');
        }

        $this->db->from($this->_table)
            ->join('contact_types', 'client_contacts.contact_type_id = contact_types.id');

        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->order_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @param $contact_id
     * @param $array_post
     *
     * @return bool
     * @throws Exception
     */
    public function create_user($contact_id, $array_post)
    {
        $this->load->model('role_model');
        $this->load->model('user_role_model');
        $this->load->model('user_model');
        $this->load->model('client_model');

        $this->db->trans_start();
        if ( $id = $this->user_model->insert($array_post) )
        {
            $client_rol = $this->role_model->find(array('where' => array('name' => 'Cliente'), 'limit' => 1));
            if( $client_rol )
            {
                $this->user_role_model->insert(array('user_id' => $id, 'role_id' => $client_rol->id));
            }
            else
            {
                throw new Exception(validation_errors());
            }

            if ( !$this->update($contact_id, array('user_id' => $id))  )
            {
                throw new Exception(validation_errors());
            }
        }
        else
        {
            throw new Exception(validation_errors());
        }
        $this->db->trans_complete();

        if ( $this->db->trans_status() === FALSE ) { return FALSE; }

        $this->load->library('correo');
        $user = $this->user_model->get($id);
        $contact = $this->get($contact_id);
        $client = $this->client_model->get($contact->client_id);

        $data = array(
            'to'            => $user->email,
            'subject'       => 'Bienvenido a ergospace',
            'content'       => 'welcome_client',
            'data'          => array(
                'client_name'   => $client->name,
                'username'      => $user->username,
                'password'      => $array_post['password']
            )
        );
        $this->correo->send($data);

        return TRUE;
    }
}
