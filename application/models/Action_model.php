<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Class Action_model
 *
 * @property Datatables $datatables
 */
class Action_model extends MY_Model {

	protected $_table = 'actions';

	protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|min_length[2]|max_length[30]'
		),
		array( // Method
			'field'     => 'method',
			'label'     => 'lang:method',
			'rules'     => 'trim|min_length[2]|max_length[30]|is_unique[actions.name]'
		),
		array( // Active
			'field'     => 'active',
			'label'     => 'lang:active',
			'rules'     => 'trim|in_list[true,false]'
		),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @return mixed
     */
	public function datatable_json()
	{
		$this->load->library('datatables');

		$this->datatables->select('
			id,
			name,
			method,
			active
		')->from('actions');

		return $this->datatables->generate();
	}

}
