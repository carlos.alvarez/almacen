<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpl_task_position_model extends MY_Model {

	protected $_table = 'tpl_tasks_positions';
    protected $_pk = array('tpl_task_id', 'position_id');

	protected $_validation_rules = array(
        array( // tpl_task_id
            'field'     => 'tpl_task_id',
            'label'     => 'lang:task',
            'rules'     => 'trim|required|is_natural_no_zero|exist[tpl_tasks.id]'
        ),
        array( // position_id
            'field'     => 'position_id',
            'label'     => 'lang:position',
            'rules'     => 'trim|required|is_natural_no_zero|exist[positions.id]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param null $options
     * @param bool|string $array_result
     * @return mixed
     */
    public function find($options = NULL, $array_result = FALSE)
    {
        if ( !isset($options['select']) )
        {
            $this->db->select('
                tpl_tasks_positions.*,
                positions.name AS position,
                positions.company_id AS company_id,
                companies.name AS company
            ', FALSE);
        }
        else { $this->db->select($options['select'], FALSE); }

        $this->db->from($this->_table)
        ->join('positions', 'tpl_tasks_positions.position_id = positions.id')
            ->join('companies', 'positions.company_id = companies.id');

        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->order_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

}
