<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User: cristian.rodriguez
 * Date: 9/7/2017
 * Time: 5:30 PM
 */

/**
 * Class Brand
 * @property Project_brand_model $project_brand_model
 */
class Brand_model extends MY_Model
{
    /**
     * @var string
     */
    protected $_table = 'brands';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // id
            'field' => 'id',
            'label' => 'ID',
            'rules' => 'is_natural_no_zero',
        ),
        array( // name
            'field' => 'name',
            'label' => 'lang:name',
            'rules' => 'trim|required|min_length[3]|max_length[150]',
        ),
        array( // created_at
            'field' => 'created_at',
            'label' => 'lang:created_at',
            'rules' => 'trim',
        ),
        array( // updated_at
            'field' => 'updated_at',
            'label' => 'lang:updated_at',
            'rules' => 'trim',
        ),
    );

    /**
     * @return string
     */
    public function datatable_json()
    {
        $this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';
        $grant_duplicate = grant_access('brand', 'create') ? 'true' : 'false';

        $this->datatables->select('
            id,
            name,
            created_at,
		    DATE_FORMAT(created_at, "%d/%b/%y %h:%i %p") AS created_at_formatted,
		    updated_at,
		    DATE_FORMAT(updated_at, "%d/%b/%y %h:%i %p") AS updated_at_formatted,
        ')->from($this->_table)
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete)
            ->add_column('duplicate', '$1', $grant_duplicate);

        return $this->datatables->generate();
    }

    public function delete($pk)
    {
        $this->load->model('project_brand_model');

        if ( $this->project_brand_model->exist_where(array('brand_id' => $pk)) ){
            throw new Exception('No se pudo eliminar la marca porque está asociado a presupuesto(s).');
        } else {
            return parent::delete($pk);
        }
    }
}