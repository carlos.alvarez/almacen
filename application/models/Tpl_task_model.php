<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Tpl_task_model
 *
 * @property Tpl_task_position_model $tpl_task_position_model
 * @property Tpl_task_prerequisite_model $tpl_task_prerequisite_model
 * @property Tpl_task_tag_model $tpl_task_tag_model
 * @property Tpl_task_requirement_model $tpl_task_requirement_model
 */
class Tpl_task_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'tpl_tasks';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // tpl_stage_id
            'field'     => 'tpl_stage_id',
            'label'     => 'lang:stage',
            'rules'     => 'trim|required|is_natural_no_zero|exist[tpl_stages.id]'
        ),
        array( // title
            'field'     => 'title',
            'label'     => 'lang:title',
            'rules'     => 'trim|required|max_length[150]'
        ),
        array( // description
            'field'     => 'description',
            'label'     => 'lang:description',
            'rules'     => 'trim'
        ),
        array( // duration_days
            'field'     => 'duration_days',
            'label'     => 'lang:duration_days',
            'rules'     => 'trim|required|is_natural'
        ),
        array( // duration_hours
            'field'     => 'duration_hours',
            'label'     => 'lang:duration_hours',
            'rules'     => 'trim|required|is_natural'
        ),
        array( // day_type
            'field'     => 'day_type',
            'label'     => 'lang:day_type',
            'rules'     => 'trim|in_list[laborable,calendar]'
        ),
        array( // weight
            'field'     => 'weight',
            'label'     => 'lang:weight',
            'rules'     => 'trim|required|numeric|greater_than[0]|less_than_equal_to[100]'
        ),
        array( // client_visibility
            'field'     => 'client_visibility',
            'label'     => 'lang:client_visibility',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // client_notification
            'field'     => 'client_notification',
            'label'     => 'lang:client_notification',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // order
            'field'     => 'order',
            'label'     => 'lang:order',
            'rules'     => 'trim|is_natural'
        ),
        array( // status_changer
            'field'     => 'status_changer',
            'label'     => 'Cambia Estado',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // stage_status_id
            'field'     => 'stage_status_id',
            'label'     => 'Estado Interno',
            'rules'     => 'trim|is_natural_no_zero|exist[stages_status.id]'
        ),
        array( // reason_id
            'field'     => 'reason_id',
            'label'     => 'Causa',
            'rules'     => 'trim|is_natural_no_zero|exist[reasons.id]'
        ),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param null $options
     * @param bool $array_result
     * @return array|null|object
     */
    public function find($options = NULL, $array_result = FALSE)
    {
        $this->db->from($this->_table)
        ->join('stages_status', 'tpl_tasks.stage_status_id = stages_status.id', 'left')
        ->join('reasons', 'tpl_tasks.reason_id = reasons.id', 'left');
        if ( isset($options['select']) )    { $this->db->select($options['select'], FALSE); }
        else
        {
            $this->db->select('
                tpl_tasks.*,
                stages_status.name AS stage_status,
                reasons.name AS reason
            ');
        }
        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->group_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @param $data
     * @param bool $skip_validation
     * @return bool
     */
	public function insert($data, $skip_validation = FALSE)
    {
        $result = parent::insert($data, $skip_validation);

        if ( $result )
        {
            $tasks = $this->find(array('where' => array('tpl_stage_id' => $data['tpl_stage_id'])));
            $perc = 100.0 / count($tasks);

            foreach ($tasks as $task)
            {
                $this->update($task->id, array('weight'=>$perc));
            }
        }

        return $result;
    }

    /**
     * @param array|int|string $pk
     * @return bool
     */
    public function delete($pk)
    {
        $this->load->model('tpl_task_position_model');
        $this->load->model('tpl_task_prerequisite_model');
        $this->load->model('tpl_task_tag_model');
        $this->load->model('tpl_task_requirement_model');

        $this->db->trans_start();
            $this->tpl_task_position_model->delete_where(array('tpl_task_id'=>$pk));
            $this->tpl_task_prerequisite_model->delete_where(array('tpl_task_id'=>$pk));
            $this->tpl_task_prerequisite_model->delete_where(array('tpl_task_prerequisite'=>$pk));
            $this->tpl_task_tag_model->delete_where(array('tpl_task_id'=>$pk));
            $this->tpl_task_requirement_model->delete_where(array('tpl_task_id'=>$pk));
            parent::delete($pk);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) { return FALSE; }

        return TRUE;
    }
}
