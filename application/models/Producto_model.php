<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Producto_model
 * @property User_role_model $user_role_model
 * @property Grant_model $grant_model
 */
class Producto_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'productos';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // name
			'field'     => 'codigo',
			'label'     => 'Código',
			'rules'     => 'trim|required|min_length[2]|max_length[30]|is_unique[productos.codigo]'
		),
		array( // Description
			'field'     => 'descripcion',
			'label'     => 'lang:description',
			'rules'     => 'trim|required|min_length[2]|max_length[200]'
		),
        array( //
            'field'     => 'cantidad',
            'label'     => 'Cantidad',
            'rules'     =>  'trim|required|is_natural'
        ),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

}
