<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User: cristian.rodriguez
 * Date: 9/7/2017
 * Time: 6:28 PM
 */

/**
 * Class Project_brand_model
 * @property Project_log_model $project_log_model
 * @property Brand_model $brand_model
 */
class Project_brand_model extends MY_Model
{
    /**
     * @var string
     */
    protected $_table = 'projects_brands';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // id
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // project_id
            'field'     => 'project_id',
            'label'     => 'lang:project',
            'rules'     =>  'trim|required|is_natural_no_zero|exist[projects.id]'
        ),
        array( // brand_id
            'field'     => 'brand_id',
            'label'     => 'lang:brand',
            'rules'     => 'trim|required|is_natural_no_zero|exist[brands.id]'
        ),
        array( // estimated_amount
            'field'     => 'estimated_amount',
            'label'     => 'lang:estimated_amount',
            'rules'     => 'trim|required'
        ),
        array( // estimated_date
            'field'     => 'estimated_date',
            'label'     => 'lang:estimated_date',
            'rules'     => 'trim'
        ),
        array( // actual_amount
            'field'     => 'actual_amount',
            'label'     => 'lang:actual_amount',
            'rules'     => 'trim'
        ),
        array( // actual_date
            'field'     => 'actual_date',
            'label'     => 'lang:actual_date',
            'rules'     => 'trim'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'lang:created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'lang:updated_at',
            'rules'     => 'trim'
        )
    );

    /**
     * @param array|null $options
     * @param bool $array_result
     * @return object|array|null
     */
    public function find($options = NULL, $array_result = FALSE)
    {
        if ( ! isset($options['select']) )
        {
            $this->db->select('
                projects_brands.*,
                brands.name as brands_name,
                brands.id as brand_id,
                projects.name as projects_name,
            ');
        }
        else { $this->db->select($options['select'], FALSE); }

        $this->db->from($this->_table)
            ->join('projects', 'projects_brands.project_id = projects.id')
            ->join('brands', 'projects_brands.brand_id = brands.id');
        if ( isset($options['select']) )    { $this->db->select($options['select'], FALSE); }
        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->group_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    public function today_datatable_json($date = NULL, $status, $company, $category, $brand, $project_id)
    {
        $this->load->library('datatables');
        $this->load->helper('date_helper');
        $date = ($date === NULL) ? date('Y-m-d') : date('Y-m-d', strtotime($date));
        $brand = (int)$brand;

        $this->datatables->select('
            projects_brands.*,
            brands.name as brand_name,
            projects.name as project_name,
            DATE_FORMAT(projects_brands.estimated_date, "%d/%b/%Y") AS estimated_date,
            DATE_FORMAT(projects_brands.actual_date, "%d/%b/%Y") AS actual_date,
            DATE_FORMAT(projects_brands.created_at, "%d/%b/%Y") AS created_at,
        ')->from('projects_brands')
            ->join('projects', 'projects.id = projects_brands.project_id')
            ->join('brands', 'brands.id = projects_brands.brand_id')
        ->group_by('projects_brands.id')
//            ->edit_column('estimated_date', '$1', 'nice_date(estimated_date, "d-M-Y")')
//            ->edit_column('actual_date', '$1', 'nice_date(actual_date, "d-M-Y")')
//            ->edit_column('created_at', '$1', 'nice_date(created_at, "d-M-Y")');
//        ->where('DATE(projects_brands.estimated_date) ="'.$date.'" OR DATE(projects_brands.actual_date) ="'.$date.'"');
        ->where('DATE(projects_brands.estimated_date) ="'.$date.'"');

        if ( $status !== 'all' ) { $status = strtolower($status); $this->datatables->where("projects.status = '{$status}'"); }
        if ( $company !== 'all' ) { $company = (int)$company; $this->datatables->where("projects.company_id = {$company}"); }
        if ( $category !== 'all' ) { $category = (int)$category; $this->datatables->where("projects.project_category_id = {$category}"); }
//        if ( $project_id !== 'all' ) { $project_id = (int)$project_id; $this->datatables->where("projects.id = {$project_id}"); }
        if ( $brand > 0 ) { $brand = (int)$brand; $this->datatables->where("projects_brands.brand_id ={$brand}"); }

        return $this->datatables->generate();
    }

    /**
     * @param array|object $data
     * @param bool $skip_validation
     *
     * @return bool|int|string
     */
    public function insert($data, $skip_validation = FALSE)
    {
        $estimated = explode('/', $data['estimated_date']);
        $data['estimated_date'] = "{$estimated[2]}-{$estimated[1]}-{$estimated[0]}";

        $estimated = explode('/', $data['actual_date']);
        $data['actual_date'] = "{$estimated[2]}-{$estimated[1]}-{$estimated[0]}";

        $result = parent::insert($data, $skip_validation);

        if ($result)
        {
            $this->load->model('project_log_model');
            $this->load->model('brand_model');
            $budget = $this->brand_model->get($this->get($result)->brand_id);
            $this->project_log_model->insert(array(
                'project_id'    =>  $data['project_id'],
                'user_id'       =>  $this->session->userdata('user_id'),
                'message'       =>  "El usuario {$this->session->userdata('full_name')} ha creado el presupuesto #{$result} marca {$budget->name}."
            ));
        }
        return $result;
    }

    /**
     * @param array|int|string $pk
     * @param array|object $data
     * @param bool $skip_validation
     *
     * @return bool
     */
    public function update($pk, $data, $skip_validation = FALSE)
    {
        $estimated = explode('/', $data['estimated_date']);
        if ( count($estimated) === 3 )
        {
            $data['estimated_date'] = "{$estimated[2]}-{$estimated[1]}-$estimated[0]";
        }

        $actual = explode('/', $data['actual_date']);
        if ( count($actual) === 3 )
        {
            $data['actual_date'] = "{$actual[2]}-{$actual[1]}-$actual[0]";
        }
        $before = $this->get($pk);
        $result = parent::update($pk, $data, $skip_validation);

        if ($result)
        {
            $after = $this->get($pk);
            if ( $after->brands_name !== $before->brands_name || $after->estimated_amount !== $before->estimated_amount
                || $after->estimated_date !== $before->estimated_date || $after->actual_amount !== $before->actual_amount
                || $after->actual_date !== $before->actual_date )
            {
                $this->load->model('project_log_model');
                $this->load->model('brand_model');

                $change = "El usuario {$this->session->userdata('full_name')} ha actualizado el presupuesto #{$pk} marca {$after->brands_name} con los siguientes cambios:";

                if( $after->brands_name !== $before->brands_name )
                {
                    $change .= "<br/>Marca: [{$before->brands_name} => {$after->brands_name}]";
                }
                if ( $after->estimated_amount !== $before->estimated_amount )
                {
                    $change .= "<br/>Monto estimado: [RD$".number_format($before->estimated_amount, 2)." => RD$".number_format($after->estimated_amount, 2)."]";
                }
                if ( $after->estimated_date !== $before->estimated_date )
                {
                    $change .= '<br/>Fecha estimado: ['.nice_date(date('Y-n-d', strtotime($before->estimated_date))).' => '.nice_date(date('Y-n-d', strtotime($after->estimated_date))).']';
                }
                if ( $after->actual_amount !== $before->actual_amount )
                {
                    $change .= '<br/>Monto real: [RD$'.number_format($before->actual_amount, 2).' => RD$'.number_format($after->estimated_amount, 2).']';
                }
                if ( $after->actual_date !== $before->actual_date )
                {
                    $change .= '<br/>Fecha real: ['.nice_date(date('Y-n-d', strtotime($before->actual_date))).' => '.nice_date(date('Y-n-d', strtotime($after->actual_date))).']';
                }

                $this->project_log_model->insert(array(
                    'project_id'    =>  $data['project_id'],
                    'user_id'       =>  $this->session->userdata('user_id'),
                    'message'       =>  $change
                ));
            }
        }
        return $result;
    }

    /**
     * @param array|int|string $pk
     *
     * @return bool
     */
    public function delete($pk)
    {
        $project_id = $this->get($pk)->project_id;
        $result = parent::delete($pk);

        if ( $result )
        {
            $this->load->model('project_log_model');
            $this->load->model('brand_model');
            $budget = $this->brand_model->get($this->get($pk)->brand_id);
            $this->project_log_model->insert(array(
                'project_id'    =>  $project_id,
                'user_id'       =>  $this->session->userdata('user_id'),
                'message'       =>  "El usuario {$this->session->userdata('full_name')} ha eliminado el presupuesto #{$pk} marca {$budget->name}."
            ));
        }

        return $result;
    }

    /**
     * @param int $id Project ID
     *
     * @return array
     */
    public function get_amounts($id)
    {
        $budget = $this->db->select(' 
                SUM(projects_brands.estimated_amount) as estimated_amount,
                SUM(projects_brands.actual_amount) as actual_amount,
                brands.name as brands_name,
                brands.id as brand_id,
                projects.name as projects_name,
        ')->from($this->_table)
            ->join('projects', 'projects_brands.project_id = projects.id')
            ->join('brands', 'projects_brands.brand_id = brands.id')
            ->group_by('brands.name')
            ->order_by('projects_brands.brand_id')
            ->where('projects_brands.project_id = '.$id)
            ->get()->result();

        return $budget;
    }

    /**
     * @param string|int $year
     * @param string|int $month
     * @param string $status
     * @param string|int $company
     * @param string|int $category
     * @param string|int $brand
     * @param boolean|int $project
     *
     * @return array
     */
    public function budget_data($year, $month, $status, $company, $category, $brand, $project, $delayed='false')
    {
        $this->db->select('
            estimated_amount,actual_amount,
            brands.name as brands_name,
            projects.name as project_name,
            projects_brands.estimated_date,
            projects_brands.actual_date,
        ')->from($this->_table)
            ->join('projects', 'projects.id = projects_brands.project_id')
            ->join('brands', 'brands.id = projects_brands.brand_id')
            ->order_by('projects_brands.id ASC');
//            ->where("YEAR({$this->_table}.estimated_date) = '{$year}' OR YEAR({$this->_table}.actual_date) = '{$year}'");
//            ->where("YEAR({$this->_table}.estimated_date) ={$year}");

        if ( $year !== 'all' ) { $year = (int)$year; $this->db->where("YEAR({$this->_table}.estimated_date) = '{$year}'"); }
        if ( $month !== 'all' ) { $month = (int)$month; $this->db->where("MONTH({$this->_table}.estimated_date) = '{$month}'"); }
//        if ( $month !== 'all' ) { $month = (int)$month; $this->db->where("MONTH({$this->_table}.estimated_date) = '{$month}' OR MONTH({$this->_table}.actual_date) = '{$month}'"); }
        if ( $status !== 'all' ) { $status = (string)$status; $this->db->where("projects.status ='{$status}'"); }
        if ( $company !== 'all' ) { $company = (int)$company; $this->db->where("projects.company_id ={$company}"); }
        if ( $category !== 'all' ) { $category = (int)$category; $this->db->where("projects.project_category_id ={$category}"); }
        if ( $brand !== 'all' ) { $brand = (int)$brand; $this->db->where("{$this->_table}.brand_id ={$brand}"); }
        if ( $project !== FALSE ) { $project = (int)$project; $this->db->where("projects.manager_id ={$project}"); }
        if ( $delayed === 'true' ) { $this->db->where('DATE(projects.estimated_end_date) < DATE(NOW()) AND projects.status = "progreso"'); }

        return $this->db->get()->result();
    }
}