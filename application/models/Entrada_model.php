<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Entrada_model
 * @property User_role_model $user_role_model
 * @property Grant_model $grant_model
 * @property Producto_model $producto_model
 */
class Entrada_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'entradas';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // producto_id
            'field'     => 'producto_id',
            'label'     => 'Producto',
            'rules'     =>  'trim|required|is_natural_no_zero|exist[productos.id]'
        ),
        array( //
            'field'     => 'cantidad',
            'label'     => 'Cantidad',
            'rules'     =>  'trim|required|is_natural'
        ),
        array( // empleado_id
            'field'     => 'empleado_id',
            'label'     => 'Empleado',
            'rules'     =>  'trim|required|is_natural_no_zero|exist[employees.user_id]'
        ),
        array( //
            'field'     => 'fecha',
            'label'     => 'Fecha',
            'rules'     =>  'trim'
        ),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param array|object $data
     * @param bool $skip_validation
     * @return bool|int|string
     */
    public function insert($data, $skip_validation = FALSE)
    {
        $this->load->model('producto_model');

        $producto = $this->producto_model->find(array('where'=>array('codigo'=>$data['codigo']), 'limit'=>1));

        if ( !$producto ) { return; }
        $data['producto_id'] = $producto->id;
        $data['fecha'] = date('Y-m-d H:i:s');
        $data['cantidad'] = 1;

        $entrada_id = parent::insert($data, $skip_validation);

        if ( $entrada_id )
        {
            $this->db->where('id', $data['producto_id']);
            $this->db->set('cantidad', 'cantidad+1', FALSE);
            $this->db->set('updated_at', 'CURRENT_TIMESTAMP', FALSE);
            $this->db->update('productos');
        }

        return $entrada_id;
    }

}
