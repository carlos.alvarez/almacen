<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends MY_Model {

	protected $_table = 'settings';

	protected $_pk = 'name';

	protected $_validation_rules = array(
        array( // name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|max_length[50]|is_unique[settings.name]'
		),
		array( // value
			'field'     => 'description',
			'label'     => 'lang:description',
			'rules'     => 'trim|required|max_length[250]'
		),
		array( // type
			'field'     => 'type',
			'label'     => 'lang:type',
			'rules'     => 'trim|in_list[int,float,string,date,bool]'
		),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

}
