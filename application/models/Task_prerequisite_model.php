<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Task_prerequisite_model
 *
 */
class Task_prerequisite_model extends MY_Model {

	protected $_table = 'tasks_prerequisites';
    protected $_pk = array('task_id', 'task_prerequisite');

	protected $_validation_rules = array(
        array( // task_id
            'field'     => 'task_id',
            'label'     => 'lang:task',
            'rules'     => 'trim|required|is_natural_no_zero|exist[tasks.id]'
        ),
        array( // task_prerequisite
            'field'     => 'task_prerequisite',
            'label'     => 'lang:prerequisite',
            'rules'     => 'trim|required|is_natural_no_zero|exist[tasks.id]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param null $options
     * @param bool|string $array_result
     * @return mixed
     */
    public function find($options = NULL, $array_result = FALSE)
    {

        if ( !isset($options['select']) )
        {
            $this->db->select('
                tasks_prerequisites.*,
                tasks.title AS prerequisite,
                stages.description AS stage
            ', FALSE);
        }
        else { $this->db->select($options['select'], FALSE); }

        $this->db->from($this->_table)
            ->join('tasks', 'tasks_prerequisites.task_prerequisite = tasks.id')
            ->join('stages', 'tasks.stage_id = stages.id');

        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->order_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @param $task_id
     * @param $prerequisite_id
     * @return bool
     */
    public function check_loop($task_id, $prerequisite_id)
    {
        $query = $this->find(array('where' => array('task_id' => $prerequisite_id)));

        foreach ($query as $element)
        {
            if ( $element->task_prerequisite == $task_id ) { return TRUE; }
            elseif ( $this->check_loop($task_id, $element->task_prerequisite) ) { return TRUE; }
        }

        return FALSE;
    }

}
