<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Startup_model
 *
 * @property CI_Loader load
 * @property CI_DB_query_builder db
 */
class Startup_model extends CI_Model {
    /**
     * Startup_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->db->query('SET time_zone = "-04:00";');  // Dominican Republic Time Zone
        $this->db->query('SET lc_time_names = "es_US";');
    }
}
