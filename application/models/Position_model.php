<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Position_model
 *
 * @property Employee_model $employee_model
 */
class Position_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'positions';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // company_id
            'field'     => 'company_id',
            'label'     => 'lang:company',
            'rules'     => 'trim|required|is_natural_no_zero|exist[companies.id]'
        ),
        array( // name
            'field'     => 'name',
            'label'     => 'lang:name',
            'rules'     => 'trim|required|max_length[100]'
        ),
        array( // description
            'field'     => 'description',
            'label'     => 'lang:description',
            'rules'     => 'trim'
        ),
        array( // manage_clients
            'field'     => 'manage_clients',
            'label'     => 'lang:manage_clients',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @return string
     */
    public function datatable_json()
    {
        $this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';

        $this->datatables->select('
            positions.id,
            positions.name,
            positions.description,
            companies.name AS company,
            companies.id   AS company_id
        ')
            ->from($this->_table)
            ->join('companies', 'companies.id = positions.company_id')
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

        return $this->datatables->generate();
    }

    public function delete($pk)
    {
        $this->load->model('employee_model');

        if ( $this->employee_model->exist_where(array('position_id' => $pk)) )
        {
            throw new Exception('No se puede eliminar el cargo porque tiene empleado(s) asociado(s).');
        }
        else
        {
            return parent::delete($pk);
        }
    }
}
