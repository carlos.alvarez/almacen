<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Company_model
 *
 * @property Datatables $datatables
 * @property Project_model $project_model
 * @property Department_model $department_model
 * @property Position_model $position_model
 */
class Company_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'companies';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // calendar_id
            'field'     => 'calendar_id',
            'label'     => 'lang:calendar',
            'rules'     => 'trim|required|is_natural_no_zero|exist[calendars.id]'
        ),
        array( // Name
            'field'     => 'name',
            'label'     => 'lang:name',
            'rules'     => 'trim|required|min_length[4]|max_length[100]'
        ),
        array( // Short Name
            'field'     => 'short_name',
            'label'     => 'lang:short_name',
            'rules'     => 'trim|max_length[30]'
        ),
        array( // tax_number
            'field'     => 'tax_number',
            'label'     => 'lang:tax_number',
            'rules'     => 'trim|max_length[30]'
        ),
        array( // Active
            'field'     => 'active',
            'label'     => 'lang:active',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // Address
            'field'     => 'address',
            'label'     => 'lang:address',
            'rules'     => 'trim|max_length[250]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
    );

    /**
     * @return string
     */
	public function datatable_json()
	{
		$this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';

		$this->datatables->select('
		    id,
		    name,
		    short_name,
		    active,
		    tax_number
		')->from($this->_table)
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

		return $this->datatables->generate();
	}

    /**
     * @param array|int|string $pk
     *
     * @return bool
     * @throws Exception
     */
    public function delete($pk)
    {
        $this->load->model('project_model');
        $this->load->model('department_model');
        $this->load->model('position_model');

        if ( $this->project_model->exist_where(array('company_id' => $pk)) ||
            $this->department_model->exist_where(array('company_id')) ||
            $this->position_model->exist_where(array('company_id')) )
        {
            throw new Exception('No se pudo eliminar la empresa porque que está asociado a proyecto(s), departamento(s) y/o área(s).');
        }
        else
        {
            return parent::delete($pk);;
        }
    }
}
