<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Stage_model
 * @property Task_model $task_model
 */
class Stage_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'stages';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // project_id
            'field'     => 'project_id',
            'label'     => 'lang:project',
            'rules'     => 'trim|required|is_natural_no_zero|exist[projects.id]'
        ),
        array( // template_id
            'field'     => 'template_id',
            'label'     => 'Plantilla',
            'rules'     => 'trim|required|is_natural_no_zero|exist[templates.id]'
        ),
        array( // title
            'field'     => 'title',
            'label'     => 'lang:title',
            'rules'     => 'trim|required|max_length[150]'
        ),
        array( // description
            'field'     => 'description',
            'label'     => 'lang:description',
            'rules'     => 'trim'
        ),
        array( // weight
            'field'     => 'weight',
            'label'     => 'lang:weight',
            'rules'     => 'trim|required|numeric|greater_than[0]|less_than_equal_to[100]'
        ),
        array( // client_visibility
            'field'     => 'client_visibility',
            'label'     => 'lang:client_visibility',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // client_notification
            'field'     => 'client_notification',
            'label'     => 'lang:client_notification',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // order
            'field'     => 'order',
            'label'     => 'lang:order',
            'rules'     => 'trim|is_natural'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
    );

    /**
     * @param array|int|string $pk
     *
     * @return bool
     */
    public function delete($pk)
    {
        $this->load->model('task_model');

        $this->db->trans_start();
            $this->task_model->delete_where(array('stage_id' => $pk));
            parent::delete($pk);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) { return FALSE; }

        return TRUE;
    }

    /**
     * @param $id_project
     * @param $stage
     * @param string $month
     * @return array
     */
    public function get_stage_task_date($id_project, $stage, $month='')
    {
        $this->db->select('stages.*,tasks.start_date as start_date')
            ->from($this->_table)
            ->join('tasks', $this->_table.'.id = tasks.stage_id')
            ->group_by('tasks.id');
        if ( $month !== '' )
        {
            $this->db->where('stages.project_id =' .$id_project. ' AND stages.id =' .$stage. ' AND MONTH(tasks.start_date) =' .$month);
        }
        else
        {
            $this->db->where('stages.project_id =' .$id_project. ' AND stages.id =' .$stage);
        }

        return $this->db->get()->result();
    }
}
