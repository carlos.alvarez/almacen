<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Reason_model
 */
class Reason_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'reasons';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // stage_status_id
            'field'     => 'stage_status_id',
            'label'     => 'Estado Interno',
            'rules'     => 'trim|required|is_natural_no_zero|exist[stages_status.id]'
        ),
        array( // name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|min_length[2]|max_length[100]'
		),
        array( // reference
            'field'     => 'reference',
            'label'     => 'Referencia',
            'rules'     => 'trim|required|min_length[2]|max_length[100]'
        ),
		array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

}
