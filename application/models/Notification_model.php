<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Role_model
 * @property User_role_model $user_role_model
 * @property Grant_model $grant_model
 */
class Notification_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'notifications';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // user_id
            'field'     => 'user_id',
            'label'     => 'lang:user',
            'rules'     => 'trim|required|is_natural_no_zero|exist[users.id]'
        ),
        array( // message
            'field'     => 'message',
            'label'     => 'Message',
            'rules'     => 'trim|required'
        ),
        array( // read
            'field'     => 'read',
            'label'     => 'Read',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // tag
			'field'     => 'tag',
			'label'     => 'Tag',
			'rules'     => 'trim|max_length[30]'
		),
        array( // actions
            'field'     => 'actions',
            'label'     => 'Actions',
            'rules'     => 'trim'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param $user_id
     * @return array|null|object
     */
    public function get_for_web($user_id)
    {
        $notifications = $this->find(array(
            'where'     => array(
                'user_id'   => $user_id
            ),
            'limit'     => 10,
            'order_by'  => 'read DESC, created_at DESC'
        ));

        foreach ($notifications as &$notification) {
            $notification->age = $this->get_time_diff($notification->created_at);
        }

        return array(
            'unread_count'      => $this->get_unreads_count($user_id),
            'notifications'     => $notifications
        );
    }

    /**
     * @param $user_id
     * @return array|null|object
     */
    public function get_for_push($user_id)
    {
        $notifications = $this->find(array(
            'where'     => array(
                'user_id'   => $user_id,
                'read'      => 'false'
            ),
            'limit'     => 3,
            'order_by'  => 'created_at DESC'
        ));


        $push_notifications = array();
        foreach ($notifications as $notification)
        {
            //$notification->age = $this->get_time_diff($notification->created_at);
            $pn = array(
                'title'     => 'ErgoSpace',
                'payload'   => array(
                    'body'      => $notification->message,
                    'tag'       => "PN_{$notification->id}",
                    'icon'      => '/public/img/logo.png'
                )
            );

            if ( $notification->actions && ($actions = json_decode($notification->actions, TRUE)) )
            {
                $pn['payload']['actions'] = $actions;
            }

            $push_notifications[] = $pn;
        }

        return $push_notifications;
    }

    /**
     * @param $created_at
     * @return string
     */
    public function get_time_diff($created_at)
    {
        $datetime1 = date_create(date('Y-m-d H:i:s'));
        $datetime2 = date_create($created_at);
        $interval = date_diff($datetime1, $datetime2);

        if ( $interval->m > 0 ) { return "Hace $interval->m ".($interval->m > 1 ? 'meses' : 'mes'); }
        if ( $interval->d > 0 ) { return "Hace $interval->d ".($interval->d > 1 ? 'días' : 'día'); }
        if ( $interval->h > 0 ) { return "Hace $interval->h ".($interval->h > 1 ? 'horas' : 'hora'); }
        if ( $interval->i > 0 ) { return "Hace $interval->i ".($interval->i > 1 ? 'minutos' : 'minuto'); }

        return 'Ahora';
    }

    /**
     * @param $user_id
     * @return int
     */
    public function get_unreads_count($user_id)
    {
        return $this->db->where('user_id', $user_id)->where('read', 'false')->from($this->_table)->count_all_results();
    }

}
