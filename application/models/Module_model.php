<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Module_model
 * @property Grant_model $grant_model
 */
class Module_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'modules';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // name
			'field'     => 'name',
			'label'     => 'lang:name',
			'rules'     => 'trim|required|min_length[4]|max_length[100]'
		),
        array( // description
            'field'     => 'description',
            'label'     => 'lang:description',
            'rules'     => 'trim|max_length[250]'
        ),
		array( // Controller
			'field'     => 'controller',
			'label'     => 'lang:controller',
			'rules'     => 'trim|min_length[2]|max_length[30]|is_unique[modules.controller]'
		),
		array( // Active
			'field'     => 'active',
			'label'     => 'lang:active',
			'rules'     => 'trim|in_list[true,false]'
		),
        array( // action_create
            'field'     => 'action_create',
            'label'     => 'lang:method_create',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // action_view
            'field'     => 'action_view',
            'label'     => 'lang:method_view',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // action_edit
            'field'     => 'action_edit',
            'label'     => 'lang:method_edit',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // action_delete
            'field'     => 'action_delete',
            'label'     => 'lang:method_delete',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @return string
     */
	public function datatable_json()
	{
		$this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';

		$this->datatables->select('
		    id,
		    name,
		    controller,
		    active,
		    action_create,
		    action_view,
		    action_edit,
		    action_delete
		')->from('modules')
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

		return $this->datatables->generate();
	}

    /**
     * @param array|int|string $pk
     *
     * @return bool
     */public function delete($pk)
    {
        $this->load->model('grant_model');
        $this->db->trans_start();
            $this->grant_model->delete_where(array('module_id' => $pk));
            parent::delete($pk);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) { return FALSE; }

        return TRUE;
    }
}
