<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Client_category_model
 *
 * @property Datatables $datatables
 * @property Client_model $client_model
 */
class Client_category_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'client_categories';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // name
            'field'     => 'name',
            'label'     => 'lang:name',
            'rules'     => 'trim|required|max_length[100]'
        ),
        array( // description
            'field'     => 'description',
            'label'     => 'lang:description',
            'rules'     => 'trim'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @return mixed
     */
    public function datatable_json()
    {
        $this->load->library('datatables');
        $grant_edit = grant_access('brand', 'edit') ? 'true' : 'false';
        $grant_delete = grant_access('brand', 'delete') ? 'true' : 'false';

        $this->datatables->select('
			id,
			name,
			description
		')->from($this->_table)
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

        return $this->datatables->generate();
    }

    /**
     * @param array|int|string $pk
     *
     * @return bool
     * @throws Exception
     */
    public function delete($pk)
    {
        $this->load->model('client_model');
        if ( $this->client_model->exist_where(array('client_category_id' => $pk)) )
        {
            throw new Exception('No se puede eliminar la categoría porque tiene cliente(s) asociado(s).');
        }
        else
        {
            return parent::delete($pk);
        }
    }
}
