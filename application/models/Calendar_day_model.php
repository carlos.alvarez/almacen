<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar_day_model extends MY_Model {

	protected $_table = 'calendar_days';
	protected $_pk = 'date';

	protected $_validation_rules = array(
        array( // date
            'field'     => 'date',
            'label'     => 'lang:date',
            'rules'     => 'trim'
        ),
        array( // week_day
            'field'     => 'week_day',
            'label'     => 'lang:week_day',
            'rules'     => 'trim|required|is_natural|less_than_equal_to[6]'
        )
	);
}
