<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Client_model
 *
 * @property Client_contact_model $client_contact_model
 * @property Datatables $datatables
 * @property Project_model $project_model
 */
class Client_model extends MY_Model {

    /**
     * @var string
     */
    protected $_table = 'clients';

    /**
     * @var array
     */
    protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // Reference Code
            'field'     => 'ref_code',
            'label'     => 'lang:ref_code',
            'rules'     => 'trim|max_length[12]'
        ),
        array( // Name
            'field'     => 'name',
            'label'     => 'lang:name',
            'rules'     => 'trim|required|min_length[4]|max_length[100]'
        ),
        array( // Short Name
            'field'     => 'short_name',
            'label'     => 'lang:short_name',
            'rules'     => 'trim|max_length[30]'
        ),
        array( // tax_number
            'field'     => 'tax_number',
            'label'     => 'lang:tax_number',
            'rules'     => 'trim|max_length[30]'
        ),
        array( // client_category_id
            'field'     => 'client_category_id',
            'label'     => 'lang:category',
            'rules'     => 'trim|is_natural_no_zero|exist[client_categories.id]'
        ),
        array( // account_manager
            'field'     => 'account_manager',
            'label'     => 'lang:account_manager',
            'rules'     => 'trim|is_natural_no_zero|exist[employees.user_id]'
        ),
        array( // Active
            'field'     => 'active',
            'label'     => 'lang:active',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // Address
            'field'     => 'address',
            'label'     => 'lang:address',
            'rules'     => 'trim|max_length[250]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
    );

    /**
     * @param null $options
     * @param bool $array_result
     * @return mixed
     */
    public function find($options = NULL, $array_result = FALSE)
    {
        if ( isset($options['select']) )
        {
            $this->db->select($options['select'], FALSE);
        }
        else
        {
            $this->db->select('
                clients.*,
                CONCAT(users.first_name," ",users.last_name) as account_manager_name
            ');
        }

        $this->db->from($this->_table)
            ->join('users', 'clients.account_manager = users.id', 'left');

        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->order_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @param $data
     * @param bool $skip_validation
     * @return bool
     */
    public function insert($data, $skip_validation = FALSE)
    {
        $this->db->trans_start();
        try {
            $id =  parent::insert($data, $skip_validation);
            if ( $id && isset($data['contacts']) )
            {
                $this->add_contacts($id, $data['contacts']);
            }
        } catch (Exception $e) {
            return FALSE;
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return FALSE;
        }

        return $id;
    }

    /**
     * @param $pk
     * @param $data
     * @param bool $skip_validation
     * @return bool
     */
    public function update($pk, $data, $skip_validation = FALSE)
    {
        $this->db->trans_start();
        try {
            $result =  parent::update($pk, $data, $skip_validation);
            if ( $result && isset($data['contacts']) )
            {
                $this->update_contacts($pk, $data['contacts']);
            }
        } catch (Exception $e) {
            return FALSE;
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return FALSE;
        }

        return $result;
    }

    /**
     * @param $client_id
     * @param array $options
     * @return mixed
     */
    public function get_contacts($client_id, $options = array())
    {
        $this->load->model('client_contact_model');

        $options['where']['client_id'] = $client_id;
        return $this->client_contact_model->find($options);
    }

    /**
     * @param $id
     * @param $contacts
     * @throws Exception
     */
    public function add_contacts($id, $contacts)
    {
        $this->load->model('client_contact_model');

        foreach ($contacts as $contact)
        {
            if ( isset($contact['id']) ) { unset($contact['id']); }
            $contact['client_id'] = $id;

            $result = $this->client_contact_model->insert($contact);

            if ( !$result )
            {
                throw new Exception(validation_errors());
            }
        }
    }

    /**
     * @param $id
     * @param $contacts
     * @throws Exception
     */
    public function update_contacts($id, $contacts)
    {
        $this->load->model('client_contact_model');

        $existing = $this->client_contact_model->find(array('where'=>'client_id = '.$id));
        $list = array();
        $newlist = array();
        $updatelist = array();
        //$deletelist = array();

        foreach ($existing as $c)
        {
            $list[$c->id] = (array) $c;
        }

        foreach ($contacts as $contact)
        {
            $contact['client_id'] = $id;
            if ( $contact['id'] === 'new' )
            {
                unset($contact['id']);
                $newlist[] = $contact;
            }
            else
            {
                $updatelist[$contact['id']] = $contact;
            }
        }
        $deletelist = array_diff_key($list, $updatelist);

        foreach ($newlist as $new)
        {
            $this->client_contact_model->insert($new);
        }

        foreach ($updatelist as $key => $update)
        {
            $this->client_contact_model->update($key, $update);
        }

        foreach ($deletelist as $key => $delete)
        {
            $this->client_contact_model->delete($key);
        }
    }

    /**
     * @return string
     */
    public function datatable_json()
    {
        $this->load->library('datatables');

        $this->datatables->select('
		    clients.id,
		    clients.ref_code,
		    clients.name,
		    clients.short_name,
		    clients.active,
		    clients.tax_number,
		    CONCAT(users.first_name," ",users.last_name) as account_manager_name
		')->from($this->_table)
            ->join('users', 'clients.account_manager = users.id', 'left')
        ->add_column('edit', (grant_access('client', 'edit')) ? 'true' : 'false')
        ->add_column('delete', (grant_access('client', 'delete')) ? 'true' : 'false');

        return $this->datatables->generate();
    }

    /**
     * @param array|int|string $pk
     *
     * @return bool
     * @throws Exception
     */
    public function delete($pk)
    {
        $this->load->model('project_model');

        if ( $this->project_model->exist_where(array('client_id' => $pk)) )
        {
            throw new Exception('No se pudo eliminar el cliente porque está asociado a proyecto(s).');
        }
        else
        {
            $this->load->model('client_contact_model');

            $this->db->trans_start();
                $this->client_contact_model->delete_where(array('client_id' => $pk));
                parent::delete($pk);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) { return FALSE; }

            return TRUE;
        }
    }
}
