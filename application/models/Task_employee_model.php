<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_employee_model extends MY_Model {

	protected $_table = 'tasks_employees';
    protected $_pk = array('task_id', 'user_id');

	protected $_validation_rules = array(
        array( // task_id
            'field'     => 'task_id',
            'label'     => 'lang:task',
            'rules'     => 'trim|required|is_natural_no_zero|exist[tasks.id]'
        ),
        array( // employee
            'field'     => 'user_id',
            'label'     => 'lang:employee',
            'rules'     => 'trim|required|is_natural_no_zero|exist[employees.user_id]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        )
	);

    /**
     * @param null $options
     * @param bool|string $array_result
     * @return mixed
     */
    public function find($options = NULL, $array_result = FALSE)
    {
        if ( !isset($options['select']) )
        {
            $this->db->select('
                tasks_employees.*,
                CONCAT(users.first_name," ",users.last_name) AS employee
            ', FALSE);
        }
        else { $this->db->select($options['select'], FALSE); }

        $this->db->from($this->_table)
            ->join('users', 'tasks_employees.user_id = users.id');

        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->order_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

}
