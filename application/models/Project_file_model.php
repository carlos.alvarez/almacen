<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * class Project_file_model
 * @property Project_model $project_model
 */
class Project_file_model extends MY_Model {

	protected $_table = 'projects_files';

	protected $_validation_rules = array(
        array( // ID
            'field'     => 'id',
            'label'     => 'ID',
            'rules'     => 'is_natural_no_zero'
        ),
        array( // project_id
            'field'     => 'project_id',
            'label'     => 'lang:project',
            'rules'     => 'trim|required|is_natural_no_zero|exist[projects.id]'
        ),
        array( // user_id
            'field'     => 'user_id',
            'label'     => 'lang:user',
            'rules'     => 'trim|required|is_natural_no_zero|exist[users.id]'
        ),
        array( // name
            'field'     => 'name',
            'label'     => 'lang:name',
            'rules'     => 'trim|required|min_length[2]|max_length[100]'
        ),
        array( // path
            'field'     => 'path',
            'label'     => 'lang:path',
            'rules'     => 'trim|required|min_length[8]|max_length[250]'
        ),
        array( // Description
            'field'     => 'description',
            'label'     => 'lang:description',
            'rules'     => 'trim'
        ),
        array( // client_visibility
            'field'     => 'client_visibility',
            'label'     => 'lang:client_visibility',
            'rules'     => 'trim|in_list[true,false]'
        ),
        array( // created_at
            'field'     => 'created_at',
            'label'     => 'created_at',
            'rules'     => 'trim'
        ),
        array( // updated_at
            'field'     => 'updated_at',
            'label'     => 'updated_at',
            'rules'     => 'trim'
        )
	);

	public function find($options = NULL, $array_result = FALSE)
    {
        if ( isset($options['select']) )    { $this->db->select($options['select'], FALSE); }
        else
        {
            $this->db->select('
                projects_files.*,
                CONCAT(users.first_name," ",users.last_name) as user
            ', FALSE);
        }

        $this->db->from($this->_table)
            ->join('users', 'projects_files.user_id = users.id');

        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
        if ( isset($options['group_by']) )  { $this->db->group_by($options['group_by']); }
        if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
        if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @param $id
     * @return string
     */
    public function project_datatable_json($id)
    {
        $this->load->library('datatables');
        $this->load->helper('files_helper');
        $this->load->model('project_model');

        $project = $this->project_model->get($id);
        $grant_edit = grant_access('project','edit') ? 'true' : 'false';
        $grant_delete = grant_access('project','delete') ? 'true' : 'false';
        $grant_visibility = grant_access('project','edit') ? 'true' : ($project->manager_id === $this->session->userdata('user_id')) ? 'true' : 'false';

        $this->datatables->select('
			projects_files.id,
			projects_files.project_id,
			projects_files.user_id,
			projects_files.name,
			projects_files.path,
			projects_files.description,
			projects_files.client_visibility,
			CONCAT(users.first_name," ",users.last_name) AS user,
			projects_files.created_at,
			DATE_FORMAT(projects_files.created_at, "%d/%b/%y %h:%i %p") AS created_at_nice,
			projects_files.updated_at,
			DATE_FORMAT(projects_files.updated_at, "%d/%b/%y %h:%i %p") AS updated_at_nice
		')->from('projects_files')
            ->join('users', 'projects_files.user_id= users.id')
            ->where(array('projects_files.project_id' => $id))
            ->add_column('icon', '$1', 'file_icon(name)')
            ->add_column('visibility', '$1', $grant_visibility)
            ->add_column('edit', '$1', $grant_edit)
            ->add_column('delete', '$1', $grant_delete);

        return $this->datatables->generate();
    }
    /**
     * @param $id
     * @return string
     */
    public function project_client_datatable_json($id)
    {
        $this->load->library('datatables');
        $this->load->helper('files_helper');

        $this->datatables->select('
			projects_files.id,
			projects_files.project_id,
			projects_files.user_id,
			projects_files.name,
			projects_files.path,
			projects_files.description,
			projects_files.client_visibility,
			CONCAT(users.first_name," ",users.last_name) AS user,
			projects_files.created_at,
			DATE_FORMAT(projects_files.created_at, "%d/%b/%y %h:%i %p") AS created_at_nice,
			projects_files.updated_at,
			DATE_FORMAT(projects_files.updated_at, "%d/%b/%y %h:%i %p") AS updated_at_nice
		')->from('projects_files')
            ->join('users', 'projects_files.user_id= users.id')
            ->where(array('projects_files.project_id' => $id, 'projects_files.client_visibility' => 'true'))
            ->add_column('icon', '$1', 'file_icon(name)');

        return $this->datatables->generate();
    }

}
