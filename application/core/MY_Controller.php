<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * MY_Controller class
 *
 * @author Carlos Alvarez
 */

/**
 * @property CI_DB_query_builder $db              This is the platform-independent base Active Record implementation class.
 * @property CI_DB_forge $dbforge                 Database Utility Class
 * @property CI_Benchmark $benchmark              This class enables you to mark points and calculate the time difference between them.<br />  Memory consumption can also be displayed.
 * @property CI_Calendar $calendar                This class enables the creation of calendars
 * @property CI_Cart $cart                        Shopping Cart Class
 * @property CI_Config $config                    This class contains functions that enable config files to be managed
 * @property CI_Controller $controller            This class object is the super class that every library in.<br />CodeIgniter will be assigned to.
 * @property CI_Email $email                      Permits email to be sent using Mail, Sendmail, or SMTP.
 * @property CI_Encrypt $encrypt                  Provides two-way keyed encoding using XOR Hashing and Mcrypt
 * @property CI_Exceptions $exceptions            Exceptions Class
 * @property CI_Form_validation $form_validation  Form Validation Class
 * @property CI_Ftp $ftp                          FTP Class
 * @property CI_Hooks $hooks                      Provides a mechanism to extend the base system without hacking.
 * @property CI_Image_lib $image_lib              Image Manipulation class
 * @property CI_Input $input                      Pre-processes global input data for security
 * @property CI_Lang $lang                        Language Class
 * @property CI_Loader $load                      Loads views and files
 * @property CI_Log $log                          Logging Class
 * @property CI_Model $model                      CodeIgniter Model Class
 * @property CI_Output $output                    Responsible for sending final output to browser
 * @property CI_Pagination $pagination            Pagination Class
 * @property CI_Parser $parser                    Parses pseudo-variables contained in the specified template view,<br />replacing them with the data in the second param
 * @property CI_Profiler $profiler                This class enables you to display benchmark, query, and other data<br />in order to help with debugging and optimization.
 * @property CI_Router $router                    Parses URIs and determines routing
 * @property CI_Session $session                  Session Class
 * @property CI_Table $table                      HTML table generation<br />Lets you create tables manually or from database result objects, or arrays.
 * @property CI_Trackback $trackback              Trackback Sending/Receiving Class
 * @property CI_Typography $typography            Typography Class
 * @property CI_Unit_test $unit_test              Simple testing class
 * @property CI_Upload $upload                    File Uploading Class
 * @property CI_URI $uri                          Parses URIs and determines routing
 * @property CI_User_agent $user_agent            Identifies the platform, browser, robot, or mobile devise of the browsing agent
 * @property CI_Xmlrpc $xmlrpc                    XML-RPC request handler class
 * @property CI_Xmlrpcs $xmlrpcs                  XML-RPC server class
 * @property CI_Zip $zip                          Zip Compression Class
 * @property CI_Utf8 $utf8                        Provides support for UTF-8 environments
 * @property CI_Security $security                Security Class, xss, csrf, etc...
 */
class MY_Controller extends CI_Controller {

    /**
     * @var null|string
     */
	protected $_controller = NULL;

    /**
     * @var null|string
     */
	protected $_model = NULL;

    /**
     * @var array
     */
	protected $data = array();

    /**
     * @var array
     */
    protected $_assets_list = array(
        'styles'	=> array(
            'public/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css',
            //'public/plugins/Responsive-master/css/responsive.bootstrap.css'
        ),
        'scripts' 	=> array(
            'public/plugins/jquery-datatable/jquery.dataTables.js',
            'public/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js',
            //'public/plugins/Responsive-master/js/dataTables.responsive.js',
            //'public/plugins/Responsive-master/js/responsive.bootstrap.js',
            'public/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js',
            'public/plugins/jquery-datatable/extensions/export/buttons.flash.min.js',
            'public/plugins/jquery-datatable/extensions/export/jszip.min.js',
            'public/plugins/jquery-datatable/extensions/export/pdfmake.min.js',
            'public/plugins/jquery-datatable/extensions/export/vfs_fonts.js',
            'public/plugins/jquery-datatable/extensions/export/buttons.html5.min.js',
            'public/plugins/jquery-datatable/extensions/export/buttons.print.min.js',
            'public/plugins/jquery-datatable/extensions/export/ellipsis.js'/*,
            'public/plugins/jquery-datatable/extensions/export/select.min.js'*/
        )
    );

    /**
     * @var array
     */
	protected $_assets_create = array(
        'styles'	=> array(
            'public/plugins/bootstrap-select/css/bootstrap-select.min.css',
            'public/plugins/multi-select/css/multi-select.css'
        ),
        'scripts'	=> array(
            'public/plugins/multi-select/js/jquery.multi-select.js',
            'public/plugins/jquery-validation/jquery.validate.js',
            'public/plugins/jquery-validation/localization/messages_es.js'
        )
    );

    /**
     * @var array
     */
    protected $_assets_edit = array(
        'styles'	=> array(
            'public/plugins/bootstrap-select/css/bootstrap-select.min.css',
            'public/plugins/multi-select/css/multi-select.css'
        ),
        'scripts'	=> array(
            'public/plugins/multi-select/js/jquery.multi-select.js',
            'public/plugins/jquery-validation/jquery.validate.js',
            'public/plugins/jquery-validation/localization/messages_es.js'
        )
    );

    /**
     * @var array
     */
    protected $_assets_view = array(
        'styles'	=> array(),
        'scripts'	=> array()
    );

    /**
     * MY_Controller constructor.
     */
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Santo_Domingo');

        /*if ( ENVIRONMENT === 'development' && $this->input->method() === 'get' && !$this->input->is_ajax_request() )
        {
            $this->output->enable_profiler(TRUE);
        }*/

        // Check authentication
		$no_redirect = array(
			'user/login',
            'user/acceso',
            'user/get_captcha',
            'migration',
            'push/subscribe',
            'push/unsubscribe',
            'push/notifications',
            'api/entrada',
            'api/salida'
		);

        if ( $this->session->userdata('acceso') == 'bloqueado' && $this->uri->uri_string() !== 'user/acceso' && $this->uri->uri_string() !== 'user/logout' )
        {
            redirect('user/acceso');
            exit();
        }

		if ( $this->session->userdata('logged_in') !== TRUE && !in_array(uri_string(), $no_redirect) )
		{
			redirect('user/login');
		}

		if ( ! grant_access($this->router->class, $this->router->method) )
		{
		    redirect();
		}

        if ( $this->_model !== FALSE )
        {
            $this->_guess_model();
            $this->load->model($this->_model);
        }

        if ( $this->_controller === NULL )
        {
            $this->_controller = strtolower(get_class($this));
        }
    }

    /**
     *
     */
	public function index()
	{
		$this->_template("{$this->_controller}/list", $this->_get_assets('list', $this->data));
	}

    /**
     *
     */
    public function create()
	{
		if ( $this->input->method() === 'post' )
		{
            if ( $id = $this->{$this->_model}->insert($this->input->post()) )
			{
			    $this->_on_insert_success($id);
                $this->_response_success();
                if ( !$this->input->is_ajax_request() )
                {
                    redirect($this->_controller);
                }
			}
            else
            {
                $this->_response_error(validation_errors());
            }
		}

        $this->_template("{$this->_controller}/create", $this->_get_assets('create', $this->data));
	}

    /**
     * @param int|string $id
     */
    public function edit($id)
	{
		$row = $this->_exist($id);

		if ( $this->input->method() === 'post' )
		{
			if ( $this->{$this->_model}->update($id, $this->input->post()) )
			{
                $this->_on_edit_success($id);
                $this->_response_success();

                if ( !$this->input->is_ajax_request() )
                {
                    redirect("{$this->_controller}/view/{$id}");
                }
			}
            else
            {
                $this->_response_error(validation_errors());
            }
		}

		$this->data[$this->_controller] = $row;
		$this->_template("{$this->_controller}/edit", $this->_get_assets('edit', $this->data));
	}

    /**
     * @param $id
     */
	public function delete($id)
    {
        if ( !$this->input->method() === 'post' ) { $this->_response_error('Invalid Method'); exit(); }
        $this->_exist($id);

        $res = FALSE;

        try {
            $res = $this->{$this->_model}->delete($id);
        } catch (Exception $error) {
            $this->_response_error($error->getMessage());
        }

        if ( $res )
        {
            $this->_response_success();
        }
        else
        {
            $this->_response_error('No se pudo eliminar este elemento.');
        }
    }

    /**
     * @param int|string $id
     */
    public function view($id)
	{
		$row = $this->_exist($id);

        if ( $this->input->is_ajax_request() )
        {
            $this->_return_json_success(lang('success_message'), array($this->_controller => $row));
        }

		$this->data[$this->_controller] = $row;
        $this->_template("{$this->_controller}/view", $this->_get_assets('view', $this->data));
	}

    /**
     * @param null|string $content
     * @param array $data
     * @param string $template
     * @param bool $return
     * @return null|string
     */
    protected function _template($content = NULL, $data = array(), $template = 'layout/main', $return = FALSE)
    {
        $data['content'] = $content;
		return $this->load->view($template, $data, $return);
    }

    /**
     * @param string $message
     */
    protected function _return_json_error($message)
    {
        $response = array(
            'error'     => TRUE,
            'message'   => $message
        );

        echo json_encode($response);
        exit();
    }

    /**
     * @param null|string $message
     * @param null|mixed $data
     */
    protected function _return_json_success($message = NULL, $data = NULL)
    {

        $response = array(
            'error'     => FALSE,
            'message'   => ($message !== NULL) ? $message : lang('success_message')
        );

        if ( $data !== NULL )
        {
            $response['data'] = $data;
        }

        echo json_encode($response);
        exit();
    }

    /**
     * @param int|string $id
     * @return null|array|object
     */
    protected function _exist($id)
    {
		if ( ! ($row = $this->{$this->_model}->get($id)) )
		{
            if ( $this->input->is_ajax_request() )
            {
                $this->_return_json_error(lang('not_found'));
            }
            else
            {
                $this->_alert(lang('not_found'), 'warning');
                redirect($this->_controller);
            }
		}

        return $row;
    }

    /**
     * @param string $message
     * @param string $type
     */
    protected function _alert($message, $type = 'info')
    {
        $_SESSION['alerts'][] = array(
            'type'      => $type,
            'message'   => $message
        );
    }

    /**
     * @param int|string $id
     */
    protected function _on_insert_success($id) {}

    /**
     * @param int|string $id
     */
    protected function _on_edit_success($id) {}

    /**
     *
     */
    protected function _get_javascript()
    {
        $js = "public/js/src/{$this->_controller}.js";
        return file_exists("./{$js}") ? $js : NULL;
    }

    /**
     * @param null|string $assets
     * @param null|array $arr
     * @return array|null
     */
    protected function _get_assets($assets = NULL, $arr = NULL)
    {
        if ( $assets !== NULL && isset($this->{'_assets_'.$assets}) )
        {
            $assets = $this->{'_assets_'.$assets};
        }
        else
        {
            $assets = array();
        }

        if ( is_array($arr) && count($arr) > 0 ) { $assets = array_merge_recursive($assets, $arr); }

        $js = $this->_get_javascript();
        if ( $js !== NULL ) { $assets['scripts'][] = $js; }

        return $assets;
    }

    /**
     * @param string $message
     */
    protected function _response_error($message)
    {
        if ( $this->input->is_ajax_request() )
        {
            $this->_return_json_error($message);
        }
        /*else
        {
            $this->_alert($message, 'danger');
        }*/
    }

    /**
     * @param string $message
     * @param null|mixed $data
     */
    protected function _response_success($message = NULL, $data = NULL)
    {
        if ( $this->input->is_ajax_request() )
        {
            $this->_return_json_success($message, $data);
        }
        else
        {
            $message = ($message !== NULL) ? $message : lang('success_message');
            $this->_alert($message, 'success');
        }
    }

    /**
     *
     */
    private function _guess_model()
    {
        if ( $this->_model === NULL )
        {
            $this->_model = strtolower(get_class($this)).'_model';
        }
    }

}
