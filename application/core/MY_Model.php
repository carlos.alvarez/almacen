<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property CI_DB_query_builder $db              This is the platform-independent base Active Record implementation class.
 * @property CI_DB_forge $dbforge                 Database Utility Class
 * @property CI_Benchmark $benchmark              This class enables you to mark points and calculate the time difference between them.<br />  Memory consumption can also be displayed.
 * @property CI_Calendar $calendar                This class enables the creation of calendars
 * @property CI_Cart $cart                        Shopping Cart Class
 * @property CI_Config $config                    This class contains functions that enable config files to be managed
 * @property CI_Controller $controller            This class object is the super class that every library in.<br />CodeIgniter will be assigned to.
 * @property CI_Email $email                      Permits email to be sent using Mail, Sendmail, or SMTP.
 * @property CI_Encrypt $encrypt                  Provides two-way keyed encoding using XOR Hashing and Mcrypt
 * @property CI_Exceptions $exceptions            Exceptions Class
 * @property MY_Form_validation $form_validation  Form Validation Class
 * @property CI_Ftp $ftp                          FTP Class
 * @property CI_Hooks $hooks                      Provides a mechanism to extend the base system without hacking.
 * @property CI_Image_lib $image_lib              Image Manipulation class
 * @property CI_Input $input                      Pre-processes global input data for security
 * @property CI_Lang $lang                        Language Class
 * @property CI_Loader $load                      Loads views and files
 * @property CI_Log $log                          Logging Class
 * @property CI_Model $model                      CodeIgniter Model Class
 * @property CI_Output $output                    Responsible for sending final output to browser
 * @property CI_Pagination $pagination            Pagination Class
 * @property CI_Parser $parser                    Parses pseudo-variables contained in the specified template view,<br />replacing them with the data in the second param
 * @property CI_Profiler $profiler                This class enables you to display benchmark, query, and other data<br />in order to help with debugging and optimization.
 * @property CI_Router $router                    Parses URIs and determines routing
 * @property CI_Session $session                  Session Class
 * @property CI_Table $table                      HTML table generation<br />Lets you create tables manually or from database result objects, or arrays.
 * @property CI_Trackback $trackback              Trackback Sending/Receiving Class
 * @property CI_Typography $typography            Typography Class
 * @property CI_Unit_test $unit_test              Simple testing class
 * @property CI_Upload $upload                    File Uploading Class
 * @property CI_URI $uri                          Parses URIs and determines routing
 * @property CI_User_agent $user_agent            Identifies the platform, browser, robot, or mobile devise of the browsing agent
 * @property CI_Xmlrpc $xmlrpc                    XML-RPC request handler class
 * @property CI_Xmlrpcs $xmlrpcs                  XML-RPC server class
 * @property CI_Zip $zip                          Zip Compression Class
 * @property CI_Javascript $javascript            Javascript Class
 * @property CI_Jquery $jquery                    Jquery Class
 * @property CI_Utf8 $utf8                        Provides support for UTF-8 environments
 * @property CI_Security $security                Security Class, xss, csrf, etc...
 * @property Datatables $datatables
 */
class MY_Model extends CI_Model {

    /**
     * @var null|string
     */
    protected $_table = NULL;
    /**
     * @var null|string|array
     */
    protected $_pk = NULL;
    /**
     * @var null|array
     */
    protected $_validation_rules = NULL;
    /**
     * @var null|array
     */
    protected $_fields = NULL;

    /**
     * MY_Model constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();

		if ( is_null($this->_validation_rules) )
        {
            throw new Exception('"$_validation_rules" property not set.', 1);
        }

        $this->_fields = array_fill_keys(array_column($this->_validation_rules, 'field'), TRUE);
        $this->_guess_table();
        $this->_guess_primary_key();
    }

    /**
     * @param int|string|array $pk
     * @return null|object|array
     */
	public function get($pk)
	{
        $this->_check_pk($pk);
		$filter = is_array($this->_pk) ? $pk : array("{$this->_table}.{$this->_pk}" => $pk);

        $options = array(
            'where'     => $filter,
            'limit'     => 1
        );

		return $this->find($options);
	}

    /**
     * @param int|string|array $pk
     * @return bool
     */
	public function exist($pk)
    {
        $this->_check_pk($pk);

        $select = is_array($this->_pk) ? $this->_pk[0] : $this->_pk;
        $filter = is_array($this->_pk) ? $pk : array($this->_pk => $pk);

        return $this->db->select($select)->from($this->_table)->where($filter)->limit(1)->count_all_results() > 0;
    }

    /**
     * @param string|array $condition
     * @param bool $get_id
     * @return int|string|array|bool
     */
    public function exist_where($condition, $get_id = FALSE)
    {
        if ( $get_id )
        {
            $result = $this->db->select($this->_pk)->from($this->_table)->where($condition)->limit(1)->get()->row_array();
            if ( $result )
            {
                return is_array($this->_pk) ? $result : $result[$this->_pk];
            }
            else { return FALSE; }
        }
        else
        {
            return $this->db->select($this->_pk)->from($this->_table)->where($condition)->limit(1)->count_all_results() > 0;
        }
    }

    /**
     * @param null|array $options
     * @return array|null|object
     */
	public function all($options = NULL)
	{
		return $this->find($options);
	}

    /**
     * @param string $field
     * @return array|null|object
     * @throws Exception
     */
	public function first($field = 'created_at')
	{
        if ( ! isset($this->_fields[$field]) )
        {
            throw new Exception("Field '{$field}' doesn't exist for this model.", 1);
        }

        $options = array(
            'limit'     => 1,
            'order_by'  => "{$field} ASC"
        );

		return $this->find($options);
	}

    /**
     * @param string $field
     * @return array|null|object
     * @throws Exception
     */
	public function last($field = 'created_at')
	{
        if ( ! isset($this->_fields[$field]) )
        {
            throw new Exception("Field '{$field}' doesn't exist for this model.", 1);
        }

        $options = array(
            'limit'     => 1,
            'order_by'  => "{$field} DESC"
        );

		return $this->find($options);
	}

    /**
     * @param array|null $options
     * @param bool $array_result
     * @return object|array|null
     */
    public function find($options = NULL, $array_result = FALSE)
    {
        $this->db->from($this->_table);
        if ( isset($options['select']) )    { $this->db->select($options['select'], FALSE); }
        if ( isset($options['where']) )     { $this->db->where($options['where']); }
        if ( isset($options['order_by']) )  { $this->db->order_by($options['order_by']); }
		if ( isset($options['group_by']) )  { $this->db->group_by($options['group_by']); }
		if ( isset($options['limit']) )     { $this->db->limit($options['limit']); }
		if ( isset($options['offset']) )    { $this->db->offset($options['offset']); }

        if( isset($options['limit']) && $options['limit'] === 1 )
        {
            return ($array_result) ? $this->db->get()->row_array() : $this->db->get()->row();
        }

        return ($array_result) ? $this->db->get()->result_array() : $this->db->get()->result();
    }

    /**
     * @param array|object $data
     * @param bool $skip_validation
     * @return int|string|bool
     */
	public function insert($data, $skip_validation = FALSE)
    {
        $this->_filter_inputs($data);

        if ( isset($this->_fields['created_at']) )
        {
            $data['created_at'] = date('Y-m-d H:i:s');
        }

		if ( !$skip_validation && !$this->_validate($data) ) { return FALSE; }

		$result = $this->db->insert($this->_table, $data);
        if ( $result )
        {
            $result = (is_array($this->_pk)) ? $result : $this->db->insert_id();
            if ( $result === 0 )
            {
                return is_object($data) ? $data->{$this->_pk} : $data[$this->_pk];
            }
        }

		return $result;
	}

    /**
     * @param int|string|array $pk
     * @param array|object $data
     * @param bool $skip_validation
     * @return bool
     */
	public function update($pk, $data, $skip_validation = FALSE)
	{
		$this->_check_pk($pk);
		$this->_filter_inputs($data);

        if ( isset($this->_fields['updated_at']) )
        {
            $data['updated_at'] = date('Y-m-d H:i:s');
        }

		if ( ! $skip_validation && ! $this->_validate($data, $pk) ) { return FALSE; }

		$filter = is_array($this->_pk) ? $pk : array($this->_pk => $pk);

		return $this->db->update($this->_table, $data, $filter);
	}

    /**
     * @param array|object $data
     * @param array|string $reference
     * @param bool $skip_validation
     * @return bool|int|string
     */
	public function insert_update($data, $reference, $skip_validation = FALSE)
    {
        if ( is_array($reference) )
            { $filter = array_intersect_key($data, array_flip($reference)); }
        else
            { $filter = array($reference => $data[$reference]); }

        if ( !$filter ) { return FALSE; }

        if ( $id = $this->exist_where($filter, TRUE) )
        {
            $id = is_array($id) ? $id : array($this->_pk => $id);
            return $this->update($id, $data, $skip_validation) ? $id : FALSE;
        }
        else
        {
            return $this->insert($data, $skip_validation);
        }
    }

    /**
     * @param int|string|array $pk
     * @return bool
     */
    public function delete($pk)
    {
        $this->_check_pk($pk);

		$filter = is_array($this->_pk) ? $pk : array($this->_pk => $pk);
        return $this->db->delete($this->_table, $filter);
    }

    /**
     * @param string|array $condition
     * @return bool
     * @throws Exception
     */
    public function delete_where($condition)
    {
        $elements = $this->find(array('where' => $condition));

        $this->db->trans_start();
        foreach ($elements as $element)
        {
            if ( is_array($this->_pk) )
            {
                $pk = array();
                foreach ($this->_pk as $_pk)
                {
                    $pk[$_pk] = $element->{$_pk};
                }
                $this->delete($pk);
            }
            else
            {
                $this->delete($element->{$this->_pk});
            }
        }
        $this->db->trans_complete();
        if ( !$this->db->trans_status() )
        {
            throw new Exception("No se pudieron eliminar todos los elementos.");
        }

        return TRUE;
    }

    /**
     * @param string $key_field
     * @param string $value_field
     * @param array $options
     * @param string|null $default_option
     * @param string|null $optgroup_field
     * @return array|null
     */
    public function dropdown($key_field, $value_field, $options = array(), $default_option = NULL, $optgroup_field = NULL)
    {
        if ( isset($this->_fields[$key_field]) && isset($this->_fields[$value_field]) && ($optgroup_field === NULL || isset($this->_fields[$optgroup_field])) )
        {
            $options['select'] = "{$this->_table}.{$key_field}, {$this->_table}.{$value_field}";
        }

        $data = $this->find($options, TRUE);

        if ( $optgroup_field === NULL )
        {
            $data = array_column($data, $value_field, $key_field);
        }
        else
        {
            $temp = array();
            foreach ( $data as $element )
            {
                $temp[$element[$optgroup_field]][$element[$key_field]] = $element[$value_field];
            }
            $data = $temp;
        }

        $default_option = is_null($default_option) ? lang('choose_an_option') : $default_option;

        return ($default_option === FALSE) ? $data : array(''=>$default_option)+$data;
    }

    /**
     * @param array|object $data
     * @param bool $edit
     * @return bool
     */
	protected function _validate(&$data, $edit = FALSE)
	{
		if ( is_null($this->_validation_rules) ) { return FALSE; }

		$validation_rules = ($edit !== FALSE) ? $this->_validation_rules_for_edit($this->_validation_rules, $edit) : $this->_validation_rules;

        $this->form_validation->reset_validation();
        $this->form_validation->set_data_ref($data);
		$this->form_validation->set_rules($validation_rules);

		return ($this->form_validation->run() === TRUE);
	}

    /**
     * @param array $rules
     * @param null|int|string $id
     * @return array
     */
	protected function _validation_rules_for_edit($rules, $id = NULL)
	{
		foreach ($rules as $k => $field)
		{
			$conditions = explode('|', $field['rules']);
			$temp = '';
			$first = TRUE;
			foreach ($conditions as $cond)
			{
                $tmp = explode('[', $cond);

                switch ($tmp[0])
                {
                    case 'required':
                        break;
                    /** @noinspection PhpMissingBreakStatementInspection */
                    case 'is_unique':
                        if ( $this->_pk !== 'id' || is_null($id) ) { break; }
                        $par = explode(']', $tmp[1]);
                        $cond = "is_unique[{$par[0]}.{$id}]";
                    default:
                    if ( $first ) { $first = FALSE; } else { $temp .= '|'; }
                    $temp .= $cond;
                }
			}
			$rules[$k]['rules'] = $temp;
		}

		return $rules;
	}

    /**
     * @param int|string|array $pk
     * @return bool
     * @throws Exception
     */
	protected function _check_pk($pk)
    {
        if ( is_array($this->_pk) )
        {
            if ( !is_array($pk) )
            {
                throw new Exception('Array expected as parameter.', 1);
            }

            if ( count($this->_pk) !== count($pk) )
            {
                throw new Exception('Incorrect PK parameters for this model. PK\'s are: '.implode(', ', $this->_pk), 1);
            }

            foreach ($pk as $k => $v)
            {
                if ( !in_array($k, $this->_pk) )
                {
                    throw new Exception("'{$k}' it's not part of the primary key for this model.", 1);
                }
            }
        }

        return TRUE;
    }

    /**
     * @param $data
     */
    protected function _filter_inputs(&$data)
    {
        foreach ($data as $k => $v)
        {
            if ( !isset($this->_fields[$k]) ) { unset($data[$k]); }
        }
    }

    /**
     *
     */
    private function _guess_table()
    {
        if ($this->_table === NULL)
        {
            $this->load->helper('inflector');
            $this->_table = plural(preg_replace('/(_m|_model)?$/', '', strtolower(get_class($this))));
        }
    }

    /**
     *
     */
    private function _guess_primary_key()
    {
        if( $this->_pk !== NULL ) { return; }

        if ( isset($this->_fields['id']) ) { $this->_pk = 'id'; }
        else
        {
            $this->_pk = $this->db->query("SHOW KEYS FROM `".$this->_table."` WHERE Key_name = 'PRIMARY'")->row()->Column_name;
        }
    }

}

