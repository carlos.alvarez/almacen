<?php defined('BASEPATH') OR exit('No direct script access allowed');

//$config['protocol'] = 'smtp';
$config['smtp_host'] = 'smtp.gmail.com'; //change this
$config['smtp_port'] = '587';
$config['smtp_user'] = 'email@test.com.do'; //change this
$config['smtp_pass'] = 'test123456'; //change this
$config['smtp_crypto'] = 'tls';
$config['mailtype'] = 'html';
$config['charset'] = 'UTF-8';
$config['wordwrap'] = TRUE;
$config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard
$config['crlf'] = "\n";
