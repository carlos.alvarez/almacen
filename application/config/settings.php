<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config['app_name'] 		= 'Almacen';
$config['app_logo']         = '<img src="/public/img/logo.png" alt="" style="max-width: 125px;">';
$config['powered_by'] 		= 'Carlos Alvarez';
$config['powered_by_url'] 	= 'Javascript:void(0);';
