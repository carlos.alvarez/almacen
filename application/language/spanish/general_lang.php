<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['settings']               = 'Configuraciones';
$lang['general_settings']       = 'Configuraciones Generales';
$lang['advanced_settings']      = 'Avanzadas';
$lang['user_settings']          = 'Usuarios';
$lang['organization']           = 'Organización';

$lang['help']                   = 'Ayuda';
$lang['back']                   = 'Atrás';
$lang['logout']                 = 'Salir';
$lang['login']                  = 'Entrar';
$lang['submit']                 = 'Guardar';
$lang['options']                = 'Opciones';
$lang['please_wait']            = 'Por favor espere...';

// Controllers names
$lang['cont_welcome']           = 'Bienvenido';
$lang['cont_user']              = 'Usuarios';
$lang['cont_role']              = 'Roles';
$lang['cont_module']            = 'Módulos';
$lang['cont_action']            = 'Acciones';
$lang['cont_grant']             = 'Permisos';
$lang['cont_company']           = 'Empresas';
$lang['cont_company_long']      = 'Empresas del grupo';
$lang['cont_department']        = 'Departamentos';
$lang['cont_employee']          = 'Empleados';
$lang['cont_template']          = 'Plantillas';
$lang['cont_position']          = 'Cargos';
$lang['cont_client']            = 'Clientes';
$lang['cont_area']              = 'Áreas';
$lang['cont_contact_type']      = 'Tipos de contacto';
$lang['cont_calendar']          = 'Calendario';
$lang['cont_project']           = 'Proyectos';
$lang['cont_client_category']   = 'Categorias de clientes';
$lang['cont_project_category']  = 'Categorias de proyectos';
$lang['cont_task']              = 'Tareas';
$lang['cont_brand']             = 'Marcas';
$lang['cont_my_space']          = 'Mi espacio';
$lang['cont_project_client']    = 'Proyecto';
$lang['cont_project_brand']     = 'Presupuesto de proyecto';

$lang['user']                   = 'Usuario';
$lang['role']                   = 'Rol';
$lang['module']                 = 'Módulo';
$lang['action']                 = 'Acción';
$lang['grant']                  = 'Permiso';
$lang['company']                = 'Empresa';
$lang['department']             = 'Departamento';
$lang['employee']               = 'Empleado';
$lang['template']               = 'Plantilla';
$lang['position']               = 'Cargo';
$lang['client']                 = 'Cliente';
$lang['area']                   = 'Área';
$lang['contact_type']           = 'Tipo de contacto';
$lang['calendar']               = 'Calendario';
$lang['project']                = 'Proyecto';
$lang['client_category']        = 'Categoria de clientes';
$lang['project_category']       = 'Categoría de proyectos';
$lang['stage']                  = 'Fase';
$lang['task']                   = 'Tarea';
$lang['brand']                  = 'Marca';
$lang['my_space']               = 'Mi espacio';
$lang['project_client']         = 'Proyecto';
$lang['project_brand']          = 'Presupuesto de proyecto';

// Methods
$lang['method_index']           = 'Listado';
$lang['method_view']            = 'Ver';
$lang['method_create']          = 'Crear';
$lang['method_edit']            = 'Editar';
$lang['method_delete']          = 'Eliminar';
$lang['method_my_projects']     = 'Mis proyectos';
$lang['method_my_projects_client']  = 'Mis proyectos';
$lang['method_my_profile']      = 'Mi perfil';
$lang['method_brand_list_date'] = 'Listado de presupuestos';

$lang['method_f_index']         = 'Listado';
$lang['method_f_view']          = 'Ver';
$lang['method_f_create']        = 'Crear';
$lang['method_f_edit']          = 'Editar';
$lang['method_f_delete']        = 'Eliminar';
$lang['method_add_template']    = 'Añadir Plantilla';

// User
$lang['profile']                = 'Perfil';
$lang['my_profile']             = 'Mi Perfil';
$lang['edit_profile']           = 'Editar Perfil';
$lang['user_image']             = 'Imagen de Usuario';
$lang['my_projects']            = 'Mis proyectos';

// Common fields
$lang['username']               = 'Usuario';
$lang['password']               = 'Contraseña';
$lang['repeat_password']        = 'Repita la Contraseña';
$lang['name']                   = 'Nombre';
$lang['description']            = 'Descripción';
$lang['first_name']             = 'Nombre';
$lang['last_name']              = 'Apellido';
$lang['email']                  = 'Correo';
$lang['active']                 = 'Activo';
$lang['controller']				= 'Controlador';
$lang['method']				    = 'Método';
$lang['created_at']             = 'Fecha de Creación';
$lang['updated_at']             = 'Fecha de Actualización';

$lang['short_name']             = 'Nombre Corto';
$lang['tax_number']             = 'Registro Contribuyente';
$lang['tax_number_short']       = 'RNC';
$lang['address']                = 'Dirección';
$lang['manager']                = 'Responsable';
$lang['color']                  = 'Color';
$lang['order']                  = 'Orden';
$lang['requirement']            = 'Requerimiento';
$lang['ref_code']               = 'Código de Referencia';
$lang['ref_code_short']         = 'Cód. Ref.';
$lang['phone']                  = 'Teléfono';
$lang['ext']                    = 'Extensión';
$lang['mobile']                 = 'Celular';
$lang['manage_clients']         = 'Maneja Clientes';
$lang['account_manager']        = 'Asesor de Ventas';
$lang['working_hours']          = 'Horas Laborables';
$lang['date']                   = 'Fecha';
$lang['week_day']               = 'Día de la Semana';
$lang['holiday']                = 'Día Feriado';
$lang['holidays']               = 'Días Feriados';
$lang['working_day']            = 'Día Laboral';
$lang['working_days']           = 'Días Laborales';
$lang['category']               = 'Categoría';
$lang['start_date']             = 'Fecha de Inicio';
$lang['estimated_date']         = 'Fecha Estimada';
$lang['estimated_end_date']     = 'Fecha Fin Estimada';
$lang['estimated_amount']       = 'Monto Estimado';
$lang['end_date']               = 'Fecha Finalización';
$lang['invoiced_by']            = 'Facturado Por';
$lang['status']                 = 'Estado';
$lang['title']                  = 'Título';
$lang['path']                   = 'Dirección';
$lang['actual_amount']          = 'Monto Real';
$lang['actual_date']            = 'Fecha Real';

// Others
$lang['user_data']              = 'Datos de Usuario';
$lang['employee_data']          = 'Datos de Empleado';
$lang['default']                = 'Por defecto';
$lang['sunday']                 = 'Domingo';
$lang['monday']                 = 'Lunes';
$lang['tuesday']                = 'Martes';
$lang['wednesday']              = 'Miércoles';
$lang['thursday']               = 'Jueves';
$lang['friday']                 = 'Viernes';
$lang['saturday']               = 'Sábado';
$lang['laborable']              = 'Laborable';
$lang['My_space']               = 'Mi espacio';
$lang['home']                   = 'Tablero';
$lang['supervisor']             = 'Supervisor';

$lang['canceled']               = 'Cancelado';
$lang['finished']               = 'Terminado';
$lang['pending']                = 'Pendiente';
$lang['process']                = 'Proceso';

$lang['year']                   = 'Año';
$lang['month']                  = 'Mes';
$lang['day']                    = 'Día';

$lang['download']               = 'Descargar';

// messages
$lang['choose_an_option']       = 'Elija una opción';
$lang['create_user_q']          = '¿Crear usuario?';
$lang['success_message']        = '¡Realizado Exitosamente!';
$lang['not_found']              = 'No se encontró lo que buscaba.';
$lang['message']                = 'Mensaje';

$lang['add_contact']            = 'Agregar Contacto';
$lang['contacts']               = 'Contactos';

$lang['choose_a_date']          = 'Elija una fecha';

