<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['login_log_in']               = 'Iniciar Sesión';
$lang['login_lost_password']        = '¿Olvidaste la contraseña?';
$lang['Wrong_user_or_password']     = 'Usuario o contraseña incorrecto.';
