
if ( $('#task-edit-form').size() > 0 ) {

    $('#task-edit-form').submit(function (e) {
        e.preventDefault();

        var data = $('#task-edit-form').serialize();
        $.post('/task/analize_reschedule/'+$('#task-edit-form').data('id'), data, function (json) {
            var obj = JSON.parse(json);
            if ( obj.error ) { sweetNotification('Error', obj.message, 'danger'); return; }

            if ( obj.data ) {
                var text = '';
                obj.data.forEach(function (t) { text += '\n' + t; })

                swal({
                        title: "Eliminar",
                        text: text,
                        type: "warning",
                        showCancelButton: true,
                        showLoaderOnConfirm: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "¡Si, proceder!",
                        closeOnConfirm: false
                    },
                    function(){
                        $.post('/task/edit/'+$('#task-edit-form').data('id'), data, function (json) {
                            var json_obj = JSON.parse(json);

                            if ( json_obj.error ) {
                                swal.showInputError(json_obj.message);
                                return false;
                            } else {
                                sweetNotification("¡Bien!", "¡Cambios guardados!", "success", 1000);
                                window.location = '/task/f_view/'+$('#task-edit-form').data('id');
                            }
                        });
                    });
            } else {
                $.post('/task/edit/'+$('#task-edit-form').data('id'), data, function (json) {
                    var json_obj = JSON.parse(json);

                    if ( json_obj.error ) {
                        sweetNotification("¡Error!", obj.message, "danger");
                        return false;
                    } else {
                        sweetNotification("¡Bien!", "¡Cambios guardados!", "success", 1000);
                        window.location = '/task/f_view/'+$('#task-edit-form').data('id');
                    }
                });
            }

        });

    });

    $('#resources').select2({
        templateResult: function (option) {
            return $('<span>'+option.text+'</span> <small class="col-grey">'+$(option.element).data('subtext')+'</small>');
        }
    });

    $('#prerequisites').select2();

    $('#tags').tagsinput({
        trimValue: true
    });

    $('input[name="start_date_n"]').datepicker({
        dateFormat: "d/m/yy",
        altFormat: "yy-mm-dd",
        altField: "#start_date"
    });

    $('input[name="estimated_end_date_n"]').datepicker({
        dateFormat: "d/m/yy",
        altFormat: "yy-mm-dd",
        altField: "#estimated_end_date"
    });

    $('#toggle-visibility').click(function () {
        var client_visibility = $('#task-edit-form').find('input[name="client_visibility"]').first();
        if ( client_visibility.val() === 'true' ) {
            client_visibility.val('false');
            $(this).attr('data-original-title', 'No visible para el cliente').find('i.material-icons').first().removeClass('col-blue').addClass('col-gray').text('visibility_off');
            var client_notification = $('#task-edit-form').find('input[name="client_notification"]').first();
            if ( client_notification.val() === 'true' ) {
                $('#toggle-notification').click();
            }
        } else {
            client_visibility.val('true');
            $(this).attr('data-original-title', 'Visible para el cliente').find('i.material-icons').first().removeClass('col-gray').addClass('col-blue').text('visibility');
        }
        $(this).tooltip("hide").mouseover();
    });

    $('#toggle-notification').click(function () {
        var client_notification = $('#task-edit-form').find('input[name="client_notification"]').first();
        var client_visibility = $('#task-edit-form').find('input[name="client_visibility"]').first();

        if ( client_visibility.val() !== 'true' && client_notification.val() !== 'true' ) { return; }

        if ( client_notification.val() === 'true' ) {
            client_notification.val('false');
            $(this).attr('data-original-title', 'No notificar al cliente').find('i.material-icons').first().removeClass('col-yellow').addClass('col-gray').text('notifications_off');
        } else {
            client_notification.val('true');
            $(this).attr('data-original-title', 'Notificar al cliente').find('i.material-icons').first().removeClass('col-gray').addClass('col-yellow').text('notifications_on');
        }
        $(this).tooltip("hide").mouseover();
    });

    $('#requirementsForm').sheepIt({
        separator: '',
        allowRemoveLast: false,
        allowRemoveCurrent: true,
        allowRemoveAll: false,
        allowAdd: true,
        allowAddN: false,
        minFormsCount: 1,
        iniFormsCount: 1,
        beforeRemoveCurrent: function(source, form) {
            swal(
                {
                    title: "Eliminar Contacto",
                    text: "¿Está seguro que desea eliminar este requerimiento?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: true
                },
                function(){
                    source.removeCurrentForm(form);
                }
            );

            return false;
        },
        data: $('#requirementsPre').data('requirements')
    });

    var status_changer = $('input[name="status_changer"]').not('[type="hidden"]').first();
    var stage_status = $('select[name="stage_status_id"]').first();
    var reason = $('select[name="reason_id"]').first();

    status_changer.on('change', function () {
        if ( $(this).is(':checked') ) {
            stage_status.attr('disabled', false).closest('.form-line').removeClass('disabled');
            reason.attr('disabled', false).closest('.form-line').removeClass('disabled');
        } else {
            stage_status.attr('disabled', true).closest('.form-line').addClass('disabled');
            reason.attr('disabled', true).closest('.form-line').addClass('disabled');
        }
    });

    stage_status.on('change', function () {
        reason.find('option[data-stage_status!="'+stage_status.val()+'"]').hide();
        var value = reason.find('option[data-stage_status="'+stage_status.val()+'"]').show().first().val();
        reason.val(value).change();
    });

    if ( status_changer.is(':checked') ) {
        status_changer.change();
        var reason_v = reason.val();
        stage_status.change();
        reason.val(reason_v).change();
    }
}

$('#cancel_task_btn').click(function () {
    var task_id = $(this).data('task');

    swal({
        title: 'Anular Tarea',
        text: "¿Está seguro que desea anular esta tarea?",
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "¡Sí, anular!",
        closeOnConfirm: false
    }, function () {
        swal({
            title: 'Causa de anulación',
            text: 'Introduzca la causa de la anulación',
            type: "input",
            confirmButtonText: "Anular",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function (causa) {
            if ( causa === false ) {
                swal.close();
                return;
            }
            if ( causa.trim() === '' ) {
                swal.showInputError('Debe escribir una causa.');
                return;
            }
            $.post('/task/cancel/'+task_id, function (json) {
                var obj = JSON.parse(json);

                if ( obj.error ) {
                    swal.showInputError(obj.message);
                    return;
                }

                sweetNotification('Tarea Anulada', 'La tarea fue anulada de forma exitosa.', 'success', 1000);
                setTimeout(location.reload(), 1300);
            });
        });
    });
});

$('#close_task_btn').click(function () {
    var task_id = $(this).data('task');

    swal({
        title: 'Cerrar Tarea',
        text: "¿Está seguro que desea cerrar esta tarea?",
        type: 'info',
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonColor: "#2b982b",
        confirmButtonText: "Cerrar",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function () {
        $.post('/task/close/'+task_id, function (json) {
            var obj = JSON.parse(json);

            if ( obj.error ) {
                swal.showInputError(obj.message);
                return;
            }

            sweetNotification('Tarea Cerrada', 'La tarea fue cerrada de forma exitosa.', 'success', 1000);
            setTimeout(location.reload(), 1300);
        });
    });
});

if ( $('#task-new-form').size() > 0 ) {

    $('#prerequisites').select2();
    $('#resources').select2({
        templateResult: function (option) {
            return $('<span>'+option.text+'</span> <small class="col-grey">'+$(option.element).data('subtext')+'</small>');
        }
    });

    $('#tags').tagsinput({
        trimValue: true
    });

    $('input[name="start_date_n"]').datepicker({
        dateFormat: "d/m/yy",
        altFormat: "yy-mm-dd",
        altField: "#start_date"
    });

    $('input[name="estimated_end_date_n"]').datepicker({
        dateFormat: "d/m/yy",
        altFormat: "yy-mm-dd",
        altField: "#estimated_end_date"
    });

    $('#toggle-visibility').click(function () {
        var client_visibility = $('#task-new-form').find('input[name="client_visibility"]').first();
        if ( client_visibility.val() === 'true' ) {
            client_visibility.val('false');
            $(this).attr('data-original-title', 'No visible para el cliente').find('i.material-icons').first().removeClass('col-blue').addClass('col-gray').text('visibility_off');
            var client_notification = $('#task-new-form').find('input[name="client_notification"]').first();
            if ( client_notification.val() === 'true' ) {
                $('#toggle-notification').click();
            }
        } else {
            client_visibility.val('true');
            $(this).attr('data-original-title', 'Visible para el cliente').find('i.material-icons').first().removeClass('col-gray').addClass('col-blue').text('visibility');
        }
        $(this).tooltip("hide").mouseover();
    });

    $('#toggle-notification').click(function () {
        var client_notification = $('#task-new-form').find('input[name="client_notification"]').first();
        var client_visibility = $('#task-new-form').find('input[name="client_visibility"]').first();

        if ( client_visibility.val() !== 'true' && client_notification.val() !== 'true' ) { return; }

        if ( client_notification.val() === 'true' ) {
            client_notification.val('false');
            $(this).attr('data-original-title', 'No notificar al cliente').find('i.material-icons').first().removeClass('col-yellow').addClass('col-gray').text('notifications_off');
        } else {
            client_notification.val('true');
            $(this).attr('data-original-title', 'Notificar al cliente').find('i.material-icons').first().removeClass('col-gray').addClass('col-yellow').text('notifications_on');
        }
        $(this).tooltip("hide").mouseover();
    });

    $('#requirementsForm').sheepIt({
        separator: '',
        allowRemoveLast: false,
        allowRemoveCurrent: true,
        allowRemoveAll: false,
        allowAdd: true,
        allowAddN: false,
        minFormsCount: 1,
        iniFormsCount: 1,
        /*beforeRemoveCurrent: function(source, form) {
            swal(
                {
                    title: "Eliminar Contacto",
                    text: "¿Está seguro que desea eliminar este requerimiento?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: true
                },
                function(){
                    source.removeCurrentForm(form);
                }
            );

            return false;
        },*/
        data: $('#requirementsPre').data('requirements')
    });

    var status_changer = $('input[name="status_changer"]').not('[type="hidden"]').first();
    var stage_status = $('select[name="stage_status_id"]').first();
    var reason = $('select[name="reason_id"]').first();

    status_changer.on('change', function () {
        if ( $(this).is(':checked') ) {
            stage_status.attr('disabled', false).closest('.form-line').removeClass('disabled');
            reason.attr('disabled', false).closest('.form-line').removeClass('disabled');
        } else {
            stage_status.attr('disabled', true).closest('.form-line').addClass('disabled');
            reason.attr('disabled', true).closest('.form-line').addClass('disabled');
        }
    });

    stage_status.on('change', function () {
        reason.find('option[data-stage_status!="'+stage_status.val()+'"]').hide();
        var value = reason.find('option[data-stage_status="'+stage_status.val()+'"]').show().first().val();
        reason.val(value).change();
    });

    if ( status_changer.is(':checked') ) {
        status_changer.change();
        var reason_v = reason.val();
        stage_status.change();
        reason.val(reason_v).change();
    }
}

$('.requirement_check').change(function () {
    var req = $(this);
    var done = 'false';
    if ( req.is(':checked') ) { done = 'true'; }
    $.post('/task/requirement_done/'+req.data('id')+'/'+done, function (json) {
        var obj = JSON.parse(json);
        if ( obj.error ) { req.prop('checked', req.is(':checked')); return; }
    });

    if ( $('.requirement_check').not(':checked').length === 0 ) {
        $('#close_task_btn').removeClass('hide');
        swal({
            title: 'Requerimientos completado',
            text: "Todos los requerimientos han sido completados, \n¿Desea cerrar la tarea?",
            type: 'info',
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#2b982b",
            confirmButtonText: "Si",
            closeOnConfirm: false
        }, function () {
            $('#close_task_btn').click();
        });
    } else {
        $('#close_task_btn').addClass('hide');
    }

});

/* ******************************************
   *            TASK FILTER LIST            *
* *******************************************/
if ( $('#tasks-filter-table').size() > 0 ) {
    var $table = $('#tasks-filter-table');
    $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/task/tasks_filter_json/'+$('#day').val()+'/'+$('#month').val()+'/'+$('#project_id').val()+'/'+$('#stage').val()+'/'+$('#status').val()+'/'+$('#brand').val()
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'id' },
            { data: 'stage' },
            {
                data: 'title',
                render: function(value, type, data) {
                    return '<a href="/task/f_view/'+data.id+'" target="_blank">'+data.title+'</a>'
                }
            },
            { data: 'description' },
            {
                data: 'status',
                render: function(value, type, obj) {
                    return obj.status.toUpperCase();
                }
            },
            {
                data: 'status_changer',
                render: function (value, type, obj, meta) {
                    var checked = obj.status_changer === 'true' ? 'checked' : '';
                    console.log(obj.status_changer);
                    return  '<div class="switch">' +
                        '   <label><input type="checkbox" disabled name="status_changer" value="true" '+checked+'><span class="lever switch-col-amber"></span></label>' +
                        '</div>';
                }
            },
            { data: 'stage_status' },
            { data: 'reason' },
            {
                data: 'progress',
                render: function(value, type, obj) {
                    return '<div class="progress">' +
                        '   <div class="progress-bar bg-green progress-bar-striped active" role="progressbar" style="width: '+obj.progress+'%">\n' +
                        '       '+obj.progress+'%' +
                        '   </div>' +
                        '</div>';
                },
                searchable: false
            },
            { data: 'start_date', render: function(value, type, obj) { return obj.start_date_nice; } },
            { data: 'estimated_end_date', render: function(value, type, obj) { return obj.estimated_end_date_nice; } },
            { data: 'end_date', render: function(value, type, obj) { return obj.end_date_nice; } },
            {
                data: 'client_visibility',
                render: function(value, type, obj) {
                    var res = '';
                    if ( obj.client_visibility === 'true' ) {
                        res += '<i class="material-icons col-blue" data-toggle="tooltip" data-original-title="Visible para el cliente">visibility</i>';
                    } else {
                        res += '<i class="material-icons col-grey" data-toggle="tooltip" data-original-title="No visible para el cliente">visibility_off</i>';
                    }
                    if ( obj.client_notification === 'true' ) {
                        res += '<i class="material-icons col-amber" data-toggle="tooltip" data-original-title="Notifica al cliente">notifications</i>';
                    } else {
                        res += '<i class="material-icons col-grey" data-toggle="tooltip" data-original-title="No notifica al cliente">notifications_off</i>';
                    }

                    return res;
                }
            }
        ]
    }));
}

if ($('#tasks-table').size() > 0){
    var $table = $('#tasks-table');
    var ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/task/datatable_json'+window.location.search
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'id' },
            {
                data: 'project',
                render: function(value, type, data) {
                    return '<a href="/project/f_view/'+data.project_id+'" target="_blank" data-toggle="tooltip" title="'+data.project_description+'">'+data.project+'</a>'
                }
            },
            { data: 'template' },
            { data: 'stage' },
            {
                data: 'title',
                render: function(value, type, data) {
                    return '<a href="/task/f_view/'+data.id+'" target="_blank" data-toggle="tooltip" title="'+data.description+'">'+data.title+'</a>'
                }
            },
            {
                data: 'status',
                render: function(value, type, obj) {
                    return obj.status.toUpperCase();
                }
            },
            {
                data: 'progress',
                render: function(value, type, obj) {
                    return '<div class="progress">' +
                        '   <div class="progress-bar bg-green progress-bar-striped active" role="progressbar" style="width: '+obj.progress+'%">\n' +
                        '       '+obj.progress+'%' +
                        '   </div>' +
                        '</div>';
                },
                searchable: false
            },
            {
                data: 'resources',
                orderable: false,
                searchable: false
            },
            { data: 'start_date', render: function(value, type, obj) { return obj.start_date_nice; } },
            { data: 'estimated_end_date', render: function(value, type, obj) { return obj.estimated_end_date_nice; } },
            { data: 'end_date', render: function(value, type, obj) { return obj.end_date_nice; } },
            {
                data: 'estimated_end_date',
                render: function (value, type, obj, meta) {
                    var color = (obj.delay.charAt(0) === '-') ? 'col-red' : 'col-green';

                    if ( obj.status === 'pendiente' || obj.status === 'proceso' ) {
                        return '<span class="'+color+'">'+obj.delay+'</span>';
                    }
                    if ( obj.status === 'cerrado' || obj.status === 'anulado' ) {
                        return '';
                    }

                    return '<span class="'+color+'">'+obj.delay+'</span>';
                },
                searchable: false
            },
            {
                data: 'status_changer',
                render: function (value, type, obj, meta) {
                    var checked = obj.status_changer === 'true' ? 'checked' : '';
                    console.log(obj.status_changer);
                    return  '<div class="switch">' +
                        '   <label><input type="checkbox" disabled name="status_changer" value="true" '+checked+'><span class="lever switch-col-amber"></span></label>' +
                        '</div>';
                }
            },
            { data: 'stage_status' },
            { data: 'reason' },
            {
                data: 'tag',
                render: function (value, type, obj, meta) {
                    var res = '';

                    if ( obj.tags ) {
                        obj.tags.split(',').forEach(function (t) {
                            if ( res !== '' ) { res+= ' '; }
                            res += '<span class="label bg-blue">'+t+'</span>';
                        });
                    }

                    return res;
                }
            },
            {
                data: 'client_visibility',
                render: function(value, type, obj) {
                    var res = '';
                    if ( obj.client_visibility === 'true' ) {
                        res += '<i class="material-icons col-blue" data-toggle="tooltip" data-original-title="Visible para el cliente">visibility</i>';
                    } else {
                        res += '<i class="material-icons col-grey" data-toggle="tooltip" data-original-title="No visible para el cliente">visibility_off</i>';
                    }
                    if ( obj.client_notification === 'true' ) {
                        res += '<i class="material-icons col-amber" data-toggle="tooltip" data-original-title="Notifica al cliente">notifications</i>';
                    } else {
                        res += '<i class="material-icons col-grey" data-toggle="tooltip" data-original-title="No notifica al cliente">notifications_off</i>';
                    }

                    return res;
                }
            }
        ]
    }));

    $('#delayed_tasks_filter').change(function () {
        if ( $(this).is(':checked') ) {
            ctable.ajax.url('/task/datatable_json'+window.location.search+'&delayed=true').order([ [12, 'asc'] ]).load();
        } else {
            ctable.ajax.url('/task/datatable_json'+window.location.search).order([ [1, 'desc'] ]).load();
        }
    });
}
