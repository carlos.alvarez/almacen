
var sticked_filters_init = function () {
    var sticked = false;
    function stick_filters() {
        if(window.innerWidth >= 768) {
            if ( sticked ) {
                $('#filters').sticky('update');
            } else {
                $('#filters').sticky({topSpacing:70,zIndex:9});
                sticked = true;
            }
        } else {
            if ( sticked ) {
                $('#filters').unstick();
                sticked = false;
            }
        }
    }

    $(document).ready(function(){
        stick_filters();
    });

    $( window ).resize(function() {
        stick_filters();
    });
};

sticked_filters_init();
