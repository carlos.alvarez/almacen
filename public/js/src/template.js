$(document).ready(function () {

    /* LIST */
    if ($('#templates-table').size() > 0) {

        var $table = $("#templates-table");
        ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
            ajax: {
                url: '/template/datatable_json'
            },
            columns: [
                {
                    data: function (data) {
                        return '<input type="checkbox" id="check-' + data.id + '" class="filled-in chk-col-teal" name="check[]" value="' + data.id + '">' +
                            '<label class="m-b-0" for="check-' + data.id + '"></label>';
                    },
                    orderable: false,
                    searchable: false
                },
                {data: 'id'},
                {
                    data: 'title',
                    render: function (value, type, obj, meta) {
                        return "<a href='/template/view/" + obj.id + "' target='_blank'>" + obj.title + "</a>"
                    }
                },
                {
                    data: 'project_category',
                    render: function (value, type, obj, meta) {
                        return "<a href='/project_category/view/" + obj.project_category_id + "' target='_blank'>" + obj.project_category + "</a>"
                    }
                },
                {
                    data: 'created_at',
                    render: function (value, type, obj, meta) {
                        return obj.created_at_formatted;
                    }
                },
                {
                    data: 'updated_at',
                    render: function (value, type, obj, meta) {
                        return obj.updated_at_formatted;
                    }
                },
                {
                    data: function(data) {
                        var buttons = '<div class="btn-group">';
                        if (data.duplicate === 'true'){
                            buttons += '<a class="btn btn-default btn-xs waves-effect duplicate-template" data-id="' + data.id + '" data-name="' + data.title + '" data-toggle="tooltip" data-original-title="Duplicar"><i class="material-icons">content_copy</i></a>';
                        }
                        if (data.edit === 'true'){
                            buttons += '<a href="/template/edit/' + data.id + '" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                        }
                        if (data.delete === 'true'){
                            buttons += '<a href="javascript:void(0);" class="btn btn-danger btn-xs waves-effect delete-btn" data-id="' + data.id + '" data-controller="template" data-text="¿Está seguro que desea eliminar esta plantilla?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                        }

                        buttons += '</div>';
                        return buttons;
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        }));

        $(document).on('click', 'a.duplicate-template', function (e) {
            e.preventDefault();
            var button = $(this);

            swal({
                title: "Duplicar Plantilla",
                text: "Escribe un nuevo nombre para la plantilla a duplicar:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Nombre Plantilla",
                showLoaderOnConfirm: true
            }, function (inputValue) {
                if (inputValue !== false && inputValue === button.data('name')) {
                    swal.showInputError("Debe escribir un nombre diferente a la plantilla original");
                    return false;
                } else if (inputValue !== false && inputValue === "") {
                    swal.showInputError("Debe escribir un nombre.");
                    return false
                } else if (inputValue !== false){
                    $.post('/template/duplicate_template', {title: inputValue, id: button.data('id')}, function (response) {
                        var res = JSON.parse(response);

                        if (res.error === true) {
                            swal.showInputError(res.message);
                            return false;
                        }
                        $table.DataTable().ajax.reload();
                        sweetNotification("¡Bien!", "¡Creado correctamente!", "success", 1000);
                    });
                }
            });
        });
    }

    var child_type = {
        "template": ["stage"],
        "stage": ["task"]
    };

    var type_translation = {
        "template": "Plantilla",
        "stage": "Fase",
        "task": "Tarea",
        "position": "Cargo",
        "requirement": "Requisito",
        "tag": "etiqueta"
    };

    var jstreePlugins = ["contextmenu", "wholerow", "types", "dnd", "state", "search"];
    if ($('#readonly-view').size() > 0) {
        jstreePlugins = ["wholerow", "types", "state", "search"];
    }

    if ($('#template-tree').size() > 0) {
        $('#template-tree').jstree({
            "plugins": jstreePlugins,
            "core": {
                "data": {
                    "url": function (node) {

                        var url = false;
                        switch (node.type) {
                            case 'template':
                                url = "/template/get_template_children_json";
                                break;
                            case 'stage':
                                url = "/template/get_stage_children_json";
                                break;
                            case 'task':
                                url = "/template/get_task_children_json";
                                break;
                            default:
                                url = "/template/get_template_node_json";
                        }

                        return url;
                    },
                    "data": function (node) {
                        return {
                            'id': node.li_attr ? node.li_attr["data-id"] : $('#template-tree').data('id')
                        };
                    }
                },
                "check_callback": function (operation, node, node_parent, node_position, more) {
                    if (operation === "move_node") {
                        if (node_parent.original === undefined) {
                            return false;
                        }
                        return $.inArray(node.type, child_type[node_parent.original.type]) >= 0;
                    }

                    if (operation === "copy_node") {
                        if (/*node_parent.original === undefined*/ true) {
                            return false;
                        }
                        return $.inArray(node.type, child_type[node_parent.original.type]) >= 0;
                    }

                    return true;
                }
            },
            "types": {
                "template": {
                    "icon": "glyphicon glyphicon-modal-window col-blue"
                },
                "stage": {
                    "icon": "glyphicon glyphicon-briefcase col-purple"
                },
                "task": {
                    "icon": "glyphicon glyphicon-tasks col-green"
                }
            },
            "contextmenu": {
                "items": function ($node) {

                    var menu = {};

                    $.each(child_type[$node.type], function (i, new_node_type) {
                        menu['create_' + new_node_type] = {
                            "separator_before": false,
                            "separator_after": true,
                            "_disabled": function () {
                                switch ($node.type) {
                                    case 'prerequisite':
                                    case 'position':
                                    case 'tag':
                                        break;
                                    default:
                                        return false;
                                }
                                return true;
                            },
                            "icon": "glyphicon glyphicon-plus col-green",
                            "label": "Agregar " + type_translation[new_node_type],
                            "action": function (data) {
                                swal({
                                        title: "Agregar " + type_translation[new_node_type],
                                        text: "Escribe el nombre del nuevo elemento:",
                                        type: "input",
                                        showCancelButton: true,
                                        closeOnConfirm: false,
                                        animation: "slide-from-top",
                                        inputPlaceholder: type_translation[new_node_type],
                                        showLoaderOnConfirm: true
                                    },
                                    function (inputValue) {
                                        if (inputValue === false) return false;
                                        if (inputValue === "") {
                                            swal.showInputError("Debe escribir un nombre.");
                                            return false
                                        }

                                        var inst = $.jstree.reference(data.reference),
                                            obj = inst.get_node(data.reference);

                                        $.post('/template/create_' + new_node_type + '/' + obj.li_attr['data-id'], {title: inputValue}, function (json) {
                                            var json_obj = JSON.parse(json);

                                            if (json_obj.error === false) {
                                                /*inst.create_node(obj, json_obj.data.node, json_obj.data.node.position, function (newNode) {
                                                    inst.open_node(obj);
                                                    inst.deselect_all();
                                                    inst.select_node(newNode);
                                                });*/
                                                $('#template-tree').jstree('refresh_node', obj);
                                                sweetNotification("¡Bien!", "¡Creado correctamente!", "success", 1000);
                                            } else {
                                                swal.showInputError("Debe escribir un nombre.");
                                            }
                                        });

                                    });
                            }
                        }
                    });

                    menu["edit"] = {
                        "separator_before": false,
                        "separator_after": false,
                        "_disabled": false,
                        "icon": "glyphicon glyphicon-edit col-blue",
                        "label": "Editar",
                        "action": function (data) {

                            switch ($node.type) {
                                case 'template':
                                    template_edit($node);
                                    break;
                                case 'stage':
                                    stage_edit($node);
                                    break;
                                case 'task':
                                    task_edit($node);
                                    break;
                                default:
                            }

                        }
                    };

                    menu["remove"] = {
                        "separator_before": false,
                        "separator_after": false,
                        "_disabled": function (data) {
                            return ($node.type === "template");
                        },
                        "icon": "glyphicon glyphicon-trash col-red",
                        "label": "Eliminar",
                        "action": function (data) {
                            swal(
                                {
                                    title: "Eliminar " + type_translation[$node.type],
                                    text: "¿Está seguro que desea eliminar este elemento?",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "¡Si, eliminar!",
                                    closeOnConfirm: false
                                },
                                function () {
                                    var inst = $.jstree.reference(data.reference),
                                        obj = inst.get_node(data.reference);

                                    $.get('/template/delete_' + obj.type + '/' + obj.li_attr['data-id'], function (json) {
                                        var json_obj = JSON.parse(json);

                                        if (json_obj.error === false) {
                                            $('#template-tree').jstree('refresh');
                                            sweetNotification("¡Bien!", "¡Eliminado correctamente!", "success", 1000);
                                        } else {
                                            sweetNotification("¡Error!", json_obj.message, "error");
                                        }
                                    });
                                }
                            );
                        }
                    };

                    return menu;
                }
            },
            "dnd": {
                "check_while_dragging": true
            }
        }).on('move_node.jstree', function (e, data) {
            swal(
                {
                    title: "Mover " + type_translation[data.node.type],
                    text: "¿Está seguro que desea mover este elemento?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "¡Si, mover!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function () {
                    $.get('/template/move_' + data.node.type + '/' + data.node.li_attr['data-id'] + '/' + $('#' + data.parent).data('id') + '/' + data.position, function (json) {
                        var obj = JSON.parse(json);
                        console.log(data);
                        if (obj.error === false) {
                            sweetNotification("¡Bien!", "¡Elemento movido correctamente!", "success", 1000);
                        } else {
                            $("#template-tree").jstree("move_node", data.node, data.old_parent, data.old_position);
                            sweetNotification("¡Error!", obj.message, "error");
                        }
                    });
                }
            );
        }).on('copy_node.jstree', function (e, data) {

        }).on('select_node.jstree', function (e, data) {
            switch (data.node.type) {
                case 'template':
                    template_details(data.node);
                    break;
                case 'stage':
                    stage_details(data.node);
                    break;
                case 'task':
                    task_details(data.node);
                    break;
                default:
                    default_details(data.node);
            }

        }).on('hover_node.jstree', function (e, data) {
            var bar = $(this).find('.jstree-wholerow-hovered');
            bar.css('height', bar.parent().children('a.jstree-anchor').height() + 'px');
        });
    }
    //$('#template-tree').find('.jstree-wholerow-hovered').css('height', bar.parent().children('a.jstree-anchor').height() + 'px');

    var to = false;
    $('#tree-search-text').keyup(function () {
        if(to) { clearTimeout(to); }
        to = setTimeout(function () {
            var v = $('#tree-search-text').val();
            $('#template-tree').jstree(true).search(v, false);
        }, 250);
    });

    default_details = function(node) {
        $('#details-container').html('');
        var container = $('#default-details').clone();
        container.attr('id', 'default-details-show');

        $('#details-container').append(container);
        container.show();
    };

    template_details = function(node) {
        $('#details-container').html('');
        var container = $('#template-details').clone();
        container.attr('id', 'template-details-'+node.li_attr['data-id']);
        container.find('div.header h2').first().text(node.text);

        var description = (node.li_attr['data-description'] === null) ? '' : node.li_attr['data-description'];
        container.find('div.body p#template_descripcion').first().text(description);

        var category = (node.li_attr['data-project_category'] === null) ? '' : node.li_attr['data-project_category'];
        container.find('div.body p#project_category').first().text(category);

        $('#details-container').append(container);
        container.show();
    };

    stage_details = function(node) {
        $('#details-container').html('');
        var container = $('#stage-details').clone();
        container.attr('id', 'stage-details-'+node.li_attr['data-id']);
        container.find('div.header h2').first().text(node.text);

        var visibility_icon = container.find('div.header i.visibility').first();
        set_visibility(visibility_icon, node.li_attr['data-client_visibility']);

        var notification_icon = container.find('div.header i.notification').first();
        set_notification(notification_icon, node.li_attr['data-client_notification']);

        var description = (node.li_attr['data-description'] === null) ? '' : node.li_attr['data-description'];
        container.find('p.description').first().text(description);

        var weight = node.li_attr['data-weight'];
        var weight_bar = container.find('.progress-bar.weight').first();
        weight_bar.css('width', weight+'%');
        weight_bar.text((Math.round(weight*100)/100)+'%');

        $('#details-container').append(container);
        container.show();
    };

    task_details = function(node) {
        $('#details-container').html('');
        var container = $('#task-details').clone();
        container.attr('id', 'task-details-'+node.li_attr['data-id']);

        var task_translation = {
            laborable: 'Laborables',
            calendar: 'Calendario'
        };

        container.find('div.header h2').first().text(node.text);
        var duration = node.li_attr['data-duration_days']+'d '+node.li_attr['data-duration_hours']+'h';
        container.find('div.duration').first().text(duration);
        container.find('div.day_type').first().text(task_translation[node.li_attr['data-day_type']]);
        if ( node.li_attr['data-day_type'] === 'laborable' ) {
            container.find('i.day_type_icon').first().text('work');
        } else {
            container.find('i.day_type_icon').first().text('today');
        }

        var tags = JSON.parse(node.li_attr['data-tags']);
        var tag_container = container.find('div.tags').first();
        $.each(tags, function (i, tag) {
            tag_container.append('<span class="label label-info">'+tag.tag+'</span> ');
        });

        var positions = JSON.parse(node.li_attr['data-positions']);
        var position_container = container.find('div.positions').first();
        $.each(positions, function (i, position) {
            position_container.append('<span class="label bg-purple" data-toggle="tooltip" data-original-title="'+position.company+'">'+position.position+'</span> ');
        });

        var prerequisites = JSON.parse(node.li_attr['data-prerequisites']);
        var prerequisite_container = container.find('div.prerequisites').first();
        $.each(prerequisites, function (i, prerequisite) {
            prerequisite_container.append('<span class="label bg-orange" data-toggle="tooltip" data-original-title="Fase: '+prerequisite.stage+'">'+prerequisite.prerequisite+'</span> ');
        });

        var requirements = JSON.parse(node.li_attr['data-requirements']);
        var requirements_container = container.find('div.requirements').first();
        $.each(requirements, function (i, req) {
            requirements_container.append('<i class="material-icons font-16 m-b-0">check_box_outline_blank</i> '+req.requirement+'<br>');
        });

        var visibility_icon = container.find('div.header i.visibility').first();
        set_visibility(visibility_icon, node.li_attr['data-client_visibility']);

        var notification_icon = container.find('div.header i.notification').first();
        set_notification(notification_icon, node.li_attr['data-client_notification']);

        var description = (node.li_attr['data-description'] === null) ? '' : node.li_attr['data-description'];
        container.find('p.description').first().text(description);

        var weight = node.li_attr['data-weight'];
        var weight_bar = container.find('.progress-bar.weight').first();
        weight_bar.css('width', weight+'%');
        weight_bar.text((Math.round(weight*100)/100)+'%');

        var status_changer_v = node.li_attr['data-status_changer'];
        var stage_status_v = node.li_attr['data-stage_status'];
        var reason_v = node.li_attr['data-reason'];

        if ( status_changer_v === 'true' ) {
            container.find('.status-changer').first().show();
            container.find('.stage-status').first().text(stage_status_v);
            container.find('.reason').first().text(reason_v);
        }

        $('#details-container').append(container);
        container.show();
    };

    template_edit = function (node) {
        $('#details-container').html('');
        var container = $('#template-edit').clone();
        container.attr('id', 'template-edit-'+node.li_attr['data-id']);

        container.find('input[name="id"]').first().val(node.li_attr['data-id']);
        container.find('input[name="title"]').first().val(node.text);
        autosize(container.find('textarea[name="description"]').first());
        container.find('textarea[name="description"]').first().val(node.li_attr['data-description']);
        container.find('select[name="project_category_id"]').first().val(node.li_attr['data-project_category_id']);

        validateMaterial(container.find('form').first());
        container.find('form').first().on('submit', function (e) {
            e.preventDefault();
            var data = $(this).serialize();
            var template_id = $(this).find('input[name="id"]').first().val();

            $.post('/template/update_template/'+template_id, data, function (json) {
                var json_obj = JSON.parse(json);

                if (json_obj.error === false) {
                    $('#template-tree').jstree('refresh');
                    sweetNotification("Bien!", "Plantilla actualizada correctamente", "success", 1000);
                } else {
                    sweetNotification("Error!", json_obj.message, "error");
                }

            });

        });

        $('#details-container').append(container);
        container.show();
        autosize.update(container.find('textarea[name="description"]').first());
    };

    stage_edit = function (node) {
        $('#details-container').html('');
        var container = $('#stage-edit').clone();
        container.attr('id', 'stage-edit-'+node.li_attr['data-id']);

        container.find('input[name="id"]').first().val(node.li_attr['data-id']);
        container.find('input[name="title"]').first().val(node.text);
        autosize(container.find('textarea[name="description"]').first());
        container.find('textarea[name="description"]').first().val(node.li_attr['data-description']);

        var notification_input = container.find('input[name="client_notification"]').first();
        var notification_icon = container.find('i.notification').first();
        var visibility_input = container.find('input[name="client_visibility"]').first();
        var visibility_icon = container.find('i.visibility').first();

        notification_input.val(set_notification(notification_icon, node.li_attr['data-client_notification']));
        visibility_input.val(set_visibility(visibility_icon, node.li_attr['data-client_visibility']));

        visibility_icon.on('click', function () {
            var state = toggle_visibility($(this), visibility_input.val());
            visibility_input.val(state);
            if ( state === 'false' ) {
                notification_input.val('false');
                set_notification(notification_icon, 'false');
            }
        });
        notification_icon.on('click', function () {
            if ( visibility_input.val() === 'false' ) { return; }
            notification_input.val(toggle_notification(notification_icon, notification_input.val()));
        });

        validateMaterial(container.find('form').first());
        container.find('form').first().on('submit', function (e) {
            e.preventDefault();
            var data = $(this).serialize();
            var stage_id = $(this).find('input[name="id"]').first().val();

            $.post('/template/update_stage/'+stage_id, data, function (json) {
                var json_obj = JSON.parse(json);

                if (json_obj.error === false) {
                    $('#template-tree').jstree('refresh_node', node.parent);
                    sweetNotification("Bien!", "Fase actualizada correctamente", "success", 1000);
                } else {
                    sweetNotification("Error!", json_obj.message, "error");
                }

            });

        });

        $('#details-container').append(container);
        container.show();
        autosize.update(container.find('textarea[name="description"]').first());

        var slider = container.find('div.weight-slider').first();
        slider.attr('id', 'weight-slider-'+node.li_attr['data-id']);
        var sss = document.getElementById(slider.attr('id'));

        noUiSlider.create(sss, {
            start: [node.li_attr['data-weight']],
            connect: 'lower',
            step: 0.01,
            range: {
                'min': [0.01],
                'max': [100]
            }
        });

        var weight_input = container.find('input[name="weight"]').first();
        weight_input.val(node.li_attr['data-weight']);

        sss.noUiSlider.on('update', function( values, handle ) {
            var value = values[handle];
            weight_input.val(value);
         });

        weight_input.on('change', function () {
            sss.noUiSlider.set($(this).val());
        });
    };

    task_edit = function (node) {
        $('#details-container').html('');
        var container = $('#task-edit').clone();
        container.attr('id', 'task-edit-'+node.li_attr['data-id']);
        container.find('input[name="id"]').first().val(node.li_attr['data-id']);
        container.find('input[name="title"]').first().val(node.text);
        autosize(container.find('textarea[name="description"]').first());
        container.find('textarea[name="description"]').first().val(node.li_attr['data-description']);

        var notification_input = container.find('input[name="client_notification"]').first();
        var notification_icon = container.find('i.notification').first();
        var visibility_input = container.find('input[name="client_visibility"]').first();
        var visibility_icon = container.find('i.visibility').first();

        notification_input.val(set_notification(notification_icon, node.li_attr['data-client_notification']));
        visibility_input.val(set_visibility(visibility_icon, node.li_attr['data-client_visibility']));

        visibility_icon.on('click', function () {
            var state = toggle_visibility($(this), visibility_input.val());
            visibility_input.val(state);
            if ( state === 'false' ) {
                notification_input.val('false');
                set_notification(notification_icon, 'false');
            }
        });
        notification_icon.on('click', function () {
            if ( visibility_input.val() === 'false' ) { return; }
            notification_input.val(toggle_notification(notification_icon, notification_input.val()));
        });

        var day_type = node.li_attr['data-day_type'];
        container.find('input[value="'+day_type+'"]').first().attr('checked', true);

        var max_hours = (day_type === 'laborable') ? node.li_attr['data-laborable_hours'] : 24;

        container.find('input[type="radio"][name="day_type"]').on('change', function () {
            var max = (this.value === 'laborable') ? node.li_attr['data-laborable_hours'] : 24;
            var duration_input = $('#duration-horas');

            duration_input.attr('max', max);

            if ( duration_input.val() > max ) {
                duration_input.val(max);
                container.find('input[name="duration"]').first().val($('#duration-dias').val()+'d,'+max+'h');
            }
        });

        var durPicker = container.find('input[name="duration"]').first().durationPicker({
            dias: {
                label: 'd',
                min: 0,
                max: 99
            },
            horas: {
                label: 'h',
                min: 0,
                max: max_hours
            },
            responsive: false
        });

        durPicker.setvalues({
            dias: node.li_attr['data-duration_days'],
            horas: node.li_attr['data-duration_hours']
        });

        container.find('input[name="duration"]').first().val(node.li_attr['data-duration_days']+'d,'+node.li_attr['data-duration_hours']+'h');
        container.find('.durationpicker-innercontainer').css('font-size', '26px');

        var status_changer_v = node.li_attr['data-status_changer'];
        var stage_status_v = node.li_attr['data-stage_status_id'];
        var reason_v = node.li_attr['data-reason_id'];

        var status_changer = container.find('input[name="status_changer"]').not('[type="hidden"]').first();
        var stage_status = container.find('select[name="stage_status_id"]').first();
        var reason = container.find('select[name="reason_id"]').first();

        status_changer.on('change', function () {
            if ( $(this).is(':checked') ) {
                stage_status.attr('disabled', false).closest('.form-line').removeClass('disabled');
                reason.attr('disabled', false).closest('.form-line').removeClass('disabled');
            } else {
                stage_status.attr('disabled', true).closest('.form-line').addClass('disabled');
                reason.attr('disabled', true).closest('.form-line').addClass('disabled');
            }
        });

        stage_status.on('change', function () {
            reason.find('option[data-stage_status!="'+stage_status.val()+'"]').hide();
            var value = reason.find('option[data-stage_status="'+stage_status.val()+'"]').show().first().val();
            reason.val(value).change();
        });

        if ( status_changer_v === 'true' ) {
            status_changer.attr('checked', true).change();
            stage_status.val(stage_status_v).change();
            reason.val(reason_v).change();
        }

        $.get('/template/get_prerequisites_json/'+$('#template-tree').data('id')+'/'+node.li_attr['data-id'], function (json) {
            var json_obj = JSON.parse(json);

            if ( json_obj.error === false ) {
                var candidates = json_obj.data;
                var preSelect = container.find('select[name=prerequisites\\[\\]]').first();
                var prereqs_json = JSON.parse(node.li_attr['data-prerequisites']);

                var prereqs = [];
                $.each(prereqs_json, function (i, pre) {
                    prereqs[pre.tpl_task_prerequisite] = true;
                });

                preSelect.html('');
                var htmlSelect = '';
                var optgroup = '';
                $.each(candidates, function (i, candidate) {
                    if ( optgroup !== candidate.stage_id ) {
                        preSelect.append('<optgroup label="'+candidate.stage+'" data-stage-id="'+candidate.stage_id+'"></optgroup>');
                        optgroup = candidate.stage_id;
                    }

                    var selected = (prereqs[candidate.task_id] === undefined) ? '' : 'selected';
                    var disabled = (candidate.t1 > 0 || candidate.t2 > 0 || candidate.t3 > 0 || candidate.t4 > 0 || candidate.t5 > 0) ? 'disabled' : '';
                    var label_color = (disabled === 'disabled') ? 'bg-grey' : 'bg-orange';
                    var message = (disabled === 'disabled') ? 'Este prerequisito causaría un bucle.' : '';
                    htmlSelect = '<option value="'+candidate.task_id+'" '+selected+' '+disabled+' data-content="<span class=\'label '+label_color+'\'>'+candidate.task_title+'</span> <small>'+message+'</small>">'+candidate.task_title+'</option>';

                    preSelect.find('optgroup[data-stage-id="'+candidate.stage_id+'"]').append(htmlSelect);
                });

                preSelect.selectpicker();
            }

        });


        var posSelect = container.find('select[name=positions\\[\\]]').first();
        var positions = JSON.parse(node.li_attr['data-positions']);

        $.each(positions, function (i, position) {
            posSelect.find('option[value="'+position.position_id+'"]').attr('selected', true);
        });

        posSelect.selectpicker();

        var tags = JSON.parse(node.li_attr['data-tags']);
        var tags_input = container.find('input[name="tags"]').first();
        var tags_text = '';
        $.each(tags, function (i, tag) {
            tags_text += ','+tag.tag;
        });

        tags_input.val(tags_text);
        tags_input.tagsinput();

        var requirements = JSON.parse(node.li_attr['data-requirements']);
        var req_add_point = container.find('.add-req-group').first();
        $.each(requirements, function (i, req) {
            var input = container.find('.req-input-model').first().clone();

            input.removeClass('req-input-model');
            input.find('input').first().attr('name', 'requirements[req_'+req.id+']');
            input.find('input').first().val(req.requirement);
            input.insertBefore(req_add_point);
            input.removeClass('hidden');
        });

        validateMaterial(container.find('form').first());
        container.find('form').first().on('submit', function (e) {
            e.preventDefault();
            var data = $(this).serialize();
            var task_id = $(this).find('input[name="id"]').first().val();

            $.post('/template/update_task/'+task_id, data, function (json) {
                var json_obj = JSON.parse(json);

                if (json_obj.error === false) {
                    console.log(node);

                    $('#template-tree').jstree('refresh_node', node.parent);
                    $('#template-tree').jstree('select_node', node.parent);

                    sweetNotification("Tarea Actualizada!", "La tarea fue actualizada correctamente.", "success", 1000);
                } else {
                    sweetNotification("Error!", json_obj.message, "error");
                }
            });

        });

        $('#details-container').append(container);
        container.show();

        autosize.update(container.find('textarea[name="description"]').first());

        var slider = container.find('div.weight-slider').first();
        slider.attr('id', 'weight-slider-'+node.li_attr['data-id']);
        var sss = document.getElementById(slider.attr('id'));

        noUiSlider.create(sss, {
            start: [node.li_attr['data-weight']],
            connect: 'lower',
            step: 0.01,
            range: {
                'min': [0.01],
                'max': [100]
            }
        });

        var weight_input = container.find('input[name="weight"]').first();
        weight_input.val(node.li_attr['data-weight']);

        sss.noUiSlider.on('update', function( values, handle ) {
            var value = values[handle];
            weight_input.val(value);
        });

        weight_input.on('change', function () {
            sss.noUiSlider.set($(this).val());
        });
    };

    toggle_visibility = function ($icon, visibility) {
        if ( visibility === 'true' ) {
            $icon.removeClass('col-blue');
            $icon.addClass('col-grey');
            $icon.text('visibility_off');
            $icon.attr('data-original-title', 'No visible para el cliente');
            return 'false';
        } else {
            $icon.removeClass('col-grey');
            $icon.addClass('col-blue');
            $icon.text('visibility');
            $icon.attr('data-original-title', 'Visible para el cliente');
            return 'true';
        }
    };

    set_visibility = function ($icon, visibility) {
        visibility = (visibility === 'true') ? 'false' : 'true';
        return toggle_visibility($icon, visibility);
    };

    toggle_notification = function ($icon, notification) {
        if ( notification === 'true' ) {
            $icon.removeClass('col-amber');
            $icon.addClass('col-grey');
            $icon.text('notifications_off');
            $icon.attr('data-original-title', 'No notificar al cliente');
            return 'false';
        } else {
            $icon.removeClass('col-grey');
            $icon.addClass('col-amber');
            $icon.text('notifications');
            $icon.attr('data-original-title', 'Notificar al cliente');
            return 'true';
        }
    };

    set_notification = function ($icon, notification) {
        notification = (notification === 'true') ? 'false' : 'true';
        return toggle_notification($icon, notification);
    };

    $(document).on('click', 'a.add-req', function () {
        var input = $(this).closest('.card').find('.req-input-model').first().clone();

        input.removeClass('req-input-model');
        input.find('input').first().attr('name', 'requirements[]');
        input.insertBefore($(this).closest('.add-req-group'));
        input.removeClass('hidden');
    });

    $(document).on('click', 'a.delete-req', function () {
        $(this).closest('.input-group').remove();
    });

    default_details();
    if ( $('#details-container').size() > 0 ) { $('#details-container').sticky({topSpacing:85}); }

    if ( $('#new-template-body').size() > 0 ) {
        var redirigido = false;
        swal(
            {
                title: "Crear Plantilla",
                text: "Escribe el nombre de la nueva plantilla:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Nombre Plantilla",
                showLoaderOnConfirm: true
            },
            function(inputValue){

                if ( inputValue === false ) {
                    window.location.href = "/template";
                    return;
                }

                if (inputValue === "") {
                    swal.showInputError("Debe escribir un nombre.");
                    return false
                }

                $.post('/template/new_template', {title: inputValue}, function (json) {
                    var obj = JSON.parse(json);

                    if (obj.error === true) {
                        swal.showInputError(obj.message);
                        return false;
                    }

                    sweetNotification("Bien!", "Creado correctamente!", "success", 1000);

                    window.location.href = "/template/edit/"+obj.data.id;
                });
            }
        );

    }
});
