/* LIST */

if ( $('#action-table').size() > 0 ) {

	var $table = $("#action-table");
	$table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/action/datatable_json'
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'id' },
            { data: 'name' },
            { data: 'method' },
            { data: 'active' },
            {
                data: function(data) {
                    return 	'<div class="btn-group"><a href="/action/edit/'+data.id+'" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>'+
                        '<a href="javascript:void(0);" class="btn btn-danger btn-xs waves-effect" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a></div>'
                },
                orderable: false,
                searchable: false
            },
        ]
    }));

}
