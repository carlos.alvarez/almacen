$(document).ready(function () {

    /* LIST */
    if ( $('#areas-table').size() > 0 ) {
        var $table = $("#areas-table");
        ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
            ajax: {
                url: '/area/datatable_json'
            },
            columns: [
                {
                    data: function(data) {
                        return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                            '<label class="m-b-0" for="check-'+data.id+'"></label>';
                    },
                    orderable: false,
                    searchable: false
                },
                { data: 'id' },
                {
                    data: 'name',
                    render: function(value, type, obj, meta) {
                        var active = (obj.active == "true") ? "col-green" : "col-red";
                        return "<a href='/area/view/"+obj.id+"' target='_blank'><span><i class='material-icons " + active + " font-18' aria-hidden='true'>album</i> " + obj.name + "</span></a>"
                    }
                },
                { data: 'short_name' },
                { data: 'manager' },
                { data: 'department_name' },
                { data: 'company_name' },
                {
                    data: function(data) {
                        var buttons = '<div class="btn-group">';
                        if (data.edit === 'true') {
                            buttons += '<a href="/area/edit/'+data.id+'" data-id="'+data.id+'" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                        }
                        if (data.delete === 'true') {
                            buttons += '<a class="btn btn-danger btn-xs waves-effect delete-btn" data-id="'+data.id+'" data-controller="area" data-text="¿Está seguro que desea eliminar esta área?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                        }
                        buttons += '</div>';
                        return buttons;
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        }));
    }

    if ( $('#area-new-form').size() > 0 ) {
        $('select[name="company_id"]').on('change', function() {
            var company = $(this).val();
            var count = 0;
            $('select[name="department_id"] option').each(function () {
                if ( $(this).val() === '' ) { return; }

                if ( $(this).data('company') == company ) {
                    $(this).show();
                    count++;
                } else {
                    $(this).hide();
                }
            });
            $('select[name="department_id"]').val('').selectpicker('refresh');
            if ( count > 0 ) {
                $('select[name="department_id"]').first().closest('.form-line').removeClass('disabled');
            } else {
                $('select[name="department_id"]').first().closest('.form-line').addClass('disabled');
            }
        });
    }

    if ( $('#area-edit-form').size() > 0 ) {
        $('select[name="company_id"]').on('change', function() {
            var company = $(this).val();
            var count = 0;
            $('select[name="department_id"] option').each(function () {
                if ( $(this).val() === '' ) { return; }

                if ( $(this).data('company') == company ) {
                    $(this).show();
                    count++;
                } else {
                    $(this).hide();
                }
            });
            $('select[name="department_id"]').val('').selectpicker('refresh');
            if ( count > 0 ) {
                $('select[name="department_id"]').first().closest('.form-line').removeClass('disabled');
            } else {
                $('select[name="department_id"]').first().closest('.form-line').addClass('disabled');
            }
        });
    }
});
