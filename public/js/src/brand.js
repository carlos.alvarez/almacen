/* LIST */
(function ($) {
    if ($('#brands-table').size() > 0) {
        var $table = $('#brands-table');
        ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
            ajax: {
                url: '/brand/get_brands_datatable_json'
            },
            columns: [
                {
                    data: function (data) {
                        return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                            '<label class="m-b-0" for="check-'+data.id+'"></label>';
                    },
                    orderable: false,
                    searchable: false
                },
                { data: 'id'},
                { data: 'name'},
                {
                    data: 'created_at',
                    render: function (value, type, obj, meta) {
                        return obj.created_at_formatted;
                    }
                },
                {
                    data: 'updated_at',
                    render: function (value, type, obj, meta) {
                        return obj.updated_at_formatted;
                    }
                },
                {
                    data: function(data) {
                        var buttons = '<div class="btn-group">';
                        if (data.edit === 'true') {
                            buttons += '<a data-id="'+data.id+'" target="_blank" class="btn btn-info btn-xs waves-effect edit-brand" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                        }
                        if (data.delete === 'true') {
                            buttons += '<a href="javascript:void(0);" class="btn btn-danger btn-xs waves-effect delete-btn" data-id="'+data.id+'" data-controller="brand" data-text="¿Está seguro que desea eliminar esta marca?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                        }
                        buttons += '</div>';
                        return buttons;
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        }));
        // EDIT BRAND
        $(document).on('click', 'a.edit-brand', function (e) {
            e.preventDefault();
            var button = $(this);
            swal({
                title: "Editar Marca",
                text: "Escriba el nuevo nombre para la marca:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Nombre de la Marca",
                showLoaderOnConfirm: true
            }, function (inputValue) {
                if (inputValue !== false && inputValue === "") {
                    swal.showInputError("Debe escribir un nombre.");
                    return false;
                } else if (inputValue !== false){
                    $.post('/brand/edit/'+button.data('id'),{name:inputValue}, function (response) {
                        var res = JSON.parse(response);

                        if (res.error === true){
                            swal.showInputError(res.message);
                            return false;
                        }
                        $table.DataTable().ajax.reload();
                        sweetNotification("¡Bien!","¡Ajuste realizado!", "success", 1000);
                    });
                }
            });
        });
    }
})(jQuery);