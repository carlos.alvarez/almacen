function padToSix(number) {
    if (number<=999999) { number = ("00000"+number).slice(-6); }
    return number;
}

(function ($){

    if ( $('#gantter').size() > 0 ) {

        $.get('/project/gantt_client/'+$('#gantter').data('project_id'), function (json) {
            var obj = JSON.parse(json);

            var g = new JSGantt.GanttChart(document.getElementById('gantter'), 'day');
            g.addLang('es', {
                january:        'Enero',
                february:       'Febrero',
                march:          'Marzo',
                april:          'Abril',
                maylong:	    'Mayo',
                june:	        'Junio',
                july:	        'Julio',
                august:	        'Agosto',
                september:	    'Septiembre',
                october:	    'Octubre',
                november:	    'Noviembre',
                december:	    'Diciembre',
                jan:	        'Ene',
                feb:	        'Feb',
                mar:	        'Mar',
                apr:	        'Abr',
                may:	        'May',
                jun:	        'Jun',
                jul:	        'Jul',
                aug:	        'Ago',
                sep:	        'Sep',
                oct:	        'Oct',
                nov:	        'Nov',
                dec:	        'Dic',
                sunday:	        'Domingo',
                monday:	        'Lunes',
                tuesday:	    'Martes',
                wednesday:	    'Miércoles',
                thursday:	    'Jueves',
                friday:	        'Viernes',
                saturday:	    'Sábado',
                sun:	        'Dom',
                mon:	        'Lun',
                tue:	        'Mar',
                wed:	        'Mié',
                thu:	        'Jue',
                fri:	        'Vie',
                sat:	        'Sáb',
                resource:	    'Recurso',
                duration:	    'Duración',
                comp:	        '% Prog.',
                completion:	    'Progreso',
                startdate:	    'Fecha Inicio',
                enddate:	    'Fecha Fin',
                moreinfo:	    'Más info',
                notes:	        'Notas',
                format:	        'Formato',
                hour:	        'Hora',
                day:	        'Día',
                week:	        'Semana',
                month:	        'Mes',
                quarter:	    'Trimestre',
                hours:	        'Horas',
                days:	        'Días',
                weeks:	        'Semanas',
                months:	        'Meses',
                quarters:	    'Trimestres',
                hr:	            'Hr',
                dy:	            'Día',
                wk:	            'Sem',
                mth:	        'Mes',
                qtr:	        'Trim',
                hrs:	        'Hrs',
                dys:	        'Días',
                wks:	        'Semns',
                mths:	        'Meses',
                qtrs:	        'Trims'
            });
            g.setLang('es');
            g.setFormatArr("day","week","month");
            g.setShowRes(0);
            g.setUseSort(0);

            g.setDayMajorDateDisplayFormat('d/mon/yy'); // Set format to display dates ('mm/dd/yyyy', 'dd/mm/yyyy', 'yyyy-mm-dd')
            g.setDateTaskTableDisplayFormat('d/mon/yy');
            g.setWeekMinorDateDisplayFormat('d mon');

            var proj = obj.project;
            g.AddTaskItem(new JSGantt.TaskItem('1'+padToSix(proj.id), proj.name, '', '', 'ggroupblack', '/project/view/'+proj.id, 0, '', 0, 1, 0, 1, '', '', proj.description, g));
            $.each(obj.templates, function (index, template) {
                g.AddTaskItem(new JSGantt.TaskItem('3'+padToSix(template.id), template.title, '', '', 'ggroupblack', '', 0, '', 0, 1, '1'+padToSix(proj.id), 1, '', '', template.description, g));
                $.each(obj.stages, function (index_s, stage) {
                    if ( stage.template_id !== template.id ) { return; }
                    var parent = (stage.template_id) ? '3'+padToSix(stage.template_id) : '1'+padToSix(proj.id);
                    g.AddTaskItem(new JSGantt.TaskItem('2'+padToSix(stage.id), stage.title, stage.start_date, stage.g_end_date, 'ggroupblack', '', 0, '', 0, 1, parent, 1, '', '', stage.description, g));
                    if ( proj.client_level_visibility === 'task' ) {
                        $.each(obj.tasks, function (index_t, task) {
                            if ( task.stage_id !== stage.id ) { return; }
                            g.AddTaskItem(new JSGantt.TaskItem(task.id, task.title, task.start_date, task.g_end_date, 'gtaskblue', '/task/view/'+task.id, 0, '', task.progress, 0, '2'+padToSix(task.stage_id), 1, task.prerequisites, '', task.description, g));
                        });
                    }
                });
            });
            /*$.each(obj.stages, function (index, stage) {
                var parent = (stage.template_id) ? '3'+padToSix(stage.template_id) : '1'+padToSix(proj.id);
                g.AddTaskItem(new JSGantt.TaskItem('2'+padToSix(stage.id), stage.title, stage.start_date, stage.g_end_date, 'ggroupblack', '', 0, '', 0, 1, parent, 1, '', '', stage.description, g));
            });
            if ( proj.client_level_visibility === 'task' ) {
                $.each(obj.tasks, function (index, task) {
                    g.AddTaskItem(new JSGantt.TaskItem(task.id, task.title, task.start_date, task.g_end_date, 'gtaskblue', '/task/view/'+task.id, 0, '', task.progress, 0, '2'+padToSix(task.stage_id), 1, task.prerequisites, '', task.description, g));
                });
            }*/
            g.Draw();
        });
    }

    // COMMENTS
    if ( $('#comments-panel').size() > 0 ) {
        $.get('/project_client/get_comments/'+$('#comments-panel').data('project_id'), function (html) {
            $('#comments-panel').html(html);
        });

        $('#send-comment').click(function () {
            var data = $('#comment-form').serializeArray();
            $.post('/project/new_comment', data, function (json) {
                var json_obj = JSON.parse(json);
                if ( json_obj.error ) {
                    sweetNotification('Error', json_obj.message, 'error');
                } else {
                    $('#comment-form').find('input[name="message"]').val('');
                    $.get('/project_client/get_comments/'+$('#comments-panel').data('project_id'), function (html) {
                        $('#comments-panel').html(html);
                    });
                }
            })
        });
    }
    // FILES
    if ( $('#files-table').size() > 0 ) {
        var $table = $('#files-table');
        var oTable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
            ajax: {
                url: '/project_client/files_datatable_json/'+$table.data('project_id')
            },
            columns: [
                {
                    data: function(data) {
                        return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                            '<label class="m-b-0" for="check-'+data.id+'"></label>';
                    },
                    orderable: false,
                    searchable: false
                },
                { data: 'id' },
                {
                    data: 'name',
                    render: function (value, type, obj, meta) {
                        return '<a href="/'+obj.path+'" target="_blank"><img src="/'+obj.icon+'"> '+value+'</a>';
                    }
                },
                { data: 'description' },
                { data: 'user' },
                { data: 'created_at', render: function(value, type, obj) { return obj.created_at_nice; } },
                { data: 'updated_at', render: function(value, type, obj) { return obj.updated_at_nice; } }
            ]
        }));

        $(document).on('click', '.delete-file', function () {
            $.post('/project/delete_file/'+$(this).data('id'), function (json) {
                var json_obj = JSON.parse(json);
                if ( json_obj.error ) {
                    sweetNotification('Error', json_obj.message, 'error');
                } else {
                    oTable.ajax.reload();
                }
            });
        });

        $('#file-input').click(function () {
            $('#project_file').click();
        });

        $('#project_file').change(function () {
            if ( $('#project_file')[0].files.length > 0 ) {
                $('#file-name').text($('#project_file')[0].files[0].name).show();
            } else {
                $('#file-name').text('').hide();
            }
        });

        $('#save-file').click(function () {
            var project_id = $('#save-file').data('project_id');
            var data = new FormData();

            if ( $('#project_file')[0].files.length === 0 ) {
                sweetNotification('Error', 'Debe seleccionar un archivo', 'error');
                return;
            }

            $('#file-spinner').show();

            data.append('project_file', $('#project_file')[0].files[0]);
            data.append('description', $('#project_file_description').val());
            data.append('client_visibility', 'true');

            $.ajax( {
                url: '/project/new_file/'+project_id,
                type: 'POST',
                data: data,
                processData: false,
                contentType: false
            }).done(function( json ) {
                var json_obj = JSON.parse(json);
                if ( json_obj.error ) {
                    sweetNotification('Error', json_obj.message, 'error');
                } else {
                    oTable.ajax.reload();
                    $('#project_file_description').val('');
                    $('#project_file').val('');
                    $('#file-name').text('').hide();
                }
                $('#file-spinner').hide();
            });

        });
    }
})(jQuery);