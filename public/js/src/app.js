/**
 * Created by Carlos Alvarez on 2/16/2017.
 */

$(document).ready(function () {

    /* Styles for validation */
    if ( typeof ($.fn.validate) !== 'undefined' ) {
        validateMaterial = function($form) {
            $form.validate({
                highlight: function (input) {
                    $(input).closest('.form-line').addClass('focused error');
                },
                unhighlight: function (input) {
                    $(input).closest('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).closest('.form-group,.input-group').append(error);
                }
            });
        };

        validateMaterial($('form.form-validate'));
    }
    /* Styles for validation - END */

    /* checkbox select for table */
    if($('table th input:checkbox').size() > 0){
        $('table th input:checkbox').on('change', function () {
            var $this = this;

            $(this).closest('table').find('tr > td:first-child input:checkbox')
                .each(function(){
                   this.checked = $this.checked;
                   $(this).closest('tr').toggleClass('selected');
                });
        })
    }
    /* End checkbox for table */

    /* Avatar inputs code */
    $('.avatar a.avatar-edit').on('click', function() {
        $(this).siblings('input.avatar-input').click();
    });

    $('.avatar input.avatar-input').on('change', function() {
        var $this = $(this);

        if ( $this.val() != '' ) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $this.parent('.avatar').css('background-image', 'url('+e.target.result+')');
            };

            reader.readAsDataURL($this[0].files[0]);
        }
    });
    /* Avatar inputs code - END */

    var $item = $('.sidebar li[data-controller="'+window.location.pathname.split( '/' )[1]+'"]');
    $item.addClass('active').parents('li').find('a.menu-toggle').click();

    /*if ( history.length === 0 ) {
        $('.go_back_button').prop('disabled', true).attr('data-original-title', 'No hay hacia donde volver').tooltip('hide').mouseover();
    }*/

    $('.go_back_button').click(function () {
        if ( history.back() === undefined ) {
            $(this).prop('disabled', true).attr('data-original-title', 'No hay hacia donde volver').tooltip('hide').mouseover();
        }
    });

});

/* Customized notifications */
notification = function(message, type) {
    var icon = '';
    switch (type) {
        case 'success':
            icon = '<i class="material-icons font-16">check_circle</i> ';
            break;
        case 'danger':
            icon = '<i class="material-icons font-16">cancel</i> ';
            break;
        case 'warning':
            icon = '<i class="material-icons font-16">warning</i> ';
            break;
        case 'info':
        default:
            icon = '<i class="material-icons font-16">info</i> ';
            type = 'info';
    }

    $.notify(icon+message, {"type": type});
};

sweetNotification = function(title, text, type, timer) {
    switch (type) {
        case 'success':
        case 'info':
        case 'warning':
        case 'error':
            break;
        case 'danger':
            type = 'error';
            break;
        default:
            type = 'info';
    }

    var obj = { title: title, text: text, type: type };
    if ( timer !== undefined ) { obj.timer = timer; }

    swal(obj);
};

var formmatterCurrency = function (number, currency) {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: currency === undefined ? 'USD' : currency,
        minimumFractionDigits: 2
    });

    return formatter.format(number);
};

preventExit = function (e) {
    var message = "Si abandona ahora la página perderá los cambios. ¿Desea salir de todas formas?",
        e = e || window.event;
    // For IE and Firefox
    if (e) {
        e.returnValue = message;
    }

    // For Safari
    return message;
};

var documentTitle = function () {
    var pathArray = window.location.pathname.split( '/' );
    return pathArray[1].toUpperCase()+' | Ergospace';
};

var DATATABLE_TEMPLATE = {
    language: { url: "/language/datatable" },
    processing: true,
    /*select: {
        style:    'multi',
        selector: 'td:first-child'
    },*/
    serverSide: true,
    responsive: true,
    ajax: {
        type: 'POST'
    },
    columnDefs: [ {
        orderable: false,
        width: "1px",
        /*className: 'select-checkbox',*/
        targets: 0
    } ],
    order: [ [1, 'desc'] ],
    dom: "<'row'<'col-sm-4 m-b-0 align-center-xs'B><'col-sm-4 m-b-0 align-center'l><'col-sm-4 m-b-0'f>>rt<'row DTTTFooter'<'col-sm-6 m-b-0'i><'col-sm-6 m-b-0'p>>",
    buttons: [
        {
            extend: "copy",
            text: "<i class='material-icons' data-toggle='tooltip' data-original-title='Copiar'>content_copy</i>",
            className: "btn-sm",
            exportOptions: {
                columns: '.copy,.all'
            }
        },
        {
            extend: "csv",
            title: documentTitle(),
            text: "<i class='material-icons' data-toggle='tooltip' data-original-title='CSV'>attachment</i>",
            className: "btn-sm",
            exportOptions: {
                columns: '.csv,.all'
            }
        },
        {
            extend: "excel",
            title: documentTitle(),
            text: "<i class='material-icons' data-toggle='tooltip' data-original-title='Excel'>blur_linear</i>",
            className: "btn-sm",
            exportOptions: {
                columns: '.excel,.all'
            }
        },
        {
            extend: "pdfHtml5",
            title: documentTitle(),
            text: "<i class='material-icons' data-toggle='tooltip' data-original-title='PDF'>picture_as_pdf</i>",
            className: "btn-sm",
            exportOptions: {
                columns: '.pdf,.all'
            }
        },
        {
            extend: "print",
            title: documentTitle(),
            text: "<i class='material-icons' data-toggle='tooltip' data-original-title='Imprimir'>print</i>",
            className: "btn-sm",
            exportOptions: {
                columns: '.print,.all'
            }
        }
    ],
    fnDrawCallback : function(oSettings) {
        var columns_in_row = $(this).children('thead').children('tr').children('th').length;
        var show_num = oSettings._iDisplayLength;
        var tr_count = $(this).children('tbody').children('tr').length;
        if (show_num > tr_count && oSettings.fnRecordsDisplay() > 0) {
            var row = '<tr>'+'<td>&nbsp;</td>'.repeat(columns_in_row)+'</tr>';
            $(this).children('tbody').append(row.repeat(show_num - tr_count));
        }
        this.api().columns.adjust();
    }
};

$('body').tooltip({
    selector: '[data-toggle="tooltip"]',
    container: 'body'
});

$('ul.ml-menu').each(function() {
    if ( $(this).find('li').not('.hide').length === 0 ) {
        $(this).closest('li').hide();
    }
});

$('body').popover({ selector: '[data-rel=popover]',html: true, container: 'body', trigger: 'click hover', delay: {show: 50, hide: 100}});

$(document).on('mouseover', 'a[data-rel="ajax_popover"]', function () {
    var $element = $(this),
        $data = $element.data();
    // set a loader image, so the user knows we're doing something
    /*$data.content = '<div class="preloader pl-size-sm">\n' +
        '                                    <div class="spinner-layer pl-cyan">\n' +
        '                                        <div class="circle-clipper left">\n' +
        '                                            <div class="circle"></div>\n' +
        '                                        </div>\n' +
        '                                        <div class="circle-clipper right">\n' +
        '                                            <div class="circle"></div>\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                </div>';
    $element.popover({
        html: true,
        trigger: 'hover'
    }).popover('show');*/
    // retrieve the real content for this popover, from location set in data-href
    $.get($data.url, function (response) {
        // set the ajax-content as content for the popover
        $data.content = response;
        // replace the popover
        //$element.popover('destroy');
        $element.popover({
            html: true,
            container: 'body',
            trigger: 'hover'
        });
        // check that we're still hovering over the preview, and if so show the popover
        if ($element.is(':hover')) {
            $element.popover('show');
        }
        $element.attr('data-rel', 'popover');
    });
});

$('input.datepicker').datepicker({
    changeYear: true
});

$(document).on('click', '.delete-btn', function (e) {
    e.preventDefault();
    var $btn = $(this);
    swal({
        title: "Eliminar",
        text: "¿Está seguro que desea eliminar este elemento?",
        type: "warning",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "¡Si, eliminar!",
        closeOnConfirm: false
    },
    function(){
        $.post('/'+$btn.data('controller')+'/delete/'+$btn.data('id'), function (json) {
            var json_obj = JSON.parse(json);

            if ( json_obj.error == true ) {
                swal.showInputError(json_obj.message);
                return false;
            } else {
                sweetNotification("¡Bien!", "¡Eliminado correctamente!", "success", 1000);
                var table = $btn.closest('table');
                $(table).DataTable().ajax.reload();
            }
        });
	});
});

function clear(text){
    var text = /*text.toLowerCase(); // a minusculas
        text =*/ text.replace(/[áàäâå]/, 'a');
    text = text.replace(/[éèëê]/, 'e');
    text = text.replace(/[íìïî]/, 'i');
    text = text.replace(/[óòöô]/, 'o');
    text = text.replace(/[úùüû]/, 'u');
    text = text.replace(/[ýÿ]/, 'y');
    text = text.replace(/[ñ]/, 'n');
    text = text.replace(/[Ñ]/, 'N');
    text = text.replace(/[ç]/, 'c');
    text = text.replace(/['"]/, '');
    // text = text.replace(/[^a-zA-Z0-9-]/, '');
    text = text.replace(/\s/g, '_');
    text = text.replace(/(_)$/, '');
    text = text.replace(/^(_)/, '');
    return text;
}