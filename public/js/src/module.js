/* LIST */

if ( $('#modules-table').size() > 0 ) {

	var $table = $("#modules-table");
	$table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/module/datatable_json'
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'id' },
            { data: 'name' },
            { data: 'controller' },
            {
                data: 'active',
                render: function(value, type, obj, meta) {
                    var icon = '<i class="material-icons col-red">cancel</i>';
                    if ( value == 'true' ) {
                        icon = '<i class="material-icons col-green">check_circle</i>';
                    }
                    return icon;
                },
                searchable: false
            },
            {
                data: 'action_create',
                render: function(value, type, obj, meta) {
                    var icon = '<i class="material-icons col-red">cancel</i>';
                    if ( value == 'true' ) {
                        icon = '<i class="material-icons col-green">check_circle</i>';
                    }
                    return icon;
                },
                searchable: false
            },
            {
                data: 'action_view',
                render: function(value, type, obj, meta) {
                    var icon = '<i class="material-icons col-red">cancel</i>';
                    if ( value == 'true' ) {
                        icon = '<i class="material-icons col-green">check_circle</i>';
                    }
                    return icon;
                },
                searchable: false
            },
            {
                data: 'action_edit',
                render: function(value, type, obj, meta) {
                    var icon = '<i class="material-icons col-red">cancel</i>';
                    if ( value == 'true' ) {
                        icon = '<i class="material-icons col-green">check_circle</i>';
                    }
                    return icon;
                },
                searchable: false
            },
            {
                data: 'action_delete',
                render: function(value, type, obj, meta) {
                    var icon = '<i class="material-icons col-red">cancel</i>';
                    if ( value == 'true' ) {
                        icon = '<i class="material-icons col-green">check_circle</i>';
                    }
                    return icon;
                },
                searchable: false
            },
            {
                data: function(data) {
                    var buttons = '<div class="btn-group">';
                    if (data.edit === 'true') {
                        buttons += '<a href="/module/edit/'+data.id+'" data-id="'+data.id+'" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                    }
                    if (data.delete === 'true') {
                        buttons += '<a class="btn btn-danger btn-xs waves-effect delete-btn" data-id="'+data.id+'" data-controller="module" data-text="¿Está seguro que desea eliminar este módulo?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                    }
                    buttons += '</div>';
                    return buttons;
                },
                orderable: false,
                searchable: false
            }
        ]
    }));

}
