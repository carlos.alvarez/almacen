(function ($) {
    // CHARTS OBJECTS
    var statusProjectsChart = null;
    var categoriesProjectsChart = null;
    var companiesProjectsChart = null;
    var budgetsProjectsChart = null;
    //
    var $year_chart = $('#year_chart');
    var $status_chart =  $('#status_chart');
    var $company_chart = $('#company_chart');
    var $category_chart = $('#category_chart');
    var $brand_chart = $('#brand_chart');

    init_charts($year_chart.val(),$status_chart.val(),$company_chart.val(),$category_chart.val(),$brand_chart.val());
    // DROPDOWN FILTERS
    $year_chart.change(function () {
        statusProjectsChart.destroy();
        categoriesProjectsChart.destroy();
        companiesProjectsChart.destroy();
        budgetsProjectsChart.destroy();

        $('.loader').show();
        init_charts($year_chart.val(),$status_chart.val(),$company_chart.val(),$category_chart.val(),$brand_chart.val());
    });
    $status_chart.change(function () {
        statusProjectsChart.destroy();
        categoriesProjectsChart.destroy();
        companiesProjectsChart.destroy();
        budgetsProjectsChart.destroy();

        $('.loader').show();
        init_charts($year_chart.val(),$status_chart.val(),$company_chart.val(),$category_chart.val(),$brand_chart.val());
    });
    $company_chart.change(function () {
        statusProjectsChart.destroy();
        categoriesProjectsChart.destroy();
        companiesProjectsChart.destroy();
        budgetsProjectsChart.destroy();

        $('.loader').show();
        init_charts($year_chart.val(),$status_chart.val(),$company_chart.val(),$category_chart.val(),$brand_chart.val());
    });
    $category_chart.change(function () {
        statusProjectsChart.destroy();
        categoriesProjectsChart.destroy();
        companiesProjectsChart.destroy();
        budgetsProjectsChart.destroy();

        $('.loader').show();
        init_charts($year_chart.val(),$status_chart.val(),$company_chart.val(),$category_chart.val(),$brand_chart.val());
    });
    $brand_chart.change(function () {
        statusProjectsChart.destroy();
        categoriesProjectsChart.destroy();
        companiesProjectsChart.destroy();
        budgetsProjectsChart.destroy();

        $('.loader').show();
        init_charts($year_chart.val(),$status_chart.val(),$company_chart.val(),$category_chart.val(),$brand_chart.val());
    });

    function init_charts(year,status,company,category,brand){
        /*********************************
         *        Status Projects        *
         * *******************************/
        $.get('/chart/status_projects'+year+status+company+category+'/my_projects', function (response){
            $('#load-status-projects').hide();
            var resp = JSON.parse(response);
            if ( resp.error ) { return; }

            var bar_config = {
                type: 'pie',
                data: {
                    labels: resp.data.label,
                    datasets: [{
                        data: resp.data.data,
                        backgroundColor: resp.data.backgroundColor
                    }]
                },
                options: {
                    responsive: true,
                    // legend: false//,
                    tooltips: {
                        callbacks: {
                            title: function(tooltipItem, data) {
                                var total;
                                for (var i in data['datasets'][0]._meta){
                                    total = data['datasets'][0]._meta[i].total;
                                }
                                var percent = Math.round((data['datasets'][0]['data'][tooltipItem[0]['index']] / total) * 100);
                                var text = data['labels'][tooltipItem[0]['index']]+': '+percent+'%';

                                return text;
                            },
                            label: function(tooltipItem, data){
                                var data = data['datasets'][0]['data'][tooltipItem['index']];
                                return 'Cantidad: '+data;
                            }
                        }
                    }
                }
            };
            statusProjectsChart = new Chart(document.getElementById("status-projects-chart").getContext("2d"), bar_config);
        }); // END STATUS_PROJECTS

        /*********************************
         *       Categories Projects     *
         * *******************************/
        $.get('/chart/categories_projects'+year+status+company+category+'/my_projects', function (response){
            $('#load-categories-projects').hide();
            var resp = JSON.parse(response);
            if ( resp.error ) { return; }

            var bar_config = {
                type: 'pie',
                data: {
                    labels: resp.data.labels,
                    datasets: [{
                        data: resp.data.data,
                        backgroundColor: resp.data.backgroundColor
                    }]
                },
                options: {
                    responsive: true,
                    // legend: false//,
                    tooltips: {
                        callbacks: {
                            title: function(tooltipItem, data) {
                                var total;
                                for (var i in data['datasets'][0]._meta){
                                    total = data['datasets'][0]._meta[i].total;
                                }
                                var percent = Math.round((data['datasets'][0]['data'][tooltipItem[0]['index']] / total) * 100);
                                var text = data['labels'][tooltipItem[0]['index']]+': '+percent+'%';
                                return text;
                            },
                            label: function(tooltipItem, data){
                                var data = data['datasets'][0]['data'][tooltipItem['index']];
                                return 'Cantidad: '+data;
                            }
                        }
                    }
                }
            };
            categoriesProjectsChart = new Chart(document.getElementById("categories-projects-chart").getContext("2d"), bar_config);
        }); // END CATEGORIES PROJECTS*/

        /*********************************
         *       Companies Projects     *
         * *******************************/
        $.get('/chart/companies_projects'+year+status+company+category+'/my_projects', function (response){
            $('#load-companies-projects').hide();
            var resp = JSON.parse(response);
            if ( resp.error ) { return; }

            var bar_config = {
                type: 'pie',
                data: {
                    labels: resp.data.label,
                    datasets: [{
                        data: resp.data.data,
                        backgroundColor: resp.data.backgroundColor
                    }]
                },
                options: {
                    responsive: true,
                    // legend: false,
                    tooltips: {
                        callbacks: {
                            title: function(tooltipItem, data) {
                                var total;
                                for (var i in data['datasets'][0]._meta){
                                    total = data['datasets'][0]._meta[i].total;
                                }
                                var percent = Math.round((data['datasets'][0]['data'][tooltipItem[0]['index']] / total) * 100);
                                var text = data['labels'][tooltipItem[0]['index']]+': '+percent+'%';
                                return text;
                            },
                            label: function(tooltipItem, data){
                                var data = data['datasets'][0]['data'][tooltipItem['index']];
                                return 'Cantidad: '+data;
                            }
                        }
                    }
                }
            };
            companiesProjectsChart = new Chart(document.getElementById("companies-projects-chart").getContext("2d"), bar_config);
        }); // END COMPANIES_PROJECTS

        /*********************************
         *           Sales funnel        *
         * *******************************/
        $.get('/chart/budget_projects'+year+status+company+category+brand+'/my_projects', function (response){
            $('#load-sales-funnel').hide();
            var resp = JSON.parse(response);
            if ( resp.error ) { return; }

            var bar_config = {
                type: 'line',
                data: {
                    labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    datasets: resp.data
                },
                options: {
                    responsive: true,
                    // legend: false,
                    scales: {
                        xAxes: [{
                            display: true,
                            gridLines: {
                                display: true
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Meses'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            gridLines: {
                                display: true
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Montos'
                            }
                        }]
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data){
                                var text = data.datasets[tooltipItem.datasetIndex].label + ': ' + formmatterCurrency(tooltipItem.yLabel);
                                return text;
                            }
                        }
                    }
                }
            };
            budgetsProjectsChart = new Chart(document.getElementById("sales-funnel-chart").getContext("2d"), bar_config);
        }); // END COMPANIES_PROJECTS
    }

    if ($('#projects-table').size() > 0){
        var $table = $('#projects-table');
        ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
            ajax: {
                url: '/my_space/get_my_projects'
            },
            columns: [
                {
                    data: function(data) {
                        return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                            '<label class="m-b-0" for="check-'+data.id+'"></label>';
                    },
                    orderable: false,
                    searchable: false
                },
                { data: 'id' },
                { data: 'name', render: function(value, type, obj) { return '<a href="/project/view/'+obj.id+'" target="_blank">'+obj.name+'</a>'; } },
                { data: 'start_date', render: function (value, type, obj) { return obj.start_date_nice; } },
                { data: 'estimated_end_date', render: function (value, type, obj) { return obj.estimated_end_date_nice; } },
                { data: 'end_date', render: function (value, type, obj) { return obj.end_date_nice; } },
                {
                    data: 'estimated_end_date',
                    render: function (value, type, obj, meta) {
                        var color = (obj.delay.charAt(0) === '-') ? 'col-red' : 'col-green';

                        if ( obj.status === 'progreso' ) {
                            return '<span class="'+color+'">'+obj.delay+'</span>';
                        }
                        if ( obj.status === 'finalizado' || obj.status === 'anulado' ) {
                            return '';
                        }

                        return '<span class="'+color+'">'+obj.delay+'</span>';
                    },
                    searchable: false
                },
                {
                    data: 'status',
                    render: function (value, type, obj, meta) {
                        return obj.status.toUpperCase();
                    }
                },
                { data: 'stage_status' },
                { data: 'reason' },
                { data: 'client' },
                { data: 'company' },
                { data: 'manager' },
                {
                    data: 'tag',
                    render: function (value, type, obj, meta) {
                        var res = '';

                        if ( obj.tags ) {
                            obj.tags.split(',').forEach(function (t) {
                                if ( res !== '' ) { res+= ' '; }
                                res += '<span class="label bg-blue">'+t+'</span>';
                            });
                        }

                        return res;
                    }
                },
                {
                    data: function(data) {
                        var buttons = '<div class="btn-group">';
                        if (data.edit === 'true'){
                            buttons += '<a href="/project/edit/' + data.id + '" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                        }
                        if (data.delete === 'true'){
                            buttons += '<a href="javascript:void(0);" class="btn btn-danger btn-xs waves-effect delete-btn" data-id="' + data.id + '" data-controller="project" data-text="¿Está seguro que desea eliminar el proyecto?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                        }

                        buttons += '</div>';
                        return buttons;
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        }));

        $('#delayed_projects_filter').change(function () {
            if ( $(this).is(':checked') ) {
                ctable.ajax.url('/my_space/get_my_projects?delayed=true').order([ [4, 'asc'] ]).load();
            } else {
                ctable.ajax.url('/my_space/get_my_projects').order([ [1, 'desc'] ]).load();
            }
        });
    }

    if ($('#tasks-table').size() > 0){
        var $table = $('#tasks-table');
        var ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
            ajax: {
                url: '/my_space/get_my_tasks'
            },
            columns: [
                {
                    data: function(data) {
                        return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                            '<label class="m-b-0" for="check-'+data.id+'"></label>';
                    },
                    orderable: false,
                    searchable: false
                },
                { data: 'id' },
                {
                    data: 'project',
                    render: function(value, type, data) {
                        return '<a href="/project/f_view/'+data.project_id+'" target="_blank" data-toggle="tooltip" title="'+data.project_description+'">'+data.project+'</a>'
                    }
                },
                { data: 'template' },
                { data: 'stage' },
                {
                    data: 'title',
                    render: function(value, type, data) {
                        return '<a href="/task/f_view/'+data.id+'" target="_blank" data-toggle="tooltip" title="'+data.description+'">'+data.title+'</a>'
                    }
                },
                {
                    data: 'status',
                    render: function(value, type, obj) {
                        return obj.status.toUpperCase();
                    }
                },
                {
                    data: 'status_changer',
                    render: function (value, type, obj, meta) {
                        var checked = obj.status_changer === 'true' ? 'checked' : '';
                        console.log(obj.status_changer);
                        return  '<div class="switch">' +
                            '   <label><input type="checkbox" disabled name="status_changer" value="true" '+checked+'><span class="lever switch-col-amber"></span></label>' +
                            '</div>';
                    }
                },
                { data: 'stage_status' },
                { data: 'reason' },
                {
                    data: 'progress',
                    render: function(value, type, obj) {
                        return '<div class="progress">' +
                            '   <div class="progress-bar bg-green progress-bar-striped active" role="progressbar" style="width: '+obj.progress+'%">\n' +
                            '       '+obj.progress+'%' +
                            '   </div>' +
                            '</div>';
                    },
                    searchable: false
                },
                {
                    data: 'resources',
                    orderable: false,
                    searchable: false
                },
                { data: 'start_date', render: function(value, type, obj) { return obj.start_date_nice; } },
                { data: 'estimated_end_date', render: function(value, type, obj) { return obj.estimated_end_date_nice; } },
                { data: 'end_date', render: function(value, type, obj) { return obj.end_date_nice; } },
                {
                    data: 'estimated_end_date',
                    render: function (value, type, obj, meta) {
                        var color = (obj.delay.charAt(0) === '-') ? 'col-red' : 'col-green';

                        if ( obj.status === 'pendiente' || obj.status === 'proceso' ) {
                            return '<span class="'+color+'">'+obj.delay+'</span>';
                        }
                        if ( obj.status === 'cerrado' || obj.status === 'anulado' ) {
                            return '';
                        }

                        return '<span class="'+color+'">'+obj.delay+'</span>';
                    },
                    searchable: false
                },
                {
                    data: 'tag',
                    render: function (value, type, obj, meta) {
                        var res = '';

                        if ( obj.tags ) {
                            obj.tags.split(',').forEach(function (t) {
                                if ( res !== '' ) { res+= ' '; }
                                res += '<span class="label bg-blue">'+t+'</span>';
                            });
                        }

                        return res;
                    }
                },
                {
                    data: 'client_visibility',
                    render: function(value, type, obj) {
                        var res = '';
                        if ( obj.client_visibility === 'true' ) {
                            res += '<i class="material-icons col-blue" data-toggle="tooltip" data-original-title="Visible para el cliente">visibility</i>';
                        } else {
                            res += '<i class="material-icons col-grey" data-toggle="tooltip" data-original-title="No visible para el cliente">visibility_off</i>';
                        }
                        if ( obj.client_notification === 'true' ) {
                            res += '<i class="material-icons col-amber" data-toggle="tooltip" data-original-title="Notifica al cliente">notifications</i>';
                        } else {
                            res += '<i class="material-icons col-grey" data-toggle="tooltip" data-original-title="No notifica al cliente">notifications_off</i>';
                        }

                        return res;
                    }
                }
            ]
        }));

        $('#delayed_tasks_filter').change(function () {
            if ( $(this).is(':checked') ) {
                ctable.ajax.url('/my_space/get_my_tasks/'+$table.data('project_id')+'?delayed=true').order([ [12, 'asc'] ]).load();
            } else {
                ctable.ajax.url('/my_space/get_my_tasks/'+$table.data('project_id')).order([ [1, 'desc'] ]).load();
            }
        });
    }

    var sticked_filters_init = function () {
        var sticked = false;
        function stick_filters() {
            if(window.innerWidth >= 768) {
                if ( sticked ) {
                    $('#filters').sticky('update');
                } else {
                    $('#filters').sticky({topSpacing:70,zIndex:9});
                    sticked = true;
                }
            } else {
                if ( sticked ) {
                    $('#filters').unstick();
                    sticked = false;
                }
            }
        }

        $(document).ready(function(){
            stick_filters();
        });

        $( window ).resize(function() {
            stick_filters();
        });
    };

    sticked_filters_init();
})(jQuery);