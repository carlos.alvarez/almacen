$(document).ready(function () {

    /* LIST */
    if ( $('#employees-table').size() > 0 ) {
        var $table = $("#employees-table");
        ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
            ajax: {
                url: '/employee/datatable_json'
            },
            columns: [
                {
                    data: function(data) {
                        return '<input type="checkbox" id="check-'+data.user_id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.user_id+'">'+
                            '<label class="m-b-0" for="check-'+data.user_id+'"></label>';
                    },
                    orderable: false,
                    searchable: false
                },
                { data: 'user_id' },
                { data: 'username' },
                {
                    data: 'full_name',
                    render: function(value, type, obj) {
                        var active = (obj.active == "true") ? "col-green" : "col-red";
                        return "<a href='/employee/view/"+obj.user_id+"' target='_blank'><span><i class='material-icons " + active + " font-18' aria-hidden='true'>album</i> " + obj.full_name + "</span></a>"
                    }
                },
                { data: 'position' },
                { data: 'area' },
                { data: 'department' },
                { data: 'company' },
                {
                    data: function(data) {
                        var buttons = '<div class="btn-group">';
                        if (data.edit === 'true') {
                            buttons += '<a href="/employee/edit/'+data.user_id+'" data-id="'+data.user_id+'" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                        }
                        if (data.delete === 'true') {
                            buttons += '<a class="btn btn-danger btn-xs waves-effect delete-btn" data-id="'+data.user_id+'" data-controller="employee" data-text="¿Está seguro que desea eliminar este empleado?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                        }
                        buttons += '</div>';
                        return buttons;
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        }));
    }

    if ( $('#employee-new-form').size() > 0 ) {
        $('input[name="creation_mode"]').on('change', function () {
            if ( $(this).val() === 'existing' ) {
                $('#user-select-div').removeClass('hidden');
                $('select[name="user_id"]').removeAttr('disabled');

                $('.optional-fields input').attr('disabled', true);
                $('.optional-fields .form-line').addClass('focused disabled');
            } else {
                $('#user-select-div').addClass('hidden');
                $('select[name="user_id"]').val("").selectpicker('refresh').attr('disabled', false);
                $('.optional-fields input').attr('disabled', false).val('');
                $('.optional-fields .form-line').removeClass('disabled focused');
                $('input[name="first_name"]').focus();
            }
        });

        $('select[name="user_id"]').on('change', function () {
            if ( $(this).val() !== '' ) {
                $.get('/user/get_user_json/'+$(this).val(), function (json) {
                    var json_obj = JSON.parse(json);
                    if ( json_obj.error === false ) {
                        var user = json_obj.data.user;
                        $('input[name="first_name"]').first().val(user.first_name);
                        $('input[name="last_name"]').first().val(user.last_name);
                        $('input[name="username"]').first().val(user.username);
                        $('input[name="email"]').first().val(user.email);

                        $('.optional-fields .form-line').addClass('focused');
                    } else {
                        $('.optional-fields input').val('');
                    }
                });
            } else {
                $('.optional-fields input').val('');
            }
        });

        $('select[name="company_id"]').on('change', function() {
            var company = $(this).val();
            var count = 0;
            $('select[name="department_id"] option').each(function () {
                if ( $(this).val() === '' ) { return; }

                if ( $(this).data('company') == company ) {
                    $(this).show();
                    count++;
                } else {
                    $(this).hide();
                }
            });
            $('select[name="department_id"]').val('').selectpicker('refresh');
            if ( count > 0 ) {
                $('select[name="department_id"]').first().closest('.form-line').removeClass('disabled');
            } else {
                $('select[name="department_id"]').first().closest('.form-line').addClass('disabled');
            }

            count = 0;
            $('select[name="position_id"] option').each(function () {
                if ( $(this).val() === '' ) { return; }

                if ( $(this).data('company') == company ) {
                    $(this).show();
                    count++;
                } else {
                    $(this).hide();
                }
            });
            $('select[name="position_id"]').val('').selectpicker('refresh');
            if ( count > 0 ) {
                $('select[name="position_id"]').first().closest('.form-line').removeClass('disabled');
            } else {
                $('select[name="position_id"]').first().closest('.form-line').addClass('disabled');
            }

            $('select[name="area_id"] option').each(function () {
                if ( $(this).val() !== '' ) { $(this).hide(); }
            });
            $('select[name="area_id"]').val('').selectpicker('refresh').closest('.form-line').addClass('disabled');
        });

        $('select[name="department_id"]').on('change', function() {
            var department = $(this).val();
            var count = 0;
            $('select[name="area_id"] option').each(function () {
                if ( $(this).val() === '' ) { return; }

                if ( $(this).data('department') == department ) {
                    $(this).show();
                    count++;
                } else {
                    $(this).hide();
                }
            });
            $('select[name="area_id"]').val('').selectpicker('refresh');
            if ( count > 0 ) {
                $('select[name="area_id"]').first().closest('.form-line').removeClass('disabled');
            } else {
                $('select[name="area_id"]').first().closest('.form-line').addClass('disabled');
            }
        });
    }

    if ( $('#employee-edit-form').size() > 0 ) {
        $('select[name="company_id"]').on('change', function() {
            var company = $(this).val();
            var count = 0;
            $('select[name="department_id"] option').each(function () {
                if ( $(this).val() === '' ) { return; }

                if ( $(this).data('company') == company ) {
                    $(this).show();
                    count++;
                } else {
                    $(this).hide();
                }
            });
            $('select[name="department_id"]').val('').selectpicker('refresh');
            if ( count > 0 ) {
                $('select[name="department_id"]').first().closest('.form-line').removeClass('disabled');
            } else {
                $('select[name="department_id"]').first().closest('.form-line').addClass('disabled');
            }

            count = 0;
            $('select[name="position_id"] option').each(function () {
                if ( $(this).val() === '' ) { return; }

                if ( $(this).data('company') == company ) {
                    $(this).show();
                    count++;
                } else {
                    $(this).hide();
                }
            });
            $('select[name="position_id"]').val('').selectpicker('refresh');
            if ( count > 0 ) {
                $('select[name="position_id"]').first().closest('.form-line').removeClass('disabled');
            } else {
                $('select[name="position_id"]').first().closest('.form-line').addClass('disabled');
            }

            $('select[name="area_id"] option').each(function () {
                if ( $(this).val() !== '' ) { $(this).hide(); }
            });
            $('select[name="area_id"]').val('').selectpicker('refresh').closest('.form-line').addClass('disabled');
        });

        $('select[name="department_id"]').on('change', function() {
            var department = $(this).val();
            var count = 0;
            $('select[name="area_id"] option').each(function () {
                if ( $(this).val() === '' ) { return; }

                if ( $(this).data('department') == department ) {
                    $(this).show();
                    count++;
                } else {
                    $(this).hide();
                }
            });
            $('select[name="area_id"]').val('').selectpicker('refresh');
            if ( count > 0 ) {
                $('select[name="area_id"]').first().closest('.form-line').removeClass('disabled');
            } else {
                $('select[name="area_id"]').first().closest('.form-line').addClass('disabled');
            }
        });
    }

});