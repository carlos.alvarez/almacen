(function ($){

    if ($('#projects-table').size() > 0){
        var $table = $('#projects-table');
        ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
            ajax: {
                url: '/my_space/get_my_projects'
            },
            columns: [
                {
                    data: function(data) {
                        return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                            '<label class="m-b-0" for="check-'+data.id+'"></label>';
                    },
                    orderable: false,
                    searchable: false
                },
                { data: 'id' },
                { data: 'name', render: function(value, type, obj) { return '<a href="/project_client/view/'+obj.id+'" target="_blank">'+obj.name+'</a>'; } },
                { data: 'start_date', render: function (value, type, obj) { return obj.start_date_nice; } },
                { data: 'estimated_end_date', render: function (value, type, obj) { return obj.estimated_end_date_nice; } },
                { data: 'end_date', render: function (value, type, obj) { return obj.end_date_nice; } },
                {
                    data: 'estimated_end_date',
                    render: function (value, type, obj, meta) {
                        var color = (obj.delay.charAt(0) === '-') ? 'col-red' : 'col-green';

                        if ( obj.status === 'progreso' ) {
                            return '<span class="'+color+'">'+obj.delay+'</span>';
                        }
                        if ( obj.status === 'finalizado' || obj.status === 'anulado' ) {
                            return '';
                        }

                        return '<span class="'+color+'">'+obj.delay+'</span>';
                    },
                    searchable: false
                },
                {
                    data: 'status',
                    render: function (value, type, obj, meta) {
                        return obj.status.toUpperCase();
                    }
                },
                { data: 'client' },
                { data: 'company' },
                { data: 'manager' },
                {
                    data: 'tag',
                    render: function (value, type, obj, meta) {
                        var res = '';

                        if ( obj.tags ) {
                            obj.tags.split(',').forEach(function (t) {
                                if ( res !== '' ) { res+= ' '; }
                                res += '<span class="label bg-blue">'+t+'</span>';
                            });
                        }

                        return res;
                    }
                }
            ]
        }));
    }

    /*if ($('#tasks-table').size() > 0){
        var $table = $('#tasks-table');
        ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
            ajax: {
                url: '/my_space/get_my_tasks'
            },
            columns: [
                {
                    data: function(data) {
                        return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                            '<label class="m-b-0" for="check-'+data.id+'"></label>';
                    },
                    orderable: false,
                    searchable: false
                },
                { data: 'id' },
                { data: 'stage' },
                {
                    data: function(data) {
                        return '<a href="/task/view/'+data.id+'" target="_blank">'+data.title+'</a>'
                    }
                },
                { data: 'description' },
                { data: 'start_date', render: function(value, type, obj) { return obj.start_date_nice; } },
                { data: 'estimated_end_date', render: function(value, type, obj) { return obj.estimated_end_date_nice; } },
            ]
        }));
    }*/
})(jQuery);
