
/* EDIT */
if ( $('#user-edit-form').size() > 0 ) {

    $('#user-edit-form').validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('focused error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            repeat_password: {
                equalTo: "#password"
            }
        }
    });

    $('#user_roles').multiSelect({
        cssClass: "small-ms"
    });
}

/* MY_PROJECT */
if ( $('#myProfile-edit-form').size() > 0 ) {

    $('#myProfile-edit-form').validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('focused error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            repeat_password: {
                equalTo: "#password"
            }
        }
    });
}

/* NEW */
if ( $('#user-new-form').size() > 0 ) {

    $('#user-new-form').validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('focused error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            repeat_password: {
                equalTo: "#password"
            }
        }
    });

    $('#user_roles').multiSelect({
        cssClass: "small-ms"
    });
}

/* LIST */

if ( $('#users-table').size() > 0 ) {

	var $table = $("#users-table");
	$table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/user/datatable_json'
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'id' },
            {
                data: 'username',
                render: function(value, type, obj, meta) {
                    var active = (obj.active == "true") ? "col-green" : "col-red";
                    return "<a href='/user/view/"+obj.id+"' target='_blank'><span><i class='material-icons " + active + " font-16' aria-hidden='true'>person</i> " + obj.username + "</span></a>";
                }
            },
            { data: 'full_name' },
            { data: 'email' },
            {
                data: function(data) {
                    var buttons = '<div class="btn-group">';
                    if (data.edit === 'true') {
                        buttons += '<a href="/user/edit/'+data.id+'" data-id="'+data.id+'" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                    }
                    if (data.delete === 'true') {
                        buttons += '<a class="btn btn-danger btn-xs waves-effect delete-btn" data-id="'+data.id+'" data-controller="user" data-text="¿Está seguro que desea eliminar este usuario?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                    }
                    buttons += '</div>';
                    return buttons;
                },
                orderable: false,
                searchable: false
            }
        ]
    }));

}
