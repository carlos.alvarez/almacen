/**
 * Created by Carlos Alvarez on 5/24/2017.
 */

var weekdays = {
    0: 'Domingo',
    1: 'Lunes',
    2: 'Martes',
    3: 'Miércoles',
    4: 'Jueves',
    5: 'Viernes',
    6: 'Sábado'
};

if ( $('#calendar-table').size() > 0 ) {
    var $table = $("#calendar-table");
    ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/calendar/datatable_json'
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'id' },
            { 
                data: 'name',
                render: function(value, type, obj, meta) {
                    var active = (obj.active === "true") ? "col-green" : "col-red";
                    return "<a href='/calendar/view/"+obj.id+"' target='_blank'><span><i class='material-icons " + active + " font-18' aria-hidden='true'>album</i> " + obj.name + "</span></a>"
                }
            },
            { data: 'description' },
            { data: 'working_hours' },
            {
                data: function(data) {
                    var output = '';
                    if ( data.wd_sunday === 'true' ) { output += ' D'; }
                    if ( data.wd_monday === 'true' ) { output += ' L'; }
                    if ( data.wd_tuesday === 'true' ) { output += ' M'; }
                    if ( data.wd_wednesday === 'true' ) { output += ' Mi'; }
                    if ( data.wd_thursday === 'true' ) { output += ' J'; }
                    if ( data.wd_friday === 'true' ) { output += ' V'; }
                    if ( data.wd_saturday === 'true' ) { output += ' S'; }

                    return output;
                },
                orderable: false,
                searchable: false
            },
            {
                data: function(data) {
                    var buttons = '<div class="btn-group">';
                    if (data.edit === 'true') {
                        buttons += '<a href="/calendar/edit/'+data.id+'" data-id="'+data.id+'" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                    }
                    if (data.delete === 'true') {
                        buttons += '<a class="btn btn-danger btn-xs waves-effect delete-btn" data-id="'+data.id+'" data-controller="calendar" data-text="¿Está seguro que desea eliminar este calendario?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                    }
                    buttons += '</div>';
                    return buttons;
                },
                orderable: false,
                searchable: false
            }
        ]
    }));
}

if ( $('#holidays-table').size() > 0 ) {

    var $table = $("#holidays-table");
    var $datatable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/calendar/holiday_datatable_json/'+$table.data('calendar-id')+'/'+$('#year').val()
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'date' },
            { data: 'name' },
            { data: 'description' },
            {
                data: function(data) {
                    return '<div class="btn-group"><a href="javascript:void(0);" data-id="'+data.id+'" data-date="'+data.date+'" data-name="'+data.name+'" data-description="'+data.description+'" class="btn btn-info edit-holiday btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>'+
                        '<a href="javascript:void(0);" class="btn btn-danger delete-holiday btn-xs waves-effect" data-id="'+data.id+'" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a></div>'
                },
                orderable: false,
                searchable: false
            }
        ]
    }));

    $('#year').change(function () {
        setTimeout(function () {
            $datatable.ajax.url('/calendar/holiday_datatable_json/'+$table.data('calendar-id')+'/'+$('#year').val()).load();
        }, 500);
    });

    $('#add-holiday-form').on('submit', function () {
        var data = $(this).serialize();
        $.post('/calendar/create_holiday', data, function (json) {
            var json_obj = JSON.parse(json);

            if ( !json_obj.error ) {
                $datatable.ajax.reload();
                $('#add-holiday-modal').modal('toggle');
                $('#add-holiday-form').find('input,textarea').not('[type="hidden"]').val('');
            } else { sweetNotification('Error', json_obj.message, 'error'); }

        });
    });

    $('#save-new-holiday').on('click', function () {
        validateMaterial($('#add-holiday-form'));
        $('#add-holiday-form').submit();
    });

    $(document).on('click', '.edit-holiday', function () {
        $('#edit-holiday-modal').modal('toggle');
        $('#edit-holiday-form').find('input[name="id"]').val($(this).data('id'));
        $('#edit-holiday-form').find('input[name="date"]').val($(this).data('date'));
        $('#edit-holiday-form').find('input[name="name"]').val($(this).data('name'));
        $('#edit-holiday-form').find('textarea[name="description"]').val($(this).data('description'));
    });

    $('#edit-holiday-form').on('submit', function () {
        var data = $(this).serialize();
        var id = $(this).find('input[name="id"]').first().val();
        $.post('/calendar/edit_holiday/'+id, data, function (json) {
            var json_obj = JSON.parse(json);

            if ( !json_obj.error ) {
                $datatable.ajax.reload();
                $('#edit-holiday-modal').modal('toggle');
                $('#edit-holiday-form').find('input,textarea').val('');
            } else { sweetNotification('Error', json_obj.message, 'error'); }

        });
    });

    $('#save-edit-holiday').on('click', function () {
        validateMaterial($('#edit-holiday-form'));
        $('#edit-holiday-form').submit();
    });

    $(document).on('click', '.delete-holiday', function () {
        var holiday_id = $(this).data('id');
        swal(
            {
                title: "Eliminar Día Feriado",
                text: "¿Está seguro que desea eliminar este día feriado?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, eliminar!",
                closeOnConfirm: true
            },
            function(){
                $.post('/calendar/delete_holiday/'+holiday_id, function (json) {
                    var json_obj = JSON.parse(json);

                    if ( json_obj.error ) {
                        sweetNotification("Error!", json_obj.message, "error");
                        return;
                    }

                    $datatable.ajax.reload();
                });
            }
        );
    });

}
