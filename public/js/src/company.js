/* LIST */

if ( $('#companies-table').size() > 0 ) {

	var $table = $("#companies-table");
    ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/company/datatable_json'
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'id' },
            {
                data: 'name',
                render: function(value, type, obj, meta) {
                    var active = (obj.active === "true") ? "col-green" : "col-red";
                    return "<a href='/company/view/"+obj.id+"' target='_blank'><span><i class='material-icons " + active + " font-18' aria-hidden='true'>album</i> " + obj.name + "</span></a>"
                }
            },
            { data: 'short_name' },
            { data: 'tax_number' },
            {
                data: function(data) {
                    var buttons = '<div class="btn-group">';
                    if (data.edit === 'true') {
                        buttons += '<a href="/company/edit/'+data.id+'" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                    }
                    if (data.delete === 'true') {
                        buttons += '<a class="btn btn-danger btn-xs waves-effect delete-btn" data-id="'+data.id+'" data-controller="company" data-text="¿Está seguro que desea eliminar esta empresa?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                    }
                    buttons += '</div>';
                    return buttons;
                },
                orderable: false,
                searchable: false
            }
        ]
    }));

}
