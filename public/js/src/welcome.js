
function getRandomData() {
    var barChartData = [];
    for (var i = 0; i <= 10; i += 1) {
        barChartData.push([i, parseInt(Math.random() * 30)]);
    }

    return barChartData;
}

$(window).ready(function () {
    var plot;

    $.get('/welcome/products_chart', function (json) {
        var obj = JSON.parse(json);
        if ( obj.error ) { return; }

        plot = $.plot('#bar_chart', obj.data, {
            series: {
                stack: 0,
                lines: {
                    show: false,
                    fill: true,
                    steps: false
                },
                bars: {
                    show: true,
                    barWidth: 0.6,
                    numbers: {
                        show : true,
                        font : {size : '18', weight: 'bold', color : 'darkorange'},
                    }
                },
                color: '#00BCD4'
            },
            grid: {
                hoverable: true,
                autoHighlight: false,
                borderColor: '#f3f3f3',
                borderWidth: 1,
                tickColor: '#f3f3f3'
            },
            legend: {
                container: $('#legend')
            },
            xaxis: {
                tickDecimals: 0
            },
            yaxis: {
                tickDecimals: 0
            }
        });

        setTimeout(updateRealTime, 1000);
    });


    function updateRealTime() {
        $.get('/welcome/products_chart', function (json) {
            var obj = JSON.parse(json);
            if ( obj.error ) { return; }

            plot.setData(obj.data);
            plot.setupGrid();
            plot.draw();

            setTimeout(updateRealTime, 1000);
        });
    }

});

