/* LIST */

if ( $('#positions-table').size() > 0 ) {

    var $table = $("#positions-table");
    ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/position/datatable_json'
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'id' },
            {
                data: 'company',
                render: function(value, type, obj, meta) {
                    return "<a href='/company/view/"+obj.company_id+"' target='_blank'>" + obj.company + "</a>"
                }
            },
            {
                data: 'name',
                render: function(value, type, obj, meta) {
                    return "<a href='/position/view/"+obj.id+"' target='_blank'>" + obj.name + "</a>"
                }
            },
            { data: 'description' },

            {
                data: function(data) {
                    var buttons = '<div class="btn-group">';
                    if (data.edit === 'true') {
                        buttons += '<a href="/position/edit/'+data.id+'" data-id="'+data.id+'" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                    }
                    if (data.delete === 'true') {
                        buttons += '<a class="btn btn-danger btn-xs waves-effect delete-btn" data-id="'+data.id+'" data-controller="position" data-text="¿Está seguro que desea eliminar este cargo?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                    }
                    buttons += '</div>';
                    return buttons;
                },
                orderable: false,
                searchable: false
            }
        ]
    }));

}
