(function ($) {
    if($('input[name="estimated_date"]').size() > 0){
        $('input[name="estimated_date"]').datepicker({
            changeYear: true
        });
        $('input[name="actual_date"]').datepicker({
            changeYear: true
        });
    }

    $('.submit,button[type="submit"]').on('click',function () {
        $(this).hide();
    });

    if ($('#project-brand-table').size() > 0){
        var $table = $("#project-brand-table");
        ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
            ajax: {
                url: '/project_brand/today_datatable_json/'+$('#budget_date').val()+'/'+$('#status').val()+'/'+$('#company').val()+'/'+$('#category').val()+'/'+$('#brand').val()+'/'+$('#project_id').val()
            },
            columns: [
                {
                    data: function(data) {
                        return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                            '<label class="m-b-0" for="check-'+data.id+'"></label>';
                    },
                    orderable: false,
                    searchable: false
                },
                { data: 'id' },
                { data: 'project_name'},
                { data: 'brand_name' },
                { data: 'estimated_amount', render: function (value, type, obj) { return formmatterCurrency(obj.estimated_amount)}},
                { data: 'estimated_date' },
                { data: 'actual_amount', render: function (value, type, obj) { return formmatterCurrency(obj.actual_amount)} },
                { data: 'actual_date' },
                { data: 'created_at'}
            ]
        }));

        $('#budget_date').change(function () {
            var date = $(this).val();
            ctable.ajax.url('/project_brand/today_datatable_json/'+date).load();
        });
    }

})(jQuery);