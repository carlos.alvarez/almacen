/* LIST */

if ( $('#clients-table').size() > 0 ) {

	var $table = $("#clients-table");
    ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/client/datatable_json'
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'id' },
            { data: 'ref_code' },
            {
                data: 'name',
                render: function(value, type, obj, meta) {
                    var active = (obj.active == "true") ? "col-green" : "col-red";
                    return "<a href='/client/view/"+obj.id+"' target='_blank'><span><i class='material-icons " + active + " font-18' aria-hidden='true'>album</i> " + obj.name + "</span></a>"
                }
            },
            { data: 'short_name' },
            { data: 'tax_number' },
            { data: 'account_manager_name' },
            {
                data: function(data) {

                    var btn = '<div class="btn-group">';
                    if ( data.edit === 'true' ) {
                        btn += '<a href="/client/edit/'+data.id+'" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                    }
                    if ( data.delete === 'true' ) {
                        btn += '<a href="javascript:void(0);" class="btn btn-danger btn-xs waves-effect delete-btn" data-id="'+data.id+'" data-controller="client" data-text="¿Está seguro que desea eliminar este cliente?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                    }
                    return btn + '</div>';
                },
                orderable: false,
                searchable: false
            }
        ]
    }));
}

if ( $('#client-new-form').size() > 0 || $('#client-edit-form').size() > 0 ) {
    $('#contactsForm').sheepIt({
        separator: '',
        allowRemoveLast: false,
        allowRemoveCurrent: true,
        allowRemoveAll: false,
        allowAdd: true,
        allowAddN: false,
        minFormsCount: 1,
        iniFormsCount: 1,
        beforeRemoveCurrent: function(source, form) {
            swal(
                {
                    title: "Eliminar Contacto",
                    text: "¿Está seguro que desea eliminar este contacto?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: true
                },
                function(){
                    source.removeCurrentForm(form);
                }
            );

            return false;
        },
        data: $('#contactsPre').data('contacts')
    });

    $('.form-line input').not('input[type="hidden"]').each(function () {
        if ( $(this).val() !== '' ) { $(this).closest('.form-line').addClass('focused'); }
        else { $(this).closest('.form-line').removeClass('focused'); }
    });

    if ($('.user_id').size() > 0){
        setTimeout(function(){
            $('.user_id').each(function () {
                if ( $(this).val() === '' || $(this).val() < 1 ) {
                    $(this).closest('.row').find('.create-user').first().show();
                } else {
                    $(this).closest('.row').find('.create-user').first().html('<i class="material-icons col-green">person</i>');
                    $(this).closest('.row').find('.create-user').first().show();
                    $(this).closest('.row').find('.create-user').first().data('title','Tiene usuario creado');
                    $(this).closest('.row').find('.create-user').first().removeClass('create-user');
                }
            });
        },3000);
    }

    var $id = '';

    $(document).on('click', '.create-user', function (){
        $('#user-create-modal').modal('show');
        validateMaterial($('#user-new-form'));
        $id = $(this).closest('.row').find('.contact_id').first().val();
        var contactName = $(this).closest('.row').find('.contact-name').first().val();
        var firstName = contactName.split(' ')[0];
        var lastName = contactName.split(' ')[1];
        var contactEmail = $(this).closest('.row').find('.contact-email').first().val();

        $('#user-first-name').val(firstName);
        $('#user-last-name').val(lastName);
        if ($('#user-last-name').val() !== ''){ $('#user-last-name').closest('.form-line').addClass('focused'); }
        $('#user-email').val(contactEmail);
        $('#user-email').closest('.form-line').addClass('focused');
    });

    $('#add-user').click(function () {
        if ($('#user-new-form').valid()){
            $('#user-new-form').submit();
        }
    });

    $('#user-new-form').submit(function () {
        var form = $('#user-new-form');
        $('.btn-modal').hide();
        $('#loader-template').show();
        $.post('/client/create_user_contact/'+$id,form.serialize(),function (response) {
            var resp = JSON.parse(response);

            if ( resp.error ) {
                swal.showInputError(resp.message);
            } else {
                sweetNotification("¡Bien!", "¡Usuario creado correctamente!", "success", 2000);
                $('#user-create-modal').modal('hide');
                location.reload();
            }
            $('.btn-modal').show();
            $('#loader-template').hide();
        });
    });
}
