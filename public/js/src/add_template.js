

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}

function update_couplings_project(templates, i)
{
    if ( i < 1 /*|| templates.length <= 1 || templates[i] === undefined*/ ) { return; }

    $.get('/project/get_structure/'+$('#project-new-form').data('id')+'/'+templates[i-1].id, function (json) {
        var json_obj = JSON.parse(json);
        var $cpl = $('#coupling-model').clone();
        $cpl.removeAttr('id');

        $cpl.find('a.former').text('Projecto');
        $cpl.find('a.former').attr('data-original-title', 'Elija una tarea del proyecto');
        $cpl.find('a.latter').text('Plantilla '+(i));
        $cpl.find('a.latter').attr('data-original-title', templates[i-1].title);

        var former = $cpl.find('select.former').first();
        var latter = $cpl.find('select.latter').first();

        former.attr('name', 'couplings_project['+templates[i-1].id+'][former]');
        latter.attr('name', 'couplings_project['+templates[i-1].id+'][latter]');
        $cpl.find('select.concurrency').first().attr('name', 'couplings_project['+templates[i-1].id+'][concurrency]');

        $.each(json_obj.former, function () {
            if ( former.find('optgroup[id="'+this.stage_id+'"]').length === 0 ) {
                former.append('<optgroup id="'+this.stage_id+'" label="'+this.stage+'"></optgroup>');
            }
            former.find('optgroup[id="'+this.stage_id+'"]').append('<option value="'+this.task_id+'">'+this.task+'</option>');
        });
        former.find('option').last().attr('selected', true);

        $.each(json_obj.latter, function () {
            if ( latter.find('optgroup[id="'+this.stage_id+'"]').length === 0 ) {
                latter.append('<optgroup id="'+this.stage_id+'" label="'+this.stage+'"></optgroup>');
            }
            latter.find('optgroup[id="'+this.stage_id+'"]').append('<option value="'+this.task_id+'">'+this.task+'</option>');
        });
        latter.find('option').eq(1).attr('selected', true);

        $('#coupling').append($cpl);
        $cpl.show();
    }).then(update_couplings(templates, i));
}

function update_couplings(templates, i)
{
    if ( i < 1 || templates.length <= 1 || templates[i] === undefined ) { return; }

    $.get('/template/get_structure/'+templates[i-1].id+'/'+templates[i].id, function (json) {
        var json_obj = JSON.parse(json);
        var $cpl = $('#coupling-model').clone();
        $cpl.removeAttr('id');

        $cpl.find('a.former').text('Plantilla '+i);
        $cpl.find('a.former').attr('data-original-title', templates[i-1].title);
        $cpl.find('a.latter').text('Plantilla '+(i+1));
        $cpl.find('a.latter').attr('data-original-title', templates[i].title);

        var former = $cpl.find('select.former').first();
        var latter = $cpl.find('select.latter').first();

        former.attr('name', 'couplings['+templates[i-1].id+'-'+templates[i].id+'][former]');
        latter.attr('name', 'couplings['+templates[i-1].id+'-'+templates[i].id+'][latter]');
        $cpl.find('select.concurrency').first().attr('name', 'couplings['+templates[i-1].id+'-'+templates[i].id+'][concurrency]');

        $.each(json_obj.former, function () {
            if ( former.find('optgroup[id="'+this.stage_id+'"]').length === 0 ) {
                former.append('<optgroup id="'+this.stage_id+'" label="'+this.stage+'"></optgroup>');
            }
            former.find('optgroup[id="'+this.stage_id+'"]').append('<option value="'+this.task_id+'">'+this.task+'</option>');
        });
        former.find('option').last().attr('selected', true);

        $.each(json_obj.latter, function () {
            if ( latter.find('optgroup[id="'+this.stage_id+'"]').length === 0 ) {
                latter.append('<optgroup id="'+this.stage_id+'" label="'+this.stage+'"></optgroup>');
            }
            latter.find('optgroup[id="'+this.stage_id+'"]').append('<option value="'+this.task_id+'">'+this.task+'</option>');
        });
        latter.find('option').eq(1).attr('selected', true);

        $('#coupling').append($cpl);
        $cpl.show();
    }).then(update_couplings(templates, i+1));
}

function update_positions(templates, i) {
    if ( templates.length < 1 || templates[i] === undefined ) { return; }

    $.get('/template/get_positions/'+templates[i].id, function (json) {
        var json_obj = JSON.parse(json);
        var $list = $('<div class="row"><div class="col-sm-12 m-b-0" id="tpl_'+templates[i].id+'"><strong><i class="glyphicon glyphicon-modal-window col-blue"></i> '+json_obj[0].template+' </strong></div></div>');

        $.each(json_obj, function (index, e) {
            if ( $list.find('#stg_'+e.stage_id).length < 1 ) {
                $list.find('#tpl_'+templates[i].id).append('<div class="col-sm-12 p-l-30 m-b-0" id="stg_'+e.stage_id+'"><strong><i class="glyphicon glyphicon-briefcase col-purple"></i> '+e.stage+'</strong></div>');
            }

            if ( $list.find('#tsk_'+e.task_id).length < 1 ) {
                $list.find('#stg_'+e.stage_id).append('<div class="col-sm-12 p-l-30 m-b-0" id="tsk_'+e.task_id+'"><strong><i class="glyphicon glyphicon-tasks col-green"></i> '+e.task+'</strong></div>');
            }

            $list.find('#tsk_'+e.task_id).append('<div class="col-sm-12 m-b-0" id="pos_'+e.position_id+'"><div class="col-sm-6 m-b-5 align-right"><h5>'+e.position+'</h5></div><div class="col-sm-6 m-b-5"><div class="input-group input-group-sm m-b-0"><span class="input-group-addon"><i class="material-icons">person</i> </span><div class="form-line focused"><select required name="positions['+e.task_id+'][]" data-position="'+e.position_id+'" class="form-control ms"></select></div></div></div></div>');
        });

        $list.appendTo('#personal');
        //$list.find('select.ms').select2({ width: '100%' });

    }).then(update_positions(templates, i+1));
}

function update_position_options() {
    var positions = {};
    //console.log('updating options');

    $('#personal').find('select.ms').each(function (i, e) {
        if ( positions[$(e).attr('data-position')] === undefined ) { positions[$(e).attr('data-position')] = true; }
    });

    $.each(positions, function (i, e) {
        $.get('/employee/get_by_position/'+i, function (json) {
            var json_obj = JSON.parse(json);
            $('#personal').find('select[data-position="'+i+'"]').each(function () {
                var $select = $(this);
                $select.append('<option value disabled selected>-- Seleccione un empleado --</option>');
                $.each(json_obj, function (index, element) {
                    if ( $select.find('optgroup[data-company="'+element.company_id+'"]').length < 1 ) {
                        $select.append('<optgroup label="'+element.company+'" data-company="'+element.company_id+'"></optgroup>');
                    }
                    $select.find('optgroup[data-company="'+element.company_id+'"]').append('<option value="'+element.user_id+'">'+element.full_name+'</option>');
                });

                $(this).select2({ width: '100%' });
                if ( json_obj.length < 1 ) {
                    $(this).closest('.form-line').addClass('error');
                }

                $(this).change(function () {
                    var value = $(this).find('option:selected').first().val();
                    // console.log(value);
                    var this_index = $('#personal').find('select[data-position="'+i+'"]').index(this);
                    $('#personal').find('select[data-position="'+i+'"]').not(this).slice(this_index).each(function () {
                        if ( $(this).find('option:selected').text() !== '-- Seleccione un empleado --' ) { return; }
                        $(this).find('option[value="'+value+'"]').first().prop('selected', true);
                        $(this).trigger('change');
                    });
                });

                if ($select.find('option').not('[value=""]').length === 1){
                    $select.find('option').not('[value=""]').first().attr('selected',true).trigger('change');
                }
            });
        });
    });

}

if ( $('#project-new-form').size() > 0 ) {
    var form = $('#project-new-form').show();
    var loanding = $('#loanding-project-new');
    form.steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        labels: {
            next: 'Siguiente',
            previous: 'Anterior',
            finish: 'Finalizar',
            loading: 'Cargando ...',
            cancel: 'Cancelar'
        },
        onInit: function (event, currentIndex) {
            //$.AdminBSB.input.activate();

            form.find('input[name="tags"]').tagsinput({
                trimValue: true
            });

            form.find('input[name="start_date"]').datepicker({
                changeYear: true
            });

            form.find('input[name="estimated_end_date"]').datepicker({
                changeYear: true
            });

            form.find('select[name="client_id"]').select2().on('change', function () {
                $.get('/client/get_client/'+$(this).val(), function (response) {
                    var resp = JSON.parse(response);
                    if( resp.error == false && resp.message.account_manager != null){
                        form.find('select[name="manager_id"] option[value="'+resp.message.account_manager+'"]').first().prop('selected', true).trigger('change');
                    } else {
                        form.find('select[name="manager_id"] option[value=""]').first().prop('selected', true).trigger('change');
                    }
                });
            });

            form.find('select[name="company_id"]').select2();

            form.find('select[name="manager_id"]').select2();

            $('#available-templates, #template-list').nestable({
                maxDepth: 1,
                group: 0
            });

            form.find('select[name="project_category_id"]').on('change', function(){
                var category = $(this).val();
                $('#available-templates').html('<ol class="dd-list"></ol>');
                if( category !== "" ){
                    var categoryName = form.find('select[name="project_category_id"] option:selected').text();
                    $('#title-category').text('de la categoría '+categoryName);
                    $('#template-list').html('<div class="dd-empty"></div>');
                    $('input[name="templates"]').val('');
                    $('#loader-template').fadeIn('slow');

                    var list = '';
                    $.get("/template/get_templates_by_category/"+category, function (resp) {
                        var response = JSON.parse(resp);
                        if( ! $.isEmptyObject(response) ){
                            $.each(response, function (index, e) {
                                list += '<li class="dd-item" data-id="'+e.id+'" data-title="'+e.title+'">';
                                list += '   <div class="dd-handle"><p>'+e.title+'</p></div>';
                                list += '</li>';
                            });
                        } else {
                            list = '<li><p>La categoría no tiene plantilla asociada.</p></li>';
                        }
                        $('.dd-list').html(list);
                    }).always(function () {
                        $('#loader-template').fadeOut();
                    }).then(function () {
                        $.get('/project/get_templates/'+$('#project-new-form').data('id'), function (json) {
                            var obj = JSON.parse(json);
                            if ( obj.error ) { return; }

                            obj.data.forEach(function (t) {
                                $('.dd-list').find('li[data-id="'+t+'"] .dd-handle').first().attr('style', 'background: #05f0ff !important').attr('data-toggle', 'tooltip').attr('title', 'Esta plantilla se ha agregado al proyecto anteriormente.');
                            });

                        });
                    });
                } else {
                    $('#title-category').text('');
                }
            }).change();

            $('#template-list').on('change', function() {
                /*var templates = $('#template-list').nestable('serialize');
                if ( templates.length > 0 ) {
                    form.find('input[name="templates"]').val(JSON.stringify(templates));
                } else {
                    form.find('input[name="templates"]').val('');
                }

                $('#coupling').html('');

                //update_couplings(templates, 1);
                update_couplings_project(templates, 1);

                $('#personal').html('');
                update_positions(templates, 0);
                setTimeout(update_position_options, 2000);*/
            });

            //Set tab width
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            //set button waves effect
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) { return true; }

            if ( currentIndex === 0 ) {
                var templates = $('#template-list').nestable('serialize');
                if ( templates.length > 0 ) {
                    form.find('input[name="templates"]').val(JSON.stringify(templates));
                } else {
                    form.find('input[name="templates"]').val('');
                    return false;
                }

                $('#coupling').html('');

                update_couplings_project(templates, 1);

                $('#personal').html('');
                update_positions(templates, 0);
                //setTimeout(update_position_options, 2000);
            }

            if ( currentIndex === 1 ) {
                update_position_options();
            }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden :not(type[type="hidden"])';
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            form.hide();
            loanding.show();
            $.post('/project/add_template_save/'+$('#project-new-form').data('id'), form.serialize(), function (json) {
                var json_obj = JSON.parse(json);
                loanding.hide();
                if ( json_obj.error === false ) {
                    swal("¡Excelente!", "¡Plantillas añadidas exitosamente!", "success");
                    setTimeout(function () {
                        window.location = '/project/view/'+$('#project-new-form').data('id');
                    }, 1000);
                }else{
                    form.show();
                }
            });

        }
    });

    //validateMaterial(form);

    form.validate({
        highlight: function (input) {
            $(input).closest('.form-line').addClass('focused error');
        },
        unhighlight: function (input) {
            $(input).closest('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).closest('.form-group,.input-group').append(error);
        }
    });
}