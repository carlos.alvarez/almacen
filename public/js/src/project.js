/**
 * Created by Carlos Alvarez on 6/8/2017.
 */

function padToSix(number) {
    if (number<=999999) { number = ("00000"+number).slice(-6); }
    return number;
}

$(document).ready(function () {
    if ( $('#projects-table').size() > 0 ) {

        var $table = $("#projects-table");
        var ctable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
            ajax: {
                url: '/project/datatable_json'
            },
            columns: [
                {
                    data: function(data) {
                        return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                            '<label class="m-b-0" for="check-'+data.id+'"></label>';
                    },
                    orderable: false,
                    searchable: false
                },
                { data: 'id' },
                { data: 'name', render: function(value, type, obj) { return '<a href="/project/view/'+obj.id+'" target="_blank">'+obj.name+'</a>'; } },
                { data: 'start_date', render: function (value, type, obj) { return obj.start_date_nice; } },
                { data: 'estimated_end_date', render: function (value, type, obj) { return obj.start_date_nice; } },
                { data: 'end_date', render: function (value, type, obj) { return obj.end_date_nice; } },
                {
                    data: 'estimated_end_date',
                    render: function (value, type, obj, meta) {
                        var color = (obj.delay.charAt(0) === '-') ? 'col-red' : 'col-green';

                        if ( obj.status === 'progreso' ) {
                            return '<span class="'+color+'">'+obj.delay+'</span>';
                        }
                        if ( obj.status === 'finalizado' || obj.status === 'anulado' ) {
                            return '';
                        }

                        return '<span class="'+color+'">'+obj.delay+'</span>';
                    },
                    searchable: false
                },
                {
                    data: 'status',
                    render: function (value, type, obj, meta) {
                        return obj.status.toUpperCase();
                    }
                },
                { data: 'stage_status' },
                { data: 'reason' },
                { data: 'client' },
                { data: 'company' },
                { data: 'manager' },
                {
                    data: 'tag',
                    render: function (value, type, obj, meta) {
                        var res = '';

                        if ( obj.tags ) {
                            obj.tags.split(',').forEach(function (t) {
                                if ( res !== '' ) { res+= ' '; }
                                res += '<span class="label bg-blue">'+t+'</span>';
                            });
                        }

                        return res;
                    }
                },
                {
                    data: function(data) {
                        var buttons = '<div class="btn-group">';
                        if (data.edit === 'true'){
                            buttons += '<a href="/project/edit/' + data.id + '" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>';
                        }
                        if (data.delete === 'true'){
                            buttons += '<a href="javascript:void(0);" class="btn btn-danger btn-xs waves-effect delete-btn" data-id="' + data.id + '" data-controller="project" data-text="¿Está seguro que desea eliminar el proyecto?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                        }

                        buttons += '</div>';
                        return buttons;
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        }));

        $('#delayed_projects_filter').change(function () {
            if ( $(this).is(':checked') ) {
                ctable.ajax.url('/project/datatable_json?delayed=true').order([ [6, 'asc'] ]).load();
            } else {
                ctable.ajax.url('/project/datatable_json').order([ [1, 'desc'] ]).load();
            }
        });
    }

    if ( $('#project-new-form').size() > 0 ) {
        var form = $('#project-new-form').show();
        var loanding = $('#loanding-project-new');
        form.steps({
            headerTag: 'h3',
            bodyTag: 'fieldset',
            transitionEffect: 'slideLeft',
            labels: {
                next: 'Siguiente',
                previous: 'Anterior',
                finish: 'Finalizar',
                loading: 'Cargando ...',
                cancel: 'Cancelar'
            },
            onInit: function (event, currentIndex) {
                //$.AdminBSB.input.activate();

                form.find('input[name="tags"]').tagsinput({
                    trimValue: true
                });

                form.find('input[name="start_date"]').datepicker({
                    changeYear: true
                });

                form.find('input[name="estimated_end_date"]').datepicker({
                    changeYear: true
                });

                form.find('select[name="client_id"]').select2().on('change', function () {
                    $.get('/client/get_client/'+$(this).val(), function (response) {
                        var resp = JSON.parse(response);
                        if( resp.error == false && resp.message.account_manager != null){
                            form.find('select[name="manager_id"] option[value="'+resp.message.account_manager+'"]').first().prop('selected', true).trigger('change');
                        } else {
                            form.find('select[name="manager_id"] option[value=""]').first().prop('selected', true).trigger('change');
                        }
                    });
                });

                form.find('select[name="company_id"]').on('change', function () {
                    var invoiced_by = form.find('select[name="invoice_company_id"]').first();
                    if ( invoiced_by.val() === '' ) {
                        invoiced_by.find('option[value="'+$(this).val()+'"]').prop('selected', true);
                    }
                });

                form.find('select[name="manager_id"]').select2();

                $('#available-templates, #template-list').nestable({
                    maxDepth: 1,
                    group: 0
                });

                form.find('select[name="project_category_id"]').on('change', function(){
                    var category = $(this).val();
                    $('#available-templates').html('<ol class="dd-list"></ol>');
                    if( category !== "" ){
                        var categoryName = form.find('select[name="project_category_id"] option:selected').text();
                        $('#title-category').text('de la categoría '+categoryName);
                        $('#template-list').html('<div class="dd-empty"></div>');
                        $('input[name="templates"]').val('');
                        $('#loader-template').fadeIn('slow');

                        var list = '';
                        $.get("/template/get_templates_by_category/"+category, function (resp) {
                            var response = JSON.parse(resp);
                            if( ! $.isEmptyObject(response) ){
                                $.each(response, function (index, e) {
                                    list += '<li class="dd-item" data-id="'+e.id+'" data-title="'+e.title+'">';
                                    list += '   <div class="dd-handle"><p>'+e.title+'</p></div>';
                                    list += '</li>';
                                });
                            } else {
                                list = '<li><p>La categoría no tiene plantilla asociada.</p></li>';
                            }
                            $('.dd-list').html(list);
                        }).always(function () {
                            $('#loader-template').fadeOut();
                        });
                    } else {
                        $('#title-category').text('');
                    }
                });

                $('#template-list').on('change', function() {
                    /*var templates = $('#template-list').nestable('serialize');
                    console.log(templates);
                    if ( templates.length > 0 ) {
                        form.find('input[name="templates"]').val(JSON.stringify(templates));
                    } else {
                        form.find('input[name="templates"]').val('');
                    }

                    if ( templates.length <= 1 ) {
                        $('#coupling').html('<h4 class="card-title center">No es necesario acoplar, puede continuar.</h4>');
                    } else {
                        $('#coupling').html('');
                    }

                    update_couplings(templates, 1);

                    $('#personal').html('');
                    update_positions(templates, 0);
                    setTimeout(update_position_options, 2000);*/
                });

                //Set tab width
                var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
                var tabCount = $tab.length;
                $tab.css('width', (100 / tabCount) + '%');

                //set button waves effect
                setButtonWavesEffect(event);
            },
            onStepChanging: function (event, currentIndex, newIndex) {
                if (currentIndex > newIndex) { return true; }

                if ( currentIndex === 1 ) {
                    var templates = $('#template-list').nestable('serialize');
                    console.log(templates);
                    if ( templates.length > 0 ) {
                        form.find('input[name="templates"]').val(JSON.stringify(templates));
                    } else {
                        form.find('input[name="templates"]').val('');
                    }

                    if ( templates.length <= 1 ) {
                        $('#coupling').html('<h4 class="card-title center">No es necesario acoplar, puede continuar.</h4>');
                    } else {
                        $('#coupling').html('');
                    }

                    update_couplings(templates, 1);

                    $('#personal').html('');
                    update_positions(templates, 0);
                    //setTimeout(update_position_options, 2000);
                }

                if ( currentIndex === 2 ) {
                    update_position_options();
                }

                if (currentIndex < newIndex) {
                    form.find('.body:eq(' + newIndex + ') label.error').remove();
                    form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
                }

                form.validate().settings.ignore = ':disabled,:hidden :not(type[type="hidden"])';
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex) {
                setButtonWavesEffect(event);
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ':disabled';
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                form.hide();
                loanding.show();
                $.post('/project/create', form.serialize(), function (json) {
                    var json_obj = JSON.parse(json);
                    loanding.hide();
                    if ( json_obj.error === false ) {
                        swal("¡Excelente!", "¡Proyecto creado exitosamente!", "success");
                        setTimeout(function () {
                            window.location = '/project';
                        }, 1000);
                    }else{
                        form.show();
                    }
                });

            }
        });

        validateMaterial(form);
    }

});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}

function update_couplings(templates, i)
{
    if ( i < 1 || templates.length <= 1 || templates[i] === undefined ) { return; }

    $.get('/template/get_structure/'+templates[i-1].id+'/'+templates[i].id, function (json) {
        var json_obj = JSON.parse(json);
        var $cpl = $('#coupling-model').clone();
        $cpl.removeAttr('id');

        $cpl.find('a.former').text('Plantilla '+i);
        $cpl.find('a.former').attr('data-original-title', templates[i-1].title);
        $cpl.find('a.latter').text('Plantilla '+(i+1));
        $cpl.find('a.latter').attr('data-original-title', templates[i].title);

        var former = $cpl.find('select.former').first();
        var latter = $cpl.find('select.latter').first();

        former.attr('name', 'couplings['+templates[i-1].id+'-'+templates[i].id+'][former]');
        latter.attr('name', 'couplings['+templates[i-1].id+'-'+templates[i].id+'][latter]');
        $cpl.find('select.concurrency').first().attr('name', 'couplings['+templates[i-1].id+'-'+templates[i].id+'][concurrency]');

        $.each(json_obj.former, function () {
            if ( former.find('optgroup[id="'+this.stage_id+'"]').length === 0 ) {
                former.append('<optgroup id="'+this.stage_id+'" label="'+this.stage+'"></optgroup>');
            }
            former.find('optgroup[id="'+this.stage_id+'"]').append('<option value="'+this.task_id+'">'+this.task+'</option>');
        });
        former.find('option').last().attr('selected', true);

        $.each(json_obj.latter, function () {
            if ( latter.find('optgroup[id="'+this.stage_id+'"]').length === 0 ) {
                latter.append('<optgroup id="'+this.stage_id+'" label="'+this.stage+'"></optgroup>');
            }
            latter.find('optgroup[id="'+this.stage_id+'"]').append('<option value="'+this.task_id+'">'+this.task+'</option>');
        });
        latter.find('option').eq(1).attr('selected', true);

        $('#coupling').append($cpl);
        $cpl.show();
    }).then(update_couplings(templates, i+1));

}

function update_positions(templates, i) {
    if ( templates.length < 1 || templates[i] === undefined ) { return; }

    $.get('/template/get_positions/'+templates[i].id, function (json) {
        var json_obj = JSON.parse(json);
        var $list = $('<div class="row"><div class="col-sm-12 m-b-0" id="tpl_'+templates[i].id+'"><strong><i class="glyphicon glyphicon-modal-window col-blue"></i> '+json_obj[0].template+' </strong></div></div>');

        $.each(json_obj, function (index, e) {
            if ( $list.find('#stg_'+e.stage_id).length < 1 ) {
                $list.find('#tpl_'+templates[i].id).append('<div class="col-sm-12 p-l-30 m-b-0" id="stg_'+e.stage_id+'"><strong><i class="glyphicon glyphicon-briefcase col-purple"></i> '+e.stage+'</strong></div>');
            }

            if ( $list.find('#tsk_'+e.task_id).length < 1 ) {
                $list.find('#stg_'+e.stage_id).append('<div class="col-sm-12 p-l-30 m-b-0" id="tsk_'+e.task_id+'"><strong><i class="glyphicon glyphicon-tasks col-green"></i> '+e.task+'</strong></div>');
            }

            $list.find('#tsk_'+e.task_id).append('<div class="col-sm-12 m-b-0" id="pos_'+e.position_id+'"><div class="col-sm-6 m-b-5 align-right"><h5>'+e.position+'</h5></div><div class="col-sm-6 m-b-5"><div class="input-group input-group-sm m-b-0"><span class="input-group-addon"><i class="material-icons">person</i> </span><div class="form-line focused"><select required name="positions['+e.task_id+'][]" data-position="'+e.position_id+'" class="form-control ms"></select></div></div></div></div>');
        });

        $list.appendTo('#personal');
        //$list.find('select.ms').select2({ width: '100%' });

    }).then(update_positions(templates, i+1));
}

function update_position_options() {
    var positions = {};
    //console.log('updating options');

    $('#personal').find('select.ms').each(function (i, e) {
        if ( positions[$(e).attr('data-position')] === undefined ) { positions[$(e).attr('data-position')] = true; }
    });

    $.each(positions, function (i, e) {
        $.get('/employee/get_by_position/'+i, function (json) {
            var json_obj = JSON.parse(json);
            $('#personal').find('select[data-position="'+i+'"]').each(function () {
                var $select = $(this);
                $select.html('');
                $select.append('<option value disabled selected>-- Seleccione un empleado --</option>');
                $.each(json_obj, function (index, element) {
                    if ( $select.find('optgroup[data-company="'+element.company_id+'"]').length < 1 ) {
                        $select.append('<optgroup label="'+element.company+'" data-company="'+element.company_id+'"></optgroup>');
                    }
                    $select.find('optgroup[data-company="'+element.company_id+'"]').append('<option value="'+element.user_id+'">'+element.full_name+'</option>');
                });

                $(this).select2({ width: '100%' });
                if ( json_obj.length < 1 ) {
                    $(this).closest('.form-line').addClass('error');
                }

                $(this).change(function () {
                    var value = $(this).find('option:selected').first().val();
                    // console.log(value);
                    var this_index = $('#personal').find('select[data-position="'+i+'"]').index(this);
                    $('#personal').find('select[data-position="'+i+'"]').not(this).slice(this_index).each(function () {
                        if ( $(this).find('option:selected').text() !== '-- Seleccione un empleado --' ) { return; }
                        $(this).find('option[value="'+value+'"]').first().prop('selected', true);
                        $(this).trigger('change');
                    });
                });

                if ($select.find('option').not('[value=""]').length === 1){
                    $select.find('option').not('[value=""]').first().attr('selected',true).trigger('change');
                }
            });
        });
    });

}

if ( $('#tasks-table').size() > 0 ) {
    var $table = $('#tasks-table');
    var tTable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/task/project_datatable_json/'+$table.data('project_id')
        },
        order: [ [0, 'asc'] ],
        columns: [
            {
                data: 'order',
                render : function(value, type, data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                searchable: false
            },
            { data: 'id' },
            {
                data: 'project',
                render: function(value, type, data) {
                    return '<a href="/project/f_view/'+data.project_id+'" target="_blank" data-toggle="tooltip" title="'+data.project_description+'">'+data.project+'</a>'
                }
            },
            { data: 'template' },
            { data: 'stage' },
            {
                data: 'title',
                render: function(value, type, data) {
                    return '<a href="/task/f_view/'+data.id+'" target="_blank" data-toggle="tooltip" title="'+data.description+'">'+data.title+'</a>'
                }
            },
            {
                data: 'status',
                render: function(value, type, obj) {
                    return obj.status.toUpperCase();
                }
            },
            {
                data: 'status_changer',
                render: function (value, type, obj, meta) {
                    var checked = obj.status_changer === 'true' ? 'checked' : '';
                    console.log(obj.status_changer);
                    return  '<div class="switch">' +
                        '   <label><input type="checkbox" disabled name="status_changer" value="true" '+checked+'><span class="lever switch-col-amber"></span></label>' +
                        '</div>';
                }
            },
            { data: 'stage_status' },
            { data: 'reason' },
            {
                data: 'progress',
                render: function(value, type, obj) {
                    return '<div class="progress">' +
                        '   <div class="progress-bar bg-green progress-bar-striped active" role="progressbar" style="width: '+obj.progress+'%">\n' +
                        '       '+obj.progress+'%' +
                        '   </div>' +
                        '</div>';
                },
                searchable: false
            },
            {
                data: 'resources',
                orderable: false,
                searchable: false
            },
            { data: 'start_date', render: function(value, type, obj) { return obj.start_date_nice; } },
            { data: 'estimated_end_date', render: function(value, type, obj) { return obj.estimated_end_date_nice; } },
            { data: 'end_date', render: function(value, type, obj) { return obj.end_date_nice; } },
            {
                data: 'estimated_end_date',
                render: function (value, type, obj, meta) {
                    var color = (obj.delay.charAt(0) === '-') ? 'col-red' : 'col-green';

                    if ( obj.status === 'pendiente' || obj.status === 'proceso' ) {
                        return '<span class="'+color+'">'+obj.delay+'</span>';
                    }
                    if ( obj.status === 'cerrado' || obj.status === 'anulado' ) {
                        return '';
                    }

                    return '<span class="'+color+'">'+obj.delay+'</span>';
                },
                searchable: false
            },
            {
                data: 'tag',
                render: function (value, type, obj, meta) {
                    var res = '';

                    if ( obj.tags ) {
                        obj.tags.split(',').forEach(function (t) {
                            if ( res !== '' ) { res+= ' '; }
                            res += '<span class="label bg-blue">'+t+'</span>';
                        });
                    }

                    return res;
                }
            },
            {
                data: 'client_visibility',
                render: function(value, type, obj) {
                    var res = '';
                    if ( obj.client_visibility === 'true' ) {
                        res += '<i class="material-icons col-blue" data-toggle="tooltip" data-original-title="Visible para el cliente">visibility</i>';
                    } else {
                        res += '<i class="material-icons col-grey" data-toggle="tooltip" data-original-title="No visible para el cliente">visibility_off</i>';
                    }
                    if ( obj.client_notification === 'true' ) {
                        res += '<i class="material-icons col-amber" data-toggle="tooltip" data-original-title="Notifica al cliente">notifications</i>';
                    } else {
                        res += '<i class="material-icons col-grey" data-toggle="tooltip" data-original-title="No notifica al cliente">notifications_off</i>';
                    }

                    return res;
                }
            }
        ]
    }));

    $('#delayed_tasks_filter').change(function () {
        if ( $(this).is(':checked') ) {
            tTable.ajax.url('/task/project_datatable_json/'+$table.data('project_id')+'?delayed=true').order([ [10, 'asc'] ]).load();
        } else {
            tTable.ajax.url('/task/project_datatable_json/'+$table.data('project_id')).order([ [0, 'asc'] ]).load();
        }
    });

}

if ( $('#tasks-edit-table').size() > 0 ) {
    var $table = $('#tasks-edit-table');
    $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/task/project_datatable_json/'+$table.data('project_id')
        },
        order: [ [0, 'asc'] ],
        columns: [
            {
                data: 'order',
                render : function(value, type, data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                searchable: false
            },
            { data: 'id' },
            { data: 'stage' },
            {
                data: 'title',
                render: function(value, type, data) {
                    return '<a href="/task/f_view/'+data.id+'" target="_blank" data-toggle="tooltip" title="'+data.description+'">'+data.title+'</a>'
                }
            },
            {
                data: 'status',
                render: function(value, type, obj) {
                    return obj.status.toUpperCase();
                }
            },
            {
                data: 'status_changer',
                render: function (value, type, obj, meta) {
                    var checked = obj.status_changer === 'true' ? 'checked' : '';
                    console.log(obj.status_changer);
                    return  '<div class="switch">' +
                        '   <label><input type="checkbox" disabled name="status_changer" value="true" '+checked+'><span class="lever switch-col-amber"></span></label>' +
                        '</div>';
                }
            },
            { data: 'stage_status' },
            { data: 'reason' },
            {
                data: 'progress',
                render: function(value, type, obj) {
                    return '<div class="progress">' +
                        '   <div class="progress-bar bg-green progress-bar-striped active" role="progressbar" style="width: '+obj.progress+'%">\n' +
                        '       '+obj.progress+'%' +
                        '   </div>' +
                        '</div>';
                },
                searchable: false
            },
            {
                data: 'resources',
                orderable: false,
                searchable: false
            },
            { data: 'start_date', render: function(value, type, obj) { return obj.start_date_nice; } },
            { data: 'estimated_end_date', render: function(value, type, obj) { return obj.estimated_end_date_nice; } },
            { data: 'end_date', render: function(value, type, obj) { return obj.end_date_nice; } },
            {
                data: 'client_visibility',
                render: function(value, type, obj) {
                    var res = '';
                    if ( obj.client_visibility === 'true' ) {
                        res += '<i class="material-icons col-blue" data-toggle="tooltip" data-original-title="Visible para el cliente">visibility</i>';
                    } else {
                        res += '<i class="material-icons col-grey" data-toggle="tooltip" data-original-title="No visible para el cliente">visibility_off</i>';
                    }
                    if ( obj.client_notification === 'true' ) {
                        res += '<i class="material-icons col-amber" data-toggle="tooltip" data-original-title="Notifica al cliente">notifications</i>';
                    } else {
                        res += '<i class="material-icons col-grey" data-toggle="tooltip" data-original-title="No notifica al cliente">notifications_off</i>';
                    }

                    return res;
                }
            },
            {
                data: function(data) {
                    return 	'<div class="btn-group"><a href="/task/edit/'+data.id+'" target="_blank" class="btn btn-info btn-xs waves-effect" data-toggle="tooltip" data-original-title="Editar"><i class="material-icons">edit</i></a>'+
                        '<a href="javascript:void(0);" class="btn btn-danger btn-xs waves-effect delete-btn" data-id="' + data.id + '" data-controller="task" data-text="¿Está seguro que desea eliminar esta tarea?" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a></div>';
                },
                orderable: false,
                searchable: false
            }
        ]
    }));
}

if ( $('#project-edit-form').size() > 0 ) {
    var form = $('#project-edit-form');
    form.find('input[name="tags"]').tagsinput({
        trimValue: true
    });
    form.find('input[name="start_date"]').datepicker({
        changeYear: true
    });
    form.find('input[name="estimated_end_date"]').datepicker({
        changeYear: true
    });

    form.find('select[name="client_id"]').on('change', function () {
        $.get('/client/get_client/'+$(this).val(), function (response) {
            var resp = JSON.parse(response);
            if( resp.error === false && resp.message.account_manager !== null){
                form.find('select[name="manager_id"] option[value="'+resp.message.account_manager+'"]').first().prop('selected', true).trigger('change');
            } else {
                form.find('select[name="manager_id"] option[value=""]').first().prop('selected', true).trigger('change');
            }
        });
    });
}

if ( $('#comments-panel').size() > 0 ) {

    var update_comments = function () {
        $.get('/project/get_comments/'+$('#comments-panel').data('project_id'), function (html) {
            $('#comments-panel').html(html);
        });
    };

    update_comments();

    $('#send-comment').click(function () {
        var data = $('#comment-form').serializeArray();
        $.post('/project/new_comment', data, function (json) {
            var json_obj = JSON.parse(json);
            if ( json_obj.error ) {
                sweetNotification('Error', json_obj.message, 'error');
            } else {
                $('#comment-form').find('input[name="message"]').val('');
                update_comments();
            }
        })
    });

    $('#client-visibility-toggle').click(function () {
        var $icon = $(this).find('i.material-icons').first();
        if ( $icon.hasClass('col-grey') ) {
            $(this).html('<input type="hidden" name="client_visibility" value="true"><i class="material-icons col-blue">visibility</i>');
        } else {
            $(this).html('<input type="hidden" name="client_visibility" value="false"><i class="material-icons col-grey">visibility_off</i>');
        }
    });

    $(document).on('click','.client-visibility-comment-toggle', function (){
        var $icon = $(this).find('i.material-icons').first();
        if ( $icon.hasClass('col-grey') ) {
            $(this).html('<input type="hidden" name="client_visibility" value="true"><i class="material-icons col-blue">visibility</i>');
        } else {
            $(this).html('<input type="hidden" name="client_visibility" value="false"><i class="material-icons col-grey">visibility_off</i>');
        }

        var val = $(this).find('input[name="client_visibility"]').first().val();
        $.post('/project/update_comment_visibility/'+$(this).data('id'),{client_visibility:val}, function (response){
           var resp = JSON.parse(response);
            if ( resp.error ) {
                if ( $icon.hasClass('col-grey') ) {
                    $(this).html('<input type="hidden" name="client_visibility" value="true"><i class="material-icons col-blue">visibility</i>');
                } else {
                    $(this).html('<input type="hidden" name="client_visibility" value="false"><i class="material-icons col-grey">visibility_off</i>');
                }
                sweetNotification('Error', resp.message, 'error');
            }
        });
    });

    setInterval(function () {
        if ( $('#chat-panel').hasClass('active') ) {
            update_comments();
        }
    }, 3000);
}

if ( $('#files-table').size() > 0 ) {
    var $table = $('#files-table');
    var oTable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/project/files_datatable_json/'+$table.data('project_id')
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'id' },
            {
                data: 'name',
                render: function (value, type, obj, meta) {
                    return '<a href="/'+obj.path+'" target="_blank"><img src="/'+obj.icon+'"> '+value+'</a>';
                }
            },
            { data: 'description' },
            { data: 'user' },
            { data: 'created_at', render: function(value, type, obj) { return obj.created_at_nice; } },
            { data: 'updated_at', render: function(value, type, obj) { return obj.updated_at_nice; } },
            {
                data: function(data) {
                    var buttons = '<div class="btn-group">';
                    var icon = '';
                    if (data.client_visibility === 'true'){
                        icon = '<input type="hidden" name="client_visibility" value="true"><i class="material-icons col-blue">visibility</i>';
                    } else {
                        icon = '<input type="hidden" name="client_visibility" value="false"><i class="material-icons col-grey">visibility_off</i>';
                    }

                    if (data.visibility === 'true') {
                        buttons += '<a href="javascript:void(0);" data-id="' + data.id + '" class="btn btn-default btn-xs waves-effect visibility-file" data-toggle="tooltip" data-original-title="Visibilidad para el cliente">'+ icon +'</a>';
                    }
                    if (data.edit === 'true'){
                        buttons += '<a href="javascript:void(0);" data-id="'+data.id+'" class="btn btn-info btn-xs waves-effect edit-file" data-toggle="tooltip" data-original-title="Editar Descripción"><i class="material-icons">edit</i></a>';
                    }
                    if (data.delete === 'true'){
                        buttons += '<a href="javascript:void(0);" data-id="'+data.id+'" class="btn btn-danger btn-xs waves-effect delete-file" data-toggle="tooltip" data-original-title="Eliminar"><i class="material-icons">delete</i></a>';
                    }
                    buttons += '</div>';

                    return buttons;
                },
                orderable: false,
                searchable: false
            }
        ]
    }));

    $('#client-visibility-file-toggle').click(function (){
        var $icon = $(this).find('i.material-icons').first();
        if ( $icon.hasClass('col-grey') ) {
            $(this).html('<input type="hidden" name="client_visibility" value="true"><i class="material-icons col-blue">visibility</i>');
        } else {
            $(this).html('<input type="hidden" name="client_visibility" value="false"><i class="material-icons col-grey">visibility_off</i>');
        }
    });

    $(document).on('click', '.visibility-file', function (){
        var $icon = $(this).find('i.material-icons').first();
        if ( $icon.hasClass('col-grey') ) {
            $(this).html('<input type="hidden" name="client_visibility" value="true"><i class="material-icons col-blue">visibility</i>');
        } else {
            $(this).html('<input type="hidden" name="client_visibility" value="false"><i class="material-icons col-grey">visibility_off</i>');
        }

        var val = $(this).find('input[name="client_visibility"]').val();
        $.post('/project/update_file_visibility/'+$(this).data('id'),{client_visibility:val}, function (response){
            var resp = JSON.parse(response);
            if ( resp.error ) {
                if ( $icon.hasClass('col-grey') ) {
                    $(this).html('<input type="hidden" name="client_visibility" value="true"><i class="material-icons col-blue">visibility</i>');
                } else {
                    $(this).html('<input type="hidden" name="client_visibility" value="false"><i class="material-icons col-grey">visibility_off</i>');
                }
                sweetNotification('Error', resp.message, 'error');
            }
        });
    });

    $(document).on('click', '.edit-file', function () {
        var file_id = $(this).data('id');
        swal({
            title: 'Cambiar descripción de archivo',
            text: 'Introduzca la nueva descripción',
            type: "input",
            confirmButtonText: "Guardar",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function (description) {
            if (description !== false && description === "") {
                swal.showInputError("Debe escribir una descripción.");
                return false;
            } else {
                $.post('/project/update_file_description/'+file_id, {description:description}, function (json) {
                    var obj = JSON.parse(json);
                    if (obj.error) { swal.showInputError(obj.message); return false; }

                    sweetNotification("¡Bien!", "¡Cambios guardados!", "success", 1000);
                    oTable.ajax.reload();
                });
            }
        });
    });

    $(document).on('click', '.delete-file', function (e) {
        e.preventDefault();
        var $btn = $(this);
        swal({
                title: "Eliminar",
                text: "¿Está seguro que desea eliminar este elemento?",
                type: "warning",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Si, eliminar!",
                closeOnConfirm: false
            },
            function() {
                $.post('/project/delete_file/' + $btn.data('id'), function (json) {
                    var json_obj = JSON.parse(json);
                    if (json_obj.error) {
                        sweetNotification('Error', json_obj.message, 'error');
                    } else {
                        sweetNotification("Bien!", "Eliminado correctamente!", "success", 1000);
                        oTable.ajax.reload();
                    }
                });
            });
    });

    $('#file-input').click(function () {
        $('#project_file').click();
    });

    $('#project_file').change(function () {
        if ( $('#project_file')[0].files.length > 0 ) {
            $('#file-name').text($('#project_file')[0].files[0].name).show();
        } else {
            $('#file-name').text('').hide();
        }
    });

    $('#save-file').click(function () {
        var project_id = $('#save-file').data('project_id');
        var data = new FormData();

        if ( $('#project_file')[0].files.length === 0 ) {
            sweetNotification('Error', 'Debe seleccionar un archivo', 'error');
            return;
        }

        $('#file-spinner').show();

        data.append('project_file', $('#project_file')[0].files[0]);
        data.append('description', $('#project_file_description').val());
        if ($('#client-visibility-file-toggle').size() > 0){
            data.append('client_visibility', $('#client-visibility-file-toggle').find('input[name="client_visibility"]').val());
        } else {
            data.append('client_visibility', 'false');
        }

        $.ajax( {
            url: '/project/new_file/'+project_id,
            type: 'POST',
            data: data,
            processData: false,
            contentType: false
        }).done(function( json ) {
            var json_obj = JSON.parse(json);
            if ( json_obj.error ) {
                sweetNotification('Error', json_obj.message, 'error');
            } else {
                oTable.ajax.reload();
                $('#project_file_description').val('');
                $('#project_file').val('');
                $('#file-name').text('').hide();

                var $icon = $('#client-visibility-file-toggle').find('i.material-icons').first();
                if ( !$icon.hasClass('col-grey') ) {
                    $('#client-visibility-file-toggle').html('<input type="hidden" name="client_visibility" value="false"><i class="material-icons col-grey">visibility_off</i>');
                }
            }
            $('#file-spinner').hide();
        });

    });

    setInterval(function () {
        if ( $('#files-panel').hasClass('active') ) {
            oTable.ajax.reload();
        }
    }, 10000);
}

if ( $('#gantter').size() > 0 ) {

    $.get('/project/gantt/'+$('#gantter').data('project_id'), function (json) {
        var obj = JSON.parse(json);

        var g = new JSGantt.GanttChart(document.getElementById('gantter'), 'day');
        g.addLang('es', {
            january:        'Enero',
            february:       'Febrero',
            march:          'Marzo',
            april:          'Abril',
            maylong:	    'Mayo',
            june:	        'Junio',
            july:	        'Julio',
            august:	        'Agosto',
            september:	    'Septiembre',
            october:	    'Octubre',
            november:	    'Noviembre',
            december:	    'Diciembre',
            jan:	        'Ene',
            feb:	        'Feb',
            mar:	        'Mar',
            apr:	        'Abr',
            may:	        'May',
            jun:	        'Jun',
            jul:	        'Jul',
            aug:	        'Ago',
            sep:	        'Sep',
            oct:	        'Oct',
            nov:	        'Nov',
            dec:	        'Dic',
            sunday:	        'Domingo',
            monday:	        'Lunes',
            tuesday:	    'Martes',
            wednesday:	    'Miércoles',
            thursday:	    'Jueves',
            friday:	        'Viernes',
            saturday:	    'Sábado',
            sun:	        'Dom',
            mon:	        'Lun',
            tue:	        'Mar',
            wed:	        'Mié',
            thu:	        'Jue',
            fri:	        'Vie',
            sat:	        'Sáb',
            resource:	    'Recurso',
            duration:	    'Duración',
            comp:	        '% Prog.',
            completion:	    'Progreso',
            startdate:	    'Fecha Inicio',
            enddate:	    'Fecha Fin',
            moreinfo:	    'Más info',
            notes:	        'Notas',
            format:	        'Formato',
            hour:	        'Hora',
            day:	        'Día',
            week:	        'Semana',
            month:	        'Mes',
            quarter:	    'Trimestre',
            hours:	        'Horas',
            days:	        'Días',
            weeks:	        'Semanas',
            months:	        'Meses',
            quarters:	    'Trimestres',
            hr:	            'Hr',
            dy:	            'Día',
            wk:	            'Sem',
            mth:	        'Mes',
            qtr:	        'Trim',
            hrs:	        'Hrs',
            dys:	        'Días',
            wks:	        'Semns',
            mths:	        'Meses',
            qtrs:	        'Trims'
        });
        g.setLang('es');
        g.setFormatArr("day","week","month");
        g.setDayMajorDateDisplayFormat('d/mon/yy'); // Set format to display dates ('mm/dd/yyyy', 'dd/mm/yyyy', 'yyyy-mm-dd')
        g.setDateTaskTableDisplayFormat('d/mon/yy');
        g.setWeekMinorDateDisplayFormat('d mon');
        g.setShowTaskInfoLink(true);
        g.setUseSort(0);

        var proj = obj.project;
        g.AddTaskItem(new JSGantt.TaskItem('1'+padToSix(proj.id), proj.name, '', '', 'ggroupblack', '/task?project='+proj.id, 0, '', 0, 1, 0, 1, '', '', proj.description, g));
        $.each(obj.templates, function (index, template) {
            g.AddTaskItem(new JSGantt.TaskItem('3'+padToSix(template.id), template.title, '', '', 'ggroupblack', '/task?project='+proj.id+'&template='+template.id, 0, '', 0, 1, '1'+padToSix(proj.id), 1, '', '', template.description, g));
            $.each(obj.stages, function (index_s, stage) {
                if ( stage.template_id !== template.id ) { return; }
                var parent = (stage.template_id) ? '3'+padToSix(stage.template_id) : '1'+padToSix(proj.id);
                g.AddTaskItem(new JSGantt.TaskItem('2'+padToSix(stage.id), stage.title, '', '', 'ggroupblack', '/task?stage='+stage.id, 0, '', 0, 1, parent, 1, '', '', stage.description, g));

                $.each(obj.tasks, function (index_t, task) {
                    if ( task.stage_id !== stage.id ) { return; }
                    g.AddTaskItem(new JSGantt.TaskItem(task.id, task.title, task.start_date, task.g_end_date, 'gtaskblue', '/task?task='+task.id, 0, task.resources, task.progress, 0, '2'+padToSix(task.stage_id), 1, task.prerequisites, '', task.description, g));
                });
            });
        });
        /*$.each(obj.stages, function (index, stage) {
            var parent = (stage.template_id) ? '3'+padToSix(stage.template_id) : '1'+padToSix(proj.id);
            g.AddTaskItem(new JSGantt.TaskItem('2'+padToSix(stage.id), stage.title, '', '', 'ggroupblack', '/task?stage='+stage.id, 0, '', 0, 1, parent, 1, '', '', stage.description, g));
        });
        $.each(obj.tasks, function (index, task) {
            g.AddTaskItem(new JSGantt.TaskItem(task.id, task.title, task.start_date, task.g_end_date, 'gtaskblue', '/task?task='+task.id, 0, task.resources, task.progress, 0, '2'+padToSix(task.stage_id), 1, task.prerequisites, '', task.description, g));
        });*/
        g.Draw();
    });
}

if ( $('#logs-table').size() > 0 ) {
    var $table = $('#logs-table');
    var lTable = $table.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/project/logs_datatable_json/' + $table.data('project_id')
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            {
                data: 'created_at', render: function (value, type, obj) {
                return obj.created_at_nice;
                }

            },
            {
                data: 'message'
            },
            {data: 'username'}
        ]
    }));

    $('#log_tab_header').on('shown.bs.tab', function (e) {
        lTable.ajax.reload();
    });
}

$(document).on('click', '.delete-budget', function (e){
    e.preventDefault();
    var $button = $(this);
    swal({
        title: "Eliminar presupuesto",
        text: "¿Está seguro que desea eliminar este presupuesto?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, eliminar!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        $.post('/project_brand/delete/' + $button.data('id'), {}, function (response) {
            var resp = JSON.parse(response);

            if (resp.error === false) {
                sweetNotification("¡Bien!", "¡Eliminado correctamente!", "success");
                window.location.reload();
            } else {
                sweetNotification("¡Error!", resp.message, "error");
            }
        });
    });
});

if ($('#budget-new-form').size() > 0){
    if($('input[name="estimated_date"]').size() > 0){
        $('#estimated-date').datepicker({
            changeYear: true
        });
        $('#actual-date').datepicker({
            changeYear: true
        });
    }

    $('#budget-new-form').on('submit',function (e) {
        e.preventDefault();
        var $form = $('#budget-new-form');
        $form.hide();
        $('#budget-spinner').show();
        $.post('/project_brand/create',$form.serialize(), function (response) {
            var resp = JSON.parse(response);
            $('#budget-spinner').hide();
            $form.show();

            if ( !resp.error ){
                sweetNotification("¡Bien!", "¡Presupuesto creado sastifactoriamente!", "success");
                $('#budget-reset').click();
                refresh_budget_table();
                /**/
            } else {
                sweetNotification('Error', resp.message, 'error');
            }
        });
    });

    $(document).on('click', '.budget-edit', function (e) {
        e.preventDefault();
        /*$('#estimated-date-edit').datepicker({
            changeYear: true
        });
        $('#actual-date-edit').datepicker({
            changeYear: true
        });*/
        $.get('/project_brand/get_budget/'+$(this).data('id'), function (response) {
            var resp = JSON.parse(response);

            if ( resp.error ) return;
            var estimatedsplit = resp.message.budget.estimated_date.split('-');
            var actualsplit = resp.message.budget.actual_date.split('-');
            $('#budget-edit-form').find('#budget-id').val(resp.message.budget.id);
            $('#budget-edit-form').find('#project-id').val(resp.message.budget.project_id);
            $('#brand-id-edit option').each(function () {
                if ($(this).val() == resp.message.budget.brand_id){
                    $(this).prop("selected",true).change();
                }
            });
            $('#budget-edit-form').find('#brand-id-edit').html(resp.message.brands);
            $('#estimated-amount-edit').val(resp.message.budget.estimated_amount);
            $('#estimated-date-edit').val(resp.message.budget.estimated_date);
            $('#actual-amount-edit').val((resp.message.budget.actual_amount == 0) ? '0.00' : resp.message.budget.actual_amount);
            $('#actual-date-edit').val(resp.message.budget.actual_date);
        });
        $('#budget-edit-modal').modal('show');
        validateMaterial($('#budget-edit-form'));
    });

    $('#edit-budget').click( function () {
        if ($('#budget-edit-form').valid()){
            $('#edit-budget').hide();
            $('#budget-edit-form').submit();
        }
    });

    $('#budget-edit-form').submit(function (e) {
        e.preventDefault();

       var form = $('#budget-edit-form');
       $.post('/project_brand/edit/'+$('#budget-id').val(), form.serialize(), function (response) {
           var resp = JSON.parse(response);

           if ( resp.error ){
               swal.showInputError(resp.message);
           } else {
               sweetNotification("¡Bien!", "¡presupuesto editado correctamente!", "success", 2000);
               $('#budget-edit-modal').modal('hide');
           }
           refresh_budget_table();
           $('#edit-budget').show();
       });
    });
}

totalBudget();
function refresh_budget_table(){
    $.get('/project/create_tbody/'+$('#budget-project-id').val(),function (response) {
        var resp = JSON.parse(response);
        if ( resp.error ) return;

        $('#brand-list').find('tbody').html(resp.data);
        totalBudget();
    });
}
function totalBudget() {
    if ($('#brand-list').size() > 0){
        var totalEstimatedAmount=0;
        var totalActualAmount=0;
        $(".estimated-amount").each(function(){
            totalEstimatedAmount+=parseFloat($(this).data('amount')) || 0;
        });
        $(".actual-amount").each(function(){
            totalActualAmount+=parseFloat($(this).data('amount')) || 0;
        });

        $('.total-actual').text(formmatterCurrency(totalActualAmount));
        $('.total-estimated').text(formmatterCurrency(totalEstimatedAmount));
    }
}

$('#brand_tab_header').on('shown.bs.tab', function (e) {
    refresh_budget_table();
});

if ($('#dashboard-panel').size() > 0){
    var project_id = window.location.pathname.split( '/' )[3];
    /*********************************
     *           Stage_task          *
     * *******************************/
    var stage_chart = null;
    create_stage_task_line_chart();
    function create_stage_task_line_chart(){
        $.get('/chart/stage_task/'+project_id, function (response){
            $('#load-stage_task').hide();
            $('#load-stage_task').siblings('h3').hide();
            var resp = JSON.parse(response);

            if ( resp.error ) { return; }

            var bar_config = {
                type: 'pie',
                data: {
                    labels: resp.data.labels,
                    datasets: [{
                        data: resp.data.data,
                        backgroundColor: resp.data.backgroundColor
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'right'
                    },
                    tooltips: {
                        callbacks: {
                            title: function(tooltipItem, data) {
                                var total;
                                for (var i in data['datasets'][0]._meta){
                                    total = data['datasets'][0]._meta[i].total;
                                }
                                var percent = Math.round((data['datasets'][0]['data'][tooltipItem[0]['index']] / total) * 100);
                                return data['labels'][tooltipItem[0]['index']]+': '+percent+'%';
                            },
                            label: function(tooltipItem, data){
                                var data = data['datasets'][0]['data'][tooltipItem['index']];
                                return 'Cantidad: ' + data;
                            }
                        }
                    },
                    onClick: function (evt) {
                        var activeElement = this.getElementAtEvent(evt);
                        if ( activeElement.length === 0 ) return;

                        var clicked_stage = 0;
                        $.each(resp.data.labelskey, function(index, element) {
                           if (element === activeElement[0]._model['label'])  clicked_stage = index; return;
                        });

                        create_stage_task_bar_chart(clicked_stage, activeElement[0]._view['backgroundColor']);
                    }
                }
            };

            stage_chart = new Chart(document.getElementById("stage_task_chart").getContext("2d"), bar_config);
            if (stage_chart !== null) { $('#load-stage_task').siblings('h3').show(); }
        }); // END STAGE_TASK
    }
    function create_stage_task_bar_chart(stage, backgroundColor){
        stage_chart.destroy();
        $('#load-stage_task').show();
        $('#load-stage_task').siblings('h3').hide();
        $.get('/chart/stage_task_date/'+project_id+'/'+stage, function (response) {
            var resp = JSON.parse(response);
            if ( resp.error ) { return; }
            
            var config = {
                type: 'bar',
                data: {
                    labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    datasets: [{
                        data: resp.data.data,
                        backgroundColor: backgroundColor
                    }]
                },
                options: {
                    response: true,
                    legend: false,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Cantidad de tareas'
                            }
                        }]
                    },
                    onClick: function (evt) {
                        var activeElement = this.getElementAtEvent(evt);
                        if ( activeElement.length === 0 ) {
                            stage_chart.destroy();
                            $('#load-stage_task').siblings('h3').hide();
                            $('#load-stage_task').show();
                            create_stage_task_line_chart();
                            return;
                        }

                        var chart = this;
                        var month = activeElement[0]._index+1;
                        $.get('/chart/stage_task_date/'+project_id+'/'+stage+'/'+month, function (response) {
                            var resp = JSON.parse(response);
                            if ( resp.error ) { return; }

                            var c_labels = chart.config.data.labels,
                                c_datasets = chart.config.data.datasets,
                                c_click_function = chart.options.onClick;
                            chart.config.data.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
                            chart.config.data.datasets = [{
                                data: resp.data.data,
                                backgroundColor: backgroundColor
                            }];
                            chart.options.onClick = function (evt) {
                                var activeElement = chart.getElementAtEvent(evt);
                                if ( activeElement.length === 0 ) {
                                    chart.data.labels = c_labels;
                                    chart.data.datasets = c_datasets;
                                    chart.options.onClick = c_click_function;
                                    chart.update();
                                    return;
                                }
                                var day = activeElement[0]._index+1;
                                month = (month < 10) ? ('0'+month) : month;
                                day = (day < 10) ? ('0'+day) : day;
                                window.open('/task/task_filter_list?day='+day+'&month='+month+'&project_id='+project_id+'&stage='+stage);
                            };
                            chart.update();
                        });
                    }
                }
            };
            $('#load-stage_task').hide();
            stage_chart = new Chart(document.getElementById("stage_task_chart").getContext("2d"), config);
            if (stage_chart !== null) {  $('#load-stage_task').siblings('h3').show();  }
        });
    }

    /*********************************
     *           Status_task         *
     * *******************************/
    var status_chart = null;
    create_status_line_chart();
    
    function create_status_line_chart(){
        $.get('/chart/task_status/'+project_id, function (response){
            $('#load-status_task').hide();
            $('#load-status_task').siblings('h3').hide();
            var resp = JSON.parse(response);
            if ( resp.error ) { return; }

            var bar_config = {
                type: 'pie',
                data: {
                    labels: resp.data.labels,
                    datasets: [{
                        data: resp.data.data,
                        backgroundColor: resp.data.backgroundColor
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'right'
                    },
                    tooltips: {
                        callbacks: {
                            title: function(tooltipItem, data) {
                                var total;
                                for (var i in data['datasets'][0]._meta){
                                    total = data['datasets'][0]._meta[i].total;
                                }
                                var percent = Math.round((data['datasets'][0]['data'][tooltipItem[0]['index']] / total) * 100);
                                return $.trim(data['labels'][tooltipItem[0]['index']])+': '+percent+'%';
                            },
                            label: function(tooltipItem, data){
                                var data = data['datasets'][0]['data'][tooltipItem['index']];
                                return 'Cantidad: '+data;
                            }
                        }
                    },
                    onClick: function (evt) {
                        var activeElement = this.getElementAtEvent(evt);

                        if ( activeElement.length === 0 ) return;

                        var clicked_status = 0;
                        $.each(resp.data.labelskey, function(index, element) {
                            if (element === activeElement[0]._model['label'])  clicked_status = index; return;
                        });

                        create_status_bar_chart(clicked_status, activeElement[0]._view['backgroundColor']);
                    }
                }
            };
            status_chart = new Chart(document.getElementById("status_task_chart").getContext("2d"), bar_config);
            if (status_chart !== null) { $('#load-status_task').siblings('h3').show(); }
        }); // END STATUS_TASK
    }

    function create_status_bar_chart(status, backgroundColor) {
        status_chart.destroy();
        var task_status = status;
        $('#load-status_task').show();
        $('#load-status_task').siblings('h3').hide();
        $.get('/chart/status_line_chart_month/'+project_id+'/'+status, function (response) {
            var resp = JSON.parse(response);
            if ( resp.error ) { return; }

            var bar_config = {
                type: 'bar',
                data: {
                    labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    datasets: [{
                        data: resp.data.data,
                        backgroundColor: backgroundColor
                    }]
                },
                options: {
                    responsive: true,
                    legend: false,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Cantidad de tickets'
                            }
                        }]
                    },
                    onClick: function (evt) {
                        var activeElement = this.getElementAtEvent(evt);
                        if ( activeElement.length === 0 ) {
                            status_chart.destroy();
                            $('#load-status_task').siblings('h3').hide();
                            $('#load-status_task').show();
                            create_status_line_chart();
                            return;
                        }

                        var chart = this;
                        var month = activeElement[0]._index+1;
                        $.get('/chart/status_line_chart_day/'+project_id+'/'+task_status+'/'+month, function (response) {
                           var resp = JSON.parse(response);
                           if ( resp.error ) { return; }

                           var c_labels = chart.config.data.labels,
                               c_datasets = chart.config.data.datasets,
                               c_click_function = chart.options.onClick;
                           chart.config.data.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
                           chart.config.data.datasets = [{
                               data: resp.data.data,
                               backgroundColor: backgroundColor
                           }];
                           chart.options.onClick = function (evt) {
                               var activeElement = chart.getElementAtEvent(evt);
                               if ( activeElement.length === 0 ) {
                                   chart.data.labels = c_labels;
                                   chart.data.datasets = c_datasets;
                                   chart.options.onClick = c_click_function;
                                   chart.update();
                                   return;
                               }
                               var day = activeElement[0]._index+1;
                               month = (month < 10) ? ('0'+month) : month;
                               day = (day < 10) ? ('0'+day) : day;
                               window.open('/task/task_filter_list?day='+day+'&month='+month+'&project_id='+project_id+'&status='+clear(task_status));
                           };
                           chart.update();
                        });
                    }
                }
            };

            $('#load-status_task').hide();
            status_chart = new Chart(document.getElementById("status_task_chart").getContext("2d"), bar_config);
            if (status_chart !== null) {  $('#load-status_task').siblings('h3').show();  }
        });
    }

    /*********************************
     *        Estimated budget       *
     * *******************************/
    var estimated_chart = null;
    create_estimated_pie_chart();
    
    function create_estimated_pie_chart(){
        $.get('/chart/estimated_budget/'+project_id, function (response){
            $('#load-estimated-budget').hide();
            $('#load-estimated-budget').siblings('h3').hide();
            var resp = JSON.parse(response);
            if ( resp.error ) { return; }

            var bar_config = {
                type: 'pie',
                data: {
                    labels: resp.data.labels,
                    datasets: [{
                        data: resp.data.data,
                        backgroundColor: resp.data.backgroundColor
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'right'
                    },
                    tooltips: {
                        callbacks: {
                            title: function(tooltipItem, data) {
                                var total;
                                for (var i in data['datasets'][0]._meta){
                                    total = data['datasets'][0]._meta[i].total;
                                }
                                var percent = Math.round((data['datasets'][0]['data'][tooltipItem[0]['index']] / total) * 100);
                                return $.trim(data['labels'][tooltipItem[0]['index']])+': '+percent+'%';
                            },
                            label: function(tooltipItem, data){
                                var data = formmatterCurrency(data['datasets'][0]['data'][tooltipItem['index']]);
                                return 'Cantidad: '+data;
                            }
                        }
                    },
                    onClick: function (evt) {
                        var activeElement = this.getElementAtEvent(evt);
                        if ( activeElement.length === 0 ) return;

                        var clicked_estimated = 0;
                        $.each(resp.data.labelskey, function(index, element) {
                            if (element === activeElement[0]._model['label'])  clicked_estimated = index; return;
                        });

                        create_estimated_bar_chart(clicked_estimated, activeElement[0]._view['backgroundColor']);
                    }
                }
            };
            estimated_chart =  new Chart(document.getElementById("estimated-budget-chart").getContext("2d"), bar_config);
            if (estimated_chart !== null) { $('#load-estimated-budget').siblings('h3').show(); }
        }); // END ESTIMATED_BUDGET
    }

    function create_estimated_bar_chart(brand, backgroundColor){
        estimated_chart.destroy();
        $('#load-estimated-budget').show();
        $('#load-estimated-budget').siblings('h3').hide();
        $.get('/chart/estimated_budget_date/'+project_id+'/'+brand, function (response) {
            var resp = JSON.parse(response);
            if ( resp.error ) { return; }

            var bar_config = {
                type: 'bar',
                data: {
                    labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    datasets: [{
                        data: resp.data.data,
                        backgroundColor: backgroundColor
                    }]
                },
                options: {
                    responsive: true,
                    legend: false,
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Montos'
                            }
                        }]
                    },
                    onClick: function (evt) {
                        var activeElement = this.getElementAtEvent(evt);
                        if ( activeElement.length === 0 ) {
                            estimated_chart.destroy();
                            $('#load-estimated-budget').siblings('h3').hide();
                            $('#load-estimated-budget').show();
                            create_estimated_pie_chart();
                            return;
                        }

                        var chart = this;
                        var month = activeElement[0]._index+1;
                        $.get('/chart/estimated_budget_date/'+project_id+'/'+brand+'/'+month, function (response) {
                            var resp = JSON.parse(response);
                            if ( resp.error ) { return; }

                            var c_labels = chart.config.data.labels,
                                c_datasets = chart.config.data.datasets,
                                c_click_function = chart.options.onClick;
                            chart.config.data.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
                            chart.config.data.datasets = [{
                                data: resp.data.data,
                                backgroundColor: backgroundColor
                            }];
                            chart.options.onClick = function (evt) {
                                var activeElement = chart.getElementAtEvent(evt);
                                if ( activeElement.length === 0 ) {
                                    chart.data.labels = c_labels;
                                    chart.data.datasets = c_datasets;
                                    chart.options.onClick = c_click_function;
                                    chart.update();
                                    return;
                                }
                                var day = activeElement[0]._index+1;
                                month = (month < 10) ? ('0'+month) : month;
                                day = (day < 10) ? ('0'+day) : day;
                                var my_year = (year === '/all') ? date.getFullYear() : $('#chart_year').val().replace('/','');
                                window.open('/project_brand/brand_list_date?date='+my_year+'-'+month+'-'+day+'&status=all&company=all&category=all&project_id='+project_id+'&brand='+brand);
                            };
                            chart.update();
                        });
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data){
                                var text = 'Monto Estimado: ' + formmatterCurrency(tooltipItem.yLabel);
                                return text;
                            }
                        }
                    }
                }
            };

            $('#load-estimated-budget').hide();
            estimated_chart = new Chart(document.getElementById("estimated-budget-chart").getContext("2d"), bar_config);
            if ( estimated_chart !== null ) { $('#load-estimated-budget').siblings('h3').show(); }
        });
    }

    /*********************************
     *          Actual budget        *
     * *******************************/
    var actual_chart = null;
    create_actual_pie_chart();
    
    function create_actual_pie_chart(){
        $.get('/chart/actual_budget/'+project_id, function (response){
            $('#load-actual-budget').hide();
            var resp = JSON.parse(response);
            if ( resp.error ) { return; }

            var pie_config = {
                type: 'pie',
                data: {
                    labels: resp.data.labels,
                    datasets: [{
                        data: resp.data.data,
                        backgroundColor: resp.data.backgroundColor
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'right'
                    },
                    tooltips: {
                        callbacks: {
                            title: function(tooltipItem, data) {
                                var total;
                                for (var i in data['datasets'][0]._meta){
                                    total = data['datasets'][0]._meta[i].total;
                                }
                                var percent = Math.round((data['datasets'][0]['data'][tooltipItem[0]['index']] / total) * 100);
                                return $.trim(data['labels'][tooltipItem[0]['index']])+': '+percent+'%';
                            },
                            label: function(tooltipItem, data){
                                var data = formmatterCurrency(data['datasets'][0]['data'][tooltipItem['index']]);
                                return 'Cantidad: '+data;
                            }
                        }
                    },
                    onClick: function (evt) {
                        var activeElement = this.getElementAtEvent(evt);

                        if ( activeElement.length === 0 ) return;

                        var clicked_actual = 0;
                        $.each(resp.data.labelskey, function(index, element) {
                            if (element === activeElement[0]._model['label'])  clicked_actual = index; return;
                        });

                        create_actual_bar_chart(clicked_actual, activeElement[0]._view['backgroundColor']);
                    }
                }
            };
            actual_chart = new Chart(document.getElementById("actual-budget-chart").getContext("2d"), pie_config);
            if ( actual_chart !== null ) { $('#load-actual-budget').siblings('h3').show(); }
        }); // END ACTUAL_BUDGET
    }
    
    function create_actual_bar_chart(brand, backgroundColor){
        actual_chart.destroy();
        $('#load-actual-budget').show();
        $('#load-actual-budget').siblings('h3').hide();
        $.get('/chart/actual_budget_date/'+project_id+'/'+brand, function (response) {
            var resp = JSON.parse(response);
            if ( resp.error ) { return; }

            var bar_config = {
                type: 'bar',
                data: {
                    labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    datasets: [{
                        data: resp.data.data,
                        backgroundColor: backgroundColor
                    }]
                },
                options: {
                    responsive: true,
                    legend: false,
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Montos'
                            }
                        }]
                    },
                    onClick: function (evt) {
                        var activeElement = this.getElementAtEvent(evt);
                        if ( activeElement.length === 0 ) {
                            actual_chart.destroy();
                            $('#load-actual-budget').siblings('h3').hide();
                            $('#load-actual-budget').show();
                            create_actual_pie_chart();
                            return;
                        }

                        var chart = this;
                        var month = activeElement[0]._index+1;
                        $.get('/chart/actual_budget_date/'+project_id+'/'+brand+'/'+month, function (response) {
                            var resp = JSON.parse(response);
                            if ( resp.error ) { return; }

                            var c_labels = chart.config.data.labels,
                                c_datasets = chart.config.data.datasets,
                                c_click_function = chart.options.onClick;
                            chart.config.data.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
                            chart.config.data.datasets = [{
                                data: resp.data.data,
                                backgroundColor: backgroundColor
                            }];
                            chart.options.onClick = function (evt) {
                                var activeElement = chart.getElementAtEvent(evt);
                                if ( activeElement.length === 0 ) {
                                    chart.data.labels = c_labels;
                                    chart.data.datasets = c_datasets;
                                    chart.options.onClick = c_click_function;
                                    chart.update();
                                    return;
                                }
                                var day = activeElement[0]._index+1;
                                month = (month < 10) ? ('0'+month) : month;
                                day = (day < 10) ? ('0'+day) : day;
                                var my_year = (year === '/all') ? date.getFullYear() : $('#chart_year').val().replace('/','');
                                window.open('/project_brand/brand_list_date?date='+my_year+'-'+month+'-'+day+'&project_id='+project_id+'&brand='+brand);
                            };
                            chart.update();
                        });
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data){
                                var text = 'Monto Real: ' + formmatterCurrency(tooltipItem.yLabel);
                                return text;
                            }
                        }
                    }
                }
            };
            $('#load-actual-budget').hide();
            actual_chart = new Chart(document.getElementById("actual-budget-chart").getContext("2d"), bar_config);
            if ( actual_chart !== null ) { $('#load-actual-budget').siblings('h3').show(); }
        });
    }

    /*********************************
     *           Sales funnel        *
     * *******************************/
    var year = $('#chart_year').val();
    var brand = $('#chart_brand').val();
    var line_chart = null;
    sales_funnel(year);

    $('#chart_year').change(function () {
        line_chart.destroy();
        year = $('#chart_yeaer').val();
        $('#load-sales-funnel').show();
        sales_funnel($('#chart_year').val(),$('#chart_brand').val());
    });
    $('#chart_brand').change(function () {
        brand = $('#chart_brand').val();
        line_chart.destroy();
        $('#load-sales-funnel').show();
        sales_funnel($('#chart_year').val(),$('#chart_brand').val());
    });
}
/*********************************
 *           Sales funnel        *
 * *******************************/
function sales_funnel(year, brand){
    $.get('/chart/sales_funnel/'+project_id+'/'+year+'/'+brand, function (response){
        $('#load-sales-funnel').hide();
        $('#chart_year').show();
        var resp = JSON.parse(response);
        if ( resp.error ) { return; }

        var bar_config = {
            type: 'line',
            data: {
                labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                datasets: resp.data
            },
            options: {
                responsive: true,
                // legend: false,
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            display: true
                        },
                        /*scaleLabel: {
                            display: true,
                            labelString: 'Meses'
                        }*/
                    }],
                    yAxes: [{
                        display: true,
                        gridLines: {
                            display: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Montos'
                        }
                    }]
                },
                onClick: function(evt) {
                    var activeElement = this.getElementAtEvent(evt);
                    if ( activeElement.length === 0 ) { return; }

                    var chart = this;
                    var mes = activeElement[0]._index+1;
                    $.get('/chart/get_sales_funnel_by_date/'+project_id+'/'+$('#chart_year').val()+'/'+mes+'/'+$('#chart_brand').val(), function (response){
                        var resp = JSON.parse(response);
                        if ( resp.error ) { return; }

                        var c_labels = chart.config.data.labels,
                            c_datasets = chart.config.data.datasets,
                            c_click_function = chart.options.onClick;
                        chart.config.data.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
                        chart.config.data.datasets = resp.data;
                        chart.options.onClick = function (evt) {
                            var activeElement = chart.getElementAtEvent(evt);
                            if ( activeElement.length === 0 ) {
                                chart.data.labels = c_labels;
                                chart.data.datasets = c_datasets;
                                chart.options.onClick = c_click_function;
                                chart.update();
                                return;
                            }
                            var dia = activeElement[0]._index+1;
                            var date = new Date();
                            mes = (mes < 10) ? ('0'+mes) : mes;
                            dia = (dia < 10) ? ('0'+dia) : dia;
                            var my_year = (year === '/all') ? date.getFullYear() : $('#chart_year').val().replace('/','');

                            window.open('/project_brand/brand_list_date?date='+my_year+'-'+mes+'-'+dia+'&brand='+$('#chart_brand').val(),'_blank');
                        };
                        chart.update();
                    });
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data){
                            var text = data.datasets[tooltipItem.datasetIndex].label + ': ' + formmatterCurrency(tooltipItem.yLabel);
                            return text;
                        }
                    }
                }
            }
        };
        line_chart = new Chart(document.getElementById("sales-funnel-chart").getContext("2d"), bar_config);
    }); // END ACTUAL_BUDGET
}

$('#cancel_project_btn').click(function () {
    var project_id = $(this).data('project');

    swal({
        title: 'Anular Proyecto',
        text: "¿Está seguro que desea anular este proyecto?",
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "¡Sí, anular!",
        closeOnConfirm: false
    }, function () {
        swal({
            title: 'Causa de anulación',
            text: 'Introduzca la causa de la anulación',
            type: "input",
            confirmButtonText: "Anular",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function (causa) {
            if ( causa === false ) {
                swal.close();
                return;
            }
            if ( causa.trim() === '' ) {
                swal.showInputError('Debe escribir una causa.');
                return;
            }
            $.post('/project/cancel/'+project_id, function (json) {
                var obj = JSON.parse(json);

                if ( obj.error ) {
                    swal.showInputError(obj.message);
                    return;
                }

                sweetNotification('Proyecto Anulado', 'El proyecto fue anulado de forma exitosa.', 'success', 1000);
                setTimeout(location.reload(), 1300);
            });
        });
    });
});

$('#close_project_btn').click(function () {
    var project_id = $(this).data('project');

    swal({
        title: 'Finalizar Proyecto',
        text: "¿Está seguro que desea finalizar este proyecto?",
        type: 'info',
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonColor: "#2b982b",
        confirmButtonText: "Finalizar",
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function () {
        $.post('/project/close/'+project_id, function (json) {
            var obj = JSON.parse(json);

            if ( obj.error ) {
                swal.showInputError(obj.message);
                return;
            }

            sweetNotification('Proyecto Finalizado', 'El proyecto fue finalizado de forma exitosa.', 'success', 1000);
            setTimeout(location.reload(), 1300);
        });
    });
});


/* ******************************************
   *          PROJECT FILTER LIST           *
* *******************************************/
if ($('#project-filter-table').size() > 0){
    // var URLsearch = window.location.search;
    // console.log(URLsearch);
    var $tablec = $('#project-filter-table');
    var cTable = $tablec.DataTable($.extend(true, {}, DATATABLE_TEMPLATE, {
        ajax: {
            url: '/project/project_filter_json/'+$('#day').val()+'/'+$('#month').val()+'/'+$('#year').val()+'/'+$('#status').val()+'/'+$('#company').val()+'/'+$('#category').val()
        },
        columns: [
            {
                data: function(data) {
                    return '<input type="checkbox" id="check-'+data.id+'" class="filled-in chk-col-teal" name="check[]" value="'+data.id+'">'+
                        '<label class="m-b-0" for="check-'+data.id+'"></label>';
                },
                orderable: false,
                searchable: false
            },
            { data: 'id' },
            { data: 'name', render: function(value, type, obj) { return '<a href="/project/view/'+obj.id+'" target="_blank">'+obj.name+'</a>'; } },
            { data: 'start_date', render: function (value, type, obj) { return obj.start_date_nice; } },
            { data: 'estimated_end_date', render: function (value, type, obj) { return obj.estimated_end_date_nice; } },
            { data: 'end_date', render: function (value, type, obj) { return obj.end_date_nice; } },
            {
                data: 'estimated_end_date',
                render: function (value, type, obj, meta) {
                    var color = (obj.delay.charAt(0) === '-') ? 'col-red' : 'col-green';

                    if ( obj.status === 'progreso' ) {
                        return '<span class="'+color+'">'+obj.delay+'</span>';
                    }
                    if ( obj.status === 'finalizado' || obj.status === 'anulado' ) {
                        return '';
                    }

                    return '<span class="'+color+'">'+obj.delay+'</span>';
                },
                searchable: false
            },
            {
                data: 'status',
                render: function (value, type, obj, meta) {
                    return obj.status.toUpperCase();
                }
            },
            { data: 'client' },
            { data: 'company' },
            { data: 'manager' },
            {
                data: 'tag',
                render: function (value, type, obj, meta) {
                    var res = '';

                    if ( obj.tags ) {
                        obj.tags.split(',').forEach(function (t) {
                            if ( res !== '' ) { res+= ' '; }
                            res += '<span class="label bg-blue">'+t+'</span>';
                        });
                    }

                    return res;
                }
            }
        ]
    }));
}
