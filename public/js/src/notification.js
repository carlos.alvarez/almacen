var user_notifications = null;

var updateNotifications = function () {
    if ( user_notifications === null ) { return; }

    var notification_list = $('#notifications');
    var icon = notification_list.find('.notification-icon').first();
    var body = notification_list.find('.notifications-body').first();

    body.html('');
    notification_list.find('.label-count').first().text(user_notifications.unread_count);
    if ( user_notifications.unread_count > 0 ) {
        icon.addClass('col-yellow animated infinite swing').text('notifications_active');
    } else {
        icon.removeClass('col-yellow animated infinite swing').text('notifications');
    }
    if ( user_notifications.notifications.length > 0 ) {
        user_notifications.notifications.forEach(function (notification) {
            var unread = (notification.read === 'false') ? 'notification-unread' : '';

            var actions = '';
            if ( notification.actions ) {
                var acts = JSON.parse(notification.actions);
                acts.forEach(function (act) {
                    actions += '<span><a href="/'+act.action+'">'+act.title+'</a></span>';
                });
            }

            /* '<div class="icon-circle bg-cyan">' +
                            '<i class="material-icons">cached</i>' +
                        '</div>' +
                         */

            body.append(
                '<li class="'+unread+'" data-id="'+notification.id+'">' +
                    '<a href="javascript:void(0);">' +
                        '<div class="menu-info">' +
                            '<h4>'+notification.message+'</h4>' +
                            '<p><i class="material-icons">access_time</i> '+notification.age+'</p>' +
                        '</div>' +
                        //actions +
                    '</a>' +
                '</li>'
            );
        });
    } else {
        body.html(
            '<li>' +
            '<a href="javascript:void(0);">' +
            '<div class="menu-info">' +
            '<h4>No tiene notificaciones</h4>' +
            '</div>' +
            '</a>' +
            '</li>'
        );
    }

    $.AdminBSB.dropdownMenu.activate();
};

var get_notifications = function () {
    $.get('/notification/json', function (json) {
        var obj = JSON.parse(json);
        if ( obj.error ) { return; }

        user_notifications = obj.data;
        updateNotifications();
    });
};

$(document).on('mouseover', '.notification-unread', function () {
    var $this = $(this);
    $this.removeClass('notification-unread');

    $.get('/notification/mark_as_read/'+$this.data('id'), function (json) {
        var obj = JSON.parse(json);
        if ( obj.error ) { return; }

        user_notifications.notifications.forEach(function (notification) {
            if ( notification.id == $this.data('id') ) {
                notification.read = 'true';
                user_notifications.unread_count--;
                return false;
            }
        });

        updateNotifications();
    }).fail(function() {
        $this.addClass('notification-unread');
    });
});

get_notifications();
setInterval(function () {
    if ( !document.hidden ) { get_notifications(); }
}, 60000);
