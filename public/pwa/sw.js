/*
Copyright 2016 Google Inc.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
(function() {
    'use strict';

    function getEndpoint() {
        return self.registration.pushManager.getSubscription()
            .then(function(subscription) {
                if (subscription) {
                    return subscription.endpoint;
                }

                throw new Error('User not subscribed');
            });
    }

    self.addEventListener('notificationclick', function(e) {
        e.notification.close();
        if ( e.action ) {
            e.waitUntil(
                clients.openWindow("/"+e.action)
            );
        }
    });

    self.addEventListener('push', function(e) {
        e.waitUntil(
            getEndpoint().then(function (endpoint) {
                var id = endpoint.split('/').slice(-1)[0];
                var data = new FormData();
                data.append('endpoint', id);
                return fetch('/push/notifications', {
                    method: "POST",
                    body: data
                });
            }).then(function (response) {
                return response.json();
            }).then(function (json) {
                if ( json.error || json.data.length === 0 ) {
                    self.registration.showNotification('Tienes nuevas notificaciones.', {
                        icon: "/public/img/logo.png"
                    });
                    return;
                }

                json.data.forEach(function (notification) {
                    self.registration.showNotification(notification.title, notification.payload);
                });
            })
        );
    });

})();