/*
Copyright 2016 Google Inc.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
var app = (function() {
    'use strict';

    var isSubscribed = false;
    var swRegistration = null;

    var pushButton = $('.js-push-btn').first();

    // DONE 2.1 - check for notification support
    if (!('Notification' in window)) {
        //console.log('This browser does not support notifications!');
        return;
    }

    // DONE 2.2 - request permission to show notifications
    Notification.requestPermission(function(status) {
        //console.log('Notification permission status:', status);
    });

    function displayNotification() {
        // DONE 2.3 - display a Notification
        if (Notification.permission === 'granted' && swRegistration !== null) {
            // DONE 2.4 - Add 'options' object to configure the notification
            var options = {
                body: 'First notification!',
                //icon: 'images/notification-flat.png',
                vibrate: [100, 50, 100],
                data: {
                    dateOfArrival: Date.now(),
                    primaryKey: 1
                },

                // DONE 2.5 - add actions to the notification
                actions: [
                    {
                        action: 'explore', title: 'Go to the site',
                        //icon: 'images/checkmark.png'
                    },
                    {
                        action: 'close', title: 'Close the notification',
                        //icon: 'images/xmark.png'
                    },
                ]

                // TODO 5.1 - add a tag to the notification

            };

            swRegistration.showNotification('Hello world!', options);
        }

    }

    function initializeUI() {

        // DONE 3.3b - add a click event listener to the "Enable Push" button
        // and get the subscription object

        if ( pushButton !== null ) {
            pushButton.find('.pushButton').on('change', function() {
                pushButton.find('.pushButton').attr('disabled', true);
                if (isSubscribed) {
                    unsubscribeUser();
                } else {
                    subscribeUser();
                }
            });
        }

        swRegistration.pushManager.getSubscription()
            .then(function(subscription) {
                isSubscribed = (subscription !== null);

                if (isSubscribed) {
                    //console.log('User IS subscribed.');
                } else {
                    //console.log('User is NOT subscribed.');
                }

                updateBtn();
            });

    }

    // TODO 4.2a - add VAPID public key

    function subscribeUser() {

        // DONE 3.4 - subscribe to the push service
        swRegistration.pushManager.subscribe({
            userVisibleOnly: true
        })
            .then(function(subscription) {
                //console.log('User is subscribed:', subscription);

                updateSubscriptionOnServer(subscription);

                isSubscribed = true;

                updateBtn();
            })
            .catch(function(err) {
                if (Notification.permission === 'denied') {
                    console.warn('Permission for notifications was denied');
                } else {
                    console.error('Failed to subscribe the user: ', err);
                }
                updateBtn();
            });

    }

    function unsubscribeUser(updateServer) {
        if ( updateServer === undefined ) { updateServer = true; }
        var toUnsubscribe = null;

        // DONE 3.5 - unsubscribe from the push service
        swRegistration.pushManager.getSubscription()
            .then(function(subscription) {
                if (subscription) {
                    toUnsubscribe = subscription;
                    return subscription.unsubscribe();
                }
            })
            .catch(function(error) {
                //console.log('Error unsubscribing', error);
            })
            .then(function() {
                if ( updateServer ) { updateSubscriptionOnServer(toUnsubscribe, 'unsubscribe'); }
                //console.log('User is unsubscribed');
                isSubscribed = false;

                updateBtn();
            });
    }

    function updateSubscriptionOnServer(subscription, action) {
        if ( action === undefined ) { action = 'subscribe'; }
        // Here's where you would send the subscription to the application server

        //console.log('Subscription Object:', subscription);

        if ( action === 'subscribe' ) {
            $.post('/push/subscribe', {subscription: JSON.stringify(subscription) }, function (json) {
                var obj = JSON.parse(json);
                if ( obj.error ) { unsubscribeUser(false); }
            });
        } else {
            $.post('/push/unsubscribe', {subscription: JSON.stringify(subscription) }, function (json) {
                //var obj = JSON.parse(json);
                //console.log(json);
            });
        }
    }

    function updateBtn() {
        if ( pushButton === null ) { return; }

        if (Notification.permission === 'denied') {
            pushButton.find('.pushText').text('Notificaciones Push Bloqueadas');
            pushButton.find('.pushButton').attr('disabled', true);
            return;
        }

        if (isSubscribed) {
            pushButton.find('.pushText').text('Notificaciones Push Habilitadas');
            pushButton.find('.pushButton').attr('checked', true);
        } else {
            pushButton.find('.pushText').text('Notificaciones Push Deshabilitadas');
            pushButton.find('.pushButton').attr('checked', false);
        }

        pushButton.find('.pushButton').attr('disabled', false);
    }

    function urlB64ToUint8Array(base64String) {
        var padding = '='.repeat((4 - base64String.length % 4) % 4);
        var base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/');

        var rawData = window.atob(base64);
        var outputArray = new Uint8Array(rawData.length);

        for (var i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    }

    if ('serviceWorker' in navigator && 'PushManager' in window) {
        //console.log('Service Worker and Push is supported');

        navigator.serviceWorker.register('/public/pwa/sw.js')
            .then(function(swReg) {
                //console.log('Service Worker is registered', swReg);

                swRegistration = swReg;

                // DONE 3.3a - call the initializeUI() function
                initializeUI();

            })
            .catch(function(error) {
                console.error('Service Worker Error', error);
            });
    } else {
        console.warn('Push messaging is not supported');
        pushButton.find('.pushText').text('Notificaciones no soportadas');
    }

})();